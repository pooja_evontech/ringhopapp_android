mainApp.factory('ContactsService', function( $http, CONSTANTS ) {
  return {
    addContact: function( contactData ) {
      return $http.post( CONSTANTS.BASE_URL+"addcontact",JSON.stringify( contactData ) );
    }, 
    blockUser: function( blockUserData ) {
      return $http.post( CONSTANTS.BASE_URL+"blockuser",JSON.stringify(blockUserData));
    },
    searchContact: function( searchData ) {
      return $http.post( CONSTANTS.BASE_URL+"addcontacts",JSON.stringify( searchData ) );
    },
    saveAfterSearch: function( searchData ) {
      return $http.post( CONSTANTS.BASE_URL+"savecontact", JSON.stringify( searchData ) );
    },
    blockedUsers: function( paramData ) {
      return $http.post( CONSTANTS.BASE_URL+"blockedusers",JSON.stringify( paramData ) );
    },
    unblockUser: function( blockUser ) {
      return $http.post( CONSTANTS.BASE_URL+"unblock",JSON.stringify( blockUser ) );
    },
    getAllContacts: function( paramData ) {
      return $http.post( CONSTANTS.BASE_URL+"contacts",JSON.stringify( paramData ) );
    },
    removeContact: function( contact ) {
      return $http.post( CONSTANTS.BASE_URL+"removecontact",JSON.stringify( contact ) );
    },
    getSuggestedPros: function( getSuggestedProsData ) {
      return $http.post( CONSTANTS.BASE_URL+"suggestpros",JSON.stringify( getSuggestedProsData ) );
    }
  };
})