//+++++++++++++++++++++++++++factory for userstats page controller+++++++++++++++++++++
proApp.factory( 'UserStatsServices', function( $http ) {
    return {
        get : function( statstype ) {
            var userstats = {};
            userstats['responsetype'] = 'json';
            userstats['statstype'] = statstype;
            return $http.post( BASE_URL+"userstats",JSON.stringify( userstats ) )
            .then( function( data, status, headers, config ) {
                return data;
            });
        }
    }
} );
