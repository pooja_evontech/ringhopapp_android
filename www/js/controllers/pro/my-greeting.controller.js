//+++++++++++++++++++++++++++mygreeting page controller+++++++++++++++++++++

proApp.controller( "MyGreetingController", function( $scope, ProService, $timeout, $state, $rootScope, $stateParams, $sce  ) {
    $( 'audio' ).audioPlayer(); 
    var user_detail = localStorage.getItem("userDetail");
    $scope.showdelaudiopopup = false;
    var userData = JSON.parse(user_detail).data;
    $scope.loginid = userData.id;
    var getgreetingsData = {};
    getgreetingsData['responsetype'] = 'json';
    ProService.myGreeting( getgreetingsData ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.voicegreet = data.data.name_greeting;
        $scope.getPin = data.data.optincode;
        $scope.greetingUrl = $sce.trustAsResourceUrl(data.data.name_greeting);
        $scope.callNo = data.data.greeting_number;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.delaudiopopup = function(){
        $scope.showdelaudiopopup = true;
    };
    $scope.closedelaudio = function(){
        $scope.showdelaudiopopup = false;
    };
    $scope.refreshpin = function () {
        var greetingsData = {};
        greetingsData['responsetype'] = 'json';
        greetingsData['userid'] = userData.id;
        if(userData.id){
            ProService.refreshPin( greetingsData ).success( function( data ) {
                if (data.pin > 0) {
                    $scope.getPin = data.pin;
                }
            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
        
    };
    $scope.deletegreeting = function(){
        var delgreetingData = {};
        delgreetingData['responsetype'] = 'json';
        delgreetingData['userid'] = userData.id;
        ProService.deleteGreeting( delgreetingData ).success( function( data ) {
        } ).error( function ( data, status, headers, config ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
        $scope.showdelaudiopopup = false;
    };
} )