//+++++++++++++++++++++++++++Create new shout page controller+++++++++++++++++++++

proApp.controller("CreateNewShoutController", function( $scope, ProService, $timeout, $state, $rootScope, $filter, CONSTANTS ) {
    $scope.previewImg = false;
    $scope.imgDelete = false;
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $(".previewDiv").hide();
    var pre_count = 1;
    var next_count = 1;
    $scope.currentCount = CONSTANTS.shoutsPerPage;
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var userId = userData.id;
    var myshoutsData = {};
    myshoutsData['responsetype'] = 'json';
    myshoutsData['page'] = 1;
    $scope.date = new Date();
    $('.ui-loader').show();
    ProService.myShouts( myshoutsData ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $('.ui-loader').hide();
        imgDimensn = [];
        $scope.fulldata = data.data;
        $scope.curr_page = data.data.current_page;
        $scope.limit = data.data.per_page;
        $scope.shoutTotal = data.data.total_count;
        $scope.shoutImages = data.data.myimages;
        $scope.Name = data.data.pro_name;
        $scope.currentCount = $scope.curr_page * $scope.limit;
        $scope.present_page_limit = $scope.curr_page;

        angular.forEach($scope.shoutImages , function(shoutImg){
            shoutImg.small_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name;
            shoutImg.big_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name_pixelated;
            shoutImg.shoutId = shoutImg.id;
            shoutImg.getFullDate = shoutImg.created_at.split(" ");
            shoutImg.initial = $filter('date')(shoutImg.getFullDate[0],'longDate');

        });
        
        if ( $scope.shoutTotal <= $scope.currentCount ) {
            $scope.currentCount = $scope.shoutTotal;
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.shoutTotal == 0 ) {
                $scope.present_page_limit = 0;
                $scope.currentCount = 0;
                $scope.noMessageDiv = true;
                $scope.noMessage = "No Shouts available.";
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});

            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
        }
        
    } ).error( function ( data ) {
        $('.ui-loader').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.prevBtn = function () {
        imgDimensn = [];
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.curr_page == 1 ) {
            $scope.prevDisabled = true;
            $scope.present_page_limit = 1;
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
            $('#arrow').css ( {"opacity":"0.5" });

        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            myshoutsData['page'] = pre_count;
            $scope.date = new Date();
            ProService.myShouts( myshoutsData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                imgDimensn = [];
                $scope.fulldata = data.data;
                $scope.curr_page = data.data.current_page;
                $scope.limit = data.data.per_page;
                $scope.shoutTotal = data.data.total_count;
                $scope.shoutImages = data.data.myimages;
                $scope.Name = data.data.pro_name;
                $scope.currentCount = $scope.curr_page * $scope.limit;
                $scope.present_page_limit = $scope.currentCount-3;
                angular.forEach($scope.shoutImages , function(shoutImg){
                    shoutImg.small_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name;
                    shoutImg.big_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name_pixelated;
                    shoutImg.shoutId = shoutImg.id;
                    shoutImg.getFullDate = shoutImg.created_at.split(" ");
                    shoutImg.initial = $filter('date')(shoutImg.getFullDate[0],'longDate');
                });
                if( $scope.curr_page == 1 ) {
                    if( $scope.shoutTotal > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
            } ).error( function ( data ) {
                // console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        imgDimensn = [];
        next_count = next_count + 1;
        $scope.currentCount = $scope.curr_page * $scope.limit;
        if ( $scope.shoutTotal >= $scope.limit && $scope.currentCount < $scope.shoutTotal ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            myshoutsData['page'] = next_count;
	        $scope.date = new Date();
            ProService.myShouts( myshoutsData ).success( function( data ) {
	            $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                imgDimensn = [];
                $scope.fulldata = data.data;
                $scope.curr_page = data.data.current_page;
                $scope.limit = data.data.per_page;
                $scope.shoutTotal = data.data.total_count;
                $scope.shoutImages = data.data.myimages;
                $scope.Name = data.data.pro_name;
                $scope.currentCount = $scope.curr_page * $scope.limit;
                $scope.present_page_limit = $scope.currentCount-3;
                angular.forEach($scope.shoutImages , function(shoutImg){
                    shoutImg.small_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name;
                    shoutImg.big_image_url = IMAGE_URL+'users/'+shoutImg.pro_id+'/'+'big_'+shoutImg.media_name_pixelated;
                    shoutImg.shoutId = shoutImg.id;
                    shoutImg.getFullDate = shoutImg.created_at.split(" ");
                    shoutImg.initial = $filter('date')(shoutImg.getFullDate[0],'longDate');
	            });

                if ( $scope.shoutTotal > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };

    $scope.closeHeaderPopup = function () {
        $('#wrapper').css('overflow','visible');
        $('#overlay').hide();
        $(".previewDiv").hide();
        $("#overlay").removeClass('active');
        $scope.previewImg = false;
        $scope.imgDelete = false;
    };
    $('#overlay').click(function() {
        $('#wrapper').css('overflow','visible');
        $('#overlay').hide();
        $(".previewDiv").addClass('ng-hide');
        $(".previewDiv").hide();
        $scope.previewImg = false;
    } );
    $scope.OpenImg = function(image,id){
        $('#wrapper').css('overflow','hidden');
        $('#overlay').show();
        $scope.previewImg = true;
        $(".previewDiv").removeClass('ng-hide');
        $(".previewDiv").show().delay(3000).fadeIn(2000);
        $scope.mainImageUrl = image;
        $scope.currImgId = id;
    };
    $scope.DeleteImg = function (id) {
        $scope.previewImg = false;
        $scope.imgDelete = true;
        $scope.currImgId = id;
    };
    $scope.deleteshout = function(id){
        $scope.imgDelete = false;
        $('.ui-loader').show();
        $scope.currImgId = id;
        $scope.limit = CONSTANTS.shoutsPerPage;
        var deleteShoutData = {};
        deleteShoutData['responsetype'] = 'json';
        deleteShoutData['shout_id'] = id;
        ProService.deleteShout( deleteShoutData ).success( function( data ) {
            $('.ui-loader').hide();
            $('#'+id).hide();
            $('#overlay').hide();
            $('#shout_'+id).hide("medium", function(){
                $(this).remove();
                // $state.transitionTo('home.createnewshout', {}, {reload: true});
            });
            $scope.imgDelete = false;
        } ).error( function ( data, status, headers, config ) {
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    }
    $scope.createShout = function() {
        console.log("correct func");
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/PhotoShout"
        });

    };

})