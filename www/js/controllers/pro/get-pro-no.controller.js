//+++++++++++++++++++++++++++get pro number page controller+++++++++++++++++++++

proApp.controller( "GetProNoController", function( $scope, $http, $timeout, $state, $rootScope, ProService ) {
    console.log("get pro no");
    // $scope.proNo_msg = false;
    $('.succesStatus').css({"display":"none"});
    $scope.showShareableNoBtn = true;
    $scope.getProNo = function( $event ) {
    	$('.ui-loader').show();
    	console.log("request no");
    	var getProNoData = {};
    	getProNoData['responsetype'] = 'json';
	    ProService.getShareableProNo( getProNoData ).success( function( data ) {
        	$('.ui-loader').hide();
        	console.log(JSON.stringify(data));
            if( data.message == 'user not logged in' ) {
                //if user's session expires
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/affter_login"
                });
            }
        	if (data.status == 'success') {
                $scope.showShareableNoBtn = false;
        		// $scope.proNo_msg = true;
                $('.succesStatus').css({"display":"block"});
                $scope.proNoSuccessMsg = data.msg;
        		// $scope.proNoSuccessMsg = 'Your request is successful.';
        	}
		} ).error( function ( data ) {
			console.log(JSON.stringify(data));
	        $('.ui-loader').hide();
	        if(navigator.connection.type == Connection.NONE) {
	            checkConnection();
	        }
	    } );
    };
} )