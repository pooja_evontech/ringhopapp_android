//+++++++++++++++++++++++++++statssummary page controller+++++++++++++++++++++

proApp.controller( "StatsSummaryController", function( $rootScope, $scope, ProService, $state, $filter ) {
    $scope.noMessageDiv = false;
    $scope.date = new Date();
    var statsSummary = {};
    statsSummary['responsetype'] = 'json';
    ProService.statsSummary( statsSummary ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.statssummhistory = data.data.periodsummary;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

} )