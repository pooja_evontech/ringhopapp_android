//+++++++++++++++++++++++++++prodashboard page controller+++++++++++++++++++++

proApp.controller( "ProDashboardController", function( $scope, ProService, $timeout, $state, $rootScope ) {
    $scope.verifyIdentity = function( $event ) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/verifyidentity"
        });
    };
    $scope.paymentDetails = function( $event ) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/paymentoption"
        });
    };
    $scope.accpetTerms = function( $event ) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/acceptterms"
        });
    };
    $scope.ProStepsShow = true;
    //get Pro data
    var getProDashboardData = {};
    getProDashboardData['responsetype'] = 'json';
    ProService.proDashboard( getProDashboardData ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.proInfo = data.data;
        localStorage.setItem("proDashboardData",JSON.stringify(data));
        if( data.data.pv_terms == 2  &&  data.data.pv_idverify == 1 && data.data.pv_payinfo == 1 ){
            $scope.ProStepsShow = false;
        }
        if( data.data.pv_terms == 1 ) {
            $('#accept_terms_btn').addClass('orangeBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-time');
            $scope.protermsData = '(Pending review)';
            $("#accept_terms_btn").unbind("click");
        } else if( data.data.pv_terms == 2 ) {
            $('#accept_terms_btn').addClass('greenBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-ok');
            $scope.protermsData = '(Completed)';
            $("#accept_terms_btn").unbind("click");
        } else if( data.data.pv_terms == -1 ) {
            $('#accept_terms_btn').addClass('redBlock');
            $('#accept_terms_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.protermsData = '(Action required)';
        } else {
            $('#accept_terms_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.protermsData = '';
        }

        if( data.data.pv_idverify == 1) {
            $('#verify_identity_btn').addClass('orangeBlock');
            $('#verify_identity_span').addClass('glyphicon glyphicon-time');
            $scope.proidverifyData = '(Pending review)';
            $("#verify_identity_btn").unbind("click");
        } else if( data.data.pv_idverify == 2 ) {
            $('#verify_identity_btn').addClass('greenBlock');
             $('#verify_identity_span').addClass('glyphicon glyphicon-ok');
            $scope.proidverifyData = '(Completed)';
            $("#verify_identity_btn").unbind("click");
        } else if( data.data.pv_idverify == -1 ) {

            $('#verify_identity_btn').addClass('redBlock');
            $('#verify_identity_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.proidverifyData = '(Action required)';
        } else {
            $('#verify_identity_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.proidverifyData = '';
        }

        if( data.data.pv_payinfo == 1) {
            $('#payment_details_btn').addClass('orangeBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-time');
            $scope.propayinfoData = '(Pending review)';
            $("#payment_details_btn").unbind("click");
        } else if( data.data.pv_payinfo == 2 ) {
            $('#payment_details_btn').addClass('greenBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-ok');
            $scope.propayinfoData = '(Completed)';
            $("#payment_details_btn").unbind("click");
        } else if( data.data.pv_payinfo == -1 ) {
            $('#payment_details_btn').addClass('redBlock');
            $('#payment_details_span').addClass('glyphicon glyphicon-warning-sign');
            $scope.propayinfoData = '(Action required)';
        } else {
            $('#payment_details_span').addClass('glyphicon glyphicon-chevron-right');
            $scope.propayinfoData = '';
        }

    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
} )