proApp.controller( "SuggestProController", function( $scope, ProService, ContactsService, UserService, $state, $stateParams, $rootScope ) {
    console.log('enter suggest pro controller');
    $(".fullwidthBluemsg_contact").css({'background':'rgba(92, 24, 98, .8)','border-color':'rgba(92, 24, 98, .8)','color': '#fff', 'text-shadow':'none'});
    $(".fullwidthWhite_contact").css({"background": "#fff","color": "#000","border-color": "#C7C3C3"});
    // $(".displaySearch_contact").css({"display":"none"}); //Nanda 10 june
    $(".buttonSelect_contact").css({"padding-top": "13px"}) //Nanda 10 june
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var user_status = JSON.parse( dashboard_data ).data.user_status;
    $scope.proInfo = proData;
    $scope.showVideoIcon = false;
    $scope.blockPopup = false;
    $scope.gotoBlockUsers = false;
    $scope.closePopupBlock = false;
    $scope.tipSendListShow = false;
    if ($scope.proInfo != 2) {
        $scope.tipSendListShow = true;
    }
    $scope.noContactDiv = false;
    $('.ui-loader').show();
    var getSuggestedProsData = {};
    getSuggestedProsData['responsetype'] = 'json';
    ContactsService.getSuggestedPros( getSuggestedProsData ).success( function( data ) {
        $('.ui-loader').hide();
        console.log(JSON.stringify(data));
        $scope.receiverProData = data.data.pro;
        if(data.data.length > 20) //Nanda 10 june
        {
            // $(".displaySearch_contact").css({"display":"block"});
            $(".buttonSelect_contact").css({"padding-top": "5px"})
        }
        if(data.data.length == 0){
            $scope.noContactDiv = true;
            $scope.noContacts = "You have not added contacts.";
        }
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        
        $scope.suggestedProsData = data.data;
        $scope.openPopup = function(contactId, contact_pro, contact_pro_status ){
            $('#mainContactsDiv').addClass('addPopupClass');
            $('#'+contactId).show();
            $('#overlay').show();
            if($scope.proInfo != contact_pro){
                $scope.showVideoIcon = true;
            } else {
                $scope.showVideoIcon = false;
            }
        };
    } ).error( function ( data ) {
        $('.ui-loader').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.gotoProfile = function(profileId,profileName){
        window.plugins.nativepagetransitions.slide( {
            "direction": "left",
            "href" : "#/home/profile"+profileId+"/"+profileName
        } );
    };
    // $scope.deleteContact = function(contactId){
    //     navigator.notification.confirm(
    //         'Are you sure you want to delete this contact?',  // message
    //         onDeleteConfirm,
    //         'Delete Contact'
    //     );
    //     $scope.toDelContact = contactId;
    // };
    // function onDeleteConfirm(button){
    //     if( button == 1 ) {
    //         console.log($scope.toDelContact);
    //         var deleteContactData = {};
    //         deleteContactData['responsetype'] = 'json';
    //         deleteContactData['contact_id'] = $scope.toDelContact;
    //         ContactsService.removeContact( deleteContactData ).success( function( data ) {
    //             $('#mainContactsDiv').removeClass('addPopupClass');
    //             $('#'+$scope.toDelContact).hide();
    //             $('#overlay').hide();
    //             $('#contact_'+$scope.toDelContact).hide("medium", function(){
    //                 $(this).remove();
    //                 if( $.trim( $( '.dashboard' ).text() ).length == 0 ) {
    //                     $('.no_msg').html("No contacts available.").show().removeClass('ng-hide');
    //                 }
    //             });
    //         } ).error( function ( data ) {
    //             if(navigator.connection.type == Connection.NONE) {
    //                 checkConnection();
    //             }
    //         } );
    //     }
    // }
    $scope.cancelPopup = function(contactId){
        $('#mainContactsDiv').removeClass('addPopupClass');
        $('#'+contactId).hide();
        $('#overlay').hide();
    };
    $scope.sortContact = function(){
        $(".fullwidthWhite_contact").css({'background':'rgba(92, 24, 98, .8)','border-color':'rgba(92, 24, 98, .8)','color': '#fff', 'text-shadow':'none'});
        $(".fullwidthBluemsg_contact").css({"background": "#fff","color": "#000","border-color": "#C7C3C3"});
        $("#search-basic").val("");
        $scope.sortType = "displayname";
        $scope.sortReverse = false;
        $scope.searchText = "";
    };
    $scope.resetContact = function(){
        $(".fullwidthBluemsg_contact").css({'background':'rgba(92, 24, 98, .8)','border-color':'rgba(92, 24, 98, .8)','color': '#fff', 'text-shadow':'none'});
        $(".fullwidthWhite_contact").css({"background": "#fff","color": "#000","border-color": "#C7C3C3"});
        $("#search-basic").val("");
        $scope.sortType = "";
        $scope.searchText = "";
    };

    $scope.searchFilter = function(obj){
        var re = new RegExp($scope.searchText,'i');
        return !$scope.searchText || re.test(obj.displayname) || re.test(obj.profile_status) || re.test(obj.online_status);
    };
    $scope.RemoveIcon = false;
    $scope.OnKeyupRemove = function(e){
        $scope.RemoveIcon= true;
        if ($scope.searchText =="") {
            $scope.RemoveIcon = false;
        }
    };

    // $scope.removeSearchText = function(){
    //     $scope.RemoveIcon = false;
    //     $scope.searchText = "";
    //     $(".searchData").val('');
    // };
    // $scope.addPro_UserContact = function(){
    //     window.plugins.nativepagetransitions.slide({
    //         "direction": 'left',
    //         "href" : "#/home/addProContact"
    //     });
    // };
    $scope.videoCallFromContacts = function(receiverId,receiver_name){
        $('.phpdebugbar-openhandler-overlay').hide();
        $('#'+receiverId).hide();
        $scope.idCallback = receiverId;
        $scope.username1 = receiver_name;
        $scope.pageState = 'notAvailable';
        $scope.pageStateInactive = 'deactivated';
        var videoData = {};
        videoData['responsetype'] = 'json';
        videoData['receiver'] = $scope.idCallback;
        UserService.getUserDetail( videoData ).success( function( data ) {
            console.log(JSON.stringify(data));
            $scope.videoCallRate = data.data.video_call_rate;
            $scope.userBalance = data.data.originator_balance;
            $scope.reqBal = data.data.bal_req;
            $scope.origBlock = data.data.originator_block;
            $scope.receiverBlock = data.data.receiver_block;
            $scope.receiverAvailable = data.data.receiver_available;
            localStorage.setItem("receiverImage_data",data.data.profile_image);
            var receiveData = JSON.stringify({'username':$scope.username1,'origId':$scope.idCallback,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'contacts','receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
            if ($scope.receiverBlock === true) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = true;
                    $scope.closePopupBlock = false;
                    $scope.blockMsg = "This user is no longer receiving video calls";
                } else if ( $scope.origBlock === true) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = false;
                    $scope.closePopupBlock = true;
                    $scope.blockMsg = "You are blocked";
                } else if( $scope.origBlock === true && $scope.receiverBlock === true ) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = true;
                    $scope.closePopupBlock = false;
                    $scope.blockMsg = "This user is no longer receiving video calls";
                } else if ( $scope.receiverAvailable == "0") {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = false;
                    $scope.closePopupBlock = true;
                    $scope.blockMsg = "This user is not available";
                } 
                else if (data.data.receiver_status == 'inactive') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageStateInactive+'/'+$scope.idCallback
                    } );
                } 
                else if (data.data.in_video_call == '1') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageState+'/'+$scope.idCallback
                    } );
                }
                else if ($scope.origBlock === false && $scope.receiverBlock === false){
                    $scope.closePopupBlock = false;
                    $('#overlay').hide();
                    if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                           $state.go('home.videoCalling',{receive_data:receiveData})
                    }else{
                        window.plugins.nativepagetransitions.slide({
                            "href" : "#/home/videoCredits/"+$scope.username1+'/'+$scope.reqBal
                        });
                    }
                }

        } ).error( function ( data ) {
            $scope.networkPopup = true;
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.backButton1 =function(){
        window.plugins.nativepagetransitions.slide({
            "direction" : "right",
            "href" : "#/home/affter_login"
        });
    };
    $scope.closeBlock = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
    };

    $scope.blockLink = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };
    $scope.sendATip = function(idForSendingTip){
        $('.phpdebugbar-openhandler-overlay').hide();
        var page_name = 'contacts';
        $scope.tipReceiveId = idForSendingTip;
        window.plugins.nativepagetransitions.slide({
            "direction": 'left',
            "href" : "#/home/sendTip/"+$scope.tipReceiveId+"/"+page_name
        });

    };
    $scope.addSuggestProContact = function(id){
        console.log(id);
        $('.ui-loader').show();
        $('#overlay').show();
        $scope.suggestedProId = id;
        var addContactData = {};
        addContactData['responsetype'] = 'json';
        addContactData['caller_id'] = $scope.suggestedProId;
        ContactsService.addContact( addContactData ).success( function( data ) {
            console.log(JSON.stringify(data));
            $('#alreadyContact').html('contact added');
            $('#alreadyContact').addClass('italicCall');
            $('#mainContactsDiv').removeClass('addPopupClass');
            $('#'+$scope.suggestedProId).hide();
            $('#overlay').hide();
            $('.ui-loader').hide();
            $('#contact_'+$scope.suggestedProId).hide("medium", function(){
                $(this).remove();
                if( $.trim( $( '.dashboard' ).text() ).length == 0 ) {
                    $('.no_msg').html("No contacts available.").show().removeClass('ng-hide');
                }
            });
        } ).error( function ( data ) {
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            $('#overlay').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    
} )