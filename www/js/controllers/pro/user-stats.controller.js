//+++++++++++++++++++++++++++userstats page controller+++++++++++++++++++++

proApp.controller( "UserStatsController", function( $scope, ProService, $state, UserStatsServices ) {
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.networkPopup = false;
    $scope.headervalue = "calls";
    $scope.limit = 4;
    var pre_count = 1;
    var next_count = 1;
    var user_detail = localStorage.getItem( "userDetail" );
    var userData = JSON.parse( user_detail ).data;
    var username = userData.displayname;
    $scope.callSection = true;
    $scope.messageSection = false;
    $scope.photoSection = false;
    $scope.tipSection = false;

    $scope.callActiveClass = 'fullwidth_bluemsg';
    $scope.msgActiveClass = 'fullwidth_white';
    $scope.photoActiveClass = 'fullwidth_white';
    $scope.tipActiveClass = 'fullwidth_white';

    $scope.statsType = 'calls';
    $('#arrow').css ( {"opacity":"0.5" });
    $('.phpdebugbar-openhandler-overlay').show();
    $('.ui-loader').show();
    
    UserStatsServices.get('calls').then( function ( stats ) {
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.serverData = stats.data;
        $scope.userstatsData = $scope.serverData.data.calls.calllogs;
        angular.forEach($scope.userstatsData, function(history , filterKey) {
            history.call_amount = parseFloat( history.call_amount).toFixed(2);
            history.call_duration = parseFloat( history.call_duration ).toFixed(2);
        } );
        $scope.limit = $scope.serverData.data.calls.per_page;
        $scope.totalMessage = $scope.serverData.data.calls.total_count;
        $scope.currentPage = $scope.serverData.data.calls.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $('#arrow1').css ( {"opacity":"1" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No call history available.";
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    },function(error){
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
    } );

    $scope.calls = function () {
        pre_count = 1;
        next_count = 1;
        $scope.statsType = 'calls';
        $scope.noMessageDiv = false;
        $scope.callSection = true;
        $scope.photoSection = false;
        $scope.messageSection = false;
        $scope.tipSection = false;
        $scope.headervalue = "calls";
        $scope.callActiveClass = 'fullwidth_bluemsg';
        $scope.msgActiveClass = 'fullwidth_white';
        $scope.photoActiveClass = 'fullwidth_white';
        $scope.tipActiveClass = 'fullwidth_white';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        UserStatsServices.get('calls').then( function ( stats ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.serverData = stats.data;
            $scope.userstatsData = $scope.serverData.data.calls.calllogs;
            angular.forEach($scope.userstatsData, function(history , filterKey) {
                history.call_amount = parseFloat( history.call_amount).toFixed(2);
                history.call_duration = parseFloat( history.call_duration ).toFixed(2);
            } );
            $scope.limit = $scope.serverData.data.calls.per_page;
            $scope.totalMessage = $scope.serverData.data.calls.total_count;
            $scope.currentPage = $scope.serverData.data.calls.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $('#arrow1').css ( {"opacity":"1" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalMessage == 0 ) {
                    $scope.noMessageDiv = true;
                    $scope.noMessage = "No call history available.";
                } else {
                    $scope.noMessageDiv = false;
                }

            }  else {
                $scope.paginateSection = true;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        },function(error){
             $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    };
    $scope.messages = function () {
        pre_count = 1;
        next_count = 1;
        $scope.statsType = 'messages';
        $scope.noMessageDiv = false;
        $scope.messageSection = true;
        $scope.callSection = false;
        $scope.photoSection = false;
        $scope.tipSection = false;
        $scope.headervalue = "messages";
        $scope.callActiveClass = 'fullwidth_white';
        $scope.msgActiveClass = 'fullwidth_bluemsg';
        $scope.photoActiveClass = 'fullwidth_white';
        $scope.tipActiveClass = 'fullwidth_white';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        UserStatsServices.get('messages').then( function ( stats ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.serverData = stats.data;
            $scope.userstatsData = $scope.serverData.data.messages.msglogs;
            angular.forEach($scope.userstatsData , function(historyKey){
                    if(historyKey.displayname == username ){
                        historyKey.displayname = "me";
                    }
                });
            $scope.limit = $scope.serverData.data.messages.per_page;
            $scope.totalMessage = $scope.serverData.data.messages.total_count;
            $scope.currentPage = $scope.serverData.data.messages.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $('#arrow1').css ( {"opacity":"1" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalMessage == 0 ) {
                    $scope.noMessageDiv = true;
                    $scope.noMessage = "No message history available.";
                } else {
                    $scope.noMessageDiv = false;
                }

            }  else {
                $scope.paginateSection = true;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        },function(error){
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    };
    $scope.photos = function () {
        pre_count = 1;
        next_count = 1;
        $scope.statsType = 'photos';
        $scope.noMessageDiv = false;
        $scope.photoSection = true;
        $scope.messageSection = false;
        $scope.callSection = false;
        $scope.tipSection = false;
        $scope.headervalue = "photos";
        $scope.callActiveClass = 'fullwidth_white';
        $scope.msgActiveClass = 'fullwidth_white';
        $scope.tipActiveClass = 'fullwidth_white';
        $scope.photoActiveClass = 'fullwidth_bluemsg';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        UserStatsServices.get('photos').then( function ( stats ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.serverData = stats.data;
            $scope.userstatsData = $scope.serverData.data.photos.photologs;
            $scope.limit = $scope.serverData.data.photos.per_page;
            $scope.totalMessage = $scope.serverData.data.photos.total_count;
            $scope.currentPage = $scope.serverData.data.photos.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $('#arrow1').css ( {"opacity":"1" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalMessage == 0 ) {
                    $scope.noMessageDiv = true;
                    $scope.noMessage = "No media purchases.";
                } else {
                    $scope.noMessageDiv = false;
                }

            }  else {
                $scope.paginateSection = true;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        },function(error){
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    };
    $scope.tips = function () {
        pre_count = 1;
        next_count = 1;
        $scope.statsType = 'tip';
        $scope.noMessageDiv = false;
        $scope.photoSection = false;
        $scope.messageSection = false;
        $scope.callSection = false;
        $scope.tipSection = true;
        $scope.headervalue = "tip";
        $scope.callActiveClass = 'fullwidth_white';
        $scope.msgActiveClass = 'fullwidth_white';
        $scope.photoActiveClass = 'fullwidth_white';
        $scope.tipActiveClass = 'fullwidth_bluemsg';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        UserStatsServices.get('tip').then( function ( stats ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide()
            $scope.serverData = stats.data;
            $scope.userstatsData = $scope.serverData.data.tips.msglogs;
            $scope.limit = $scope.serverData.data.tips.per_page;
            $scope.totalMessage = $scope.serverData.data.tips.total_count;
            $scope.currentPage = $scope.serverData.data.tips.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $('#arrow1').css ( {"opacity":"1" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalMessage == 0 ) {
                    $scope.noMessageDiv = true;
                    $scope.noMessage = "No tip history available.";
                } else {
                    //alert(false);
                    $scope.noMessageDiv = false;
                }

            }  else {
                $scope.paginateSection = true;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        },function(error){
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    };

    $scope.prevBtn = function (statsType) {
        //alert(statsType);
        $('#arrow1').css ( {"opacity":"1" });
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.prevDisabled = true;
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            var statsData = {};
            statsData['responsetype'] = 'json';
            statsData['statstype'] = statsType;
            statsData['page'] = pre_count;
            ProService.userStats( statsData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( statsType == 'calls' ) {
                    $scope.limit = data.data.calls.per_page;
                    $scope.userstatsData = data.data.calls.calllogs;
                    angular.forEach($scope.userstatsData, function(history , filterKey) {
                        history.call_amount = parseFloat( history.call_amount).toFixed(2);
                        history.call_duration = parseFloat( history.call_duration ).toFixed(2);
                    } );
                    $scope.totalMessage = data.data.calls.total_count;
                    $scope.currentPage = data.data.calls.current_page;
                } else if( statsType == 'messages' ) {
                    $scope.limit = data.data.messages.per_page;
                    $scope.userstatsData = data.data.messages.msglogs;
                    $scope.totalMessage = data.data.messages.total_count;
                    $scope.currentPage = data.data.messages.current_page;
                } else if( statsType == 'photos' ){ 
                    $scope.limit = data.data.photos.per_page;
                    $scope.userstatsData = data.data.photos.photologs;
                    $scope.totalMessage = data.data.photos.total_count;
                    $scope.currentPage = data.data.photos.current_page;
                } else{
                    $scope.limit = data.data.tips.per_page;
                    $scope.userstatsData = data.data.tips.msglogs;
                    $scope.totalMessage = data.data.tips.total_count;
                    $scope.currentPage = data.data.tips.current_page; 
                }

                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function (statsType) {
        $('#arrow').css ( {"opacity":"1" });
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            var statsData = {};
            statsData['responsetype'] = 'json';
            statsData['statstype'] = statsType;
            statsData['page'] = next_count;
            ProService.userStats( statsData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                
                if( statsType == 'calls' ) {
                    $scope.limit = data.data.calls.per_page;
                    $scope.userstatsData = data.data.calls.calllogs;
                    angular.forEach($scope.userstatsData, function(history , filterKey) {
                        history.call_amount = parseFloat( history.call_amount ).toFixed(2);
                        history.call_duration = parseFloat( history.call_duration ).toFixed(2);
                    } );
                    $scope.totalMessage = data.data.calls.total_count;
                    $scope.currentPage = data.data.calls.current_page
                } else if( statsType == 'messages' ) {
                    $scope.limit = data.data.messages.per_page;
                    $scope.userstatsData = data.data.messages.msglogs;
                    angular.forEach($scope.userstatsData , function(historyKey){
                        if(historyKey.displayname == username ){
                            historyKey.displayname = "me";
                        }
                    });
                    $scope.totalMessage = data.data.messages.total_count;
                    $scope.currentPage = data.data.messages.current_page
                } else if( statsType == 'photos' ){ 
                    $scope.limit = data.data.photos.per_page;
                    $scope.userstatsData = data.data.photos.photologs;
                    $scope.totalMessage = data.data.photos.total_count;
                    $scope.currentPage = data.data.photos.current_page;
                } else{
                    $scope.limit = data.data.tips.per_page;
                    $scope.userstatsData = data.data.tips.msglogs;
                    $scope.totalMessage = data.data.tips.total_count;
                    $scope.currentPage = data.data.tips.current_page; 
                }

                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $('#arrow1').css ( {"opacity":"0.5" });
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                $( '.phpdebugbar-openhandler-overlay' ).hide();
                $( '.ui-loader' ).hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $( "#nextBtn" ).css({ "background":"#dedede","border-color":"#dedede","color":"#acacac" });
            $scope.nextDisabled = true;
            $('#arrow1').css ( {"opacity":"0.5" });
        }
    };
} )