//+++++++++++++++++++++++++++TextMessage page controller+++++++++++++++++++++

messageApp.controller( "RecentMessageController", function( $scope, MessagesService, $state, $rootScope, $timeout ) {
    $scope.prevDisabled = false;
    $scope.nextDisabled = false;
    $scope.messageOptions = false;
    $scope.paginateSection = false;
    $scope.noMessageDiv = false;
    $scope.showLeadstext = false;
    $scope.showmeleads = true;
    $scope.limit = 4;
    $scope.recentChats = {};
    var checkVal = false;
    var pre_count = 0;
    var next_count = 0;
    var users = 'billable';
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    // console.log(dashboard_data);
    $scope.userid = JSON.parse( dashboard_data ).data.profile.user_id;
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var userId = userData.id;
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var getMessageData1 = {};
    getMessageData1['responsetype'] = 'json';
    getMessageData1['timezone'] = offsetTimezone;
    if( proData == 2 ) {
        //for pro users
        $scope.isPro = true;
        $scope.messageOptions = true;
    } else {
        $scope.isPro = false;
    }
    $('.ui-loader').show();
    getrecentmsgdata();
    function getrecentmsgdata(){
        $('.ui-loader').show();
        MessagesService.messages( getMessageData1 ).success( function( data ) {
            $('.ui-loader').hide();
            console.log(JSON.stringify(data));
            if( data.message == 'user not logged in' ) {
                //if user's session expires
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/affter_login"
                });
            }
            $scope.limit = data.data.per_page; //update the max limit of message per page
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
            $scope.dontShowmeAgainCheck = data.data.dontShowmeAgain;
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalMessage == 0 ) {
                    $scope.noMessageDiv = true;
                    $scope.noMessage = "No recent messages available.";
                } else {
                    $scope.noMessageDiv = false;
                }

            }  else {
                $scope.paginateSection = true;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
            if ( $scope.totalMessage == 0 ) {
            }
            angular.forEach($scope.recentChats , function(item){
                var getDate = item.maxtime;
                var dateFormat = 'MM/DD/YYYY';
                var utcdate = item.utc_time;
                // console.log(utcdate);
                item.formattedDate = getDate;
                var timestamp = moment.unix(utcdate).format("YYYY-MM-DD HH:mm:ss");
                var momentTimeobj = moment(timestamp);
                console.log(momentTimeobj);
                var timeformat = moment().localeData().longDateFormat('LT')
                var localDate = momentTimeobj.format(dateFormat);
                var localTime = momentTimeobj.format(timeformat);
                var dateReceivedFormat = momentTimeobj.format('YYYY-MM-DD');
                var getYesterdayDateM = moment().subtract(1, 'day');
                var getYesterdayDate = getYesterdayDateM.format('YYYY-MM-DD');
                // var excludetwoDateM = moment().subtract(2, 'days');
                // var excludetwoDate = excludetwoDateM.format('YYYY-MM-DD');
                var weekDateM = moment().subtract(8, 'days');
                var weekDate = weekDateM.format('YYYY-MM-DD');
                // console.log("localDate = ",momentTimeobj.format(dateFormat));
                // console.log("localTime = ",momentTimeobj.format(timeformat));
                console.log("compareFormat = ",dateReceivedFormat);
                console.log("currentTime = ",currentTime);
                console.log("Yesterday = ",getYesterdayDate);
                // console.log("twodaysbefore= ",excludetwoDate);
                console.log("oneweekbefore= ",weekDate);
                item.formattedDate = localTime;
                if (moment(dateReceivedFormat).isSame(currentTime)) {
                    item.formattedDate = localTime;
                    console.log("1st case");
                }else if (moment(getYesterdayDate).isSame(dateReceivedFormat)) {
                    item.formattedDate = 'Yesterday';
                    console.log("2nd case");
                }else if (moment(dateReceivedFormat).isBetween(weekDate,getYesterdayDate)) {
                    console.log("3rd case");
                    item.formattedDate = moment(dateReceivedFormat).format('dddd');
                    console.log(moment(dateReceivedFormat).format('dddd'));
                }else{
                    item.formattedDate = localDate;
                    console.log('4th case');
                }
            });
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    }

    $scope.backButtonMessages = function(){
        console.log(checkVal);
        if (checkVal === true) {
            // console.log("hello");
            checkVal = false;
            pre_count = 0;
            next_count = 0;
            users = 'billable';
            $('#billable_user').addClass('fullwidth_bluemsg');
            $('#billable_user').removeClass('fullwidth_white');
            $('#trial_user').addClass('fullwidth_white');
            $('#trial_user').removeClass('fullwidth_bluemsg');
            getrecentmsgdata();

        }else if ($scope.showLeadstext === true) {
            $scope.showmeleads = true;
            $scope.showLeadstext = false;
            checkVal = false;
            $scope.isPro = true;
            $scope.messageOptions = true;
            pre_count = 0;
            next_count = 0;
            users = 'billable';
            $('#billable_user').addClass('fullwidth_bluemsg');
            $('#billable_user').removeClass('fullwidth_white');
            $('#trial_user').addClass('fullwidth_white');
            $('#trial_user').removeClass('fullwidth_bluemsg');
            getrecentmsgdata();
        }else{
            window.plugins.nativepagetransitions.slide( {
                "direction": 'right',
                "href" : '#/home/affter_login'
            } );
        }
    };

    $scope.billableShow = true;
    $scope.billableUser = function () {
        checkVal = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'billable';
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#billable_user').removeClass('fullwidth_white');
        $('#trial_user').addClass('fullwidth_white');
        $('#trial_user').removeClass('fullwidth_bluemsg');
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var getMessageData1 = {};
        getMessageData1['responsetype'] = 'json';
        getMessageData1['timezone'] = offsetTimezone;
        MessagesService.messages( getMessageData1 ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            // $scope.dontShowmeAgainCheck = data.data.dontShowmeAgain;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            } else {
                $scope.noMessageDiv = false;
            }
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $scope.paginateSection = true;
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
        } ).error( function ( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.leadsUser = function(){
        checkVal = true;
        $scope.showLeadstext = false;
        $scope.paginateSection = true;
        $scope.showmeleads = true;
        $scope.messageOptions = true;
    };
    $scope.showLeads = function( evt ){
        if(checkVal == true){
            evt.preventDefault();
        }else{
        $('.ui-loader').show();
        $('.phpdebugbar-openhandler-overlay').show();
        // $timeout(function(){
        if ($scope.dontShowmeAgainCheck == '0') {
            $scope.showLeadstext = true;
            $scope.paginateSection = false;
            $scope.messageOptions = false;
            $scope.showmeleads = false;
        }else{
            checkVal = true;
            $scope.showLeadstext = false;
            $scope.paginateSection = true;
            $scope.showmeleads = true;
            $scope.messageOptions = true;
        }
        // $scope.showLeadstext = true;
        // $scope.paginateSection = false;
        // $scope.messageOptions = false;
        // $scope.showmeleads = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'trial';
        $('#billable_user').addClass('fullwidth_white');
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#trial_user').addClass('fullwidth_bluemsg');
        $('#trial_user').removeClass('fullwidth_white');
        var getMessageData = {};
        getMessageData['responsetype'] = 'json';
        getMessageData['sort'] = 'free';
        getMessageData1['timezone'] = offsetTimezone;
        MessagesService.messages( getMessageData ).success( function( data ) {
            $('.ui-loader').hide();
            $('.phpdebugbar-openhandler-overlay').hide();
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            } else {
                $scope.noMessageDiv = false;
            }
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $scope.paginateSection = true;
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
            $scope.paginateSection = false;
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
        checkVal = false;
      }
        
    };
    $scope.replyLink = function (originator_id) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/reply"+originator_id
        });
    };
    $scope.prevBtn = function () {
        $scope.recentChats = {};
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });

        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = pre_count;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            getMessageData1['timezone'] = offsetTimezone;
            MessagesService.messages( getMessageData1 ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ){
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                next_count = next_count - 1;
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        $scope.recentChats = {};
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = next_count+1;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            getMessageData1['timezone'] = offsetTimezone;
            console.log(JSON.stringify(getMessageData1))
            MessagesService.messages( getMessageData1 ).success( function( data ) {
                console.log(JSON.stringify(data));
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                console.log(JSON.stringify(data));
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
    $scope.deleteRecentMsg = function(dlt_Id){
        var originator_id = dlt_Id;
        console.log(userId,originator_id);
        if( parseInt( userId ) < parseInt( originator_id ) ) {
            channel1 = userId+'X'+originator_id;
            // console.log("if>>"+channel1);
        } else {
            channel1 = originator_id+'X'+userId;
            // console.log("else>>"+channel1);
        }
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        if (channel1) {
            // console.log(channel1);
            var clearChatHistory = {};
            clearChatHistory['responsetype'] = 'json';
            clearChatHistory['channel'] = channel1;
            MessagesService.clearHistory( clearChatHistory ).success( function( data ) {
                // console.log(data);
                $('.ui-loader').hide();
                $('#overlay').hide();
                if (data == 'success') {
                    $('#'+dlt_Id).hide();
                    $('#recentMsg_'+dlt_Id).hide("medium", function(){
                        $(this).remove();
                    });
                }
            } ).error( function ( data ) {
                // console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };

} )