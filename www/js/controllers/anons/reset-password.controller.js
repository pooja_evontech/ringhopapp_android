//+++++++++++++++++++++++++++Reset Password after forgot page controller+++++++++++++++++++++

anonApp.controller( "ResetPWDController", function( $scope, AuthService, $timeout, $state, $rootScope ) {
    var flag = true;

    $scope.resetForgotPwd = function( $event ) {
        $( '#reset_status' ).html( '' );
        var password = $scope.password;
        var confirm_passwd = $scope.confirmpassword;
        $( '#passwd' ).keydown(function ( e ) {
            if(e.keyCode == 8){
                $( '#reset_status' ).html('');
                if( $('#passwd').val().length <= 6 && $('#passwd').val().length > 0 ) {
                    flag = false;
                    $( '#passwd_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#passwd').val().length == 1 ){
                    $( '#passwd_error' ).html( 'This field is required.' );
                    flag = false;
                } 
            } else {
                $( '#reset_status' ).html('');
                if( $('#passwd').val().length <= 6 && $('#passwd').val().length > 0 ) {
                    flag = false;
                    $( '#passwd_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#passwd').val().length >= 5) {
                    $( '#passwd_error' ).html('');
                    $( '#passwd_label' ).removeAttr('style');
                    flag = 1;
                }
            }
        } );
        $( '#con_passwd' ).keydown( function ( e ) {
            if (e.keyCode == 8) {
                $( '#reset_status' ).html('');
                if( $('#con_passwd').val().length <= 6 && $('#con_passwd').val().length > 0 ) {
                    flag = false;
                    $( '#con_passwd_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#con_passwd').val().length == 1 ){
                    $( '#con_passwd_error' ).html( 'This field is required.' );
                    flag = false;
                } 
            } else {
                $( '#reset_status' ).html('');
                if( $('#con_passwd').val().length <= 6 && $('#con_passwd').val().length > 0 ) {
                flag = false;
                    $( '#con_passwd_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#con_passwd').val().length >= 5) {
                    $( '#con_passwd_error' ).html('');
                    $( '#con_passwd_label' ).removeAttr('style');
                    flag = 2;
                }
            }
        } );
        if( flag == 1 && flag == 2 ) {
            flag = true;
        }
        if( $( '#con_passwd_error' ).html() && $( '#passwd_error' ).html() == '' ) {
            flag = true;
        }
        if( $( '#passwd' ).val() == '' && $('#con_passwd').val() == '' ) {
            $( '#passwd_error' ).html( 'This field is required.' );
            $( '#con_passwd_error' ).html( 'This field is required.' );
            $( '#passwd_label' ).css( 'color', 'red' );
            $( '#con_passwd_label' ).css( 'color', 'red' );
        } else {
            if( $( '#passwd' ).val() == '' && $( '#con_passwd' ).val() != '' ) {
                flag = false;
                $( '#passwd_error' ).html( 'This field is required.' );
                $( '#passwd_label' ).css( 'color', 'red' );
                if( $( '#con_passwd' ).val().length < 6 ) {
                    $( '#con_passwd_error' ).html( 'Please enter at least 6 characters.' );
                    $( '#con_passwd_label' ).css( 'color', 'red' );
                }
            } else if( $( '#passwd' ).val() != '' && $( '#con_passwd' ).val() == '' ) {
                flag = false;
                $( '#con_passwd_error' ).html( 'This field is required.' );
                $( '#con_passwd_label' ).css( 'color', 'red' );
                if( $( '#passwd' ).val().length < 6 ) {
                    $( '#passwd_error' ).html( 'Please enter at least 6 characters.' );
                    $( '#passwd_label' ).css( 'color', 'red' );
                }
            } else {
                flag = true;
            }
            if( password == confirm_passwd ) {
                if ( flag == true ) {
                    var user_phone = localStorage.getItem( 'UserPhone_FG' );
                    var getResetData = {};
                    getResetData[ 'password' ] = password;
                    getResetData[ 'confirmpassword' ] = confirm_passwd;
                    getResetData[ 'phonenumber' ] = user_phone;
                    getResetData[ 'responsetype' ] = 'json';
                    $( '.phpdebugbar-openhandler-overlay' ).show();
                    $( '.ui-loader' ).show();

                    AuthService.resetPassword( getResetData ).success( function( data ) {
                        //console.log(data);
                        $( '.phpdebugbar-openhandler-overlay' ).hide();
                        $( '.ui-loader' ).hide();
                        if( data.status == 'success' ) {
                            window.plugins.nativepagetransitions.slide( {
                                "href" : "#/home/login"
                            } );
                        } else {
                            $( '.phpdebugbar-openhandler-overlay' ).hide();
                            $( '.ui-loader' ).hide();
                            $( '#reset_status' ).html( data.message );
                        }
                    } ).error( function ( err ) {
                        $( '.phpdebugbar-openhandler-overlay' ).hide();
                        $( '.ui-loader' ).hide();
                        if( navigator.connection.type == Connection.NONE ) {
                            checkConnection();
                        }
                    } );
                }
            } else {
                flag = false;
                $( '#reset_status' ).html( 'password is not matched' );
            }
        }
    };
 } )