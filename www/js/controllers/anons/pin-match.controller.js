//+++++++++++++++++++++++++++Pin Confirm for register page controller+++++++++++++++++++++

anonApp.controller( "PinMatchController", function( $scope, AuthService, $timeout, $state, $rootScope) {
    var flag = false;
    var pinPattern = /^[0-9]+$/;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    $scope.show_toContactDivP = false;
    var height = $( window ).height();
    $( '.wrapper, #sidebar-wrapper' ).css( 'min-height', height );
    var dataofpro = localStorage.getItem("connectWithProData");
    if (localStorage.getItem("connectWithProData")) {
        if (JSON.parse(dataofpro).data) {
            $scope.dataofpro_data = JSON.parse(dataofpro).data;
            // $scope.dataofproimg = JSON.parse(dataofpro).data.pro_image;
            // $scope.dataofproname = JSON.parse(dataofpro).data.pro_displayname; 
                if ($scope.dataofpro_data.length != 0) {
                $scope.show_toContactDivP = true;
                $scope.dataofproimg = JSON.parse(dataofpro).data.pro_image;
                $scope.dataofproname = JSON.parse(dataofpro).data.pro_displayname; 
                var image = $('#profile_pic');
                image.css('background-image', 'url(' + $scope.dataofproimg +')');
            }
        }
    }
    
    // $scope.dataofproimg = 'http://res.cloudinary.com/nobetek-llc/image/upload/c_limit,d_default-profile-150x113.jpg,h_140,w_105/v1444884891/profile_188.jpg';
    // var image = $('#profile_pic');
    // image.css('background-image', 'url(' + $scope.dataofproimg +')');
    // $scope.dataofproname = 'Abella';
    $scope.pinbackButton = function(){
        console.log($rootScope.previousState);
        if($rootScope.previousState == 'home.register' || $rootScope.previousState == 'home.register_step2'){
                window.plugins.nativepagetransitions.slide({
                "direction" : "right",
                "href" : "#/home/register"
            });
        }else{
            window.plugins.nativepagetransitions.slide({
                "direction" : "right",
                "href" : "#/home/login"
            });
        }
    };  
    $scope.pinMatchAction = function ( $event ) {
        $('#pin_error').html( '' );
        $('#pinMatch_status').html( '' );
        var pin_number = $( '#inputEmail6' ).val();
        $( '#inputEmail6' ).keydown(function (e) {
            var withoutSpace = $('#inputEmail6').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if(e.keyCode == 8){
                $( '#pinMatch_status' ).html('');
                withoutSpaceLength = withoutSpaceLength-1;
                if( withoutSpaceLength <= 4 && withoutSpaceLength > 0 ) {
                    flag = false;
                    $( '#pin_error' ).html( 'Please enter at least 4 characters.' );
                    $( '#pin_label' ).css( 'color', 'red' );
                }else if( withoutSpaceLength > 4){
                    flag = false;
                     $( '#pin_error' ).html( 'Please enter no more than 4 characters.' );
                     $( '#pin_label' ).css( 'color', 'red' );
                }
                if( withoutSpaceLength == 4) {
                    $( '#pin_error' ).html('');
                    $( '#pin_label' ).removeAttr('style');
                    flag = 1;
                }
                if( withoutSpaceLength <= 0 ){
                    $( '#pin_error' ).html( 'This field is required.' );
                    $( '#pin_label' ).css( 'color', 'red' );
                    flag = false;
                } 
            }else{
                $( '#pinMatch_status' ).html('');
                if( withoutSpaceLength <= 3 && withoutSpaceLength > 0 ) {
                    flag = false;
                    $( '#pin_error' ).html( 'Please enter at least 4 characters.' );
                    $( '#pin_label' ).css( 'color', 'red' );
                }else if( withoutSpaceLength > 3){
                    flag = false;
                    $( '#pin_error' ).html( 'Please enter no more than 4 characters.' );
                    $( '#pin_label' ).css( 'color', 'red' );
                }
                if( withoutSpaceLength == 3) {
                    $( '#pin_error' ).html('');
                    $( '#pin_label' ).removeAttr('style');
                    flag = 1;
                }
            }
        } );
        
        if( $( '#pin_error' ).html() == '' ) {
            flag = true;
        }
        if( pin_number != '' ) {
            var withoutSpace = $( '#inputEmail6' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength < 4 ) {
                $( '#pinMatch_status' ).html('');
                flag = false;
                $( '#pin_label' ).css( 'color', 'red' );
                if( withoutSpaceLength == 0 ) {
                    $( '#pin_error' ).html( 'This field is required.' );
                } else {
                    $( '#pin_error' ).html( 'Please enter at least 4 characters.' );
                }
            }
            if( withoutSpaceLength > 4 ){
                $( '#pinMatch_status' ).html('');
                flag = false;
                $( '#pin_error' ).html( 'Please enter no more than 4 characters.' );
                $( '#pin_label' ).css( 'color', 'red' );
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pin_error' ).html( '' );
                $( '#pin_label' ).removeAttr( 'style' );
                if( !$( '#inputEmail6' ).val().match( pinPattern ) ) {
                    $( '#pinMatch_status' ).html( 'The pin must be a number.' );
                    flag = false;
                } else{
                    flag = true;
                }
            }
            if( flag == true ) {
                var user_phone_stored = localStorage.getItem( 'regUserPhone' );
                localStorage.setItem( "regUserPin", $scope.pinNumber );
                var getPinData = {};
                getPinData[ 'pin' ] = $scope.pinNumber;
                getPinData[ 'responsetype' ] = 'json';
                getPinData[ 'phonenumber' ] = user_phone_stored;
                $( '.phpdebugbar-openhandler-overlay' ).show();
                $( '.ui-loader' ).show();

                AuthService.pinMatch( getPinData ).success( function( data ){
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    console.log(JSON.stringify(data));
                    if( data.status == 'success' ) {
                        if (data.message == 'You are blocked.') {
                            $( '#pinMatch_status' ).html( data.message );
                        }else{
                           localStorage.setItem( "user_userid", data.userid );
                            window.plugins.nativepagetransitions.slide( {
                                "href" : "#/home/register_step2"
                            } ); 
                        }
                    } else {
                        $( '#pinMatch_status' ).html( data.message );
                    }
                } ).error( function ( err ) {
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        } else {
            $( '#pin_error' ).html( 'This field is required.' );
            $( '#pin_label' ).css( 'color', 'red' );
        }
    };
} )