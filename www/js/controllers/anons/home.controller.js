//+++++++++++++++++++++++++++home page controller+++++++++++++++++++++

anonApp.controller( "HomeController", function( $scope, $http, $state, $location ) {
    $scope.devicehight = $( window ).height();
    $scope.login = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/login"
        });
    };
    $scope.register = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/register"
        });
    };
} )