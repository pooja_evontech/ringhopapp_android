//+++++++++++++++++++++++++++phoneSettings page controller+++++++++++++++++++++

userApp.controller( "AvailabilityController", function( $scope, $timeout, $state, $rootScope, UserService ) {
    var statusData = {};
    statusData['responsetype'] = "json";
    UserService.phoneSettings( statusData ).success( function( data ) {
        //console.log(data);
        $scope.call_available = data.data.call_available == 0 ? false : true  ;
        $scope.sms_available = data.data.sms_available == 0 ? false : true ;
        $scope.video_available = data.data.video_call_available == 0 ? false : true ;
        if( data.data.profile_status != null ) {
            $scope.profile_status = data.data.profile_status;
        }
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );
    $scope.updateStatus = function ( $event ) {
        statusData = {};
        statusData[ 'responsetype' ] = "json";
        $( '#availStatusCheck' ).addClass('errorStatus').removeClass('succesStatus');
        if( $scope.sms_available == true ) {
            statusData[ 'sms_available' ] = $scope.sms_available;
        }
        if( $scope.call_available == true ) {
            statusData[ 'call_available' ] = $scope.call_available;
        }
        if( $scope.video_available == true ) {
            statusData[ 'video_call_available' ] = $scope.video_available;
        }
        statusData[ 'profile_status' ] = $scope.profile_status;
        $( '.phpdebugbar-openhandler-overlay' ).show();
        $( '.ui-loader' ).show();
        UserService.phoneSettings( statusData ).success( function( data ) {
            if( data.message == 'user not logged in' ) {
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/affter_login"
                } );
            }
            if( data.status == 'success' ){
                $( '#availStatusCheck' ).removeClass('errorStatus').addClass('succesStatus');
            } else {
                $( '#availStatusCheck' ).addClass('errorStatus').removeClass('succesStatus');
            }
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $( '.ui-loader' ).hide();
            $( '#availStatusCheck' ).html( data.message );
        } ).error( function ( data ) {
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
} )
