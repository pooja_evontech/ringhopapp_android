//+++++++++++++++++++++++++++DashBoard page controller+++++++++++++++++++++

userApp.controller( "DashboardController", function( $scope, constantData, UserService, PaymentService, $timeout, $state, $rootScope, $location, AuthService  ) {
    ringtoneSound = false;
    $rootScope.previousState;
    $rootScope.currentState;
    $scope.date = new Date();
    $scope.messageCount = 0; 
    $scope.callCount = 0;
    $scope.all_cont_div_show = false;
    $scope.cont_headline_show = false;
    $scope.become_pro_popupShow = false;
    $scope.appVersionDashboard = localStorage.getItem('Latest_App_Version');
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
    });
    //$scope.upgradeDisplay = false;
    var user_detail = localStorage.getItem( "userDetail" );
    // authToken = (JSON.parse( user_detail )).token;
    // console.log('hello '+'1');
    var userData = JSON.parse( user_detail ).data;
    var profileImg = localStorage.getItem( "profileData" );
    $scope.profileImg =profileImg;
     var image = $('#profile_pic');
     image.css('background-image', 'url(' + profileImg +')');
    $scope.pro_info = 2;
    if (localStorage.getItem( "dashboard_data" )) {
        var dashboard_data = localStorage.getItem( "dashboard_data" );
        var pro_info_chngd = JSON.parse( dashboard_data ).data.profile.pro;
        $scope.pro_info = pro_info_chngd;
    }
    var username = userData.displayname;
    $('#username').html(username);
    $scope.proSection = false;
    $scope.femaleProLink = false;
    $scope.submitReviewPro = false;
    $scope.actionRequired = false;
    $scope.learnMore = true;
    $scope.yellowUpgrade = false;
    $scope.completeProSetup = false;
    $scope.updatePayment = false;
    $scope.shoutsection = false;
    $scope.earnMoneyPro = false;
    $scope.afterLearnMorePro = false;
    var getDashboardData = {};
    getDashboardData['responsetype'] = "json";
    getDashboardData['app'] = 1;
    getDashboardData['app_version'] = $scope.appVersionDashboard;
    getDashboardData['os'] = deviceOS;
    // getDashboardData['native'] = 1;
    var user_id = JSON.parse(user_detail).data.id;
    $('.ui-loader').show();
    console.log(JSON.stringify(getDashboardData));
    UserService.dashboard( getDashboardData ).success( function( data ){
        $('.ui-loader').hide();
        console.log(JSON.stringify(data));
        if( data.message == 'user not logged in' ){
            var rememberme_check = localStorage.getItem( "remember_appLaunch" );
            if (rememberme_check) {
                var rememberme_local1 = localStorage.getItem( "rememberme_data" );
                var remember_data1 = JSON.parse( rememberme_local1 );
                var getLoginData = {};
                getLoginData[ 'email' ] = remember_data1.email;
                getLoginData[ 'password' ] = remember_data1.password;
                getLoginData[ 'responsetype' ] = "json";
                getLoginData[ 'signin_remember' ] = remember_data1.remember;
                $( '.phpdebugbar-openhandler-overlay' ).show();
                $( '.ui-loader' ).show();
                AuthService.login( getLoginData ).success( function( data ){
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( data.status == 'success') {
                        // window.plugins.nativepagetransitions.slide( {
                        //     "href" : "#/home/affter_login"
                        // } );
                        localStorage.setItem( "userDetail", JSON.stringify( data ) );
                        localStorage.setItem( "profileData", data.profile_image );
                        UserService.dashboard( getDashboardData ).success( function( data ){
                            $scope.loadDashboardData(data);
                        } ).error( function ( data ) {
                            if(navigator.connection.type == Connection.NONE) {
                                checkConnection();
                            }
                        } );
                    }
                }). error( function(err) {
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                });
            }else{
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/login"
                });
            }
            
        } else if (data.message == 'Your account is blocked. Contact support Team.') {
            navigator.notification.alert(
                data.message,  // message
                alertDismis,         // callback
                'Ringhop',            // title
                'OK'                  // buttonName
            );
        }else{
            if (data.data.updateVersion == 'yes') {
                navigator.notification.confirm(
                    'New version update is available.Click OK to update the version.',  // message
                    onConfirmVersion,
                    'UPDATE'
                );
            }
            function onConfirmVersion( button ) {
                    if( button == 1 ) {
                    // console.log($rootScope.appData.iphone_app_url);
                    window.open($rootScope.appData.iphone_app_url,'_blank');
                }
            }
            $scope.loadDashboardData(data);
        }
        

    } ).error( function ( data ) {
        $('.ui-loader').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    function alertDismis(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/login"
        });
    }
    $scope.loadDashboardData = function(data){
       console.log("loading dashboard");
        $scope.recentlyAddedContacts = data.data.recentContacts;
        $scope.suggestedPros = data.data.suggestPros;
        localStorage.setItem('getSuggestedProData',data.data);
        if ($scope.recentlyAddedContacts.length > 0) {
            $scope.all_cont_div_show = true;
            $scope.cont_headline_show = true;
            $scope.recentcont_Section = 'recentcont_Section';
        }
        if ($scope.suggestedPros.length > 0) {
            $scope.suggest_div_show = true;
            $scope.suggest_headline_show = true;
            $scope.recentcont_Section = 'recentcont_Section';
        }
        $('#username').html(data.data.profile.fullname);
        $scope.messageCount = data.data.unread_msg_count;  
        $scope.msgStatus = data.data.msg_read_status; 
        $scope.callCount = data.data.recent_call_count;
        var getDeviceData = {};
        getDeviceData['os'] = deviceOS;
        getDeviceData['uuid'] = uuid;
        getDeviceData['app_version'] = $scope.appVersionDashboard;
        getDeviceData['version'] = deviceVersion;
        getDeviceData['user_id'] = user_id;
        getDeviceData['device_token'] = device_token;
        getDeviceData['responsetype'] = "json";
        console.log(JSON.stringify(getDeviceData));
        UserService.savePhoneDetails( getDeviceData ).success( function( data ){
            // console.log( JSON.stringify( data ) );
        } ).error( function ( data ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );

        if( data.status != 'error' ) {
            $scope.textToCopy = data.data.userProfileLink;
            localStorage.setItem('User_ProfileLink',data.data.userProfileLink);
            var user_number = data.data.profile.ringhop_number;
            if (user_number !== null) {
                console.log("check");
                $scope.show_pro_no = true;
            }else{
                $scope.show_pro_no = false;
            }
            localStorage.setItem("dashboard_data",JSON.stringify(data));
            $('#usersStatus').html(data.data.status);
            $('#user_loc').html(data.data.profile.profile_status);
            $scope.profileImg =data.data.profile_image_big;
            var image = $('#profile_pic');
            image.css('background-image', 'url(' + $scope.profileImg +')');
            localStorage.setItem("profileData", data.data.profile_image_big);
            var proDetail = data.data.profile.pro;
            $scope.pro_info = proDetail;
            if(proDetail == 2){
                $scope.shoutsection = true;
            }
            if( proDetail == 0 ) {
                $scope.earnMoneyPro = true;
                $scope.learn_more_pro = 'Earn money';
                $scope.pro_status = 'BECOME A PRO'
                $scope.pro_tagline = 'CHARGE YOUR FANS & CLIENTS'
                if( data.data.profile.gender == 'F' ) {
                    $scope.femaleProLink = true;
                }
            } else if( proDetail == 1 ) {
                $scope.earnMoneyPro = false;
                $scope.afterLearnMorePro = true;
                $scope.learn_more = 'Complete Ringhop Pro setup';
                $scope.pro_status = 'Ringhop Pro verification process.';
            } else {
                $scope.earnMoneyPro = false;
                $scope.learn_more = '';
                $scope.pro_status = '';
            }
            if( data.data.pro_data != null ) {
                if( proDetail == 1 ) {
                    if( data.data.pro_data.pv_notes == 'admin_pending' ) {
                        $scope.submitReviewPro = true;
                    } else {
                        $scope.completeProSetup = true;
                    }
                }
                if( data.data.pro_data.pv_terms == -1 || data.data.pro_data.pv_idverify == -1 || data.data.pro_data.pv_idverify == -1 || proDetail == -1 ) {
                    $scope.actionRequired = true;
                }
            }
            if( data.data.user_status == 'billingissue' ) {
                $scope.updatePayment = true;
            }
            if( data.data.user_status == 'pending' && proDetail != 2 && data.data.profile.gender == 'M') {
                $scope.yellowUpgrade = true;
            }
            $scope.proStatusLink = function() {
                $('#wrapper').css('overflow','hidden');
                $scope.become_pro_popupShow = true;
                $('#overlay').show();
                $('#BMPopup').show();
                // if( proDetail == 0 ) {
                //     window.plugins.nativepagetransitions.slide({
                //         "href" : "#/home/learnmore"
                //     });
                // }
                // if( proDetail == 1 ) {
                //     window.plugins.nativepagetransitions.slide({
                //         "href" : "#/home/prodashboard"
                //     });
                // }
            };
            //pro user detail in dashboard
            if( proDetail == 2 ) {
                if (user_number) {
                    var pro_number = "("+user_number.substr(1, 3)+") "+user_number.substr(4, 3)+"-"+user_number.substr(7, 4);
                    $('#pro_number').html(pro_number);
                }
                $scope.proSection = true;
                $scope.submitReviewPro = false;
            }
            /*if (proDetail != 2) {
                if( data.data.user_status == 'pending' ) {
                    $scope.upgradeDisplay = true;
                } else {
                    $scope.upgradeDisplay = false;
                }
            }*/
        } else {
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }  
    };

    $scope.learnMoreForPro = function(){
        $scope.become_pro_popupShow = false;
        $('#overlay').hide();
        $('#BMPopup').hide();
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/learnmore"
        });
    };
    $scope.gotoCallerHistory = function(contact_data_dashboard){
        window.plugins.nativepagetransitions.slide({
            "direction" : "left",
            "href" : "#/home/callerhistory" +contact_data_dashboard
        });
    };
    $scope.gotoProfile = function(profileId,profileName){
        console.log(profileId,profileName);
        window.plugins.nativepagetransitions.slide( {
            "direction": "left",
            "href" : "#/home/profile"+profileId+"/"+profileName
        } );
    };
    
    $scope.closeHeaderPopup = function () {
        $('#wrapper').css('overflow','visible');
        $scope.become_pro_popupShow = false;
        $('#overlay').hide();
        $('#BMPopup').hide();
    };
    $scope.buyCredits = function () {
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var user_detail = localStorage.getItem( "userDetail" );
        var userid = JSON.parse( user_detail ).data.id;
        var creditData = {};
        var page_name = 'login';
        creditData['responsetype'] = 'json';
        creditData['userid'] = userid;

        PaymentService.addCredits( creditData, 'GET' ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.paymentVia == 'in_app' ) {
                    window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+userid
                });
            }
            else if(data.paymentVia == 'credit_card'){
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+userid
                });
            }
        }).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.clickForCopy = function(){
        cordova.plugins.clipboard.copy($scope.textToCopy);
    };
    $scope.shoutLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/createnewshout"
        });
    };
    $scope.settingLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/settings"
        });
    };
    $scope.statusAndAvaillink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/phonesettings"
        });
    };
    $scope.chatLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/messages"
        });
    };
    $scope.callLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/recentcalls"
        });
    };
    $scope.contactsLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/contacts"
        });
    };
    $scope.myLibrary = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/myLibrary"
        });
    };
    $scope.searchLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/search1"
        });
    };
    $scope.suggestProLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/suggestPro"
        });
    };
    $scope.gotoCallerHistory = function(contact_data_dashboard){
        $('#overlay').hide();
        $('.phpdebugbar-openhandler-overlay').hide();
        window.plugins.nativepagetransitions.slide({
            "direction" : "left",
            "href" : "#/home/callerhistory" +contact_data_dashboard
        });
    };
} )