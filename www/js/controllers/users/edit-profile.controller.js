//+++++++++++++++++++++++++++editProfile page controller+++++++++++++++++++++

userApp.controller( "EditProfileController", function( $scope, UserService, $timeout, $state, $rootScope ) {
    $scope.image_available = false;
    $scope.takePhotoOpt = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var proDisplayFlag = 0;
    var editProfileData = {};
    $scope.isPro = false;
    if( proData != 2 ) {
        //for pro users
        $scope.isPro = false;
    } else {
        $scope.isPro = true;
    }
    editProfileData['responsetype'] = 'json';
    UserService.editProfile( editProfileData ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.isDisplayPro = data.display_ringhop === 0 ? true  : false ;
        if( data.data.display_callstatus == 0 ) {
            $scope.isDisplayCall = true;
        } else {
            $scope.isDisplayCall = false;
        }
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );

    $scope.socialLinks = function () {
        back( 'left', '#/home/forsale' );
    };

    $scope.displayPro = function (isDisplayPro) {
        if( isDisplayPro == true ){
            proDisplayFlag = 0;
        } else {
            proDisplayFlag = 1;
        }
        var proDisplayData = {};
        proDisplayData[ 'display_ringhop' ] = proDisplayFlag;
        proDisplayData[ 'responsetype' ] = 'json';
        $('.ui-loader').show();
        UserService.editProfileSettings( proDisplayData ).success( function( data ) {
            $('.ui-loader').hide();
        } ).error( function ( data ) {
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };

    $scope.uploadPhoto = function () {
        $scope.takePhotoOpt = true;
    };

    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    $scope.editProfileLink = function ( $event ) {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/editProfile"
        } );
    };

    function onPhotoDataSuccess( imageData ) {
        console.log("success");
        var profileImage = document.getElementById( 'profile_image' );
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageString = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function ( $event ) {
        $scope.takePhotoOpt = false;
        $scope.image_available = true;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture( onPhotoDataSuccess, onFail, { quality: 75, targetWidth:1000,
        	targetHeight: 1000, correctOrientation: true,
        	destinationType: Camera.DestinationType.DATA_URL
        } );
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $scope.takePhotoOpt = false;
        $scope.image_available = true;
        navigator.camera.getPicture( onPhotoDataSuccess, onFail, { quality: 50, targetWidth:1000,
        	targetHeight: 1000, destinationType: Camera.DestinationType.DATA_URL,
         	sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        } );
    };


    // Called if something bad happens.
    function onFail( message ) {
        console.log("error>" +message);
        $scope.takePhotoOpt = false;
        $scope.image_available = false;
    }

    //upload profile pic
    $scope.saveProfilePic = function ( $event ) {
        console.log("save_dp");
        var updateProfileData = {};
        updateProfileData[ 'responsetype' ] = "json";
        updateProfileData[ 'image' ] = imageString;
        $( '.phpdebugbar-openhandler-overlay' ).show();
        $( '.ui-loader' ).show();
        UserService.saveProfilePic( updateProfileData ).success( function( data ) {
            $('#after_img_select').html(data.message).css('color','green').addClass('col-sm-12');
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } ).error( function ( data ) {
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $( '.ui-loader' ).hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    //cancel upload
    $scope.cancelUpload = function () {
        $scope.image_available = false;
    };

    $scope.deletePhoto = function () {
        navigator.notification.confirm(
            'Are you sure you want to delete photo?',  // message
            onConfirm,
            'Delete Photo'
        );
    };

    function onConfirm( button ) {
        if( button == 1 ) {
            var deletePhotoData = {};
            deletePhotoData[ 'responsetype' ] = "json";
            UserService.deleteProfilePic( deletePhotoData ).success( function( data ) {
                $( '#deleteBtn' ).html( data.message );
                $( '#deleteBtn' ).addClass( 'errorStatus' );
            } ).error( function ( data ) {
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        }
    }

    $scope.setLocation = function() {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/editlocation"
        } );
    };

    $scope.aboutMe = function() {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/editaboutme"
        } );
    };

} )