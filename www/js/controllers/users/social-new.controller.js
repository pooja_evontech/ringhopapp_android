//+++++++++++++++++++++++++++social page controller+++++++++++++++++++++

userApp.controller( "SocialNewController", function( $scope, ContactsService, UserService, $state, $stateParams, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.date = new Date();
    $scope.loginid = userData.id;
    $scope.userid = $stateParams.userid;
    var viewprofileData = {};
    viewprofileData['responsetype'] = 'json';
    viewprofileData['userid'] = $stateParams.userid;
    UserService.socialProfile( viewprofileData ).success( function( data ) {
        // console.log( JSON.stringify( data ) );
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.user_data = data.data;
        $scope.profileData = data.data.sociallinks;
        var user_number = data.data.userprofile.ringhop_number;
        if(user_number != ''){
            $scope.usr_number = "+"+user_number.substr(0,1)+" ("+user_number.substr(1, 3)+") "+user_number.substr(4, 3)+"-"+user_number.substr(7, 4);
        } else {
            $scope.usr_number = "";
        }
        if( data.data.iscontact == false ) {
            $('#savecontact').html('Save Contact');
        } else {
            $('#savecontact').html('Already a contact');
        }
    } ).error( function ( data ) {
        // console.log( JSON.stringify( data ) );
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.socialprofileLink = function(id_of_user){
        window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : "#/home/reply"+id_of_user
        } );
    };
    $scope.saveContact = function () {
        if( $scope.user_data.iscontact == false ){
            var saveContactData = {};
            saveContactData['responsetype'] = 'json';
            saveContactData['caller_id'] = $stateParams.userid;
            ContactsService.addContact( saveContactData ).success( function( data ) {
                // console.log( JSON.stringify( data ) );
                $scope.contactMsg = data.message;
                if( data.status == 'error' ){
                    $('#savecontact').html('Already a contact');
                } else {
                    $('#savecontact').html('Contact added');
                }

            } ).error( function ( data ) {
                // console.log( JSON.stringify( data ) );
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )