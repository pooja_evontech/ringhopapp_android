//+++++++++++++++++++++++++++viewprofilenew page controller+++++++++++++++++++++

userApp.controller( "ViewProfileNewController", function( $scope, UserService, ContactsService, $state, $stateParams, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.date = new Date();
    $scope.loginid = userData.id;
    $scope.userid = $stateParams.userid;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.proInfo = proData;
    $scope.showVideoIcon = false;
    $scope.blockPopup = false;
    $scope.gotoBlockUsers = false;
    $scope.closePopupBlock = false;
    // console.log($stateParams.uname+'?responsetype=json');
    var viewprofileData = {};
    viewprofileData['responsetype'] = 'json';
    UserService.viewProfile( $stateParams.uname+'?responsetype=json' ).success( function( data ) {
        console.log( JSON.stringify( data ) );
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.user_data = data.data;
        $scope.profileData = data.data.sociallinks;
        $scope.viewProfileProData = data.data.caller;
        var user_number = data.data.ringhop_number;
        $scope.profilenew = data.data.profile_images.profile_image;
        var image = $('#profile_pic');
        image.css('background-image', 'url(' + $scope.profilenew +')');
        if(user_number !== null && user_number !== ''){
            //console.log(user_number);
            $scope.user_data.ringhop_number = "("+user_number.substr(1, 3)+") "+user_number.substr(4, 3)+"-"+user_number.substr(7, 4);
        } else {
            $scope.user_data.ringhop_number = "";
        }
        if($scope.proInfo != $scope.viewProfileProData.pro){
            $scope.showVideoIcon = true;
        } else {
            $scope.showVideoIcon = false;
        }
        $scope.blockUser = function(){
            if($('#alreadyBlocked').html()=='Block this user') {
                navigator.notification.confirm(
                    'Are you sure you want to block this user?',  // message
                    onConfirm,
                    'Block User'
                );
            }
        };
        function onConfirm(button) {
            if( button == 1 ) {
                var blockUsrData = {};
                blockUsrData['responsetype'] = 'json';
                blockUsrData['caller_id'] = data.data.caller.id;
                ContactsService.blockUser( blockUsrData ).success( function( data ) {
                    $('#alreadyBlocked').html('user blocked');
                    $('#alreadyBlocked').removeClass('redCallHistory');
                    $('#alreadyBlocked').addClass('italicCall');
                } ).error( function ( data ) {
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            }
        }
        if( data.data.iscontact == true ) {
            $('#alreadyContact').removeClass('blueCallHistory');
            $('#alreadyContact').addClass('italicCall');
            $('#alreadyContact').html('already a contact');
        } else {
            $('#alreadyContact').removeClass('italicCall');
            $('#alreadyContact').addClass('blueCallHistory');
            $('#alreadyContact').html('Add as contact');
            $scope.addAsContact = function(){
                var addContactData = {};
                addContactData['responsetype'] = 'json';
                addContactData['caller_id'] = data.data.caller.id;
                ContactsService.addContact( addContactData ).success( function( data ) {
                    $('#alreadyContact').html('contact added');
                    $('#alreadyContact').removeClass('blueCallHistory');
                    $('#alreadyContact').addClass('italicCall');
                } ).error( function ( data ) {
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            };
        }

    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );

    $scope.openLink = function(url){
        var ref = window.open(url, '_blank');
    };

    $scope.saveContact = function () {
        if( $scope.user_data.iscontact == false ){
            var saveContactData = {};
            saveContactData['responsetype'] = 'json';
            saveContactData['caller_id'] = $stateParams.userid;
            ContactsService.addContact( saveContactData ).success( function( data ) {
                $scope.contactMsg = data.message;
                if( data.status == 'error' ){
                    $('#savecontact').html('Already a contact');
                } else {
                    $('#savecontact').html('Contact added');
                }

            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.videoCallFromProfile =function(id_receiver , name_receiver){
        $('.phpdebugbar-openhandler-overlay').hide();
        $scope.idCallback = id_receiver;
        $scope.username1 = name_receiver;
        $scope.pageState = 'notAvailable';
        $scope.pageStateInactive = 'deactivated';
        var videoData = {};
        videoData['responsetype'] = 'json';
        videoData['receiver'] = $scope.idCallback;
        UserService.getUserDetail( videoData ).success( function( data ) {
            $scope.videoCallRate = data.data.video_call_rate;
            $scope.userBalance = data.data.originator_balance;
            $scope.reqBal = data.data.bal_req;
            $scope.origBlock = data.data.originator_block;
            $scope.receiverBlock = data.data.receiver_block;
            $scope.receiverAvailable = data.data.receiver_available;
            localStorage.setItem("receiverImage_data",data.data.profile_image);
            var receiveData = JSON.stringify({'username':$scope.username1,'origId':$scope.idCallback,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'callerhistory', 'receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
            if ($scope.receiverBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            }else if ( $scope.origBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "You are blocked";
            }
            else if( $scope.origBlock === true && $scope.receiverBlock === true ) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            } else if ( $scope.receiverAvailable == "0") {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "This user is not available";
            }else if (data.data.receiver_status == 'inactive') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageStateInactive+'/'+$scope.idCallback
                    } );
                }
            else if (data.data.in_video_call == '1') {
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageState+'/'+$scope.idCallback
                } );
            }
            else if ($scope.origBlock === false && $scope.receiverBlock === false){ 
                $scope.closePopupBlock = false;
                $('#overlay').hide();
                if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                   $state.go('home.videoCalling',{receive_data:receiveData})
                }else{
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/videoCredits/"+$scope.username1+'/'+$scope.reqBal
                    });
                }
            }
        } ).error( function ( data ) {
            $scope.networkPopup = true;
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.closeBlock = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
    };

    $scope.blockLink = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };
    $scope.menuloggedout = function () {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
} )
