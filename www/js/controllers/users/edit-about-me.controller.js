//+++++++++++++++++++++++++++editaboutme page controller+++++++++++++++++++++

userApp.controller( "EditAboutMeController", function( $scope, UserService, $state, $stateParams, $rootScope ) {
    var flag = false;
    var aboutmeData = {};
    aboutmeData['responsetype'] = 'json';
    UserService.aboutMe( aboutmeData ).success( function( data ) {
        $('#aboutme').val(data.data.aboutme);
    } ).error( function ( data ) {
    } );
    $scope.updateAboutMe = function() {
        $('#aboutmeStatusCheck').removeClass('succesStatus').addClass('errorStatus');
        if( $('#aboutme').val() == '' ) {
            $('#aboutmeError').html('This field is required');
            $("label[for='aboutme']").css('color','red');
            flag = false;
        } else {
            flag = true;
        }
        $( '#aboutme' ).keyup(function (e) {
            var withoutSpace = $('#aboutme').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $('#aboutme').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $('#aboutmeError').html('This field is required');
                    $("label[for='aboutme']").css('color','red');
                } else {
                    $('#aboutmeError').html('');
                    $("label[for='aboutme']").removeAttr('style');
                    flag = true;
                }
            }
            if( $('#aboutme').val().length == 0 ) {
                $('#aboutmeError').html('This field is required');
                $("label[for='aboutme']").css('color','red');
                flag = false;
            } else {
                if( withoutSpaceLength == 0 ) {
                    $('#aboutmeError').html('This field is required');
                    $("label[for='aboutme']").css('color','red');
                } else {
                    $('#aboutmeError').html('');
                    $("label[for='aboutme']").removeAttr('style');
                    flag = true;
                }
            }
        } );
        if( flag == true ) {
            aboutmeData['aboutme'] = $scope.aboutme;
            $('.ui-loader').show();
            UserService.aboutMe( aboutmeData ).success( function( data ) {
                $('.ui-loader').hide();
                if( data.message == 'user not logged in' ) {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/affter_login"
                    });
                }
                if( data.status == 'success' ){
                    $('#aboutmeStatusCheck').removeClass('errorStatus').addClass('succesStatus');
                } else {
                    $('#aboutmeStatusCheck').removeClass('succesStatus').addClass('errorStatus');
                }
                $('#aboutmeStatusCheck').html(data.message);
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )