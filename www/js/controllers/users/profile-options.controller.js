//+++++++++++++++++++++++++++Profile options page controller+++++++++++++++++++++

userApp.controller( "ProfileOptionsCtrl", function( $scope, $http, $timeout, $state, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.user_id1 = userData.id;
    $scope.viewProfile = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/profile"+$scope.user_id1
        });
    };
    $scope.statusAndAvail = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/phonesettings"
        });
    };
    $scope.editProfileLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/editProfile"
        });
    };
})