//+++++++++++++++++++++++++++viewcredit page controller+++++++++++++++++++++
paymentApp.controller( "ViewCreditController", function( $scope, PaymentService, $state, $stateParams, $rootScope ) {
    var viewcreditData = {};
    viewcreditData['responsetype'] = 'json';
    PaymentService.viewCredit( viewcreditData ).success( function( data ) {
    	if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.creditInfo = data.message;
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );
} );