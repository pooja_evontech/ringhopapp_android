//+++++++++++++++++++++++++++addcredits page controller+++++++++++++++++++++
paymentApp.controller( "AddCreditsController", function( $scope, PaymentService, $state, $stateParams, $rootScope, $timeout, constantData, PaymentService ) {
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
    });
    $scope.credisList = constantData.creditsList();
    $scope.InApp_credits = [
        { credit:'10', price:'10'},
        { credit:'20', price:'20'},
        { credit:'40', price:'40'},
        { credit:'60', price:'60'}
    ];
    // $scope.Credit = 20;
    // $('#inapperrror').css ( {"display":"none !important" });
    //$scope.inAppShow =  false;
    $scope.inAppPurchaseErrorMsg = '';
    $scope.errorMsg = '';
    var user_detail = localStorage.getItem( "userDetail" );
    var userid = JSON.parse( user_detail ).data.id;
    var creditData = {};
    creditData['responsetype'] = 'json';
    creditData['userid'] = userid;
    PaymentService.addCredits( creditData, 'GET' ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.creditInfo = data.balance_msg;
        $scope.payment_method = data.paymentVia;
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );
    var productIds = ['com.ringhop.ringhop.10','com.ringhop.ringhop.20','com.ringhop.ringhop.40','com.ringhop.ringhop.60'];
    inAppPurchase
      .getProducts(productIds)
      .then(function (products) {
        console.log(products);
        $scope.products = products;
      })
      .catch(function (err) {
        console.log(err);
      });
    var nameOfPage = $stateParams.pageName;
    $scope.creditSubmit = function( credit, price ) {
        //$scope.inAppShow =  false;
        $scope.inAppPurchaseErrorMsg = '';
        $scope.errorMsg ='';
        console.log(credit, price );
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        if ($scope.payment_method == 'in_app') {
            $scope.credit_id1 = 'com.ringhop.ringhop.'+credit;
            console.log($scope.credit_id1);
            inAppPurchase
              .buy($scope.credit_id1)
              .then(function (data) {
                console.log(JSON.stringify(data));
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                // The consume() function should only be called after purchasing consumable products
                // otherwise, you should skip this step
                    var InAppcreditData = {};
                    InAppcreditData['responsetype'] = 'json';
                    InAppcreditData['credits'] =credit;
                    InAppcreditData['amount'] = price;
                    InAppcreditData['transactionId'] = data.signature;
                    InAppcreditData['receipt'] = data.receipt;
                    InAppcreditData['payment_via'] = deviceOS;
                    InAppcreditData['status'] = 'success';
                    InAppcreditData['message'] = 'transaction successful';
                    PaymentService.paymentViaInApp( InAppcreditData ).success( function( data ) {
                        $('.ui-loader').hide();
                        // console.log(JSON.stringify(data));
                        if (data.status == 'success') {
                            $scope.creditInfo = data.account_msg;
                            window.plugins.nativepagetransitions.slide( {
                                "direction": 'left',
                                "href" : '#/home/creditscomplete/'+$stateParams.originatrId
                            } );
                        }else{
                            $scope.errorMsg = data.message;
                        }
                    } ).error( function ( data ) {
                        // console.log(data);
                        $('.ui-loader').hide();
                        if( navigator.connection.type == Connection.NONE ) {
                            checkConnection();
                        }
                    } );
                return inAppPurchase.consume(data.type, data.receipt, data.signature);
              } )
              .then(function (data) {
                console.log(data);
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
              } )
              .catch(function (err) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                console.log(JSON.stringify(err));
                    var InAppcreditData = {};
                    InAppcreditData['responsetype'] = 'json';
                    InAppcreditData['credits'] =credit;
                    InAppcreditData['amount'] = price;
                    InAppcreditData['payment_via'] = deviceOS;
                    InAppcreditData['transactionId'] = '';
                    InAppcreditData['receipt'] = '';
                    InAppcreditData['status'] = 'failure';
                    InAppcreditData['message'] = err.text;
                    console.log(JSON.stringify(InAppcreditData));
                    PaymentService.paymentViaInApp( InAppcreditData ).success( function( data ) {
                        $('.ui-loader').hide();
                    console.log(JSON.stringify(data));
                    } ).error( function ( data ) {
                        console.log(data);
                        $('.ui-loader').hide();
                        if( navigator.connection.type == Connection.NONE ) {
                            checkConnection();
                        }
                    } );
                    $scope.inAppPurchaseErrorMsg = 'Issue communicating with app store for purchase. Please close app, relaunch and try again.'
                    if (!$scope.$$phase) $scope.$apply()
              } );

        }else{
            var creditData = {};
            creditData['responsetype'] = 'json';
            creditData['credits'] = credit;
            creditData['userid'] = userid;
            creditData['phoneip'] = deviceIP;
            $('.ui-loader').show();
            PaymentService.addCredits( creditData, 'POST' ).success( function( data ) {
                $('.ui-loader').hide();
                $('.phpdebugbar-openhandler-overlay').hide();
                if(data.status == 'error'){
                    $scope.errorMsg = data.message;
                }
                if( data.status == 'success' ) {
                    $scope.msg = data.message;
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : '#/home/creditscomplete/'+$stateParams.originatrId
                    } );
                }
            } ).error( function ( data ) {
                $('.ui-loader').hide();
                $('.phpdebugbar-openhandler-overlay').hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        }
        
    };
    // function alertDismissedForCredits(){
    //     console.log("alertdismissed");
    // }
});