//+++++++++++++++++++++++++++creditscomplete page controller+++++++++++++++++++++
paymentApp.controller( "CreditsCompleteController", function( $scope, $http, $state, $stateParams, $rootScope ) {
    $scope.successLink = function(){
        window.plugins.nativepagetransitions.slide( {
            "direction": 'right',
            "href" : '#/home/affter_login'
        } );
    };
} );