//+++++++++++++++++++++++++++videoProReject page controller+++++++++++++++++++++

callApp.controller( "VideoProRejectController", function( $scope, $http, $timeout, $state, $stateParams, $rootScope, $location, $interval, $filter, CallsService  ) {
    snd1.stop();
    snd1.release();
    receiver_tone = true;
    caller_tone = true;
    var user_detail = localStorage.getItem( "userDetail" );
    var userid = JSON.parse( user_detail ).data.id;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    $scope.pro_info = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.id2 = $stateParams.id_receiver1;
    console.log(userid,$scope.id2);
    if ($scope.pro_info != 2) {
        $('.ui-loader').show();
        $('#overlay').show();
        var videoSuggestPro = {};
        videoSuggestPro['responsetype'] = 'json';
        videoSuggestPro['receiver_id'] = $scope.id2;
        videoSuggestPro['originator_id'] = userid;
        console.log(JSON.stringify(videoSuggestPro));
        CallsService.showSuggestedPros( videoSuggestPro ).success( function( data ) {
            $('.ui-loader').hide();
            $('#overlay').hide();
            console.log(JSON.stringify(data));
            $scope.suggestedPros = data.data;
            if ($scope.suggestedPros.length > 0) {
                // $scope.suggest_div_show = true;
                $scope.suggest_headline_show = true;
                $scope.recentcont_Section = 'recentcont_Section';
            }
        } ).error( function ( data ) {
            $('.ui-loader').hide();
            $('#overlay').hide();
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    }
    
    // $scope.pro_info = JSON.parse( dashboard_data ).data.profile.pro;
    // console.log(localStorage.getItem('getSuggestedProData'));
    // var getDataSuggest = localStorage.getItem('getSuggestedProData').suggestPros;
    // $scope.suggestedPros = getDataSuggest;
    // console.log($scope.suggestedPros);
    
    var page_name = 'video';
    $scope.videoCallBusy = false;
    $scope.missedCall = true;
    $scope.videoCallInactive = false;
    $scope.diplayUsername = $stateParams.user_name;
    $scope.pro_name = $stateParams.user_name;
    $scope.previous_State = $stateParams.state_page;
    if ($stateParams.state_page == 'notAvailable') {
        $scope.videoCallBusy = true;
        $scope.missedCall = false;
    }else if ($stateParams.state_page == 'deactivated') {
        $scope.missedCall = false;
        $scope.videoCallBusy = false;
        $scope.videoCallInactive = true;
    }
    else{
        $scope.videoCallBusy = false;
        $scope.videoCallInactive = false;
        $scope.missedCall = true;
    }
    $scope.goBack = function(){
        if ($scope.previous_State == 'home.recentcalls' || $scope.previous_State == 'home.videocallMissed') {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/recentcalls"
            } );
         }else if($scope.previous_State == 'home.contacts'){
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/contacts"
            } );
         }else if($scope.previous_State == 'home.callerhistory'){
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/callerhistory"+$scope.id2
            } );
         }
         else{
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/messages"
            } );
         }
    };
    $scope.backButton2 = function(){
        window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/affter_login"
                } );
    };
    $scope.gotoProfile = function(profileId,profileName){
        window.plugins.nativepagetransitions.slide( {
            "direction": "left",
            "href" : "#/home/profile"+profileId+"/"+profileName
        } );
    };
    
});