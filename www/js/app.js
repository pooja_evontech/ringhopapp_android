//alert(1);
var mainApp = angular.module( "RinghopMain", ['ngCordova', 'ngTouch', 'ngAnimate', 'angularMoment', 'ui.bootstrap', 'ui.router', 'angular-carousel', 'AnonApp', 'UserApp', 'ContactApp', 'PaymentApp', 'MessageApp', 'ProApp', 'TermApp', 'CallApp'] );
// var BASE_URL = "https://dev.ringhop.com/mobile/users/";
// var SITE_URL = "https://dev.ringhop.com/";
// var IMAGE_URL = "https://dev.ringhop.com/images/";
var BASE_URL = "https://ringhop.com/mobile/users/";
var SITE_URL = "https://ringhop.com/";
var IMAGE_URL = "https://ringhop.com/images/";
var imageString ='';
var deviceOS = '';
var deviceVersion = '';
var uuid = ''
var deviceType = '';
var device_token = '';
var imageStringVerify = '';
var forgotFlag = 0;
var loadCount = 0;
var subscribeArr = [];
var snd;
var snd1 = '';
var caller_tone = false;
var receiver_tone = false;
var authToken;
var deviceIP = '';
// var callerRingtoneLoop = false;
var pushIDFlag = false;
var ringtoneSound;
var offsetTimezone = '';
var currentTime = '';
/*
# bootstrapping app with angular.bootstrap rather than ng-app="app name"...
# this will help in loading angular js pages after device ready..
*/

'use strict';

document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("resume", onResume, false);
document.addEventListener("pause", onPause, false);
function onPause(){
    console.log("enter-pause");
    cordova.plugins.notification.badge.set(0);
}
function onResume(){
    if (ringtoneSound) {
        snd.stop();
        if (deviceType == 'Android') {
            snd = new Media( 'file:///android_asset/www/sounds/ringhop_2.mp3', null , null , onStatus );
            snd.play({ numberOfLoops: 6 });
        }else{
           snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
           snd.play({ numberOfLoops: 5 });
           snd.setVolume('1.0');
        }
        // snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
        
    }
    var data = 'responsetype=json&device_token=' + device_token;
        $.ajax({ 
            url:BASE_URL+"updatenotificount",
            data : data,
            type:"post",
            success: function(){
            },
            error : function(result) {
                console.log("error>>" +result);
            }
        }); 
}

function onDeviceReady() {
   // alert('ready')
    document.addEventListener("backbutton", function(e){
        
    }, false);
    ringtoneSound = false;
    angular.bootstrap($('body'), ['RinghopMain']);
    window.analytics.startTrackerWithId('UA-64731097-2')
    window.analytics.trackView('ringhop')
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;
    device = window.device;
    deviceOS = device.platform;
    deviceVersion = device.version;
    uuid = device.uuid;
    deviceType = device.platform;
    offsetTimezone = new Date().toString().match(/\(([A-Za-z\s].*)\)/)[1];
    if (deviceType == 'Android') {
        snd = new Media( 'file:///android_asset/www/sounds/ringhop_2.mp3' , null , null , onStatus);
        snd1 = new Media( 'file:///android_asset/www/sounds/CallerRingtone.mp3', null , null , onStatus );
    }else{
        snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
        snd1 = new Media( 'sounds/CallerRingtone.caf', null , null , onStatus );
    }
    networkinterface.getIPAddress(function (ip) { 
        // console.log(ip); 
        deviceIP = ip;
    });
    universalLinks.subscribe('universalLink', function (eventData) {
      // do some work
      // console.log('Did launch application from the universalLink:');
      // console.log(JSON.stringify(eventData));
    });
    universalLinks.subscribe('universalRegister', function (eventData) {
       //console.log('Did launch application from the universalRegister:');
       console.log(JSON.stringify(eventData),localStorage.getItem( "userDetail" ));
       var rememberme_check = localStorage.getItem( "remember_appLaunch" );
        if( !rememberme_check ) {
            if (localStorage.getItem( "userDetail" )) {
                $.ajax({ 
                    url:BASE_URL+"logout",
                    data : {'responsetype':'json'},
                    type:"post",
                    success: function(data){
                        console.log('success>>',data)
                        localStorage.removeItem("userDetail");
                        localStorage.setItem('universal_userid',eventData.params.userid);
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/register_step2"
                        } );
                    },
                    error : function(result) {
                        console.log("error>>" +result);
                    }
                }); 
            }
        }
      if (!localStorage.getItem( "userDetail" )) {
        localStorage.setItem('universal_userid',eventData.params.userid);
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/register_step2"
        } );
      }
    });
    universalLinks.subscribe('universalAddcredits', function (eventData) {
      // console.log('Did launch application from the universalAddcredits:');
       console.log(JSON.stringify(eventData));
      if (localStorage.getItem( "userDetail" )) {
        // var user_detail = localStorage.getItem( "userDetail" );
        var universalLink_userid = eventData.params.userid;
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/addcredits/"+universalLink_userid
        });
      }else{
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/login"
        });
      }
    });
     universalLinks.subscribe('universalReply', function (eventData) {
      // do some work
      // console.log('Did launch application from the universalReply:');
       console.log(JSON.stringify(eventData,eventData.params.uid));
      var universalLink_replyId = eventData.params.uid;
          if (localStorage.getItem( "userDetail" )) {
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/reply"+universalLink_replyId
            });
        }else{
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }
    });
    universalLinks.subscribe('universalMessages', function (eventData) {
      // console.log('Did launch application from the universalMessages:');
       console.log(JSON.stringify(eventData));
      if (localStorage.getItem( "userDetail" )) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/messages"
        });
      }else{
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/login"
        });
      }
    });
    universalLinks.subscribe('universalContact', function (eventData) {
        console.log(JSON.stringify(eventData));
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/contactus"
        });
    });
    var push = PushNotification.init({
        android: {
            senderID: "788156349635"
        },
        ios: {
            alert: "true",
            badge: "true",
            clearBadge: "true",
            sound: "true"
        },
        windows: {}
    });

    push.on('registration', function(data) {
        // console.log(data,data.registrationId);
        device_token = data.registrationId;
        
    });

    push.on('notification', function(data) {
        console.log(JSON.stringify(data));
        // console.log(localStorage.getItem("pushID"),data.additionalData.randomNo)
        if(!localStorage.getItem("pushID") || localStorage.getItem("pushID") !== data.additionalData.randomNo){
            // console.log('processing..');
            localStorage.setItem("event_name",data.additionalData.event);
            localStorage.setItem("pushID",data.additionalData.randomNo)
            var user_detail_check = localStorage.getItem( "userDetail" );
            if( user_detail_check ) {
                if ( data.additionalData.session_id ){
                    var callerdata = JSON.stringify({'u_name': data.additionalData.originator_displayname, 'sessionId': data.additionalData.session_id , 'sessionToken': data.additionalData.video_token, 'image':data.additionalData.profile_image, 'receiverid': data.additionalData.receiver_id, 'userId':data.additionalData.originator_id, 'CallId':data.additionalData.call_id,
                        'video_call_rate':data.additionalData.video_call_rate,'receiver_balance':data.additionalData.receiver_balance,'bal_req':data.additionalData.bal_req})
                    localStorage.setItem("caller_Data",callerdata);
                    window.plugins.nativepagetransitions.slide( {
                        "href" : "#/home/videocallAccept"
                    } ); 
                }
                else if(data.additionalData.event == "user_rejected") {
                    var missedUser_name = JSON.stringify(data.additionalData);
                    // console.log(missedUser_name);
                    var missedUser_name1 = JSON.parse( missedUser_name ).originator_displayname;
                    var user_detail = localStorage.getItem( "userDetail" );
                    var userid = JSON.parse( user_detail ).data.id;
                    var Id = data.additionalData.receiver_id;
                    var Id1 = data.additionalData.originator_id;
                    VideoPlugin.endCalling(" ");
                    if( userid == Id){
                            window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/videocallMissed/"+missedUser_name1+'/'+Id1
                        } ); 
                     }
                    else{
                            window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/recentcalls"
                        } );
                    } 
                }
                else if (data.additionalData.event == "pro_rejected") {
                    VideoPlugin.endCalling("proRejectsCall");
                    snd1.stop();
                    snd1.release();
                }
                // else if(data.additionalData.event == "ended") {
                //     var dashboard_data = localStorage.getItem( "dashboard_data" );
                //     var proData = JSON.parse( dashboard_data ).data.profile.pro;
                //     var user_detail = localStorage.getItem( "userDetail" );
                //     var userid = JSON.parse( user_detail ).data.id;
                //     var Id = data.additionalData.receiver_id;
                //     var Id1 = data.additionalData.originator_id;
                //     if (proData == 2) {
                //         var costOfCall1 = data.additionalData.call_rev_share;
                //     }else{
                //         var costOfCall1 = data.additionalData.call_cost;
                //     }
                //     var durationOfCall1 = data.additionalData.call_duration;
                //     var showNamedata1 = data.additionalData.originator_displayname;
                //     var IdReceiver1 = data.additionalData.originator_id;
                //     var identity = proData;
                //     var pg  = '';
                //        window.plugins.nativepagetransitions.slide( {
                //             "direction": 'right',
                //             "href" : '#/home/videocallEnd/'+costOfCall1+'/'+durationOfCall1+'/'+showNamedata1+'/'+IdReceiver1+'/'+identity+'/'+pg
                //         } );
                // }
                else if(data.additionalData.msg == "tip received successfully"){
                    // console.log("received tip");
                    var tip_amt = data.additionalData.tip_amount;
                    VideoPlugin.tipReceived(tip_amt);
                }else if (data.additionalData.event == "in_progress") {
                    VideoPlugin.showLowBalanceWarning("");
                }else if (data.additionalData.event == "add_credit") {
                    // console.log("credit done");
                    VideoPlugin.receivedResponseFromAPI("credit","success","0");
                }
                else{
                    if(data.additionalData.foreground === false){
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/messages"
                        } );
                    }
                }
            }
          if ( data.sound )
            {   
                // console.log(data.sound,deviceType);
                // console.log('file:///android_asset/www/sounds/'+data.sound);
                snd.stop();
                if (deviceType == 'Android') {
                    snd = new Media( 'file:///android_asset/www/sounds/'+data.sound , null , null , onStatus );
                    snd.play();
                    // console.log(snd);
                    // if (data.sound == 'ringhop_2.mp3') {
                    //   console.log("androidsound");
                    //   snd.play();
                    //   // snd.setVolume('1.0');  
                    // }else{
                    //   snd.play();
                    //   // snd.setVolume('1.0');
                    // }
                }else{
                    snd = new Media( 'sounds/'+data.sound , null , null , onStatus );
                    if (data.sound == 'ringhop_2.caf') {
                        console.log('first');
                      snd.play({ numberOfLoops: 9 });
                      snd.setVolume('1.0');  
                    }else{
                      snd.play();
                      snd.setVolume('1.0');
                    }
                }
            }
        } 
        

        if ( data.count )
        {   console.log(data.count)
            cordova.plugins.notification.badge.set(data.count,successHandler);
            // push.setApplicationIconBadgeNumber(successHandler, errorHandler, data.count);
        }

    });

    push.on('error', function(e) {
    });
}

    function onStatus(status) {
        // console.log(status);
        if (snd.src == 'file:///android_asset/www/sounds/ringhop_2.mp3') {
            // console.log("inside>>" +status);
            if( status == Media.MEDIA_STOPPED ) {
                //if(loopMediaflag === true){
                    // snd.play();
                //}
                // console.log("tone>>" +receiver_tone);
                 if (receiver_tone) {
                    snd.stop();
                    snd.release();
                }else{
                    snd.play();
                }
            }
            
        }

    }
    // if(snd){
    //     snd.stop();
    // }


    function successHandler (success) {
        console.log("data>>" +success)
        // var data = 'responsetype=json&device_token=' + device_token;
        // $.ajax({ 
        //     url:BASE_URL+"updatenotificount",
        //     data : data,
        //     type:"post",
        //     success: function(){
        //     },
        //     error : function(result) {
        //         console.log("error>>" +result);
        //     }
        // }); 
    }
    function errorHandler(error){
        console.log("error>>" +error);
    }

function checkConnection() {
    console.log("checkConnection");
    var networkState = navigator.connection.type;
    loadCount = loadCount+1;
    var states = {};
    states[Connection.NONE] = 'No network connection';
        navigator.notification.alert(
        states[networkState],  // message
        alertDismissed,         // callback
        'RingHop',            // title
        'OK'                  // buttonName
    );
    // }
    
}
// function alertDismissed(){
//     // console.log("dismiss");
// }
function back( direction, href ) {
    window.plugins.nativepagetransitions.slide({
        "direction": direction,
        "href" : href
    });
}

//+++++++++++++++++++++++++++App sub modules+++++++++++++++++++++
var anonApp = angular.module('AnonApp', []);
var userApp = angular.module('UserApp', []);
var contactApp = angular.module('ContactApp', []);
var messageApp = angular.module('MessageApp', []);
var paymentApp = angular.module('PaymentApp', []);
var proApp = angular.module('ProApp', []);
var termApp = angular.module('TermApp', []);
var callApp = angular.module('CallApp', []);

//+++++++++++++++++++++++++++App run settings+++++++++++++++++++++

mainApp.run( function( $rootScope, $window, moment, $state, $location, $filter, $http, CallsService, AuthService, $cordovaAppVersion ) {
    // $('#updateVersionPopup').hide();
   // alert('run')
    var rememberme_check = localStorage.getItem( "remember_appLaunch" );
    if( !rememberme_check ) {
        if (localStorage.getItem( "userDetail" )) {
            AuthService.logout().success( function( data ){
                localStorage.removeItem("userDetail");
            });
        }
    }

    CallsService.generateSessionId().success( function( data ) {
        var secondsInDay = 86400000;
        // 8640000 Credentials
        var apiKey = data[0].partner_id;
        console.log(apiKey);
        var secret = 'cf6f43cc8bef8070ec86be37183aa550e6981a74';
        var id_session = data[0].session_id;
        localStorage.setItem("dynamic_sessionID" , id_session);
        // Token Params
        var timeNow = Math.floor(Date.now()/1000);
        var expire = timeNow+secondsInDay;
        var role = "publisher";
        var data = "Second";
        TB.setLogLevel(TB.DEBUG);
        // Calculation
        data = escape(data);
        var rand = Math.floor(Math.random()*999999);
        var dataString =  "session_id="+id_session+"&create_time="+timeNow+"&expire_time="+expire+"&role="+role+"&connection_data="+data+"&nonce="+rand;
        // Encryption
        var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, secret);
        hmac.update( dataString );
        hash = hmac.finalize();
        preCoded = "partner_id="+apiKey+"&sig="+hash+":"+dataString;
        var id_token = "T1=="+$.base64.encode( preCoded )
        localStorage.setItem("dynamic_token" , id_token);
    } ).error(function(error){
    } );
    // ___________________________******************____________________________
    // if (!sessionStorage.getItem('timezone')) {
    //   var tz = jstz.determine() || 'UTC';
    //   sessionStorage.setItem('timezone', tz.name());
    // }
    // var currTz = sessionStorage.getItem('timezone');
    // console.log(currTz);
// ___________________________******************____________________________
    var networkState = navigator.connection.type;
    $cordovaAppVersion.getVersionNumber().then(function (version) {
        console.log(version);
        localStorage.setItem( "Latest_App_Version", version );
        AuthService.getAppConstants(version).success( function( data ){
            console.log(JSON.stringify(data));
            localStorage.setItem( "constantDataStore", JSON.stringify(data) );
            localStorage.setItem( "data_Store",JSON.stringify(data.data));
            if (data.updateVersion == 'yes') {
                // $('#updateVersionPopup').show();
                navigator.notification.confirm(
                    'New version update is available.Click OK to update the version.',  // message
                    onConfirm,
                    'UPDATE'
                );
            }
            $rootScope.appData = data.data.ringhop_constants;
            $rootScope.appDataVersion = data.updateVersion;
            console.log($rootScope.appData);
        }).error(function(error){
           localStorage.setItem( "constantDataStore", JSON.stringify({data:{ringhop_constants:{}}}) );  
           if(navigator.connection.type == Connection.NONE) {
              checkConnection();
            }
        } );
    });
    function onConfirm( button ) {
            if( button == 1 ) {
            // console.log("update");
            window.open($rootScope.appData.iphone_app_url,'_blank');
        }
    }
    currentTime = moment().format('YYYY-MM-DD');
    $rootScope.previousState;
    $rootScope.currentState;
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
    });
    document.addEventListener("backbutton", function(e){
        console.log($rootScope.currentState ,$rootScope.previousState );
        if($rootScope.currentState == "home.affter_login"){
            // console.log("open confirm box");
            navigator.notification.confirm(
                'Are you sure you want to logout?',  // message
                onConfirmlogout,
                'LOGOUT'
            );
        }else {
            if (($rootScope.currentState == "home.login" && $rootScope.previousState == "home.affter_login")|| ($rootScope.currentState == "home.login" && $rootScope.previousState == "home.settings")) {
                // console.log("goto home");
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home"
                } );
            }else if ($rootScope.currentState == "home.recentcalls") {
                // console.log("recentcalls");
                window.plugins.nativepagetransitions.slide( {
                   "href" : "#/home/affter_login"
                } );
            }
            else if (($rootScope.currentState == "home.videoCalling" || $rootScope.currentState == "home.videocallAccept" || $rootScope.currentState == "home.videocallEnd" || $rootScope.currentState == "home.videoProReject"
            || $rootScope.currentState == "home.videocallMissed" || $rootScope.currentState == "home")){
                // console.log("video pages");
                e.preventDefault();
            }else{
               // console.log("other pages");
               // window.history.back(); 
               window.history.go(-1);
               // alert(window.history.back());
               // alert(window.history.go(-1));
            }
        }
        
    }, false);

    function onConfirmlogout(button){
        if (button == 1) {
            // console.log("correct confirm box");
            var getLogoutData = {};
            getLogoutData['responsetype'] = "json";
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            AuthService.logout( getLogoutData ).success( function( data ) {
                //console.log(data);
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( data.status == 'success' ) {
                    localStorage.removeItem("userDetail");
                    if(localStorage.getItem("rememberme_flag") == 'false'){
                        localStorage.removeItem("rememberme_data");
                    }
                    localStorage.removeItem("remember_appLaunch");
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/login"
                    });

                } else {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/login"
                    });
                }
            } ).error( function ( err ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    }
    $rootScope.IMAGE_URL = IMAGE_URL;
    $.cloudinary.config( { cloud_name: 'nobetek-llc', api_key: '247749274532722' } );
    $rootScope.date = new Date();
    $rootScope.forward = function( direction, href ) {
        window.plugins.nativepagetransitions.slide( {
            "direction": direction,
            "href" : href
        } );
    };
    $rootScope.convertTZ = function(old_date, zone) {
        // console.log(old_date,zone);
        var d = old_date;
        $rootScope.tzArray = {
            'MIT':-39600,
            'HAST':-36000,
            'AKST':-32400,
            'AKDT':-28800,
            'PST':-28800,
            'PDT':-25200,
            'MST':-25200,
            'MDT':-21600,
            'CST':-21600,
            'CDT':-18000,
            'EST':-14400,
            'EDT':-14400,
            'PRT':-14400,
            'CNT':-12600,
            'AGT':-10800,
            'BET':-10800,
            'CAT':-3600,
            'UTC':0,
            'GMT':0,
            'WET':0,
            'WEST':3600,
            'CET':3600,
            'CEST':7200,
            'EET':7200,
            'EEST':10800,
            'ART':7200,
            'EAT':10800,
            'MET':12600,
            'NET':14400,
            'PLT':18000,
            'IST':19800,
            'BST':21600,
            'ICT':25200,
            'CTT':28800,
            'SGT':28800,
            'AWST':28800,
            'JST':32400,
            'ACST':34200,
            'AEST':36000,
            'SST':39600,
            'NZST':43200,
            'NZDT':46800
        };
        angular.forEach($rootScope.tzArray, function(filterObj , filterKey) {
            if(filterKey == zone) {
                var d1 = d.split(' ')[0].split('-');
                var d2 = d.split(' ')[1].split(':');
                var a = new Date(d1[0],d1[1]-1,d1[2],d2[0],d2[1],d2[2],0);
                var changedTime = a.getTime()+(filterObj*1000);
                $rootScope.newTime = $filter('date')(changedTime,'yyyy-MM-dd HH:mm:ss');
            }
        } );
        return $rootScope.newTime;
    };
    console.log($rootScope.newTime);
    
} );