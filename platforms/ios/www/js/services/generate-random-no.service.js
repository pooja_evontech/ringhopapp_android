mainApp.factory('GenerateRandomNo', function() {
    var result = '';
    return {
        randomString: function(length, chars) {
            result = '';
              for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
          return result;
        }
    };
});