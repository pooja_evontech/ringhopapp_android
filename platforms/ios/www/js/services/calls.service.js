callApp.factory('CallsService', function( $http, CONSTANTS ) {
  return {
    callerHistory: function( getCallerHistoryData ) {
      return $http.post( CONSTANTS.BASE_URL+"callerhistory",JSON.stringify( getCallerHistoryData ) );
    },
    getRecentCalls: function( getCallData ) {
      return $http.post( CONSTANTS.BASE_URL+"recentcalls", JSON.stringify( getCallData ) );
    },
    setVideoCallDetails: function( paramData ) {
      return $http.post( CONSTANTS.BASE_URL+"setvideocalldetails",JSON.stringify( paramData ) );
    },
    generateSessionId: function() {
      return $http({
          url : 'https://api.opentok.com/session/create',
          method : 'POST',
          async : true,
          crossDomain : true,
          headers : { 'Accept' : 'application/json' , 'x-tb-partner-auth': '45598312:cf6f43cc8bef8070ec86be37183aa550e6981a74', 'p2p.preference':'enabled'},
      });
    },
    sendVideoNotification: function( notificationData ) {
      return $http.post( CONSTANTS.BASE_URL+"sendvideonoti",JSON.stringify( notificationData ) );
    },
    sendVideoErrors: function( errorCallLog ) {
      return $http.post( CONSTANTS.BASE_URL+"videoerrorlog",JSON.stringify( errorCallLog ) );
    }
  };
} )