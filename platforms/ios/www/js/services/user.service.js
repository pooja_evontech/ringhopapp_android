userApp.factory('UserService', function( $http, CONSTANTS ) {
  return {
    dashboard: function( getDashboardData ) {
      return $http.post( CONSTANTS.BASE_URL+"dashboard", JSON.stringify(getDashboardData) );
    },
    getUserDetail: function( userData ) {
      return $http.post( CONSTANTS.BASE_URL+"userdetails", JSON.stringify(userData) );
    },
    savePhoneDetails: function( getDeviceData ) {
      return $http.post( CONSTANTS.BASE_URL+"userphonedetails",JSON.stringify(getDeviceData) );
    },
    phoneSettings: function( statusData ) {
      return $http.post( CONSTANTS.BASE_URL+"phonesettings", JSON.stringify( statusData ) );
    },
    deactivate: function( getdeactivationData ) {
      return $http.post( CONSTANTS.BASE_URL+"deactivate",JSON.stringify( getdeactivationData ) );
    },
    aboutMe: function( aboutmeData ) {
      return $http.post( CONSTANTS.BASE_URL+"editaboutme",JSON.stringify( aboutmeData ) );
    },
    location: function( locationSetData ) {
      return $http.post( CONSTANTS.BASE_URL+"editlocation",JSON.stringify( locationSetData ) );
    },
    editProfile: function( editProfileData ) {
      return $http.post( CONSTANTS.BASE_URL+"editprofile",JSON.stringify( editProfileData ) );
    },
    editProfileSettings: function( proDisplayData ) {
      return $http.post( CONSTANTS.BASE_URL+"editprofilesettings",JSON.stringify( proDisplayData ) );
    },
    saveProfilePic: function( updateProfileData ) {
      return $http.post( CONSTANTS.BASE_URL+"uploadresized",JSON.stringify( updateProfileData ) );
    },
    deleteProfilePic: function( deletePhotoData ) {
      return $http.post( CONSTANTS.BASE_URL+"removeprofilephoto",JSON.stringify( deletePhotoData ) );
    },
    socialLinks: function( socialData ) {
      return $http.post( CONSTANTS.BASE_URL+"sociallinks",JSON.stringify( socialData ) );
    },
    deleteSocialLink: function( deleteSocialData ) {
      return $http.post( CONSTANTS.BASE_URL+"deletesociallink",JSON.stringify( deleteSocialData ) );
    },
    myLibrary: function( libData ) {
      return $http.post( CONSTANTS.BASE_URL+"mylibrary",JSON.stringify( libData ) );
    },
    notificationSettings: function( notifyData ) {
      return $http.post( CONSTANTS.BASE_URL+"notificationsettings",JSON.stringify( notifyData ) );
    },
    updateNotiSetting: function( notifyData ) {
      return $http.post( CONSTANTS.BASE_URL+"updatenotificationsettings",JSON.stringify( notifyData ) );
    },
    cancelProApplication: function( proApplicationDta ) {
      return $http.post( CONSTANTS.BASE_URL+"cancel-request",JSON.stringify( proApplicationDta ) );
    },
    socialProfile: function( profileData ) {
      return $http.post( CONSTANTS.BASE_URL+"socialprofile",JSON.stringify( profileData ) );
    }, 
    viewProfile: function( dataofurl ) {
      return $http.get( CONSTANTS.SITE_URL+dataofurl );
    }
  };
})