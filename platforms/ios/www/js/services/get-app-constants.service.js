mainApp.factory('getAppConstants', function ( $http ) {
    var factory = {            
        getData: function () {
            var serverData = localStorage.getItem( "constantDataStore" );
            return JSON.parse(serverData).data.ringhop_constants;
        }
    }       
    return factory; 
});

mainApp.factory('constantData', function ( $http, getAppConstants) {
    var promise;
    var factory = {            
        getCountryData: function () {
            promise = getAppConstants.getData().country_list;
            return promise;
        },
        getOtherCountryData: function () {
            promise = getAppConstants.getData().other_country_list;
            return promise;
        },
        getCountryAllListData: function () {
            promise = getAppConstants.getData().country_all_list;
            return promise;
        },
        getStateData: function () {
            promise = getAppConstants.getData().state_list;
            return promise;
        },
        getCanadianStateData: function () {
            promise = getAppConstants.getData().canada_state_list;
            return promise;
        },
        getNotificationData: function () {
            promise = getAppConstants.getData().notifications_frequency;
            return promise;
        },
        getSetRatesData: function () {
            promise = {};
            var call_rates = getAppConstants.getData().call_rates;
            var sms_rates = getAppConstants.getData().sms_rates;
            promise.call_rates = call_rates;
            promise.sms_rates = sms_rates;
            return promise;
        },
        socialLinks: function () {
            promise = getAppConstants.getData().social_links;
            return promise;
        },
        creditsList: function () {
            promise = getAppConstants.getData().credits_list;
            return promise;
        },
        getshoutMediaData: function(){
            promise = getAppConstants.getData().shout_rates;
            return promise;

        },
        getvideoCallRatesData: function(){
            promise = getAppConstants.getData().video_call_rates;
            return promise;

        },
        getvideoCallMinAmountData: function(){
            promise = getAppConstants.getData().video_call_min_amounts;
            return promise;

        },
        getshoutRecepientsList: function(){
            promise = {};
            var sent_to_list = getAppConstants.getData().sent_to_list;
            promise.sent_to_list = sent_to_list;
            return promise;

        }
    };       
    return factory; 
});


