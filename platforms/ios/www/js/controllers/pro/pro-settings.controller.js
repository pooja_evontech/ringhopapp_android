//+++++++++++++++++++++++++++prosettings page controller+++++++++++++++++++++

proApp.controller( "ProSettingsController", function( $scope, ProService, $timeout, $state, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.greetingshow = false;
    $scope.loginid = userData.id;
    $scope.trialSetting = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/trialsettings"
        } );
    };
    $scope.viewProfile = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/profile"+$scope.proSetData.id+'/'+$scope.proSetData.displayname
        } );
    };
    $scope.setCall = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/setrates"
        });
    };
    $scope.viewEarning = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/stats"
        });
    };
    $scope.forwardSetting = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/forwardingsettings"
        });
    };
    $scope.voicegreeting = function(){
        $state.transitionTo( 'home.mygreeting',{ ringhop_number:$scope.proSetData.ringhop_number } );
    };
    var proSettingData = {};
    proSettingData['responsetype'] = 'json';
    ProService.proSettings( proSettingData ).success( function( data ) {
        $scope.proSetData = data.data;
        if ($scope.proSetData.ringhop_number !== null) {
            $scope.proSetData.ringhop_number = "+"+$scope.proSetData.ringhop_number.substr(0,1)+" ("+$scope.proSetData.ringhop_number.substr(1, 3)+") "+$scope.proSetData.ringhop_number.substr(4, 3)+"-"+$scope.proSetData.ringhop_number.substr(7, 4);
        }else{
            $scope.proSetData.ringhop_number = "";
        }
        $scope.greeting = data.data.name_greeting;
        if($scope.greeting != null){
            $('.voicelist').css({"background":"#fff"});
            $scope.greetingshow = true;
        }else{
           $scope.greetingshow = false; 
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    
} )