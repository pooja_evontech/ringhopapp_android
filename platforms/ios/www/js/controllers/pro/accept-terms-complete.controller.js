//+++++++++++++++++++++++++++accepttermsComplete page controller+++++++++++++++++++++

proApp.controller( "AcceptTermsCompleteController", function( $scope, $http, $timeout, $state, $rootScope ) {
    $scope.completeLink = function( $event ) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/prodashboard"
        });
    };
} )
