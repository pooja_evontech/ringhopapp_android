//+++++++++++++++++++++++++++trialsettings page controller+++++++++++++++++++++

proApp.controller( "TrialSettingsCtrl", function( $rootScope, $scope, ProService, $state ) {
    var settingData = {};
    settingData['responsetype'] = 'json';
    ProService.trialSetting( settingData, 'GET' ).success( function( data ) {
        console.log(JSON.stringify(data));
        $scope.proSetData = data.data;
        if( $scope.proSetData.do_trials == 1 ) {
            $scope.trial_calls = true;
        } else {
            $scope.trial_calls = false;
        }
        if( $scope.proSetData.do_smstrials == 1 ) {
            $scope.trial_sms = true;
        } else {
            $scope.trial_sms = false;
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.acceptTrial = function () {
        $( '#updateCheck' ).addClass('errorStatus').removeClass('succesStatus');
        var acceptData = {};
        if( $scope.trial_calls == true ){
            acceptData['trial_calls'] = 1;
        }
        if( $scope.trial_sms == true ) {
            acceptData['trial_sms'] = 1;
        }
        acceptData['responsetype'] = 'json';
        $('.ui-loader').show();

        ProService.trialSetting( acceptData, 'POST' ).success( function( data ) {
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            if( data.status == 'success' ){
                $( '#updateCheck' ).removeClass('errorStatus').addClass('succesStatus');
            } else {
                $( '#updateCheck' ).addClass('errorStatus').removeClass('succesStatus');
            }
            $scope.status = data.message;
        } ).error( function ( data ) {
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };

} )