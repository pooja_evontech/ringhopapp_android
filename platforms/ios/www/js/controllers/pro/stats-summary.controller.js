//+++++++++++++++++++++++++++statssummary page controller+++++++++++++++++++++

proApp.controller( "StatsSummaryController", function( $rootScope, $scope, ProService, $state, $filter ) {
    $scope.noMessageDiv = false;
    $scope.date = new Date();
    var statsSummary = {};
    statsSummary['responsetype'] = 'json';
    ProService.statsSummary( statsSummary ).success( function( data ) {
        $scope.statssummhistory = data.data.periodsummary;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

} )