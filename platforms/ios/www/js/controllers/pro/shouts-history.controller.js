//+++++++++++++++++++++++++++shoutsHistory page controller+++++++++++++++++++++

proApp.controller( "ShoutsHistoryController", function( $scope, ProService, $state, $stateParams, $rootScope ) {
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.limit = 4;
    var pre_count = 1;
    var next_count = 1;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.isPro = false;
    if( proData != 2 ) {
        //for pro users
        $scope.isPro = false;
    } else {
        $scope.isPro = true;
    }
    if( $scope.isPro == true ) {
        var shouthistoryURL = BASE_URL+"pro/stats_shouts";
    } else {
        var shouthistoryURL = BASE_URL+"userstats_shouts";
    }
    var shouthistorydata = {};
    shouthistorydata['responsetype'] = 'json';
    shouthistorydata['page'] = 1;
    ProService.shoutHistory( shouthistorydata, shouthistoryURL ).success( function( data ) {
        $scope.statsshouthistory = data.data.photologs;
        $scope.limit = data.data.per_page;
        $scope.totalShouts = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalShouts <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalShouts == 0 ) {
                $scope.noMessageDiv = true;
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.prevBtn = function () {
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            shouthistorydata['page'] = pre_count;
            ProService.shoutHistory( shouthistorydata, shouthistoryURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.statsshouthistory = data.data.photologs;
                $scope.totalShouts = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalShouts > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            shouthistorydata['page'] = next_count;
            ProService.shoutHistory( shouthistorydata, shouthistoryURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentChats = '';
                $scope.limit = data.data.per_page;
                $scope.statsshouthistory = data.data.photologs;
                $scope.totalShouts = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalShouts > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
} )