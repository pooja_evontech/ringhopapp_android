//+++++++++++++++++++++++++++verifyidentity page controller+++++++++++++++++++++

proApp.controller( "VerifyIdentityController", function( $scope, ProService, $timeout, $state, $rootScope ) {
    $scope.div_visible = true;
    $scope.takePhotoOpt = false;
    imageStringVerify = '';
    $scope.uploadVerifyPhoto = function () {
        $scope.takePhotoOpt = true;
    };

    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    function onPhotoDataSuccess(imageData) {
        $('#uploadVerify_id').html('');
        var profileImage = document.getElementById('verify_image');
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageStringVerify = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function () {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:1000, targetHeight: 1000,
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL
        });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:1000, targetHeight: 1000,
        destinationType: destinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY });
    };

    // Called if something bad happens.
    function onFail(message) {
        //alert('Failed because: ' + message);
        $scope.takePhotoOpt = false;
        $scope.div_visible = true;
    }

    //when success upload
    $scope.saveVerifyPic = function(){
        $('#uploadVerify_id').html('');
        //console.log(imageStringVerify);
        if(imageStringVerify !== '' ) {
            var updateVerifyData = {};
            updateVerifyData['responsetype'] = "json";
            updateVerifyData['image'] = imageStringVerify;
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            ProService.verifyIdentity( updateVerifyData ).success( function( data ) {
                if( data.status == 'success' ) {
                    $('#uploadVerify_id').removeClass('errorStatus').addClass('succesStatus');
                } else {
                    $('#uploadVerify_id').removeClass('succesStatus').addClass('errorStatus');
                }
                $('#uploadVerify_id').html(data.message);
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/prodashboard"
                });
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $('#uploadVerify_id').html('Please try again..');
        }
        
    };

    $scope.cancelUpload = function ( $event) {
        $('#uploadVerify_id').html('');
        $scope.div_visible = true;
    };

})