//+++++++++++++++++++++++++++Send Photo Shout page controller+++++++++++++++++++++

proApp.controller("SendPhotoShoutController", function( $scope, ProService, $timeout, $state, $rootScope, constantData) {
    $scope.takePhotoOpt = false;
    var flag = false;
    var checkflag = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.div_visible = true;
    $scope.div_visible1 = false;
    $(".loaderId").hide();
    $scope.shoutRecepientsList = [{ id:'', value:'Choose recipients'}];
    angular.forEach(constantData.getshoutRecepientsList().sent_to_list, function(value, key){
        var item = { id:key, value:value };
        $scope.shoutRecepientsList.push(item);
    });
    
    $scope.uploadshoutPhoto = function () {
        $scope.takePhotoOpt = true;
    };

    $scope.cancelPhoto = function () {
        $('#sendShout_id').html('');
        $('#sendShout_id' ).removeClass( 'errorStatus' );
        $scope.takePhotoOpt = false;
        $scope.div_visible = true;
        $scope.div_visible1 = false;
    };

    function onPhotoDataSuccess( imageData ) {
        $('#sendShout_id').html('');
        $('#sendShout_id' ).removeClass( 'errorStatus' );
        var profileImage = document.getElementById( 'shout_image' );
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageString = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function ( $event ) {
        $('#sendShout_id').html('');
        $('#sendShout_id' ).removeClass( 'errorStatus' );
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        $scope.div_visible1 = true;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:500,
            targetHeight: 500, correctOrientation: true,
        destinationType: Camera.DestinationType.DATA_URL });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $('#sendShout_id').html('');
        $('#sendShout_id' ).removeClass( 'errorStatus' );
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        $scope.div_visible1 = true;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:1000,
            targetHeight: 1000, destinationType: destinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY });
    };

    // Called if something bad happens.
    function onFail(message) {
        $state.transitionTo('home.PhotoShout', {}, {reload: true});
        // $scope.takePhotoOpt = false;
        // $scope.div_visible = true;
        // $scope.div_visible1 = false;
    }

    $('#media_price').change(function(){
        if (imageString == '') {
           $('#sendShout_id').html('Please try again..');
           $('#sendShout_id' ).addClass( 'errorStatus' ); 
        }else if($( "#media_price" ).val() == '' || $( "#media_price" ).val() > 99 || $( "#media_price" ).val() < 0){
          $(".okshoutspan1" ).css('display','none');  
          $(".numberedprice" ).css('display','block');
          $("#media_price" ).css('border','1px solid red'); 
        }else {
          $(".numberedprice" ).css('background','none');
          $(".numberedprice" ).css('display','none');
          $(".okshoutspan1" ).css('display','block'); 
          $("#media_price" ).css('border','none'); 
          $("#media_error" ).html('');
        }
        
    });
    $scope.changeRecepient = function(receipientType){
        if (imageString == '') {
           $('#sendShout_id').html('Please try again..');
           $('#sendShout_id' ).addClass( 'errorStatus' ); 
        }else if( $scope.sent_to_list == ''){
          $(".okshoutspan2" ).css('display','none');  
          $(".numberedprice1" ).css('display','block');
          $("#sent_to_list" ).css('border','1px solid red');
        }else{
          $(".numberedprice1" ).css('background','none');
          $(".numberedprice1" ).css('display','none');
          $(".okshoutspan2" ).css('display','block'); 
          $("#sent_to_list" ).css('border','none'); 
        }
    };
    $scope.sendShout = function() {
        if($( "#media_price" ).val() == '' || $( "#media_price" ).val() > 99 || $( "#media_price" ).val() < 0){
            flag = false;
            $("#media_price" ).css('border','1px solid red');
            $("#media_error" ).html('Please enter media price between 0-99.');
        }else{
           $("#media_price" ).css('border','none'); 
           $("#media_error" ).html('');
        }
        
        if( $scope.sent_to_list == '' ) {
             flag = false;
            $("#sent_to_list" ).css('border','1px solid red');  
        }
        if( $( "#media_price" ).val() != '' && $( "#media_price" ).val() <= 99 && $( "#media_price" ).val() >= 0 && $scope.sent_to_list != '' ){
            flag = true;
        }
        if( flag == true ) {
            submitShout();
        }
    };
    function submitShout(){
        $('#sendShout_id').html('');
        $('#sendShout_id' ).removeClass( 'errorStatus' );
        var user_detail = localStorage.getItem("userDetail");
        var userData = JSON.parse(user_detail).data;
        if($scope.tagline === undefined){
            $scope.tagline = "";
        }
        $scope.div_visible = false;
        $('#checkbtn').show();
        $('#checkform').hide();
        $('#checkform1').hide();
        $('#checkform2').hide();
        $('#checkform3').hide();
        var data = 'responsetype=json&image=' + imageString + '&media_price=' + $scope.media_price + '&sent_to_list='+ $scope.sent_to_list + '&tagline='+ $scope.tagline ;
        if (imageString !== '') {
            $('.phpdebugbar-openhandler-overlay').show();
            $("#checkLoader").show();
            var uploadShoutData = {};
            uploadShoutData['responsetype'] = 'json';
            uploadShoutData['image'] = imageString;
            uploadShoutData['media_price'] = $scope.media_price;
            uploadShoutData['sent_to_list'] = $scope.sent_to_list;
            uploadShoutData['tagline'] = $scope.tagline;
            ProService.uploadShoutMedia( uploadShoutData ).success( function( result ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                var display_msg = result.message;
               	$("#checkLoader").hide();
            	$('#checkbtn').hide();
                $('#checkLoader').replaceWith("<div style='color:green; margin-left: 14px; font-size: 15px; margin-bottom: -5px;margin-top:5px;'><p>" + display_msg + "</p></div>");
            } ).error( function ( data ) {
            	$("#checkLoader").hide();
            	$('.phpdebugbar-openhandler-overlay').hide();
            } );
        }else{
            $('#sendShout_id').html('Please try again..');
            $('#sendShout_id' ).addClass( 'errorStatus' );
        }
    }

})