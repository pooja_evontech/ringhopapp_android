//+++++++++++++++++++++++++++setrates page controller+++++++++++++++++++++

proApp.controller( "SetRatesController", function( $scope, ProService, $timeout, $state, $rootScope, constantData ) {
    $scope.per_minute_select = [];
    angular.forEach(constantData.getSetRatesData().call_rates, function(value, key){
        var item = { id:key, value:value };
        $scope.per_minute_select.push(item);
    });
    
    $scope.per_text_select = [];
    angular.forEach(constantData.getSetRatesData().sms_rates, function(value, key){
        var item = { id:key, value:value };
        $scope.per_text_select.push(item);
    });
    
    $scope.per_video_select = [];
    angular.forEach(constantData.getvideoCallRatesData(), function(value, key){
        var item = { id:key, value:value };
        $scope.per_video_select.push(item);
    });

    $scope.per_min_video_call = [];
    angular.forEach(constantData.getvideoCallMinAmountData(), function(value, key){
        var item = { id:key, value:value };
        $scope.per_min_video_call.push(item);
    });

    var rateData = {};
    rateData['responsetype'] = 'json';
    ProService.setRates( rateData ).success( function( data ) {
        $scope.perMinuteCode = data.data.call_rate;
        $scope.perTextCode = data.data.text_rate;
        $scope.perSendText = data.data.send_text_rate;
        $scope.perPhotoCode = data.data.photo_rate;
        $scope.perVideoCode =  data.data.video_call_rate;
        $scope.perMinVideoCode = data.data.video_call_min_amount.toString();
        $scope.per_text_select = [];
        $scope.per_minute_select = [];
        $scope.per_text_send_select = [];
        $scope.per_photo_select = [];
        $scope.per_video_select = [];
        $scope.per_min_video_call = [];
        angular.forEach(data.data.text_rates, function(value, key){
            var item = { id:key, value:value };
            $scope.per_text_select.push(item);
        });
        angular.forEach(data.data.call_rates, function(value, key){
            var item = { id:key, value:value };
            $scope.per_minute_select.push(item);
        });
        angular.forEach(data.data.send_text_rates, function(value, key){
            var item = { id:key, value:value };
            $scope.per_text_send_select.push(item);
        });
        angular.forEach(data.data.photo_rates, function(value, key){
            var item = { id:key, value:value };
            $scope.per_photo_select.push(item);
        });
        angular.forEach(data.data.video_call_rates, function(value, key){
            var item = { id:key, value:value };
            $scope.per_video_select.push(item);
        });
        
        angular.forEach(data.data.video_call_min_amounts, function(value, key){
            var item = { id:key, value:value };
            $scope.per_min_video_call.push(item);
        });
    } ).error( function ( data, status, headers, config ) {
    } );
    $scope.updateRates = function(){
        $('#rate_status').addClass('errorStatus').removeClass('succesStatus');
        var setrates = {};
        setrates['perminute_rate'] = $scope.perMinuteCode;
        setrates['pertext_rate'] = $scope.perTextCode;
        setrates['pertext_send_rate'] = $scope.perSendText;
        setrates['perphoto_rate'] = $scope.perPhotoCode;
        setrates['video_perminute_rate'] = $scope.perVideoCode;
        setrates['video_call_min_amount'] = $scope.perMinVideoCode;
        setrates['responsetype'] = 'json';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        ProService.setRates( setrates ).success( function( data ) {
            if(data.status == 'success') {
                $('#rate_status').removeClass('errorStatus').addClass('succesStatus');
            } else {
                $('#rate_status').addClass('errorStatus').removeClass('succesStatus');
            }
            $('#rate_status').html(data.message);
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };

} )