//+++++++++++++++++++++++++++forwardingsettings page controller+++++++++++++++++++++

proApp.controller( "ForwardingSettingsCtrl", function( $rootScope, $scope, ProService, $state ) {
    var statusData = {};
    statusData['responsetype'] = "json";
    ProService.forwardingSettings( statusData,'GET' ).success( function( data ) {
        $scope.forwardData = data.data;
        if( data.data.call_forwarding == 1 && data.data.take_referred_calls == 0 ) {
            $scope.call_forwarding = true;
            $scope.take_referred_calls = false;
        }
        if( data.data.call_forwarding == 0 && data.data.take_referred_calls == 1 ) {
            $scope.call_forwarding = false;
            $scope.take_referred_calls = true;
        }
        if( data.data.call_forwarding == 1 && data.data.take_referred_calls == 1 ) {
            $scope.call_forwarding = true;
            $scope.take_referred_calls = true;
        }
        if( data.data.call_forwarding == 0 && data.data.take_referred_calls == 0 ) {
            $scope.call_forwarding = false;
            $scope.take_referred_calls = false;
        }

    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.updateStatus = function ( $event ) {
        statusData = {};
        statusData[ 'responsetype' ] = "json";
        $( '#updateCheck' ).addClass('errorStatus').removeClass('succesStatus');
        if( $scope.call_forwarding == true ) {
            statusData[ 'call_forwarding' ] = 1;
        }
        if( $scope.take_referred_calls == true ) {
            statusData[ 'take_referred_calls' ] = 1;
        }
        $( '.phpdebugbar-openhandler-overlay' ).show();
        $( '.ui-loader' ).show();
        ProService.forwardingSettings( statusData,'POST' ).success( function( data ) {
            if( data.message == 'user not logged in' ) {
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/login"
                } );
            }
            if( data.status == 'success' ){
                $( '#updateCheck' ).removeClass('errorStatus').addClass('succesStatus');
            } else {
                $( '#updateCheck' ).addClass('errorStatus').removeClass('succesStatus');
            }
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $( '.ui-loader' ).hide();
            $( '#updateCheck' ).html( data.message );
        } ).error( function ( data ) {
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };

} )