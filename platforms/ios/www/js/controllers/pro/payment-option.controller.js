//+++++++++++++++++++++++++++paymentoption page controller+++++++++++++++++++++

proApp.controller( "PaymentOptionController", function( $scope, ProService, $timeout, $state, $rootScope, $filter, constantData, AuthService ) {
    $scope.countryList = [{ id:'', value:'(Select Country)'}];
    angular.forEach(constantData.getCountryAllListData(), function(value, key){
        var item = { id:key, value:value };
        $scope.countryList.push(item);
    });
    console.log(constantData.getCountryAllListData());
    console.log(constantData.getCountryData());
    // $rootScope.previousState;
    // $rootScope.currentState;
    // $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
    //     $rootScope.previousState = from.name;
    //     $rootScope.currentState = to.name;
    //     if( $rootScope.previousState == 'home.terms' && $rootScope.currentState == 'home.paymentoption' ) {
    //         forgotFlag = 1;
    //     } else {
    //         forgotFlag = 0;
    //     }
    // });

    var paymentOptionData = {};
    paymentOptionData['responsetype'] = 'json';
    ProService.paymentOption( paymentOptionData, 'GET' ).success( function( data ) {
        console.log(JSON.stringify(data));
        if( data.status != 'failed' ){
            $scope.fullname = data.data.payinfo.full_name;
            $scope.dob_label = data.data.payinfo.birth_date.split("-");
            // $scope.address1 = data.data.payinfo.street_address1;
            // $scope.address2 = data.data.payinfo.street_address2;
            // $scope.city = data.data.payinfo.city;
            // $scope.state = data.data.payinfo.state;
            // $scope.zipcode = data.data.payinfo.zipcode;
            // $scope.country = data.data.payinfo.country;
            // if (data.data.payinfo.country == 'CA') {
            // $scope.stateList = [{ id:'', value:'(Select State)'}];
            //     angular.forEach(constantData.getCanadianStateData(), function(value, key){
            //     var item = { id:key, value:value };
            //     $scope.stateList.push(item);
            // }); 
            // }else{
            //     $scope.stateList = [{ id:'', value:'(Select State)'}];
            //         angular.forEach(constantData.getStateData(), function(value, key){
            //         var item = { id:key, value:value };
            //         $scope.stateList.push(item);
            //     }); 
            // }
            $scope.birth_month = $filter('date')(new Date($scope.dob_label[0],$scope.dob_label[1]-1,$scope.dob_label[2]),'MM');
            $scope.birth_year = $filter('date')(new Date($scope.dob_label[0],$scope.dob_label[1]-1,$scope.dob_label[2]),'yyyy');
            $scope.birth_day = $filter('date')(new Date($scope.dob_label[0],$scope.dob_label[1]-1,$scope.dob_label[2]),'dd');
        }
    } ).error(function (data, status, headers, config) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.countryOption = function(countryOptionValue){
        console.log(countryOptionValue);
        if( countryOptionValue == '' ) {
            flag = false;
            $('#country_error').html('This field is required.');
            $('#country_label').css('color','red');
        }else{
            $('#country_error').html('');
            $('#country_label').css('color','#000');
        }
        // if (countryOptionValue == 'CA') {
        // $scope.stateList = [{ id:'', value:'(Select State)'}];
        //     angular.forEach(constantData.getCanadianStateData(), function(value, key){
        //     var item = { id:key, value:value };
        //     $scope.stateList.push(item);
        //     $scope.state = $scope.stateList[0].id;
        // }); 
        // }else{
        //     $scope.stateList = [{ id:'', value:'(Select State)'}];
        //         angular.forEach(constantData.getStateData(), function(value, key){
        //         var item = { id:key, value:value };
        //         $scope.stateList.push(item);
        //         $scope.state = $scope.stateList[0].id;
        //     }); 
        // }
    };
    $scope.directBankDep = true;
    if ($scope.directBankDep = true) {
        $scope.paymentValue = 'direct_bank_deposit';
    }else{
       $scope.paymentValue = ''; 
    }
    $scope.deposit_value = '';
    $scope.payment_value = '';
    $scope.value_mail = '';
    $scope.bankdep = function(valuedep){
        console.log(valuedep);
        if (valuedep == true) {
            $scope.paymentcard =  false;
            $scope.mail = false;
            $scope.paymentValue = 'direct_bank_deposit';
            $('#paymentMethod_error' ).html('');
        }else{
            console.log("else>>" +$scope.paymentValue);
            $scope.paymentValue = '';
        }
    };
    $scope.cardpayment = function(cardValue){
        console.log(cardValue);
        if (cardValue == true) {
            $scope.mail =  false;
            $scope.directBankDep =  false;
            $scope.paymentValue = 'link_payment_card';
            $('#paymentMethod_error' ).html('');
        }else{
            console.log("else>>" +$scope.paymentValue);
            $scope.paymentValue = '';
        }
    };
    $scope.viaMail = function(mailvalue){
        console.log(mailvalue);
        if (mailvalue == true) {
             $scope.directBankDep =  false;
             $scope.paymentcard =  false;   
             $scope.paymentValue = 'check_by_mail';
             $('#paymentMethod_error' ).html('');
        }else{
            console.log("else>>" +$scope.paymentValue);
            $scope.paymentValue = '';
        }
    };
    $scope.birthyearChange = function(getBirthYear){
        console.log(getBirthYear);
        if (getBirthYear == '') {
            $('#dob_error').html('This field is required.');
            $('#dob_label').css('color','red');
        }
        if (getBirthYear !== '' && $scope.birth_month !== '' && $scope.birth_day !== ''){
            console.log("getBirthYear");
            $('#dob_error').html('');
            $('#dob_label').css('color','#000');
        }
    };
    $scope.birthmonthChange = function(getBirthMonth){
        console.log(getBirthMonth);
        if (getBirthMonth == '') {
            $('#dob_error').html('This field is required.');
            $('#dob_label').css('color','red');
        }
        if ($scope.birth_year !== '' && getBirthMonth !== '' && $scope.birth_day !== ''){
            console.log("getBirthMonth");
            $('#dob_error').html('');
            $('#dob_label').css('color','#000');
        }
    };
    $scope.birthdayChange = function(getBirthDay){
        console.log(getBirthDay);
        if (getBirthDay == '') {
            $('#dob_error').html('This field is required.');
            $('#dob_label').css('color','red');
        }
        if ($scope.birth_year !== '' && $scope.birth_month !== '' && getBirthDay !== ''){
            console.log("getBirthDay");
            $('#dob_error').html('');
            $('#dob_label').css('color','#000');
        }
    };
    var flag = false;
    $scope.paymentAddrSubmit = function( $event ) {
        console.log($scope.birth_year);
        console.log($scope.birth_month);
        console.log($scope.birth_day);
        var new_val = "";
        var paymentOptionDiv = document.getElementById('paymentoption_form');
        var input_id = '';
        var elementsLength = paymentOptionDiv.getElementsByTagName('input').length;
        if( $scope.output == true ) {
            console.log("output>>" +$scope.output);
            $('#output').val('Y');
        }else {
            flag = false;
            console.log($scope.output);
            $('#output').val('N');
            $('#output_error').html('This field is required.');
            $('#output_label').css('color','red');
        }
        if ($scope.paymentValue == '') {
            console.log("null>>" +$scope.paymentValue);
            flag = false;
            $('#paymentMethod_error' ).html( 'Please select one payment method.' );
            $('#paymentMethod_error').css('color','red');
        }else{
            console.log("else>>" +$scope.paymentValue);
            $('#paymentMethod_error' ).html('');
        }
        if( $scope.country == '' ) {
            flag = false;
            $('#country_error').html('This field is required.');
            $('#country_label').css('color','red');
        }else{
            $('#country_error').html('');
            $('#country_label').css('color','#000');
        }
        if( $scope.birth_year === undefined || $scope.birth_month === undefined || $scope.birth_day === undefined ) {
            console.log("birth");
            $('#dob_error').html('This field is required.');
            $('#dob_label').css('color','red');
            flag = false;
        }else if ($scope.birth_year !== '' && $scope.birth_month !== '' && $scope.birth_day !== ''){
            console.log("elsebirth");
            $('#dob_error').html('');
            $('#dob_label').css('color','#000');
        }
        $( '#fullname' ).keydown(function (e) {
            if(e.keyCode == 8){
                if( $('#fullname').val().length <= 2 && $('#fullname').val().length > 0 ) {
                    // console.log($( '#fullname' ).val().length);
                    flag = false;
                    $( '#fullname_error1' ).html( 'Please enter at least 2 characters.' );
                }
                if( $('#fullname').val().length == 1 ){
                    // console.log($( '#fullname' ).val().length);
                    $( '#fullname_error1' ).html( 'This field is required.' );
                    flag = false;
                    } 
            }else{
                if( $('#fullname').val().length <= 2 && $('#fullname').val().length > 0 ) {
                    flag = false;
                    $( '#fullname_error1' ).html( 'Please enter at least 2 characters.' );
                }
                if( $('#fullname').val().length >= 1) {
                    $( '#fullname_error1' ).html('');
                    $( '#fullname_label1' ).removeAttr('style');
                    flag = 1;
                }
            }
        } );
        $scope.outputChange = function(outputval){
            console.log(outputval);
            if (outputval == true) {
                console.log("if>>" +outputval);
                $( '#output_error' ).html('');
                $( '#output_label' ).removeAttr('style');
            }else{
                console.log("else>>" +outputval);
                $( '#output_error' ).html('This field is required.');
                $( '#output_label' ).css('color','red');
            }
        }
        console.log($('#fullname').val().length,$scope.fullname);

        if( $scope.fullname != '' && $('#fullname').val().length < 2) {

            $( '#fullname_error1' ).html('Please enter at least 2 characters.');
            $( '#fullname_label1' ).css('color','red');
        }
        if ( $scope.output == true && $scope.paymentValue != '' && $scope.country != '' && $( '#fullname_error1' ).html() == '' && $( '#dob_error' ).html() == '') {
            console.log("enter>>" +$scope.paymentValue);
            flag = true;
        }
        console.log($scope.paymentValue);
        var paymentOptionData = {};
        paymentOptionData['fullname'] = $scope.fullname;
        paymentOptionData['payment_method'] = $scope.paymentValue;
        paymentOptionData['country'] = $scope.country;
        paymentOptionData['output'] = $scope.output;
        paymentOptionData['birth_year'] = $scope.birth_year;
        paymentOptionData['birth_month'] = $scope.birth_month;
        paymentOptionData['birth_day'] = $scope.birth_day;
        paymentOptionData['responsetype'] = 'json';
        console.log(JSON.stringify(paymentOptionData));
        if( flag == true ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            ProService.paymentOption( paymentOptionData, 'POST' ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( data.status == 'success' ) {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/paymentoptcomplete"
                    });
                } else {
                    $('#paymentOptionStatus').html( data.message );
                }
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )