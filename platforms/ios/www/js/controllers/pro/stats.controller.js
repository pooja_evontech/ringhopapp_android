//+++++++++++++++++++++++++++stats page controller+++++++++++++++++++++

proApp.controller( "StatsController", function( $scope, ProService, $timeout, $state, $filter, $rootScope ) {
    $scope.date = new Date();
    var currmonth = $filter('date')(new Date(),'MM');
    var curryear = $filter('date')(new Date(),'yyyy');
    var currdate = $filter('date')(new Date(),'dd');
    var x = 0;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.isPro = false;
    if( proData != 2 ) {
        //for pro users
        $scope.isPro = false;
    } else {
        $scope.isPro = true;
    }
    var d = new Date(2015, 1 + 1, 0);
    if( currdate > 0 && currdate <= 15 ) {
        $scope.currPeriodSelect = [
            {id: currmonth+'/01/'+curryear+'-'+currmonth+'/15/'+curryear, value:'Current period - '+currmonth+'/1 to '+currmonth+'/15/'+curryear}
        ];
        for( var i = currmonth; i > ( currmonth-3 ); i -= 0.5 ) {
            if( Math.ceil(i-1) == 0 ) {
                x = 12;
            } else if( Math.ceil(i-1) == -1 ) {
                x = 11;
            } else if( Math.ceil(i-1)  == -2 ) {
                x = 10;
            } else {
                x = Math.ceil(i-1);
            }
            var d = new Date(curryear, x, 0);
            var lastdate = $filter('date')(d,'dd');

            if( i % 1 != 0 ){
                var item = { id: x +'/01/'+(curryear)+'-'+ x +'/15/'+(curryear), value: x +'/01 to '+ x +'/15/'+(curryear) };
             } else {
                var item = { id: x +'/16/'+(curryear)+'-'+ x +'/'+lastdate+'/'+(curryear), value: x +'/16 to '+ x +'/'+lastdate+'/'+(curryear) };
            }
            $scope.currPeriodSelect.push( item );
        }
    } else {
        $scope.currPeriodSelect = [
            { id: currmonth+'/16/'+curryear+'-'+currmonth+'/31/'+curryear, value:'Current period - '+currmonth+'/16 to '+currmonth+'/31/'+curryear },
           {id: currmonth+'/01/'+curryear+'-'+currmonth+'/15/'+curryear, value:currmonth+'/01 to '+currmonth+'/15/'+curryear}
        ];
        for( var i = currmonth; i > ( currmonth-3 ); i -= 0.5 ) {
            if( Math.floor( i ) == 0 ) {
                x = 12;
            } else if( Math.floor( i ) == -1 ) {
                x = 11;
            } else if( Math.floor( i )  == -2 ) {
                x = 10;
            } else {
                x = Math.floor(i);
            }
            var d = new Date(curryear, x, 0);
            var lastdate = $filter('date')(d,'dd');
            if( i % 1 != 0 ) {
                var item = { id: x+'/16/'+(curryear)+'-'+ x +'/'+lastdate+'/'+(curryear), value: x+'/16 to '+x+'/' + lastdate+'/'+(curryear) };
            } else {
                var item = { id: x +'/01/'+(curryear)+'-'+ x +'/15/'+(curryear), value: x +'/01 to '+ x +'/15/' + (curryear) };
            }
            $scope.currPeriodSelect.push( item );
        }
    }

    if( $scope.isPro == true ) {
        var statsURL = BASE_URL+"pro/stats";
    } else {
        var statsURL = BASE_URL+"userstats";
    }
    var statsData = {};
    statsData['responsetype'] = 'json';
    ProService.stats( statsData, statsURL ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }
        var changeDate = data.data.stats_period.split("-");
        if( changeDate != '' ) {
            $scope.initial = $filter('date')(new Date(changeDate[0]),'MMM d');
            $scope.final1 = $filter('date')(new Date(changeDate[1]),'MMM d, yyyy');
        } else {
            $scope.initial = $filter('date')(new Date(),'MMM d');
            $scope.final1 = $filter('date')(new Date(),'MMM d, yyyy');
        }

        $scope.stats = data.data;
        var curr_charge = parseFloat( data.data.calls_currentperiod.call_charge ) + parseFloat( data.data.msg_currentperiod.msg_charge )+parseFloat(50.00);
        $scope.totalCurrCharge = curr_charge.toFixed(2);
        var today_charge = parseFloat(data.data.calls_today.call_charge) + parseFloat(data.data.msg_today.msg_charge)+parseFloat(10.00);
        $scope.totalTodayCharge = today_charge.toFixed(2);
        var call_time = data.data.calls_currentperiod.call_secs;
        $scope.callTime = Math.floor(call_time);
        var curr_call = parseFloat(data.data.calls_currentperiod.call_charge);
        $scope.currCallCharge = curr_call.toFixed(2);
        $scope.todayCallCharge = parseFloat(data.data.calls_today.call_charge).toFixed(2);
        var curr_per_msg_chrg = parseFloat(data.data.msg_currentperiod.msg_charge);
        var curr_per_msg_chrg1 = parseFloat(data.data.msg_currentperiod1.msg_charge);
        var currPerMsgChrg = curr_per_msg_chrg+curr_per_msg_chrg1;
        $scope.currMsgChrg = (Math.round( currPerMsgChrg * 100 ) / 100).toFixed(2);
        var num = parseFloat(data.data.total_period_charge);
		$scope.TotalperiodChrg = (Math.round( num * 100 ) / 100).toFixed(2);
        $scope.currPerMsgChrg = parseFloat(data.data.msg_currentperiod.msg_charge).toFixed(2);
        $scope.msgtodayChrg = parseFloat(data.data.msg_today.msg_charge).toFixed(2);
        $scope.photoCurrChrg = parseFloat(data.data.photo_currentperiod.photo_charge).toFixed(2);
        $scope.phototodayChrg = parseFloat(data.data.photo_today.photo_charge).toFixed(2);
        $scope.callcurr_ref_in = parseFloat(data.data.calls_currentperiod_ref_in.call_charge).toFixed(2);
        $scope.calltoday_ref_in = parseFloat(data.data.calls_today_ref_in.call_charge).toFixed(2);
        $scope.callcurr_ref_out = parseFloat(data.data.calls_currentperiod_ref_out.call_charge).toFixed(2);
        $scope.calltoday_ref_out = parseFloat(data.data.calls_today_ref_out.call_charge).toFixed(2);
        $scope.tiptodayChrg = parseFloat(data.data.tip_today.tip_charge).toFixed(2);
        var tipCharge = parseFloat(data.data.tip_currentperiod.tip_charge);
        $scope.currPerTipChrg = (Math.round( tipCharge * 100 ) / 100).toFixed(2)
        var call_time_ref_in = data.data.calls_currentperiod_ref_in.call_secs;
        var call_time_ref_out = data.data.calls_currentperiod_ref_out.call_secs;
        $scope.callTime_ref_in = Math.floor(call_time_ref_in);
        $scope.callTime_ref_out = Math.floor(call_time_ref_out);
        var shoutCharge = parseFloat(data.data.shouts_currentperiod.shout_charge);
        $scope.shouts_Charge = (Math.round( shoutCharge * 100 ) / 100).toFixed(2);
    	$scope.shouttodayChrg = parseFloat(data.data.shouts_today.shout_charge).toFixed(2);
         var vcallsCharge = parseFloat(data.data.vcalls_currentperiod.call_charge);
        $scope.call_charge = (Math.round( vcallsCharge * 100 ) / 100).toFixed(2);
        $scope.videocall_today_chrg = parseFloat(data.data.vcalls_today.call_charge).toFixed(2);
    } ).error( function ( data, status, headers, config ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.currPeriodChange = function() {
        statsData['stats_period'] = $scope.period_select;
        ProService.stats( statsData, statsURL ).success( function( data ) {
            var changeDate = data.data.stats_period.split("-");
            $scope.initial = $filter('date')(new Date(changeDate[0]),'MMM d');
            $scope.final1 = $filter('date')(new Date(changeDate[1]),'MMM d, yyyy');
            $scope.stats = data.data;
            var curr_charge = parseFloat(data.data.calls_currentperiod.call_charge) + parseFloat(data.data.msg_currentperiod.msg_charge)+parseFloat(50.00);
            $scope.totalCurrCharge = curr_charge.toFixed(2);
            var today_charge = parseFloat(data.data.calls_today.call_charge) + parseFloat(data.data.msg_today.msg_charge)+parseFloat(10.00);
            $scope.totalTodayCharge = today_charge.toFixed(2);
            var call_time = data.data.calls_currentperiod.call_secs;
            $scope.callTime = Math.floor(call_time);
            var curr_call = parseFloat(data.data.calls_currentperiod.call_charge);
            $scope.currCallCharge = curr_call.toFixed(2);
            $scope.todayCallCharge = parseFloat(data.data.calls_today.call_charge).toFixed(2);
            var curr_per_msg_chrg = parseFloat(data.data.msg_currentperiod.msg_charge);
	        var curr_per_msg_chrg1 = parseFloat(data.data.msg_currentperiod1.msg_charge); 
	        var currPerMsgChrg = curr_per_msg_chrg+curr_per_msg_chrg1;
	        $scope.currMsgChrg = (Math.round( currPerMsgChrg * 100 ) / 100).toFixed(2);
	        var num = parseFloat(data.data.total_period_charge);
			$scope.TotalperiodChrg = (Math.round( num * 100 ) / 100).toFixed(2);
            $scope.currPerMsgChrg = parseFloat(data.data.msg_currentperiod.msg_charge).toFixed(2);
            $scope.msgtodayChrg = parseFloat(data.data.msg_today.msg_charge).toFixed(2);
            $scope.photoCurrChrg = parseFloat(data.data.photo_currentperiod.photo_charge).toFixed(2);
            $scope.phototodayChrg = parseFloat(data.data.photo_today.photo_charge).toFixed(2);
            $scope.callcurr_ref_in = parseFloat(data.data.calls_currentperiod_ref_in.call_charge).toFixed(2);
            $scope.calltoday_ref_in = parseFloat(data.data.calls_today_ref_in.call_charge).toFixed(2);
            $scope.callcurr_ref_out = parseFloat(data.data.calls_currentperiod_ref_out.call_charge).toFixed(2);
            $scope.calltoday_ref_out = parseFloat(data.data.calls_today_ref_out.call_charge).toFixed(2);
            var tipCharge = parseFloat(data.data.tip_currentperiod.tip_charge);
            $scope.currPerTipChrg = (Math.round( tipCharge * 100 ) / 100).toFixed(2);
            var shoutCharge = parseFloat(data.data.shouts_currentperiod.shout_charge);
            $scope.shouts_Charge = (Math.round( shoutCharge * 100 ) / 100).toFixed(2);
            $scope.tiptodayChrg = parseFloat(data.data.tip_today.tip_charge).toFixed(2);
            $scope.shouttodayChrg = parseFloat(data.data.shouts_today.shout_charge).toFixed(2);
            var call_time_ref_in = data.data.calls_currentperiod_ref_in.call_secs;
            var call_time_ref_out = data.data.calls_currentperiod_ref_out.call_secs;
            $scope.callTime_ref_in = Math.floor( call_time_ref_in );
            $scope.callTime_ref_out = Math.floor( call_time_ref_out );
            var vcallsCharge = parseFloat(data.data.vcalls_currentperiod.call_charge);
            $scope.call_charge = (Math.round( vcallsCharge * 100 ) / 100).toFixed(2);
            $scope.videocall_today_chrg = parseFloat(data.data.vcalls_today.call_charge).toFixed(2);
        } ).error( function ( data, status, headers, config ) {
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
} )