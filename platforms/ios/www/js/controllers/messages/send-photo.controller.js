//+++++++++++++++++++++++++++sendPhoto page controller+++++++++++++++++++++

messageApp.controller( "SendPhotoController", function( $scope, MessagesService, $state, $stateParams, $rootScope ) {
    $scope.takePhotoOpt = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.pro_data = false;
    $(".loaderId").hide();
    if( proData == 2 ) {
        $scope.pro_data = true;
    }
    $scope.div_visible = true;
    $scope.div_visible_1 = false;
    if ($scope.photoMode == '' || $scope.photoMode === undefined) {
        console.log("paid>>" +$scope.disabled);
        $scope.disabled = false;
        $("#media_price" ).css('border','none');
    }else{
        console.log("free>>" +$scope.disabled);
        $scope.disabled = true;
        $("#media_price" ).css('border','1px solid grey');
        $scope.media_price = '0.00';
    }
    $scope.uploadChatPhoto = function () {
        $scope.takePhotoOpt = true;
    };

    $scope.cancelPhoto = function () {
        $scope.takePhotoOpt = false;
    };

    function onPhotoDataSuccess( imageData ) {
        $('#sendPhoto_id').html('');
        $('#sendPhoto_id' ).removeClass( 'errorStatus' );
        var profileImage = document.getElementById( 'chat_image' );
        profileImage.style.display = 'block';
        profileImage.src = "data:image/jpeg;base64," + imageData;
        imageString = imageData;
    }

    // A button will call this function
    $scope.takePhoto = function ( $event ) {
        $('#sendPhoto_id').html('');
        $('#sendPhoto_id' ).removeClass( 'errorStatus' );
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        $scope.div_visible_1 = true;
        // Take picture using device camera and retrieve image as base64-encoded string
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:500,
        	targetHeight: 500, correctOrientation: true,
        destinationType: Camera.DestinationType.DATA_URL });
    };

    //take photo from phone gallery
    $scope.photoLibrary = function ( ) {
        $('#sendPhoto_id').html('');
        $('#sendPhoto_id' ).removeClass( 'errorStatus' );
        $scope.takePhotoOpt = false;
        $scope.div_visible = false;
        $scope.div_visible_1 = true;
        navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50, targetWidth:1000,
        	targetHeight: 1000, destinationType: destinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY });
    };

    // Called if something bad happens.
    $scope.idUser = $stateParams.recieverId;
    function onFail(message) {
        console.log('Failed because: ' + message);
        $state.transitionTo('home.sendPhoto',{
            recieverId: $scope.idUser
          },{
            reload: true
        });
        // $scope.takePhotoOpt = false;
        // $scope.div_visible = true;
        // $scope.div_visible_1 = false;
    }

    //upload profile pic
    var flag = false;
    $scope.saveChatPic = function ( $event ) {
        if (imageString == '') {
           flag = false;
           $('#sendPhoto_id').html('Please try again..');
           $('#sendPhoto_id' ).addClass( 'errorStatus' ); 
        }else if($( "#media_price" ).val() == '' || $( "#media_price" ).val() > 99 || $( "#media_price" ).val() < 0){
            flag = false;
            $("#media_price" ).css('border','1px solid red');
            $("#media_error" ).html('Please enter media price between 0-99.');
        }else{
            flag = true;
           $("#media_price" ).css('border','none'); 
           $("#media_error" ).html('');
        }
        if(flag == true){
            $('#uploadChat_id').hide();
            $('#media_price').hide();
            $('#sendBtn').hide();
            $('#cancelBtn').hide();
            $('#optionalcommentdiv').hide(); 
            $('#send_Photo_select').hide();   
            var chatImageData = {};
            var mediaCharge = $('#media_price').val();
            chatImageData['responsetype'] = "json";
            chatImageData['image'] = imageString;
            chatImageData['uid'] = $stateParams.recieverId;
            chatImageData['tagline'] = $scope.tagline;
            if( proData == 2 ) {
                if( mediaCharge == '' ) {
                    chatImageData['media_price'] = 0;
                } else {
                    chatImageData['media_price'] = mediaCharge;
                }
            } else {
                chatImageData['media_price'] = 0;
            }
            //console.log(JSON.stringify(chatImageData));
            $('.phpdebugbar-openhandler-overlay').show();
            $(".loaderId").show();

            MessagesService.uploadChatMedia( chatImageData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $(".loaderId").hide();
                //console.log('success=>'+JSON.stringify(data));
                var display_msg = 'Photo message sent!';
                $('#checkLoader').replaceWith("<div style='color:green; margin-left: 14px; font-size: 15px; margin-bottom: -5px;margin-top:-5px;'><p>" + display_msg + "</p></div>");
            } ).error( function ( data ) {
                // console.log( 'error'+JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $(".loaderId").hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.photoModeChange = function (getPhotoMode) {
        $scope.media_price = getPhotoMode == 'free' ? '0.00' : '';
        if (getPhotoMode == 'free') {
            
            $("#media_price" ).css('border','1px solid grey');
            $scope.media_price = '0.00';
            $scope.disabled = true;
            $('#media_price').val('0.00');
        }else{
            $scope.disabled = false;
            $("#media_price" ).css('border','none');
            $('#media_price').val('');
        }
    }

    $scope.cancelUpload = function () {
        $scope.div_visible = true;
        $scope.div_visible_1 = false;
    };

} )