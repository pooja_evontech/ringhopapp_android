//+++++++++++++++++++++++++++TextMessage page controller+++++++++++++++++++++

messageApp.controller( "RecentMessageController", function( $scope, MessagesService, $state, $rootScope, $timeout ) {
    $scope.prevDisabled = false;
    $scope.nextDisabled = false;
    $scope.messageOptions = false;
    $scope.paginateSection = false;
    $scope.noMessageDiv = false;
    $scope.showLeadstext = false;
    $scope.showmeleads = true;
    $scope.limit = 4;
    $scope.recentChats = {};
    var checkVal = false;
    var pre_count = 0;
    var next_count = 0;
    var users = 'billable';
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    $scope.userid = JSON.parse( dashboard_data ).data.profile.user_id;
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    if( proData == 2 ) {
        //for pro users
        $scope.isPro = true;
        $scope.messageOptions = true;
    } else {
        $scope.isPro = false;
    }
    $('.ui-loader').show();
    var getMessageData1 = {};
    getMessageData1['responsetype'] = 'json';

    MessagesService.messages( getMessageData1 ).success( function( data ) {
        $('.ui-loader').hide();
        if( data.message == 'user not logged in' ) {
            //if user's session expires
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }
        $scope.limit = data.data.per_page; //update the max limit of message per page
        $scope.recentChats = data.data.msghistory;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
        }
        if ( $scope.totalMessage == 0 ) {
        }
    } ).error( function ( data ) {
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
    } );

    $scope.backButtonMessages = function(){
        window.plugins.nativepagetransitions.slide({
            "direction": 'right',
            "href" : '#/home/affter_login'
        });
    };

    $scope.billableShow = true;
    $scope.billableUser = function () {
        checkVal = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'billable';
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#billable_user').removeClass('fullwidth_white');
        $('#trial_user').addClass('fullwidth_white');
        $('#trial_user').removeClass('fullwidth_bluemsg');
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var getMessageData1 = {};
        getMessageData1['responsetype'] = 'json';
        MessagesService.messages( getMessageData1 ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            } else {
                $scope.noMessageDiv = false;
            }
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $scope.paginateSection = true;
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
        } ).error( function ( data, status, headers, config ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.leadsUser = function(){
        checkVal = true;
        $scope.showLeadstext = false;
        $scope.paginateSection = true;
        $scope.showmeleads = true;
        $scope.messageOptions = true;
    };
    $scope.showLeads = function( evt ){
        if(checkVal == true){
            evt.preventDefault();
        }else{
        $('.ui-loader').show();
        $('.phpdebugbar-openhandler-overlay').show();
        $timeout(function(){
          $scope.showLeadstext = true;
          $('.ui-loader').hide();
          $('.phpdebugbar-openhandler-overlay').hide();
        }, 2500);
        $scope.paginateSection = false;
        $scope.messageOptions = false;
        $scope.showmeleads = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'trial';
        $('#billable_user').addClass('fullwidth_white');
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#trial_user').addClass('fullwidth_bluemsg');
        $('#trial_user').removeClass('fullwidth_white');
        var getMessageData = {};
        getMessageData['responsetype'] = 'json';
        getMessageData['sort'] = 'free';
        MessagesService.messages( getMessageData ).success( function( data ) {
            $scope.limit = data.data.per_page;
            $scope.recentChats = data.data.msghistory;
            $scope.totalMessage = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No recent messages available.";
            } else {
                $scope.noMessageDiv = false;
            }
            if ( $scope.totalMessage <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $scope.paginateSection = true;
                $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
            $scope.paginateSection = false;
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
        checkVal = false;
      }
        
    };
    $scope.replyLink = function (originator_id) {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/reply"+originator_id
        });
    };
    $scope.prevBtn = function () {
        $scope.recentChats = {};
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });

        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = pre_count;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            MessagesService.messages( getMessageData1 ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ){
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                next_count = next_count - 1;
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        $scope.recentChats = {};
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getMessageData1['page'] = next_count+1;
            if ( users == 'trial' ) {
                getMessageData1['sort'] = 'free';
            }
            MessagesService.messages( getMessageData1 ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.recentChats = data.data.msghistory;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };

} )