//+++++++++++++++++++++++++++paidphoto page controller+++++++++++++++++++++

messageApp.controller( "PaidPhotoController", function( $scope, PaymentService, $state, $stateParams, $rootScope ) {
    var page_name = 'paidphoto';
    var paidPhotoData = {};
    paidPhotoData['displayname'] = $stateParams.displayname;
    paidPhotoData['media_id'] = $stateParams.media_id;
    paidPhotoData['model_id'] = $stateParams.model_id;
    paidPhotoData['responsetype'] = 'json';
    $scope.photoInfo = [];
    PaymentService.photoPurchase( paidPhotoData ).success( function( data ) {
        $scope.photoInfo = data;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.purchasePhoto = function () {
        if( $scope.photoInfo.button_color == 'red' ) {
            if( $scope.photoInfo.button_text == 'Add credits' || $scope.photoInfo.button_text == 'Click to upgrade') {
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+ $stateParams.model_id+'/'+page_name
                });
            }
            if( $scope.photoInfo.button_text == 'Click here to view.' ) {
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/reply"+$stateParams.model_id
                } );

            }
        } else if ( $scope.photoInfo.button_color == 'green' ) {
            var purchasePhotoData = {};
            purchasePhotoData['media_id'] = $stateParams.media_id;
            purchasePhotoData['displayname'] = $stateParams.displayname;
            purchasePhotoData['model_id'] = $stateParams.model_id;
            purchasePhotoData['responsetype'] = 'json';
            PaymentService.processPurchase( purchasePhotoData ).success( function( data ) {
                if( $rootScope.previousState == 'home.reply') {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/paidphotoadd/"+$stateParams.model_id+"/reply"
                    });
                } else {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/paidphotoadd/"+$stateParams.model_id+"/library"
                    });
                }
            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )