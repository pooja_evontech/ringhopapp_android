//+++++++++++++++++++++++++++Caller History page controller+++++++++++++++++++++

callApp.controller( "CallerHistoryController", function( $scope, CallsService, $state, $stateParams, $rootScope, UserService, ContactsService, PaymentService) {
    var originator_id = $stateParams.userId;
    var getCallerHistoryData = {};
    getCallerHistoryData['responsetype'] = 'json';
    getCallerHistoryData['uid'] = $stateParams.userId;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var user_status = JSON.parse( dashboard_data ).data.user_status;
    $scope.proInfo = proData;
    $scope.showVideoIcon = false;
    $scope.blockPopup = false;
    $scope.gotoBlockUsers = false;
    $scope.closePopupBlock = false;
    if( proData == 2 ) {
        //for pro users
        $scope.isPro = true;
    } else {
        $scope.isPro = false;
    }
    $('.ui-loader').show();
    CallsService.callerHistory( getCallerHistoryData ).success( function( data ) {
        $('.ui-loader').hide();
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }
        $scope.callerHistory = data.data;
        $scope.callerProfile = data.data.profile_images.profile_image;
        $scope.callerHistoryProData = data.data.caller;
        var image = $('#profile_pic');
            image.css('background-image', 'url(' + $scope.callerProfile +')');
         angular.forEach($scope.callerHistory.usercallhistory , function(history , filterKey) {
            if( history.call_total_amount != null ) {
                history.call_total_amount = parseFloat( history.call_total_amount ).toFixed(2);
            } else {
                history.call_total_amount = 0.00;
            }
        } );
        if($scope.proInfo != $scope.callerHistoryProData.pro){
            $scope.showVideoIcon = true;
        } else {
            $scope.showVideoIcon = false;
        }
        if( data.data.isblocked == true ) {
            $('#alreadyBlocked').removeClass('redCallHistory');
            $('#alreadyBlocked').addClass('italicCall');
            $('#alreadyBlocked').html('already blocked');
        } else {
            $('#alreadyBlocked').removeClass('italicCall');
            $('#alreadyBlocked').addClass('redCallHistory');
            $('#alreadyBlocked').html('Block this user');
            $scope.blockUser = function(){
                if($('#alreadyBlocked').html()=='Block this user') {
                    navigator.notification.confirm(
                        'Are you sure you want to block this user?',  // message
                        onConfirm,
                        'Block User'
                    );
                }
            };
        }
        function onConfirm(button) {
            if( button == 1 ) {
                var blockUsrData = {};
                blockUsrData['responsetype'] = 'json';
                blockUsrData['caller_id'] = data.data.caller.id;
                ContactsService.blockUser( blockUsrData ).success( function( data ) {
                    $('#alreadyBlocked').html('user blocked');
                    $('#alreadyBlocked').removeClass('redCallHistory');
                    $('#alreadyBlocked').addClass('italicCall');
                } ).error( function ( data ) {
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        }
        if( data.data.iscontact == true ) {
            $('#alreadyContact').removeClass('blueCallHistory');
            $('#alreadyContact').addClass('italicCall');
            $('#alreadyContact').html('already a contact');
        } else {
            $('#alreadyContact').removeClass('italicCall');
            $('#alreadyContact').addClass('blueCallHistory');
            $('#alreadyContact').html('Add as contact');
            $scope.addAsContact = function(){
                var addContactData = {};
                addContactData['responsetype'] = 'json';
                addContactData['caller_id'] = data.data.caller.id;
                ContactsService.addContact( addContactData ).success( function( data ) {
                    $('#alreadyContact').html('contact added');
                    $('#alreadyContact').removeClass('blueCallHistory');
                    $('#alreadyContact').addClass('italicCall');
                } ).error( function ( data ) {
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            };
        }

    } ).error( function ( data ) {
        $('.ui-loader').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    //profile page link from profile pic
    $scope.profileLink = function() {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/socialprofile"+$scope.callerHistory.caller.id
        });
    };
    $scope.sendTip = function() {
        $('#overlay').show();
        $('.ui-loader').show();
        var page_name = 'callerhistory';
        var tipData = {};
        tipData['responsetype'] = 'json';
        tipData['proid'] = originator_id;
        PaymentService.sendTip( tipData, 'GET' ).success( function( data ) {
            $('#overlay').hide();
            $('.ui-loader').hide();
            $scope.sendTipInfo = data.message;
            if( data.status == 'error' ) {
            }else{
                if(data.data.linktext == "Blocked users"){
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/viewTip/"+originator_id+"/"+page_name+"/0"
                    } );
                }else{
                     window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/sendTip/"+originator_id+"/"+page_name
                    } );
                }
                   
                
            }
        } ).error( function ( data ) {
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.videoCallFromCallerhistory =function(id_receiver , name_receiver){
        $('.phpdebugbar-openhandler-overlay').hide();
        $scope.idCallback = id_receiver;
        $scope.username1 = name_receiver;
        $scope.pageState = 'notAvailable';
        $scope.pageStateInactive = 'deactivated';
        var videoData = {};
        videoData['responsetype'] = 'json';
        videoData['receiver'] = $scope.idCallback;
        UserService.getUserDetail( videoData ).success( function( data ) {
            $scope.videoCallRate = data.data.video_call_rate;
            $scope.userBalance = data.data.originator_balance;
            $scope.reqBal = data.data.bal_req;
            $scope.origBlock = data.data.originator_block;
            $scope.receiverBlock = data.data.receiver_block;
            $scope.receiverAvailable = data.data.receiver_available;
            localStorage.setItem("receiverImage_data",data.data.profile_image);
            var receiveData = JSON.stringify({'username':$scope.username1,'origId':$scope.idCallback,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'callerhistory', 'receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
            if ($scope.receiverBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            }else if ( $scope.origBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "You are blocked";
            }
            else if( $scope.origBlock === true && $scope.receiverBlock === true ) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            } else if ( $scope.receiverAvailable == "0") {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "This user is not available";
            }else if (data.data.receiver_status == 'inactive') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageStateInactive+'/'+$scope.idCallback
                    } );
                }
            else if (data.data.in_video_call == '1') {
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageState+'/'+$scope.idCallback
                } );
            }
            else if ($scope.origBlock === false && $scope.receiverBlock === false){ 
                $scope.closePopupBlock = false;
                $('#overlay').hide();
                if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                   $state.go('home.videoCalling',{receive_data:receiveData})
                }else{
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/videoCredits/"+$scope.username1+'/'+$scope.reqBal
                    });
                }
            }
        } ).error( function ( data ) {
            $scope.networkPopup = true;
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.backButtonfrmCallerhist = function(){
        window.plugins.nativepagetransitions.slide({
            "direction" : "right",
            "href" : "#/home/affter_login"
        });
    };
    $scope.closeBlock = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
    };

    $scope.blockLink = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };
} )