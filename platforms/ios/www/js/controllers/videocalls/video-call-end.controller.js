//++++++++++++++++++++++++++++video call end controller +++++++++++++++++++++++++++++++++++//

callApp.controller( "VideoCallEndController", function( $scope, $http, $timeout, $state, $stateParams, $rootScope, $location, $interval, $filter  ) {
    connectionflag = false;
    $scope.showCall_time = $stateParams.totalcalltime;
    $scope.proShowdiv = false;
    $scope.userShowdiv = false;
    if($stateParams.check_identity == 2 ){
        $scope.proShowdiv = true;
        $scope.showCall_bal = $stateParams.totalcallcost;
     }else{
        $scope.userShowdiv = true;
        $scope.showCall_bal = $stateParams.totalcallcost; 
     }
    $scope.showUserName = $stateParams.userName;
    $scope.receiverId1 = $stateParams.idReceive;
    $scope.backToreply = function(){
        console.log("back");
        window.plugins.nativepagetransitions.slide({
            "direction": 'left',
            "href" : "#/home/affter_login"
        });
    };
});