//+++++++++++++++++++++++++++videoProReject page controller+++++++++++++++++++++

callApp.controller( "VideoProRejectController", function( $scope, $http, $timeout, $state, $stateParams, $rootScope, $location, $interval, $filter  ) {
    var user_detail = localStorage.getItem( "userDetail" );
    var userid = JSON.parse( user_detail ).data.id;
    var page_name = 'video';
    $scope.videoCallBusy = false;
    $scope.missedCall = true;
    $scope.videoCallInactive = false;
    $scope.diplayUsername = $stateParams.user_name;
    $scope.pro_name = $stateParams.user_name;
    $scope.previous_State = $stateParams.state_page;
    $scope.id2 = $stateParams.id_receiver1;
    if ($stateParams.state_page == 'notAvailable') {
        $scope.videoCallBusy = true;
        $scope.missedCall = false;
    }else if ($stateParams.state_page == 'deactivated') {
        $scope.missedCall = false;
        $scope.videoCallBusy = false;
        $scope.videoCallInactive = true;
    }
    else{
        $scope.videoCallBusy = false;
        $scope.videoCallInactive = false;
        $scope.missedCall = true;
    }
    $scope.goBack = function(){
        if ($scope.previous_State == 'home.recentcalls' || $scope.previous_State == 'home.videocallMissed') {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/recentcalls"
            } );
         }else if($scope.previous_State == 'home.contacts'){
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/contacts"
            } );
         }else if($scope.previous_State == 'home.callerhistory'){
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/callerhistory"+$scope.id2
            } );
         }
         else{
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/messages"
            } );
         }
    };
    $scope.backButton2 = function(){
        window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/affter_login"
                } );
    };

});