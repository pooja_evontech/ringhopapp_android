//+++++++++++++++++++++++++++videocallMissed page controller+++++++++++++++++++++

callApp.controller( "VideoCallMissedController", function( $scope, UserService, $timeout, $state, $stateParams, $rootScope, $location, $interval, $filter  ) {
    clearTimeout($rootScope.time_out);
    ringtoneSound = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.proInfo = proData;
    $scope.showMissedName = $stateParams.user_name;
    $scope.idCallback = $stateParams.callBackid;
    var receiveAftrNotiData = localStorage.getItem("caller_Data");
    var afterNoti = JSON.parse( receiveAftrNotiData );
    console.log(afterNoti);
    var profileImageUrl = afterNoti.image;
    var image = $('#profile_pic');
    image.css('background-image', 'url(' + profileImageUrl +')');
     $scope.callBackUser = function($event){
        $event.stopPropagation();
        //snd.play();
        var videoData = {};
        videoData['responsetype'] = 'json';
        videoData['receiver'] = $scope.idCallback;
        UserService.getUserDetail( videoData ).success( function( data ) {
            $scope.videoCallRate = data.data.video_call_rate;
            $scope.userBalance = data.data.originator_balance;
            $scope.reqBal = data.data.bal_req;
            localStorage.setItem("receiverImage_data",data.data.profile_image);
            var receiveData = JSON.stringify({'username':$scope.showMissedName,'origId':$scope.idCallback,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'','receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
            if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                $state.go('home.videoCalling',{receive_data:receiveData})
            }else{
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/videoCredits/"+$scope.showMissedName+'/'+$scope.reqBal
                });
            }
        } ).error( function ( data ) {
            $scope.networkPopup = true;
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );

     };
     $scope.callRejectback = function(){
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/affter_login"
        } );
     };


});