//++++++++++++++++++++++++++++++++videocallAccept Controller+++++++++++++++++++++++++++++++++++++++//

callApp.controller("VideoCallAcceptController", function( $scope, $filter, CallsService, $state, $stateParams, $timeout, $rootScope, constantData, GenerateRandomNo ) {
    ringtoneSound = true;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.proInfo = proData;
    if ($scope.proInfo != 2) {
        $('.perMinChargeText').css({"width":"85%"});
    }
    var receiveAftrNotiData = localStorage.getItem("caller_Data");
    var afterNoti = JSON.parse( receiveAftrNotiData );
    console.log(afterNoti);
    $scope.showNamedata = afterNoti.u_name;
    $scope.IdReceiver = afterNoti.receiverid;
    $scope.Call_Id = afterNoti.CallId;
    $scope.missedId = afterNoti.userId;
    $scope.videoCallCharge = afterNoti.video_call_rate;
    var currDate = new Date();
    $scope.newCurrDate = $filter('date')(currDate,'yyyy-MM-dd HH:mm:ss');
    var token_random = localStorage.getItem("Random_token");
    var profileImageUrl = afterNoti.image;
    var image = $('#profile_pic');
    image.css('background-image', 'url(' + profileImageUrl +')');
    var data = '';
    var userid = afterNoti.userId;
    $rootScope.time_out = setTimeout( TimeoutFunction, 62000);
    function TimeoutFunction() {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/videocallMissed/"+$scope.showNamedata+'/'+$scope.missedId
        } ); 
    }
    $scope.callAccept = function(){
        clearTimeout($rootScope.time_out);
        ringtoneSound = false;
        snd.release();
        $state.go('home.videoCalling',{receive_data:data})
    };

    $scope.callReject = function(){
        clearTimeout($rootScope.time_out);
        ringtoneSound = false;
        snd.release();
        var callerNotificationData = {};
        callerNotificationData['responsetype'] = 'json';
        callerNotificationData['receiver_id'] = $scope.IdReceiver;
        callerNotificationData['originator_id'] = userid;
        callerNotificationData['call_id'] = $scope.Call_Id;  
        callerNotificationData['event'] = 'pro_rejected';
        callerNotificationData['cur_time'] = $scope.newCurrDate;
        callerNotificationData['notify_data'] = {};
        callerNotificationData['notify_data']['sound'] = 'othersoundnoti.caf';
        CallsService.setVideoCallDetails( callerNotificationData ).success( function( data ) {
        } ).error( function ( data ) {
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/affter_login"
        } );
    };
});