//+++++++++++++++++++++++++++blockedusers page controller+++++++++++++++++++++

contactApp.controller( "BlockedUsersController", function( $scope, ContactsService, $state, $stateParams, $rootScope ) {
    var getBlockUserData = {};
    getBlockUserData['responsetype'] = 'json';
    $scope.noBlockUsersDiv = false;
    ContactsService.blockedUsers( getBlockUserData ).success( function( data ) {
        if(data.data.length == 0){
            $scope.noBlockUsersDiv = true;
            $scope.noBlockUsers = "No users are blocked.";
        }
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        }
        $scope.blockUsers = data.data;
        $scope.openPopup = function(blockId){
            $('#'+blockId).show();
            $('#overlay').show();
        };
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.blockeduserLink = function(blocked_data){
        window.plugins.nativepagetransitions.slide({
            "direction" : "left",
            "href" : "#/home/callerhistory" +blocked_data
        });
    };

    $scope.unblockUser = function(blockId,id1){
        navigator.notification.confirm(
            'Are you sure you want to UN-BLOCK this contact?',  // message
            onConfirm,
            'UN-BLOCK Contact'
        );
        function onConfirm( button ) {
            if( button == 1 ) {
                var unblockData = {};
                unblockData['responsetype'] = 'json';
                unblockData['contact_id'] = blockId;
                ContactsService.unblockUser( unblockData ).success( function( data ) {
                    // console.log(JSON.stringify(data));
                    $( '#'+id1 ).hide();
                    $( '#overlay' ).hide();
                    $( '#blockUser_'+id1 ).hide( "medium", function(){
                        $(this).remove();
                        if( $.trim( $( '.dashboard' ).text() ).length == 0 ) {
                            $('.no_msg').html("No users are blocked.").show().removeClass('ng-hide');
                        }
                    } );
                } ).error( function ( data ) {
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        }
    };
    $scope.cancelPopup = function(blockId){
        $('#'+blockId).hide();
        $('#overlay').hide();
    };

} )