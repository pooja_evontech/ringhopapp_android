////+++++++++++++++++++++++++++sendTip page controller+++++++++++++++++++++
paymentApp.controller( "SendTipController", function( $scope, PaymentService, $state, $stateParams, $rootScope, constantData ) {
var errorFlag = false;
$('#overlay').hide();
$('.ui-loader').hide();
var intRegex = /^\d+$/;
var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
$scope.credisList = constantData.creditsList();
    $scope.Credit = 20;
    $scope.errorMsg = '';
    var originator_id = $stateParams.originatrId;
    var user_detail = localStorage.getItem( "userDetail" );
    var userid = JSON.parse( user_detail ).data.id;
    var page_name = $stateParams.pagename;
    var page_name2 = 'tip';
    var tipData = {};
    tipData['responsetype'] = 'json';
    tipData['proid'] = originator_id;
    PaymentService.sendTip( tipData, 'GET' ).success( function( data ) {
        $scope.sendTipInfo = data.message;
        if ($scope.sendTipInfo == 'You must upgrade your account first.') {
            $(".addcreditsanchor").css({"top":"58px"});
            $("#successTipInfo").css({"display":"block","width":"70%"});
        }
        if( data.status == 'error' ) {
        }else{
            if(data.message == 'You are attempting to send a tip to a user you have blocked. If you do wish to send tip to this user, please first remove from block list.'){
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/viewTip/"+originator_id+"/"+page_name
                } );
            }
        }
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );

    $scope.addcreditsTip = function(){
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : "#/home/addcredits/"+originator_id+'/'+page_name2
        } );
    };

    $scope.Sendtipdata = function (inputFlag,chatInput) {
        $('#email_status').html('');
        var str = $('#input_chat').val();
        if(inputFlag == 0){
            if($('#input_chat').val() != '' && ($( '#input_chat' ).val().match( intRegex ) || $( '#input_chat' ).val().match( floatRegex ) )){
                if( $( '#input_chat' ).val() >=1 ){
                    $('#email_status').html('');
                    errorFlag = true;
                }
                else{
                    $( '#email_status' ).html('Please enter tip amount greater than or equal to 1.');
                }
            }
            else{
                $( '#email_status' ).html('Please enter a valid tip amount.');
            }
        } else {
            errorFlag = true;
            
        }
        if(errorFlag){
            $('#overlay').show();
            $('.ui-loader').show();
            var page_name = $stateParams.pagename;
            var originator_id = $stateParams.originatrId;
            var sendTipData = {};
            sendTipData['responsetype'] = 'json';
            sendTipData['tip_amount'] = chatInput;
            sendTipData['proid'] = originator_id;
            PaymentService.sendTip( sendTipData, 'POST' ).success( function( data ) {
                $('#overlay').hide();
                $('.ui-loader').hide();
                if( data.status == 'error' ) {
                    alert("error");
                } else {
                    $state.transitionTo('home.viewTip',{originatrId:originator_id,pagename:page_name,sendTipData:JSON.stringify({'balance_msg':data.data.balance_msg,'linktext':data.data.linktext,'message':data.message,'color':data.data.color})})
                }
            } ).error( function ( data ) {
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } ); 
        }
        
        
        $( '#input_chat' ).keyup( function (e) {
            if( $('#input_chat').val() != '' && ($( '#input_chat' ).val().match( intRegex ) || $( '#input_chat' ).val().match( floatRegex ) )){
                if($('#input_chat').val() >= 1){
                    $( '#email_status' ).html('');
                }
                else{
                    $( '#email_status' ).html('Please enter tip amount greater than or equal to 1.');
                }
            }
            else{
                $( '#email_status' ).html('Please enter a valid tip amount.');
            }
        });
    };
})