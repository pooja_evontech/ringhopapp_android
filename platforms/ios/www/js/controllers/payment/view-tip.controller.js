////+++++++++++++++++++++++++++viewTip page controller+++++++++++++++++++++

paymentApp.controller( "ViewTipController", function( $scope, PaymentService, $state, $stateParams, $rootScope ) {
    var page_name2 = 'tip';
    var sendTipData = {};
    var originator_id = $stateParams.originatrId;
    sendTipData['responsetype'] = 'json';
    PaymentService.sendTip( sendTipData, 'POST' ).success( function( data ) {
        $scope.sendTipInfo = data.data.balance_msg;
    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );
    $scope.sendTipInfo = JSON.parse($stateParams.sendTipData).balance_msg;
    $scope.sendTipMsg = JSON.parse($stateParams.sendTipData).message;
    $scope.link_info = JSON.parse($stateParams.sendTipData).linktext;
    if(JSON.parse($stateParams.sendTipData).color == "error"){
        $('.succesStatus1').css('color','red');
    }
    if (($scope.link_info == 'Upgrade account') || ($scope.link_info == 'Send another tip')) {
        $("#viewTipLink").css({"left":"70px"});
    }
    $scope.sendAnotherTip = function(){
        if($scope.link_info == 'Blocked users'){
            window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : "#/home/blockedusers"
            } );
        }
        if(($scope.link_info == 'Add credits' ) || ($scope.link_info == 'Upgrade account')){
            window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : "#/home/addcredits/"+$stateParams.originatrId+'/'+page_name2
            } );
        }
        if($scope.link_info == 'Send another tip'){
            window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : window.history.back()
            } );
        } 
    };
    $scope.backButton = function() {
        if($stateParams.pagename == 'reply'){
           window.plugins.nativepagetransitions.slide( {
                    "direction": 'right',
                    "href" : "#/home/reply"+$stateParams.originatrId
                } ); 
       }else if($stateParams.pagename == 'callerhistory'){
            window.plugins.nativepagetransitions.slide({
                "direction": 'right',
                "href" : "#/home/callerhistory"+$stateParams.originatrId
            });
       }else{
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
       }
    };
    
} )