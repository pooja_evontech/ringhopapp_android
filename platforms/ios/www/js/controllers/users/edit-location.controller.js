//+++++++++++++++++++++++++++editlocation page controller+++++++++++++++++++++

userApp.controller( "EditLocationController", function( $scope, UserService, $state, $stateParams, $rootScope ) {
    var flag = false;
    var locationSetData = {};
    locationSetData['responsetype'] = 'json';
    UserService.location( locationSetData ).success( function( data ) {
        $( '#userLocation' ).val(data.data.location);
    } ).error( function ( data ) {
    } );
    $scope.updateLocation = function() {
        $('#locationStatusCheck').html('');
        if( $('#userLocation').val().length == 0 ) {
            $('#locationError').html('This field is required');
            $("label[for='userLocation']").css('color','red');
            flag = false;
        } else {
            flag = true;
        }
        $( '#userLocation' ).keyup(function (e) {
            $('#locationStatusCheck').html('');
            var withoutSpace = $('#userLocation').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $('#userLocation').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $('#locationError').html('This field is required');
                    $("label[for='userLocation']").css('color','red');
                } else {
                    $('#locationError').html('');
                    $("label[for='userLocation']").removeAttr('style');
                    flag = true;
                }
            }
            if( $('#userLocation').val().length == 0 ) {
                $('#locationError').html('This field is required');
                $("label[for='userLocation']").css('color','red');
                flag = false;
            } else {
                if( withoutSpaceLength == 0 ) {
                    $('#locationError').html('This field is required');
                    $("label[for='userLocation']").css('color','red');
                } else {
                    $('#locationError').html('');
                    $("label[for='userLocation']").removeAttr('style');
                    flag = true;
                }
            }
        } );
        if( $('#userLocation').val() != '' ) {
            var withoutSpace = $('#userLocation').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $("#label[for='userLocation']").css('color','red');
                $( '#locationError' ).html('This field is required.');
                flag = false;
            } else {
                $('#locationError').html('');
                $("label[for='userLocation']").removeAttr('style');
                flag = true;
            }
        }
        if( flag == true ) {
            locationSetData['location'] = $scope.location;
            $('.ui-loader').show();
            UserService.location( locationSetData ).success( function( data ) {
                $('.ui-loader').hide();
                if( data.message == 'user not logged in' ){
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/login"
                    });
                }
                if( data.status == 'success' ) {
                    $('#locationStatusCheck').removeClass('errorStatus').addClass('succesStatus');
                } else {
                    $('#locationStatusCheck').removeClass('succesStatus').addClass('errorStatus');
                }
                $('#locationStatusCheck').html( data.message );
            } ).error( function ( data ) {
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
} )