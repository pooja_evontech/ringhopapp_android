//+++++++++++++++++++++++++++creditscomplete page controller+++++++++++++++++++++

userApp.controller( "MyLibraryController", function( $scope, UserService, $state, $stateParams, $rootScope ) {
    $scope.slides2 = [
        { id:1, image: 'img/imgA.jpg', label: 'Image 01', odd:true }
    ];
    $scope.devheight = $( window ).height();
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.showNewimg = false;
    $scope.limit = 6;
    var pre_count = 1;
    var next_count = 1;
    var imgWidth,imgHeight;
    var imgObj = [];
    $scope.imgPopup = false;
    var libraryData = {};
    var imgDimensn = [];
    libraryData['responsetype'] = 'json';
    libraryData['page'] = 1;
    $('.ui-loader').show();
    UserService.myLibrary( libraryData ).success( function( data ) {
        $('.ui-loader').hide();
        imgDimensn = [];
        $scope.libraries = data.data.myimages;
        $scope.limit = data.data.per_page;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        angular.forEach($scope.libraries , function(lib){
            $scope.isNew(lib);
        });

        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No billing history available.";
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});

            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
        }
        for ( var i = 0; i < $scope.libraries.length; i++ ) {
            getMeta( $scope.libraries[i].image_big, i, function(width, height, index) {
                var imgid = index+1;
                var item = { hgt: height, wdt: width };
                imgDimensn.push(item);
            } );

        }
    } ).error( function ( data, status, headers, config ) {
        $('.ui-loader').hide();
    } );

    $scope.prevBtn = function () {
        imgDimensn = [];
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $scope.prevDisabled = true;
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
            $('#arrow').css ( {"opacity":"0.5" });

        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            libraryData['page'] = pre_count;
            UserService.myLibrary( libraryData ).success( function( data ) {
                $scope.libraries = '';
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.libraries = data.data.myimages;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                angular.forEach($scope.libraries , function(lib){
                    $scope.isNew(lib);
                });
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                for ( var i = 0; i < $scope.libraries.length; i++ ) {
                    getMeta( $scope.libraries[i].image_big, i, function(width, height, index) {
                        var imgid = index+1;
                        var item = { hgt: height, wdt: width };
                        imgDimensn.push(item);
                    } );

                }
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        imgDimensn = [];
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            libraryData['page'] = next_count;
            UserService.myLibrary( libraryData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.libraries = '';
                $scope.limit = data.data.per_page;
                $scope.libraries = data.data.myimages;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;

                $scope.currentCount = $scope.currentPage * $scope.limit;
                angular.forEach($scope.libraries , function(lib){
                    $scope.isNew(lib);
                });

                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }
                for ( var i = 0; i < $scope.libraries.length; i++ ) {
                    getMeta( $scope.libraries[i].image_big, i, function(width, height, index) {
                        var imgid = index+1;
                        var item = { hgt: height, wdt: width };
                        imgDimensn.push(item);
                    } );

                }

            } ).error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
    $scope.popupShow = false;

    $scope.openImage = function (image) {
        $scope.slides = [];
        for ( var i = 0; i < $scope.libraries.length; i++ ) {
            if( $scope.libraries[i].media_status == 'new' && $scope.libraries[i].charge_amount != '0.00' ) {
                continue;
            }
            var item = {
                'image': $scope.libraries[i].image_big,
                'description': $scope.libraries[i].charge_amount,
                'sender_id': $scope.libraries[i].originator_id,
                'sender_name': $scope.libraries[i].sender_name,
                'imgHgt': imgDimensn[i].hgt,
                'imgWdt':imgDimensn[i].wdt
            };
            $scope.slides.push( item );
        }
        function getSlide( target, style ) {
            var i = target.length;
            return {
                id: (i + 1),
                label: 'slide #' + (i + 1),
                img: $scope.slides[i].image,
                charge: $scope.slides[i].description,
                sender_name: $scope.slides[i].sender_name,
                sender_id: $scope.slides[i].sender_id,
                imgHgt: $scope.slides[i].imgHgt,
                imgWdt: $scope.slides[i].imgWdt,
                odd: (i % 2 === 0)
            };
        }

        function addSlide( target, style, val_i ) {
            target.push( getSlide( target, style ) );
            if( target[ val_i ].img == image) {
                $scope.carouselIndex2 = target[ val_i ].id-1;
            }
        };
        function addSlides( target, style, qty ) {
            for ( var i = 0; i < qty; i++ ) {
                addSlide( target, style, i );
            }
        }
        $scope.slides2 = [];
        addSlides( $scope.slides2, 'sports', $scope.slides.length );
        $scope.popupShow = true;
    };
    $scope.isNew = function( lib ){
        if(lib.media_status == 'new' && lib.charge_amount != '0.00'){
            $scope.showNewimg = false;
        }
        else if((lib.media_status == 'purchased') || (lib.media_status == 'new' && lib.charge_amount == '0.00')) {
            $scope.showNewimg = true;
        }
    };


    $scope.closePopup = function () {
        $scope.popupShow = false;
    };
    $scope.profileLink = function (userid,uname) {
        window.plugins.nativepagetransitions.slide( {
            "direction": "left",
            "href" : "#/home/profile"+userid+"/"+uname
        } );
    };
    function getMeta(url, index, callback) {
        var img = new Image();
        img.src = url;
        img.onload = function() { callback(this.width, this.height, index); }
    }

    $scope.buyNow = function(media_id, model_id, displayname) {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'right',
            "href" : '#/home/paidphoto/'+media_id+'/'+model_id+'/'+displayname
        } );
    };

} );