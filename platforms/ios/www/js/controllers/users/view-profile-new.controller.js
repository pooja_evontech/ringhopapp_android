//+++++++++++++++++++++++++++viewprofilenew page controller+++++++++++++++++++++

userApp.controller( "ViewProfileNewController", function( $scope, UserService, ContactsService, $state, $stateParams, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.date = new Date();
    $scope.loginid = userData.id;
    $scope.userid = $stateParams.userid;
    var viewprofileData = {};
    viewprofileData['responsetype'] = 'json';
    UserService.viewProfile( $stateParams.uname+'?responsetype=json' ).success( function( data ) {
        //console.log( JSON.stringify( data ) );
        $scope.user_data = data.data;
        $scope.profileData = data.data.sociallinks;
        var user_number = data.data.ringhop_number;
        $scope.profilenew = data.data.profile_images.profile_image;
        var image = $('#profile_pic');
            image.css('background-image', 'url(' + $scope.profilenew +')');
        if(user_number !== null && user_number !== ''){
            //console.log(user_number);
            $scope.user_data.ringhop_number = "("+user_number.substr(1, 3)+") "+user_number.substr(4, 3)+"-"+user_number.substr(7, 4);
        } else {
            $scope.user_data.ringhop_number = "";
        }

        $scope.blockUser = function(){
            if($('#alreadyBlocked').html()=='Block this user') {
                navigator.notification.confirm(
                    'Are you sure you want to block this user?',  // message
                    onConfirm,
                    'Block User'
                );
            }
        };
        function onConfirm(button) {
            if( button == 1 ) {
                var blockUsrData = {};
                blockUsrData['responsetype'] = 'json';
                blockUsrData['caller_id'] = data.data.caller.id;
                ContactsService.blockUser( blockUsrData ).success( function( data ) {
                    $('#alreadyBlocked').html('user blocked');
                    $('#alreadyBlocked').removeClass('redCallHistory');
                    $('#alreadyBlocked').addClass('italicCall');
                } ).error( function ( data ) {
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            }
        }
        if( data.data.iscontact == true ) {
            $('#alreadyContact').removeClass('blueCallHistory');
            $('#alreadyContact').addClass('italicCall');
            $('#alreadyContact').html('already a contact');
        } else {
            $('#alreadyContact').removeClass('italicCall');
            $('#alreadyContact').addClass('blueCallHistory');
            $('#alreadyContact').html('Add as contact');
            $scope.addAsContact = function(){
                var addContactData = {};
                addContactData['responsetype'] = 'json';
                addContactData['caller_id'] = data.data.caller.id;
                ContactsService.addContact( addContactData ).success( function( data ) {
                    $('#alreadyContact').html('contact added');
                    $('#alreadyContact').removeClass('blueCallHistory');
                    $('#alreadyContact').addClass('italicCall');
                } ).error( function ( data ) {
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            };
        }

    } ).error( function ( data ) {
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
    } );

    $scope.openLink = function(url){
        var ref = window.open(url, '_blank');
    };

    $scope.saveContact = function () {
        if( $scope.user_data.iscontact == false ){
            var saveContactData = {};
            saveContactData['responsetype'] = 'json';
            saveContactData['caller_id'] = $stateParams.userid;
            ContactsService.addContact( saveContactData ).success( function( data ) {
                $scope.contactMsg = data.message;
                if( data.status == 'error' ){
                    $('#savecontact').html('Already a contact');
                } else {
                    $('#savecontact').html('Contact added');
                }

            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.menuloggedout = function () {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
} )
