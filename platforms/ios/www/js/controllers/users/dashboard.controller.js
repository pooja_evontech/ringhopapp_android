//+++++++++++++++++++++++++++DashBoard page controller+++++++++++++++++++++

userApp.controller( "DashboardController", function( $scope, constantData, UserService, PaymentService, $timeout, $state, $rootScope, $location  ) {
    ringtoneSound = false;
    $rootScope.previousState;
    $rootScope.currentState;
    $scope.date = new Date();
    $scope.messageCount = 0; 
    $scope.callCount = 0;
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
    });
    //$scope.upgradeDisplay = false;
    var user_detail = localStorage.getItem( "userDetail" );
    var userData = JSON.parse( user_detail ).data;
    var profileImg = localStorage.getItem( "profileData" );
    $scope.profileImg =profileImg;
     var image = $('#profile_pic');
     image.css('background-image', 'url(' + profileImg +')');
    $scope.pro_info = 2;
    if (localStorage.getItem( "dashboard_data" )) {
        var dashboard_data = localStorage.getItem( "dashboard_data" );
        var pro_info_chngd = JSON.parse( dashboard_data ).data.profile.pro;
        $scope.pro_info = pro_info_chngd;
    }
    var username = userData.displayname;
    $scope.proSection = false;
    $scope.femaleProLink = false;
    $scope.submitReviewPro = false;
    $scope.actionRequired = false;
    $scope.learnMore = true;
    $scope.yellowUpgrade = false;
    $scope.completeProSetup = false;
    $scope.updatePayment = false;
    $scope.shoutsection = false;
    $scope.earnMoneyPro = false;
    $scope.afterLearnMorePro = false;
    var getDashboardData = {};
    getDashboardData['responsetype'] = "json";
    getDashboardData['app'] = 1;
    var user_id = JSON.parse(user_detail).data.id;
    $('.ui-loader').show();
    UserService.dashboard( getDashboardData ).success( function( data ){
        $('.ui-loader').hide();
        console.log(data)
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/login"
            });
        } else{
            $scope.messageCount = data.data.unread_msg_count;  
            $scope.msgStatus = data.data.msg_read_status; 
            $scope.callCount = data.data.recent_call_count;
            var getDeviceData = {};
            getDeviceData['os'] = deviceOS;
            getDeviceData['uuid'] = uuid;
            getDeviceData['version'] = deviceVersion;
            getDeviceData['user_id'] = user_id;
            getDeviceData['device_token'] = device_token;

            UserService.savePhoneDetails( getDeviceData ).success( function( data ){
                //console.log( JSON.stringify( data ) );
            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );

            if( data.status != 'error' ) {
                $scope.textToCopy = data.data.userProfileLink;
                var user_number = data.data.profile.ringhop_number;
                localStorage.setItem("dashboard_data",JSON.stringify(data));
                $('#usersStatus').html(data.data.status);
                $('#user_loc').html(data.data.profile.profile_status);
                $scope.profileImg =data.data.profile_image_big;
                var image = $('#profile_pic');
                image.css('background-image', 'url(' + $scope.profileImg +')');
                localStorage.setItem("profileData", data.data.profile_image_big);
                var proDetail = data.data.profile.pro;
                $scope.pro_info = proDetail;
                if(proDetail == 2){
                    $scope.shoutsection = true;
                }
                if( proDetail == 0 ) {
                    $scope.earnMoneyPro = true;
                    $scope.learn_more_pro = 'Earn money';
                    $scope.pro_status = 'BECOME A PRO'
                    $scope.pro_tagline = 'CHARGE YOUR FANS & CLIENTS'
                    if( data.data.profile.gender == 'F' ) {
                        $scope.femaleProLink = true;
                    }
                } else if( proDetail == 1 ) {
                    $scope.earnMoneyPro = false;
                    $scope.afterLearnMorePro = true;
                    $scope.learn_more = 'Complete Ringhop Pro setup';
                    $scope.pro_status = 'Ringhop Pro verification process.';
                } else {
                    $scope.earnMoneyPro = false;
                    $scope.learn_more = '';
                    $scope.pro_status = '';
                }
                if( data.data.pro_data != null ) {
                    if( proDetail == 1 ) {
                        if( data.data.pro_data.pv_notes == 'admin_pending' ) {
                            $scope.submitReviewPro = true;
                        } else {
                            $scope.completeProSetup = true;
                        }
                    }
                    if( data.data.pro_data.pv_terms == -1 || data.data.pro_data.pv_idverify == -1 || data.data.pro_data.pv_idverify == -1 || proDetail == -1 ) {
                        $scope.actionRequired = true;
                    }
                }
                if( data.data.user_status == 'billingissue' ) {
                    $scope.updatePayment = true;
                }
                if( data.data.user_status == 'pending' && proDetail != 2 && data.data.profile.gender == 'M') {
                    $scope.yellowUpgrade = true;
                }
                $scope.proStatusLink = function() {
                    if( proDetail == 0 ) {
                        window.plugins.nativepagetransitions.slide({
                            "href" : "#/home/learnmore"
                        });
                    }
                    if( proDetail == 1 ) {
                        window.plugins.nativepagetransitions.slide({
                            "href" : "#/home/prodashboard"
                        });
                    }
                };
                //pro user detail in dashboard
                if( proDetail == 2 ) {
                    if (user_number) {
                        var pro_number = "("+user_number.substr(1, 3)+") "+user_number.substr(4, 3)+"-"+user_number.substr(7, 4);
                        $('#pro_number').html(pro_number);
                    }
                    $scope.proSection = true;
                    $scope.submitReviewPro = false;
                }
                /*if (proDetail != 2) {
                    if( data.data.user_status == 'pending' ) {
                        $scope.upgradeDisplay = true;
                    } else {
                        $scope.upgradeDisplay = false;
                    }
                }*/
            } else {
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/login"
                });
            } 
        }
        

    } ).error( function ( data ) {
        $('.ui-loader').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    
    $('#username').html(username);
    $scope.buyCredits = function () {
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var user_detail = localStorage.getItem( "userDetail" );
        var userid = JSON.parse( user_detail ).data.id;
        var creditData = {};
        var page_name = 'login';
        creditData['responsetype'] = 'json';
        creditData['userid'] = userid;

        PaymentService.addCredits( creditData, 'GET' ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.paymentVia == 'in_app' ) {
                    window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+userid+'/'+page_name
                });
            }
            else if(data.paymentVia == 'credit_card'){
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+userid+'/'+page_name
                });
            }
        }).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.clickForCopy = function(){
        cordova.plugins.clipboard.copy($scope.textToCopy);
    };
    $scope.shoutLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/createnewshout"
        });
    };
    $scope.settingLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/settings"
        });
    };
    $scope.statusAndAvaillink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/phonesettings"
        });
    };
    $scope.chatLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/messages"
        });
    };
    $scope.callLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/recentcalls"
        });
    };
    $scope.contactsLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/contacts"
        });
    };
    $scope.myLibrary = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/myLibrary"
        });
    };
    $scope.searchLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/search1"
        });
    };
} )