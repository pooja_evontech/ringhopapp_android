//+++++++++++++++++++++++++++forsale page controller+++++++++++++++++++++

userApp.controller( "ForSaleController", function( $scope, UserService, $state, $stateParams, $rootScope, constantData ) {
    $scope.submitSocialShow = true;
    $scope.social_link_select = [{ id:'', value:'(Select social link)'}];;
    angular.forEach(constantData.socialLinks(), function(value, key){
        var item = { id:key, value:value };
        $scope.social_link_select.push(item);
    });
    var defaultSocialData = {};
    defaultSocialData['responsetype'] = 'json';
    UserService.socialLinks( defaultSocialData ).success( function( data ) {
        $scope.socialData = data.data.sociallinks;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.submitSocialLink = function(isEdit) {
        $('#socialLinkStatus').removeClass('succesStatus').addClass('errorStatus');
        if( $( '#profile_name' ).val() != '' ) {
            $( '#errorsocialname' ).html( '' );
            $( "label[for='profile_name']" ).removeAttr('style');
            var socialLinksData = {};
            socialLinksData['responsetype'] = 'json';
            socialLinksData['link_type'] = $scope.socialLinkSelected;
            socialLinksData['profile_name'] = $scope.profile_name;
            socialLinksData['editprofilename'] = isEdit;
            $('.ui-loader').show();
            UserService.socialLinks( socialLinksData ).success( function( data ) {
                $('.ui-loader').hide();
                $state.go($state.current, {}, {reload: true});
                $scope.submitSocialShow = true;
                $scope.serverMsg = data.message;
                $scope.socialData = data.data.sociallinks;
                $scope.socialLinkSelected = $scope.social_link_select[0].id;
                $('#profile_name').val('');
                if( data.status == 'success' ){
                    $('#socialLinkStatus').removeClass('errorStatus').addClass('succesStatus');
                } else {
                    $('#socialLinkStatus').removeClass('succesStatus').addClass('errorStatus');
                }
            } ).error( function ( data ) {
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $( '#profile_name' ).keydown(function (e) {
                if( $('#profile_name').val() != '' ) {
                    $('#errorsocialname').html('');
                    $("label[for='profile_name']").removeAttr('style');
                } else {
                    $('#errorsocialname').html('This field is required.');
                    $("label[for='profile_name']").css('color','red');
                }
            } );
            $('#errorsocialname').html('This field is required.');
            $("label[for='profile_name']").css('color','red');
        }

    };


    $scope.editLink = function ( id ) {
        $("#social_"+id+",.phpdebugbar-openhandler-overlay").fadeIn();
    };
    $scope.cancel = function ( id ) {
        $("#social_"+id+",.phpdebugbar-openhandler-overlay").fadeOut();
    };
    $scope.editSocialLink = function ( id, profileName, socialsite ) {
        $scope.submitSocialShow = false;
        $scope.profile_name = profileName;
        $scope.socialLinkSelected = socialsite;
        $("#social_"+id+",.phpdebugbar-openhandler-overlay").fadeOut();
    };
    $scope.deletelink = function ( id ) {
        navigator.notification.confirm(
            'Are you sure you want to delete this link?',  // message
            onConfirm,
            'Delete link'
        );
        function onConfirm(button){
            if( button == 1 ) {
                var deleteSocialData = {};
                deleteSocialData['responsetype'] = 'json';
                deleteSocialData['linkid'] = id;
                UserService.deleteSocialLink( deleteSocialData ).success( function( data ) {
                    $("#social_"+id+",.phpdebugbar-openhandler-overlay").hide();
                    $('#link_'+id).hide("medium", function(){ $(this).remove(); });
                } ).error( function ( data ) {
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            }
        }
    };
} )
