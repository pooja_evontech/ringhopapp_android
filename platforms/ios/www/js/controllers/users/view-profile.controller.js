//+++++++++++++++++++++++++++viewprofile page controller+++++++++++++++++++++

userApp.controller( "viewprofileController", function( $scope, $http, $state, $stateParams, $rootScope ) {
    $scope.playSlide = false;
    $( ".hide-content-row" ).hide();
    $( '.save-list-section' ).hide();
    $( "#shortDesc" ).click ( function() {
        $( ".hide-content-row" ).show();
        $( '.save-list-section' ).show();
        $( "#shortDesc" ).hide();
    } );
    $scope.saveContacts = true;
    $scope.saveClick = function () {
        if($scope.saveContacts == true) {
            $scope.saveContacts = false;
        } else {
            $scope.saveContacts = true;
        }
    };
    if( $stateParams.user_id == '' || $stateParams.user_id == null ) {
    } else {
        var profileData = {};
        profileData['responsetype'] = 'json';
        profileData['uid'] = $stateParams.user_id;
        var responsePromise = $http.post( BASE_URL+"profile", JSON.stringify( profileData ) );
        responsePromise.success( function( data, status, headers, config ) {
            $scope.profileInfo = data.data;
            $scope.slides = [
                { image: $scope.profileInfo.profile_image_big, description: 'Image 00' },
                { image: 'img/imgA.jpg', description: 'Image 01' },
                { image: 'img/imgB.jpg', description: 'Image 02' },
                { image: 'img/imgC.jpg', description: 'Image 03' },
                { image: 'img/imgD.jpg', description: 'Image 04' },
                { image: 'img/imgE.jpg', description: 'Image 05' }
            ];
            function getSlide(target, style) {
                var i = target.length;
                return {
                    id: (i + 1),
                    label: 'slide #' + (i + 1),
                    img: $scope.slides[i].image,
                    odd: (i % 2 === 0)
                };
            }

            function addSlide(target, style) {
                target.push(getSlide(target, style));
            };
            $scope.carouselIndex2 = 0;
            function addSlides(target, style, qty) {
                for (var i=0; i < qty; i++) {
                    addSlide(target, style);
                }
            }
            $scope.slides2 = [];
            addSlides($scope.slides2, 'sports', 6);

            $scope.playSlider = function () {
                if ( $scope.playSlide == false ) {
                    $scope.playSlide = true;
                    $("#slider_ul1").attr("rn-carousel-auto-slide","");
                    $("#slider_ul").attr("rn-carousel-auto-slide","");
                } else {
                    $scope.playSlide = false;
                    $("#slider_ul1").removeAttr("rn-carousel-auto-slide");
                    $("#slider_ul").removeAttr("rn-carousel-auto-slide");
                }

            };
        } );
        responsePromise.error( function ( data, status, headers, config ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );

    }

    $(".slider-popup-frame").hide();
    $scope.openPopup = function () {
        $(".slider-popup-frame").show();
    };
    $scope.closePopup = function () {
        $(".slider-popup-frame").hide();
    };
    $scope.discription = function () {
        $(".hide-content-row").hide();
        $("#shortDesc").show();
        $(".save-list-section").hide();
    };
    /** angular slider**/
} );
