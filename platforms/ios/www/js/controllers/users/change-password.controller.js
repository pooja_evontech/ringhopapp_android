//+++++++++++++++++++++++++++changepwd page controller+++++++++++++++++++++

userApp.controller( "ChangePWDController", function( $scope, AuthService, $timeout, $state, $rootScope ) {
    var flag = false;
    $scope.changePasswd = function( $event ) {
        $('#changepwd_status').html( '' );
        $('#changepwd_status').removeClass('succesStatus').addClass('errorStatus');
        var password = $scope.password;
        var confirm_passwd = $scope.confirmpassword;
        var oldpassword = $scope.oldpassword;
        var new_val = "";
        var resetFormDiv = document.getElementById( 'changePasswdForm' );
        var input_id = '';
        var elementsLength = parseInt( resetFormDiv.getElementsByTagName( 'input' ).length );
        for ( var i=0; i<elementsLength; i++ ) {
            input_id = resetFormDiv.getElementsByTagName('input')[i].id;
            new_val = resetFormDiv.getElementsByTagName('input')[i].value;
            $( "label[for='"+input_id+"']" ).removeAttr('style');
            document.getElementsByTagName( "span" )[i].innerHTML = '';
            if(new_val == '') {
                document.getElementsByTagName( "span" )[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css('color','red');
            }
        }
        $( '#oldpassword' ).keydown(function (e) {
            if(e.keyCode == 8){
                $( '#changepwd_status' ).html('');
                    if( $('#oldpassword').val().length <= 6 && $('#oldpassword').val().length > 0 ) {
                        flag = false;
                        $( '#resetpwdold_error' ).html( 'Please enter at least 6 characters.' );
                    }else if( $('#oldpassword').val().length >=15){
                        flag = false;
                         $( '#resetpwdold_error' ).html( 'Please enter no more than 15 characters.' );
                    }
                    if( $('#oldpassword').val().length == 1 ){
                        $( '#resetpwdold_error' ).html( 'This field is required.' );
                        flag = false;
                        } 
                }else{
                    $( '#changepwd_status' ).html('');
                    if( $('#oldpassword').val().length <= 6 && $('#oldpassword').val().length > 0 ) {
                    flag = false;
                    $( '#resetpwdold_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#oldpassword').val().length >= 5) {
                        $( '#resetpwdold_error' ).html('');
                        $( '#resetpwdold_label' ).removeAttr('style');
                        flag = 1;
                }
            }
            

        } );
        $( '#new_password' ).keydown(function (e) {
            if(e.keyCode == 8){
                $( '#changepwd_status' ).html('');
                    if( $('#new_password').val().length <= 6 && $('#new_password').val().length > 0 ) {
                        flag = false;
                        $( '#resetpwd_error' ).html( 'Please enter at least 6 characters.' );
                    }else if( $('#new_password').val().length >=15){
                        flag = false;
                         $( '#resetpwd_error' ).html( 'Please enter no more than 15 characters.' );
                    }
                    if( $('#new_password').val().length == 1 ){
                        $( '#resetpwd_error' ).html( 'This field is required.' );
                        flag = false;
                        } 
                }else{
                    $( '#changepwd_status' ).html('');
                    if( $('#new_password').val().length <= 6 && $('#new_password').val().length > 0 ) {
                    flag = false;
                    $( '#resetpwd_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#new_password').val().length >= 5) {
                        $( '#resetpwd_error' ).html('');
                        $( '#resetpwd_label' ).removeAttr('style');
                        flag = 1;
                }
            }
            

        } );
        $( '#con_resetpassword' ).keydown(function (e) {
            if(e.keyCode == 8){
                $( '#changepwd_status' ).html('');
                    if( $('#con_resetpassword').val().length <= 6 && $('#con_resetpassword').val().length > 0 ) {
                        flag = false;
                        $( '#resetpwdcon_error' ).html( 'Please enter at least 6 characters.' );
                    }else if( $('#con_resetpassword').val().length >=15){
                        flag = false;
                         $( '#resetpwdcon_error' ).html( 'Please enter no more than 15 characters.' );
                    }
                    if( $('#con_resetpassword').val().length == 1 ){
                        $( '#resetpwdcon_error' ).html( 'This field is required.' );
                        flag = false;
                        } 
                }else{
                    $( '#changepwd_status' ).html('');
                    if( $('#con_resetpassword').val().length <= 6 && $('#con_resetpassword').val().length > 0 ) {
                    flag = false;
                    $( '#resetpwdcon_error' ).html( 'Please enter at least 6 characters.' );
                }
                if( $('#con_resetpassword').val().length >= 5) {
                        $( '#resetpwdcon_error' ).html('');
                        $( '#resetpwdcon_label' ).removeAttr('style');
                        flag = true;
                }
            }
            
        } );

        if ( $('#oldpassword').val() != '' ) {
            var withoutSpace = $('#oldpassword').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwdold_error' ).html('This field is required.');
                $( '#resetpwdold_label' ).css('color','red');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwdold_error' ).html('Please enter no more than 15 characters.');
                $( '#resetpwdold_label' ).css('color','red');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwdold_error' ).html('Please enter at least 6 characters.');
                $( '#resetpwdold_label' ).css('color','red');
            } else {
                $( '#resetpwdold_error' ).html('');
                $( '#resetpwdold_label' ).removeAttr('style');
            }
        }

        if ( $('#new_password').val() != '' ) {
            var withoutSpace = $('#new_password').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('This field is required.');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('Please enter no more than 15 characters.');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwd_label' ).css('color','red');
                $( '#resetpwd_error' ).html('Please enter at least 6 characters.');
            } else {
                $( '#resetpwd_label' ).removeAttr('style');
                $( '#resetpwd_error' ).html('');
            }
        }

        if( $('#con_resetpassword').val() != '' ) {
            var withoutSpace = $('#con_resetpassword').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#resetpwdcon_error' ).html('This field is required.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else if( withoutSpaceLength > 15 ) {
                $( '#resetpwdcon_error' ).html('Please enter no more than 15 characters.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else if( withoutSpaceLength < 6 ) {
                $( '#resetpwdcon_error' ).html('Please enter at least 6 characters.');
                $( '#resetpwdcon_label' ).css('color','red');
            } else {
                $( '#resetpwdcon_error' ).html('');
                $( '#resetpwdcon_label' ).removeAttr('style');
            }
        }

        if( $('#resetpwdold_error').html() == '' && $('#resetpwd_error').html() == '' && $('#resetpwdcon_error').html() == ''){
            flag = true;
        } else {
        	flag = false;
        }
        if( flag == true ) {
            if( password == confirm_passwd ) {
                var getResetData = {};
                getResetData['oldpassword'] = oldpassword;
                getResetData['password'] = password;
                getResetData['confirmpassword'] = confirm_passwd;
                getResetData['responsetype'] = 'json';
                $('.ui-loader').show();

                AuthService.changePassword( getResetData ).success( function( data ) {
                    $('.ui-loader').hide();
                    $('#changepwd_status').html( data.message );
                    if( data.status == 'success' ){
                        $('#changepwd_status').removeClass('errorStatus').addClass('succesStatus');
                    } else {
                        $('#changepwd_status').removeClass('succesStatus').addClass('errorStatus');
                    }

                } ).error( function ( err ) {
                    $('.ui-loader').hide();
                    if(navigator.connection.type == Connection.NONE) {
                        checkConnection();
                    }
                } );
            } else {
                $( '#changepwd_status' ).html( 'Password and confirm password mismatch' );
            }
        }
    };
} )
