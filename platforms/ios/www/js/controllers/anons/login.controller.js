//+++++++++++++++++++++++++++Login page controller+++++++++++++++++++++

anonApp.controller( "LoginController", function( $scope, AuthService, $location, $state, $rootScope ) {
    var flagMenu = 0;
    $scope.forgot_msg = false;
    $scope.reg_msg = false;
    $scope.logout_msg = false;
    $scope.deactivate_msg = false;
    $rootScope.previousState;
    $rootScope.currentState;
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
        if( $rootScope.previousState == 'home.forgotpasswordnext' && $rootScope.currentState == 'home.login' ) {
            forgotFlag = 1;
        } else {
            forgotFlag = 0;
        }
    });


    if( forgotFlag == 1 ) {
        $scope.forgot_msg = true;
    }
    
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css( 'min-height', height );
    if($rootScope.previousState == 'home.deactivation' && $rootScope.currentState == 'home.login'){
        $scope.deactivate_msg = true;
    }
    
    if($rootScope.previousState == 'home.settings' && $rootScope.currentState == 'home.login'){
        $scope.logout_msg = true;
    }
    
    if( ($rootScope.previousState == 'home.settings' && $rootScope.currentState == 'home.login') || ($rootScope.previousState == 'home' && $rootScope.currentState == 'home.login') || ($rootScope.previousState == 'home.affter_login' && $rootScope.currentState == 'home.login') || ($rootScope.previousState == 'home.forgotpassword' && $rootScope.currentState == 'home.login')) {
        var rememberme_local = localStorage.getItem( "rememberme_data" );
        if( rememberme_local ) {
            var remember_data = JSON.parse( rememberme_local );
            $scope.email = remember_data.email;
        } else {
            $scope.email="";
            $scope.password="";
        }
    }
    $scope.remember = true;
    var flag = false;
    
    $scope.signIn = function ( $event ) {
        $scope.logout_msg = false;
        var inputEmail = $( '#inputEmail3' ).val();
        var inputPasswd = $( '#inputPassword3' ).val();
        var numberPattern = /^[A-Za-z0-9]{5,}$/;
        // $( '#register_cvv2' ).keyup( function (e) {
        //     if( $( '#cvv2_error' ).html() == 'This field is required.' ) {
        //         $( '#cvv2_error' ).html('Please enter at least 3 characters.');
        //     }
        //     if( $('#register_cvv2').val().length >= 3 ) {
        //             $( '#cvv2_error' ).html('');
        //             $( '#cvv2_label' ).removeAttr('style');
        //     } else {
        //         if($('#register_cvv2').val().length == 0){
        //            $( '#cvv2_error' ).html('This field is required.'); 
        //         }else{
        //             $( '#cvv2_error' ).html('Please enter at least 3 characters.');
        //             $( '#cvv2_label' ).css('color','red');
        //         }
                
        //     }
        // } );
        $( '#inputEmail3' ).keyup( function (e) {
        $scope.logout_msg = false;
        if( $( '#email_status' ).html() == 'This field is required.' ) {
            $( '#email_status' ).html('Please enter at least 5 characters.');
        }
        if( $( '#inputEmail3' ).val().length >= 5 ) {
            if( $('#inputEmail3').val().match( numberPattern ) ) {
                $( '#email_status' ).html('');
                $( '#email_label' ).removeAttr(  'style' );
                flag = 1;
            }
        }
        else {
            if($('#inputEmail3').val().length == 0){
               $( '#email_status' ).html('This field is required.'); 
            }else{
                if ($( '#inputEmail3' ).val().length >= 5) {
                    if( $('#inputEmail3').val().match( numberPattern ) ) {
                        $( '#email_status' ).html('');
                        $( '#email_label' ).removeAttr(  'style' );
                        flag = 1;
                    }
                }else{
                    $( '#email_status' ).html('Please enter at least 5 characters.');
                    $( '#email_label' ).css('color','red');
                }
            }
                
        }
    } );
        $( "#inputPassword3" ).keydown(function (e)   {
            $scope.logout_msg = false;
            var withoutSpace = $( '#inputEmail3' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if(e.keyCode == 8){
                $( '#error_text' ).html('');
                if( $( '#inputPassword3' ).val().length <= 6 && $( '#inputPassword3' ).val().length > 0 ) {
                    flag = false;
                    $( '#passwd_status' ).html( 'Please enter at least 6 characters.' );
                }
                if( $( '#inputPassword3' ).val().length == 1 ){
                    $( '#passwd_status' ).html( 'This field is required.' );
                    flag = false;
                } 
            } else {
                $( '#error_text' ).html('');
                    if( $( '#inputPassword3' ).val().length <= 6 && $( '#inputPassword3' ).val().length > 0 ) {
                    flag = false;
                    $( '#passwd_status' ).html( 'Please enter at least 6 characters.' );
                }
                if( $( '#inputPassword3' ).val().length >= 5 ) {
                        $( '#passwd_status' ).html('');
                        $( '#pwd_label' ).removeAttr('style');
                        flag = 2;
                }
                if( $('#inputPassword3').val().indexOf(' ') >= 0 ) {
                    if( withoutSpaceLength == 0 ) {
                        $( '#passwd_status' ).html( 'This field is required.' );
                        $( '#pwd_label' ).css( 'color', 'red' );
                    }
                }
            }
        });

        if(flag == 1 && flag == 2) {
            flag = true;
        }
        if( $('#inputPassword3').val().length < 6 ) {
            $( '#passwd_status' ).html('Please enter at least 6 characters.');
            $( '#pwd_label' ).css('color','red');
            flag = false;
        }
        if( $( '#email_status' ).html() == '' && $( '#passwd_status' ).html() == '' ) {
            flag = true;
        }
        
        if( inputEmail == '' && inputPasswd == '' ) {
            flag = false;
            $( '#email_status' ).html( 'This field is required.' );
            $( '#passwd_status' ).html( 'This field is required.' );
            $( '#email_label' ).css( 'color', 'red' );
            $( '#pwd_label' ).css( 'color', 'red' );
        } else {
            if( inputEmail == '' && inputPasswd != '' ) {
                flag = false;
                $( '#email_status' ).html( 'This field is required.' );
                $( '#email_label' ).css( 'color', 'red' );
                if( inputPasswd.length < 6 ) {
                    $( '#passwd_status' ).html( 'Please enter at least 6 characters.' );
                    $( '#pwd_label' ).css( 'color','red' );
                }
            } 
            if( inputEmail != '' && inputPasswd == '' ) {
                flag = false;
                $( '#passwd_status' ).html( 'This field is required.' );
                $( '#pwd_label' ).css('color','red');
                if( !inputEmail.match( numberPattern ) ) {
                    $( '#email_status' ).html( 'Please enter at least 5 characters.' );
                    $( '#email_label' ).css( 'color', 'red' );
                }
            }
            if( flag == true ) {
                $scope.logout_msg = false;
                var getLoginData = {};
                getLoginData[ 'email' ] = $scope.email;
                getLoginData[ 'password' ] = $scope.password;
                getLoginData[ 'responsetype' ] = "json";
                getLoginData[ 'signin_remember' ] = $scope.remember;
                $( '.phpdebugbar-openhandler-overlay' ).show();
                $( '.ui-loader' ).show();
                AuthService.login( getLoginData ).success( function( data ){
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( data.status == 'success') {
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/affter_login"
                        } );
                        localStorage.setItem( "userDetail", JSON.stringify( data ) );
                        localStorage.setItem( "profileData", data.profile_image );
                        if( $scope.remember == true ) {
                            localStorage.setItem( "rememberme_data", JSON.stringify( getLoginData ) );
                            localStorage.setItem( "rememberme_flag", 'true' );
                            localStorage.setItem( "remember_appLaunch", 'true' );
                        } else {
                            localStorage.setItem( "rememberme_flag", 'false' );
                        }
                    }
                    else {
                        if(data.message == 'Your account is inactive. Contact support Team'){
                            window.plugins.nativepagetransitions.slide( {
                                "href" : "#/home/inactiveUser"
                             } )

                        }
                        else{
                            $( '#error_text' ).html( data.message );
                         } 
                        $scope.logout_msg = false;
                        $scope.forgot_msg = false;
                        $scope.deactivate_msg = false;
                    }
                }). error( function(err) {
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                });
            }
        }
    };
    $scope.register = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/register"
        } );
    };
    $scope.forgotPasswd = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/forgotpassword"
        } );
    };

} )