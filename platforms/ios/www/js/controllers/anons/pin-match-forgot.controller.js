//+++++++++++++++++++++++++++Pin Confirm for Forgot page controller+++++++++++++++++++++

anonApp.controller( "PinMatchFGController", function( $scope, AuthService, $timeout, $state, $rootScope ) {
    var flag = false;
    var pinPattern = /^[0-9]+$/;
    $scope.pinMatchFGAction = function ( $event ) {
        $( '#pinFG_error' ).html( '' );
        $( '#pin_send_status' ).remove();
        $( '#pinMatchFG_status' ).html( '' );
        var pin_number = $( '#inputEmail7' ).val();
        var getPinData = {};
        $( '#inputEmail7' ).keydown(function (e) {
            var withoutSpace = $('#inputEmail7').val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if(e.keyCode == 8){
                $( '#pinMatchFG_status' ).html('');
                withoutSpaceLength = withoutSpaceLength-1;
                    if( withoutSpaceLength <= 4 && withoutSpaceLength > 0 ) {
                        flag = false;
                        $( '#pinFG_error' ).html( 'Please enter at least 4 characters.' );
                        $( '#pinnumber_label' ).css( 'color', 'red' );
                    }else if( withoutSpaceLength > 4){
                        flag = false;
                         $( '#pinFG_error' ).html( 'Please enter no more than 4 characters.' );
                         $( '#pinnumber_label' ).css( 'color', 'red' );
                    }
                    if( withoutSpaceLength == 4) {
                            $( '#pinFG_error' ).html('');
                            $( '#pinnumber_label' ).removeAttr('style');
                            flag = 1;
                    }
                    if( withoutSpaceLength <= 0 ){
                        $( '#pinFG_error' ).html( 'This field is required.' );
                        $( '#pinnumber_label' ).css( 'color', 'red' );
                        flag = false;
                        } 
                }else{
                    $( '#pinMatchFG_status' ).html('');
                    if( withoutSpaceLength <= 3 && withoutSpaceLength > 0 ) {
                    flag = false;
                    $( '#pinFG_error' ).html( 'Please enter at least 4 characters.' );
                    $( '#pinnumber_label' ).css( 'color', 'red' );
                }else if( withoutSpaceLength > 3){
                        flag = false;
                         $( '#pinFG_error' ).html( 'Please enter no more than 4 characters.' );
                         $( '#pinnumber_label' ).css( 'color', 'red' );
                }
                if( withoutSpaceLength == 3) {
                        $( '#pinFG_error' ).html('');
                        $( '#pinnumber_label' ).removeAttr('style');
                        flag = 1;
                }
            }
        } );
        if( $( '#pinFG_error' ).html() == '') {
            flag = true;
        }
        if( pin_number != '' ) {
            var withoutSpace = $( '#inputEmail7' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( $( '#inputEmail7' ).val().indexOf(' ') >= 0 ) {
                $( '#pinnumber_label' ).css( 'color', 'red' );
                if( withoutSpaceLength == 0 ) {
                    $( '#pinFG_error' ).html( 'This field is required.' );
                }
            }
            if( withoutSpaceLength > 4 ){
                $( '#pinMatchFG_status' ).html( '' );
                flag = false;
                $( '#pinFG_error' ).html( 'Please enter no more than 4 characters.' );
                $( '#pinnumber_label' ).css( 'color', 'red' );
            }
            if( withoutSpaceLength == 4 ) {
                $( '#pinFG_error' ).html( '' );
                $( '#pinnumber_label' ).removeAttr( 'style' );
                if( !$('#inputEmail7').val().match( pinPattern ) ) {
                    $( '#pinMatchFG_status' ).html( 'The pin must be a number.' );
                    flag = false;
                } else {
                    flag = true;
                }
            }
            if( withoutSpaceLength < 4 ) {
                $( '#pinMatchFG_status' ).html('');
                flag = false;
                $( '#pinFG_error' ).html( 'Please enter at least 4 characters.' );
                $( '#pinnumber_label' ).css( 'color', 'red' );
            }

            if( flag == true ) {
                var user_phone_stored = localStorage.getItem( 'UserPhone_FG' );
                localStorage.setItem( "regUserPin", $scope.pinNumber );
                getPinData[ 'pin' ] = $scope.pinNumber;
                getPinData[ 'responsetype' ] = 'json';
                getPinData[ 'phonenumber' ] = user_phone_stored;
                $( '.phpdebugbar-openhandler-overlay' ).show();
                $( '.ui-loader' ).show();

                AuthService.pinMatch( getPinData ).success( function( data ){
                    //console.log(data);
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( data.status == 'success' ) {
                        if( data.message == 'You are blocked.' ){
                            $( '#pinMatchFG_status' ).html( data.message );
                        } else {
                            window.plugins.nativepagetransitions.slide( {
                                "href" : "#/home/forgotpasswordnext"
                            } );
                        }
                    } else {
                        $( '#pinMatchFG_status' ).html( data.message );
                    }
                } ).error( function ( err ) {
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        } else {
            $( '#pinFG_error' ).html( 'This field is required.' );
            $( '#pinnumber_label' ).css( 'color', 'red' );
        }
    };
} )
