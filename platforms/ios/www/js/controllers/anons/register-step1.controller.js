//+++++++++++++++++++++++++++Register step1 controller+++++++++++++++++++++

anonApp.controller( "RegStep1Controller", function( constantData, $scope, AuthService, $timeout, $state ) {
    $scope.showFirstNo = true;
    $scope.showotherCountry = false;
    var flag = false;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };

    var height = $( window ).height();
    $( '.wrapper, #sidebar-wrapper' ).css( 'min-height', height );
    $scope.countries = [
        { id:'1', value:'+1 United States' },
        { id:'1', value:'+1 Canada' },
        { id:'0', value:'Others(International)' }
    ];
    $scope.countryCode = $scope.countries[0];
    $scope.otherCountryList = [{ id:'', value:'Choose Country'}];
    // angular.forEach(constantData.getOtherCountryData(), function(value, key){
    //     var item = { id:value, value:key };
    //     $scope.otherCountryList.push(item);
    // });
    var checkChange = false;  

    AuthService.getAppConstants().success( function( data ){
        angular.forEach(data.data.ringhop_constants.other_country_list, function(value, key){
            var item = { id:value, value:key };
            $scope.otherCountryList.push(item);
        });
        // console.log(JSON.stringify(data));
        localStorage.setItem( "constantDataStore", JSON.stringify(data) );    
    }). error( function ( err ) {
        console.log(err)
        localStorage.setItem( "constantDataStore", JSON.stringify({data:{ringhop_constants:{}}}) );  
    });

    $scope.othercountrychng = function(item) {
        checkChange = true;
        var phoneNumber = $( '#reg_phone_number' ).val();
        var regPattern = /^\d+$/;
        $scope.accessItem = item;
        if($scope.accessItem == '1') {
            //console.log("first");
            $scope.showFirstNo = true;
            $scope.showotherCountry = false;
        } else {
            $scope.showFirstNo = false;
            $scope.showotherCountry = true;
        }
        $( ".form-control2" ).keyup(function (e)   {
            var withoutSpace = $( '#reg_phone_number' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;

            if(  $( '#reg_phone_number' ).val().length == 0 ) {
                flag = false;
                $( '#phone_status' ).html( 'Phone number is required.' );
            }
            if( $( '#reg_phone_number' ).val().length < 8 && $( '#reg_phone_number' ).val().length >0 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 8 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
            
            if( $( '#reg_phone_number' ).val().length == 8 ) {
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                }  else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
            if( $('#reg_phone_number').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#phone_status' ).html( 'Phone number is required.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }
        });
        
        if( $( '#phone_status' ).html() == '' ) {
            flag = true;
        }
        $( ".form-control1" ).keyup(function (e)   {
            var withoutSpace = $( '#reg_phone_number' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;

            if(  $( '#reg_phone_number' ).val().length == 0 ) {
                flag = false;
                $( '#phone_status' ).html( 'Phone number is required.' );
            }
            if( $( '#reg_phone_number' ).val().length < 10 && $( '#reg_phone_number' ).val().length >0 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 10 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
        
            if( $( '#reg_phone_number' ).val().length > 10 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter no more than 10 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
            if( $( '#reg_phone_number' ).val().length == 10 ) {
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                }  else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
            if( $('#reg_phone_number').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#phone_status' ).html( 'Phone number is required.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }
        });

        if( $( '#phone_status' ).html() == '' ) {
            flag = true;
        }
        
    };
    $scope.registerAction = function ( $event ) {
        var cc = $scope.countryCode;
        $( '#stateSelect_status' ).html( '' );
        $( '#phone_status' ).html( '' );
        $( '#server_status' ).html( '' );
        var regPattern = /^\d+$/;
        var countrySelect = $( '#country_code' ).val();
        var phoneNumber = $( '#reg_phone_number' ).val();
        localStorage.setItem( "regUserPhone", phoneNumber );
        if(cc.id == '1'){
            $scope.showFirstNo = true;
            $scope.showotherCountry = false;
        }else{
            $scope.showFirstNo = false; 
            $scope.showotherCountry = true;
        }
        $( ".form-control2" ).keyup(function (e)   {
            var withoutSpace = $( '#reg_phone_number' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;

            if(  $( '#reg_phone_number' ).val().length == 0 ) {
                flag = false;
                $( '#phone_status' ).html( 'Phone number is required.' );
            }
            if( $( '#reg_phone_number' ).val().length < 8 && $( '#reg_phone_number' ).val().length >0 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 8 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
            if( $( '#reg_phone_number' ).val().length == 8 ) {
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                }  else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
            if( $('#reg_phone_number').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#phone_status' ).html( 'Phone number is required.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }

        });
        
        if( $( '#phone_status' ).html() == '' ) {
            flag = true;
        }
        $( ".form-control1" ).keyup(function (e)   {
            var withoutSpace = $( '#reg_phone_number' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;

            if(  $( '#reg_phone_number' ).val().length == 0 ) {
                flag = false;
                $( '#phone_status' ).html( 'Phone number is required.' );
            }
            if( $( '#reg_phone_number' ).val().length < 10 && $( '#reg_phone_number' ).val().length >0 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 10 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }

            if( $( '#reg_phone_number' ).val().length > 10 ) {
                flag = false;
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter no more than 10 characters.' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                }
            }
            if( $( '#reg_phone_number' ).val().length == 10 ) {
                if( $( '#reg_phone_number' ).val().match( regPattern ) ) {
                    $( '#phone_status' ).html('');
                    $( '#reg_phone_label' ).removeAttr('style');
                    flag = true;
                }  else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
            if( $('#reg_phone_number').val().indexOf(' ') >= 0 ) {
                if( withoutSpaceLength == 0 ) {
                    $( '#phone_status' ).html( 'Phone number is required.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }
        });
        if( $( '#phone_status' ).html() == '' ) {
            flag = true;
        }
        if( phoneNumber != '') {
            var withoutSpace = $( '#reg_phone_number' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if(cc.id == '0'){
            $scope.showotherCountry = true;
            if( phoneNumber.length < 8 ) {
                flag = false;
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 8 characters.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                } else {
                    if(  withoutSpaceLength == 0 ) {
                        flag = false;
                        $( '#phone_status' ).html('Phone number is required.');
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    } else {
                        $( '#phone_status' ).html( 'Please enter a valid number.' );
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    }
                }

            }

            if( phoneNumber.length >= 8 ) {
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( '' );
                    $( '#reg_phone_label' ).removeAttr( 'style' );
                    flag = true;
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }  
        }else{
            $scope.showotherCountry = false;
            if( phoneNumber.length < 10 ) {
                flag = false;
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter at least 10 characters.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                } else {
                    if(  withoutSpaceLength == 0 ) {
                        flag = false;
                        $( '#phone_status' ).html('Phone number is required.');
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    } else {
                        $( '#phone_status' ).html( 'Please enter a valid number.' );
                        $( '#reg_phone_label' ).css( 'color', 'red' );
                    }
                }

            }
            if( phoneNumber.length > 10 ) {
                flag = false;
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( 'Please enter no more than 10 characters.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                }
            }

            if( phoneNumber.length == 10 ) {
                if( phoneNumber.match( regPattern ) ) {
                    $( '#phone_status' ).html( '' );
                    $( '#reg_phone_label' ).removeAttr( 'style' );
                    flag = true;
                } else {
                    $( '#phone_status' ).html( 'Please enter a valid number.' );
                    $( '#reg_phone_label' ).css( 'color', 'red' );
                    flag = false;
                }
            }
        }
        if( flag == true ) {
            var country_id = $scope.countryCode.id;
            var other_country_id = $scope.otherCountryCode;
            var getRegData = {};
            getRegData[ 'country_code' ] = country_id;
            getRegData[ 'other_country_code' ] = other_country_id;
            getRegData[ 'responsetype' ] = "json";
            getRegData[ 'phonenumber' ] = $scope.phoneNumber;
            $( '.phpdebugbar-openhandler-overlay' ).show();
            $( '.ui-loader' ).show();

            AuthService.registerStep1( getRegData ).success( function( data ){
                $( '.phpdebugbar-openhandler-overlay' ).hide();
                $( '.ui-loader' ).hide();
                if( data.status == 'success' ) {
                    responseHtml = data.message;
                    window.plugins.nativepagetransitions.slide( {
                        "href" : "#/home/pinConfirm"
                    } );
                } else {
                    if( data.message == 'Phone already registered.' ) {
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/pinsent"
                        } );
                    } else {
                        $( '#server_status' ).html( data.message );
                    }
                }
            }).error( function ( err ) {
                $( '.phpdebugbar-openhandler-overlay' ).hide();
                $( '.ui-loader' ).hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            });
        }
        } else {
            $( '#phone_status' ).html( 'Phone number is required.' );
            $( '#reg_phone_label' ).css( 'color', 'red' );
        }

    };
    $scope.loginUser = function()
    {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/login"
        });
    };
} )
