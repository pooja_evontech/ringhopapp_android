//+++++++++++++++++++++++++++Registration step2 page controller+++++++++++++++++++++

anonApp.controller( "RegStepTwoController", function( $scope, AuthService, $timeout, $state ) {
    $( '#register2Status' ).html( '' );
    var flag = false;
    var count = 0;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,3})+$/;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $( '.wrapper, #sidebar-wrapper' ).css( 'min-height', height );
    $scope.registerSubmit = function( $event ) {
        var withoutSpace = $( '#regPassword7' ).val().replace(/ /g,"");
        var withoutSpaceLength = withoutSpace.length;
        var user_phone_stored = localStorage.getItem( 'regUserPhone' );
        var new_val = "";
        var regFormDiv = document.getElementById( 'register_form2' );
        var input_id = '';
        var elementsLength = parseInt( regFormDiv.getElementsByTagName( 'input' ).length );
        if( $scope.agreement == true ) {
            $( '#agreement' ).val(1);
        }
        for ( var i=0; i<elementsLength; i++ ) {
            input_id = regFormDiv.getElementsByTagName( 'input' )[i].id;
            new_val = regFormDiv.getElementsByTagName( 'input' )[i].value;
            $( "label[for='"+input_id+"']" ).removeAttr( 'style' );
            $( '#terms_error' ).html( '' );
            $( '#terms_label' ).removeAttr( 'style' );
            document.getElementsByTagName( "span" )[i].innerHTML = '';
            if( new_val == '' ) {
                document.getElementsByTagName( "span" )[i].innerHTML = 'This field is required.';
                $( "label[for='"+input_id+"']" ).css( 'color', 'red' );
            }

            if( i == elementsLength-1 ){
                if( $( '#agreement' ).val() == 0 || $scope.agreement == false ) {
                    $( '#terms_error' ).html( 'This field is required.' );
                    $( '#terms_label' ).css( 'color', 'red' );
                }
            }
        }
        $( '#username_input' ).keydown( function (e) {
            if(e.keyCode == 8){
                if( $( '#username_input' ).val().length <= 5 && $( '#username_input' ).val().length > 0 ) {
                    flag = false;
                    $( '#username_error' ).html( 'Please enter at least 5 characters.' );
                }
                if( $( '#username_input' ).val().length == 1 ){
                    $( '#username_error' ).html( 'This field is required.' );
                    flag = false;
                } 
            }else{
                if( $( '#username_input' ).val().length <= 5 && $( '#username_input' ).val().length > 0 ) {
                    flag = false;
                    $( '#username_error' ).html( 'Please enter at least 5 characters.' );
                }
                if( $( '#username_input' ).val().length > 24 ) {
                    flag = false;
                    $( '#username_error' ).html( 'Please enter no more than 25 characters.' );
                } 
                if( $( '#username_input' ).val().length >= 4 ) {
                    // console.log("check");
                    $( '#username_error' ).html('');
                    $( '#username_label' ).removeAttr('style');
                    flag = 1;
                }
                if( ( $('#username_input').val().indexOf(' ') >= 0 ) || ( /^[a-zA-Z0-9- ]*$/.test( $( '#username_input' ).val() ) == false ) ) {
                    $( '#username_error' ).html( 'The username may only contain letters and numbers.' );
                    $( '#username_label' ).css( 'color', 'red' );
                } 
            }
        
        } );

        $( '#regPassword7' ).keydown( function (e) {
            var withoutSpace = $( '#regPassword7' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if(e.keyCode == 8){
                    if( withoutSpaceLength <= 6 && withoutSpaceLength > 0 ) {
                        flag = false;
                        $( '#password2_error' ).html( 'Please enter at least 6 characters.' );
                    }else if( withoutSpaceLength >=15){
                        flag = false;
                         $( '#password2_error' ).html( 'Please enter no more than 15 characters.' );
                    }
                    if( withoutSpaceLength == 1 ){
                        $( '#password2_error' ).html( 'This field is required.' );
                        flag = false;
                        } 
                }else{
                    if( withoutSpaceLength <= 6 && withoutSpaceLength > 0 ) {
                    flag = false;
                    $( '#password2_error' ).html( 'Please enter at least 6 characters.' );
                }else if( withoutSpaceLength >=15){
                    flag = false;
                    $( '#password2_error' ).html( 'Please enter no more than 15 characters.' );
                }
                if( withoutSpaceLength >= 5 && withoutSpaceLength < 15) {
                    $( '#password2_error' ).html('');
                    $( '#password2_label' ).removeAttr('style');
                    flag = 1;
                }
            }

        } );

        if( $( '#username_input' ).val() != '' && $( '#username_input' ).val().length < 5 ) {
            $( '#username_error' ).html( 'Please enter at least 5 characters.' );
            $( '#username_label' ).css( 'color', 'red' );
        }

        if( $( '#username_input' ).val() != '' && $( '#username_input' ).val().length > 25 ) {
            $( '#username_error' ).html( 'Please enter no more than 25 characters.' );
            $( '#username_label' ).css( 'color', 'red' );
        }

        if( $( '#regPassword7' ).val() != '' && withoutSpaceLength < 6 ) {
            $( '#password2_error' ).html( 'Please enter at least 6 characters.' );
            $( '#password2_label' ).css( 'color', 'red' );
        }

        if( $('#regPassword7').val() != '' && withoutSpaceLength > 15 ) {
            $( '#password2_error' ).html( 'Please enter no more than 15 characters.' );
            $( '#password2_label' ).css( 'color', 'red' );
        }
        if( ( $('#username_input').val().indexOf(' ') >= 0 ) || ( /^[a-zA-Z0-9- ]*$/.test( $( '#username_input' ).val() ) == false ) ) {
            $( '#username_error' ).html( 'The username may only contain letters and numbers.' );
            $( '#username_label' ).css( 'color', 'red' );
        }
        if( $( '#regPassword7' ).val().indexOf(' ') >= 0 ) {
            var withoutSpace = $( '#regPassword7' ).val().replace(/ /g,"");
            var withoutSpaceLength = withoutSpace.length;
            if( withoutSpaceLength == 0 ) {
                $( '#password2_label' ).css( 'color', 'red' );
                $( '#password2_error' ).html( 'This field is required.' );
            } else if( withoutSpaceLength > 15 ) {
                $( '#password2_label' ).css( 'color', 'red' );
                $( '#password2_error' ).html( 'Please enter no more than 15 characters.' );
            } else if( withoutSpaceLength < 6 ) {
                $( '#password2_label' ).css( 'color', 'red' );
                $( '#password2_error' ).html( 'Please enter at least 6 characters.' );
            }

        }
        if ( $( '#username_error' ).html() == '' && $( '#password2_error' ).html() == '' && $( '#terms_error' ).html() == '' ) {
            flag = true;
        }
        var getregStepTwoData = {};
        getregStepTwoData[ 'username' ] = $scope.register_username;
        getregStepTwoData[ 'email' ] = '';
        getregStepTwoData[ 'password' ] = $scope.register_password;
        getregStepTwoData[ 'gender' ] = 'M';
        getregStepTwoData[ 'register_optout' ] = $scope.agreement;
        getregStepTwoData[ 'phonenumber' ] = user_phone_stored;
        getregStepTwoData[ 'responsetype' ] = 'json';
        if ( flag == true ) {
            $( '.phpdebugbar-openhandler-overlay' ).show();
            $( '.ui-loader' ).show();

            AuthService.registerStep2( getregStepTwoData ).success( function( data ){
                localStorage.setItem( "rememberme_flag", true );
                $( '.phpdebugbar-openhandler-overlay' ).hide();
                $( '.ui-loader' ).hide();
                if( data.status == 'success' ) {
                    localStorage.setItem( "userDetail", JSON.stringify( data ) );
                    window.plugins.nativepagetransitions.slide( {
                        "href" : "#/home/affter_login"
                    } );
                } else {
                    $( '#register2Status' ).html( data.message );
                }
            } ).error( function ( err ) {
                $( '.phpdebugbar-openhandler-overlay' ).hide();
                $( '.ui-loader' ).hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        }
    };

    // === Nanda ===

    $scope.termsService = function()
    {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : '#/home/terms'
        });
    }
    $scope.privacy_Policy = function()
    {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : '#/home/privacypolicy'
        });
    }
} )