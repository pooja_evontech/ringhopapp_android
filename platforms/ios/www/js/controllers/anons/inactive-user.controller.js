
//+++++++++++++++++++++++++++Inactive user controller+++++++++++++++++++++

anonApp.controller( "InactiveUsercontroller", function( constantData, $scope, $http, $timeout, $state ) {
    //console.log(constantData.getCountryData());
    var flag = false;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $( '.wrapper, #sidebar-wrapper' ).css( 'min-height', height );
})
