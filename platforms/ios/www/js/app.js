//alert(1);
var mainApp = angular.module( "RinghopMain", [ 'ngAnimate', 'ui.bootstrap', 'ui.router', 'angular-carousel', 'AnonApp', 'UserApp', 'ContactApp', 'PaymentApp', 'MessageApp', 'ProApp', 'TermApp', 'CallApp'] );
var BASE_URL = "https://dev.ringhop.com/mobile/users/";
var SITE_URL = "https://dev.ringhop.com/";
var IMAGE_URL = "https://dev.ringhop.com/images/";
// var BASE_URL = "https://ringhop.com/mobile/users/";
// var SITE_URL = "https://ringhop.com/";
// var IMAGE_URL = "https://ringhop.com/images/";
var imageString ='';
var deviceOS = '';
var deviceVersion = '';
var uuid = ''
var deviceType = '';
var device_token = '';
var imageStringVerify = '';
var forgotFlag = 0;
var loadCount = 0;
var subscribeArr = [];
var snd;
var snd1 = '';

// var callerRingtoneLoop = false;
var pushIDFlag = false;
var ringtoneSound;

/*
# bootstrapping app with angular.bootstrap rather than ng-app="app name"...
# this will help in loading angular js pages after device ready..
*/

'use strict';

document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("resume", onResume, false);
document.addEventListener("pause", onPause, false);
function onPause(){
    console.log("enter-pause");
    cordova.plugins.notification.badge.set(0);
}
function onResume(){
    if (ringtoneSound) {
        snd.stop();
        if (deviceType == 'Android') {
            snd = new Media( 'file:///android_asset/www/sounds/ringhop_2.mp3', null , null , onStatus );
            snd.play({ numberOfLoops: 6 });
        }else{
           snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
           snd.play({ numberOfLoops: 5 });
           snd.setVolume('1.0');
        }
        // snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
        
    }
}

function onDeviceReady() {
    ringtoneSound = false;
    angular.bootstrap($('body'), ['RinghopMain']);
    window.analytics.startTrackerWithId('UA-64731097-2')
    window.analytics.trackView('ringhop')
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;
    device = window.device;
    deviceOS = device.platform;
    deviceVersion = device.version;
    uuid = device.uuid;
    deviceType = device.platform;
    console.log(deviceType);
    if (deviceType === 'Android') {
        snd = new Media( 'file:///android_asset/www/sounds/ringhop_2.mp3' , null , null , onStatus);
        snd1 = new Media( 'file:///android_asset/www/sounds/CallerRingtone.mp3', null , null , onStatus );
        console.log(snd);
    }else{
        snd = new Media( 'sounds/ringhop_2.caf' , null , null , onStatus);
        snd1 = new Media( 'sounds/CallerRingtone.caf', null , null , onStatus );
        console.log(snd);
    }
    document.addEventListener("backbutton", onBackKeyDown, false);
    function onBackKeyDown(e) {
      e.preventDefault();
    }

    var push = PushNotification.init({
        android: {
            senderID: "788156349635"
        },
        ios: {
            alert: "true",
            badge: "true",
            clearBadge: "true",
            sound: "true"
        },
        windows: {}
    });

    push.on('registration', function(data) {
        
        device_token = data.registrationId;
        
    });

    push.on('notification', function(data) {
        console.log(JSON.stringify(data));
        // console.log(localStorage.getItem("pushID"),data.additionalData.randomNo)
        if(!localStorage.getItem("pushID") || localStorage.getItem("pushID") !== data.additionalData.randomNo){
            // console.log('processing..');
            localStorage.setItem("event_name",data.additionalData.event);
            localStorage.setItem("pushID",data.additionalData.randomNo)
            var user_detail_check = localStorage.getItem( "userDetail" );
            if( user_detail_check ) {
                if ( data.additionalData.session_id ){
                    var callerdata = JSON.stringify({'u_name': data.additionalData.originator_displayname, 'sessionId': data.additionalData.session_id , 'sessionToken': data.additionalData.video_token, 'image':data.additionalData.profile_image, 'receiverid': data.additionalData.receiver_id, 'userId':data.additionalData.originator_id, 'CallId':data.additionalData.call_id,
                        'video_call_rate':data.additionalData.video_call_rate,'receiver_balance':data.additionalData.receiver_balance,'bal_req':data.additionalData.bal_req})
                    localStorage.setItem("caller_Data",callerdata);
                    window.plugins.nativepagetransitions.slide( {
                        "href" : "#/home/videocallAccept"
                    } ); 
                }
                else if(data.additionalData.event == "user_rejected") {
                    var missedUser_name = JSON.stringify(data.additionalData);
                    var missedUser_name1 = JSON.parse( missedUser_name ).originator_displayname;
                    var user_detail = localStorage.getItem( "userDetail" );
                    var userid = JSON.parse( user_detail ).data.id;
                    var Id = data.additionalData.receiver_id;
                    var Id1 = data.additionalData.originator_id;
                    VideoPlugin.endCalling(" ");
                    if( userid == Id){
                            window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/videocallMissed/"+missedUser_name1+'/'+Id1
                        } ); 
                     }
                    else{
                            window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/recentcalls"
                        } );
                    } 
                }
                else if (data.additionalData.event == "pro_rejected") {
                    VideoPlugin.endCalling("proRejectsCall");
                }
                // else if(data.additionalData.event == "ended") {
                //     var dashboard_data = localStorage.getItem( "dashboard_data" );
                //     var proData = JSON.parse( dashboard_data ).data.profile.pro;
                //     var user_detail = localStorage.getItem( "userDetail" );
                //     var userid = JSON.parse( user_detail ).data.id;
                //     var Id = data.additionalData.receiver_id;
                //     var Id1 = data.additionalData.originator_id;
                //     if (proData == 2) {
                //         var costOfCall1 = data.additionalData.call_rev_share;
                //     }else{
                //         var costOfCall1 = data.additionalData.call_cost;
                //     }
                //     var durationOfCall1 = data.additionalData.call_duration;
                //     var showNamedata1 = data.additionalData.originator_displayname;
                //     var IdReceiver1 = data.additionalData.originator_id;
                //     var identity = proData;
                //     var pg  = '';
                //        window.plugins.nativepagetransitions.slide( {
                //             "direction": 'right',
                //             "href" : '#/home/videocallEnd/'+costOfCall1+'/'+durationOfCall1+'/'+showNamedata1+'/'+IdReceiver1+'/'+identity+'/'+pg
                //         } );
                // }
                else if(data.additionalData.msg == "tip received successfully"){
                    var tip_amt = data.additionalData.tip_amount;
                    VideoPlugin.tipReceived(tip_amt);
                }else if (data.additionalData.event == "in_progress") {
                    VideoPlugin.showLowBalanceWarning("");
                }else if (data.additionalData.event == "add_credit") {
                    VideoPlugin.receivedResponseFromAPI("credit","success","0");
                }
                else{
                    if(data.additionalData.foreground === false){
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/messages"
                        } );
                    }
                }
            }
          if ( data.sound )
            {   
                // console.log(data.sound);
                // console.log('file:///android_asset/www/sounds/'+data.sound);
                snd.stop();
                if (deviceType === 'Android') {
                    snd = new Media( 'file:///android_asset/www/sounds/'+data.sound , null , null , onStatus );
                    console.log(snd);
                    if (data.sound == 'ringhop_2.mp3') {
                      console.log("androidsound");
                      snd.play({ numberOfLoops: 5 });
                      // snd.setVolume('1.0');  
                    }else{
                      snd.play();
                      // snd.setVolume('1.0');
                    }
                }else{
                    snd = new Media( 'sounds/'+data.sound , null , null , onStatus );
                    if (data.sound == 'ringhop_2.caf') {
                      snd.play({ numberOfLoops: 5 });
                      snd.setVolume('1.0');  
                    }else{
                      snd.play();
                      snd.setVolume('1.0');
                    }
                }
            }
        } 
        

        if ( data.count )
        {   console.log(data.count)
            cordova.plugins.notification.badge.set(data.count,successHandler);
            // push.setApplicationIconBadgeNumber(successHandler, errorHandler, data.count);
        }

    });

    push.on('error', function(e) {
    });
}

    function onStatus(status) {
        console.log(snd.src);
        if (snd.src == 'sounds/ringhop_2.caf') {
            // if( status == Media.MEDIA_STOPPED ) {
            //     if(loopMediaflag === true){
            //         snd.play();
            //     }
            // }
            // if(loopMediaflag === false){
            //     snd.stop();
            // }
        }

    }
    // if(snd){
    //     snd.stop();
    // }


    function successHandler (success) {
        console.log("data>>" +success)
        var data = 'responsetype=json&device_token=' + device_token;
        $.ajax({ 
            url:BASE_URL+"updatenotificount",
            data : data,
            type:"post",
            success: function(){
            },
            error : function(result) {
                console.log("error>>" +result);
            }
        }); 
    }
    function errorHandler(error){
        console.log("error>>" +error);
    }

function checkConnection() {
    console.log("checkConnection");
    var networkState = navigator.connection.type;
    loadCount = loadCount+1;
    var states = {};
    states[Connection.NONE] = 'No network connection';
        navigator.notification.alert(
        states[networkState],  // message
        alertDismissed,         // callback
        'RingHop',            // title
        'OK'                  // buttonName
    );
    // }
    
}
function alertDismissed(){
    // console.log("dismiss");
}
function back( direction, href ) {
    window.plugins.nativepagetransitions.slide({
        "direction": direction,
        "href" : href
    });
}

//+++++++++++++++++++++++++++App sub modules+++++++++++++++++++++
var anonApp = angular.module('AnonApp', []);
var userApp = angular.module('UserApp', []);
var contactApp = angular.module('ContactApp', []);
var messageApp = angular.module('MessageApp', []);
var paymentApp = angular.module('PaymentApp', []);
var proApp = angular.module('ProApp', []);
var termApp = angular.module('TermApp', []);
var callApp = angular.module('CallApp', []);

//+++++++++++++++++++++++++++App run settings+++++++++++++++++++++

mainApp.run( function( $rootScope, $window, $state, $location, $filter, $http, CallsService, AuthService ) {

    CallsService.generateSessionId().success( function( data ) {
        var secondsInDay = 86400;
        // Credentials
        var apiKey = data[0].partner_id;
        var secret = 'cf6f43cc8bef8070ec86be37183aa550e6981a74';
        var id_session = data[0].session_id;
        localStorage.setItem("dynamic_sessionID" , id_session);
        // Token Params
        var timeNow = Math.floor(Date.now()/1000);
        var expire = timeNow+secondsInDay;
        var role = "publisher";
        var data = "Second";
        TB.setLogLevel(TB.DEBUG);
        // Calculation
        data = escape(data);
        var rand = Math.floor(Math.random()*999999);
        var dataString =  "session_id="+id_session+"&create_time="+timeNow+"&expire_time="+expire+"&role="+role+"&connection_data="+data+"&nonce="+rand;
        // Encryption
        var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, secret);
        hmac.update( dataString );
        hash = hmac.finalize();
        preCoded = "partner_id="+apiKey+"&sig="+hash+":"+dataString;
        var id_token = "T1=="+$.base64.encode( preCoded )
        localStorage.setItem("dynamic_token" , id_token);
    } ).error(function(error){
    } );
    // ___________________________******************____________________________

    var networkState = navigator.connection.type;
    AuthService.getAppConstants().success( function( data ){
        // console.log(JSON.stringify(data));
        localStorage.setItem( "constantDataStore", JSON.stringify(data) ); 
    }).error(function(error){
       localStorage.setItem( "constantDataStore", JSON.stringify({data:{ringhop_constants:{}}}) );  
       if(navigator.connection.type == Connection.NONE) {
          checkConnection();
        }
    } );
    $rootScope.IMAGE_URL = IMAGE_URL;
    $.cloudinary.config( { cloud_name: 'nobetek-llc', api_key: '247749274532722' } );
    $rootScope.date = new Date();
    $rootScope.forward = function( direction, href ) {
        window.plugins.nativepagetransitions.slide( {
            "direction": direction,
            "href" : href
        } );
    };
    $rootScope.convertTZ = function(old_date, zone) {
        var d = old_date;
        $rootScope.tzArray = {
            'MIT':-39600,
            'HAST':-36000,
            'AKST':-32400,
            'AKDT':-28800,
            'PST':-28800,
            'PDT':-25200,
            'MST':-25200,
            'MDT':-21600,
            'CST':-21600,
            'CDT':-18000,
            'EST':-14400,
            'EDT':-14400,
            'PRT':-14400,
            'CNT':-12600,
            'AGT':-10800,
            'BET':-10800,
            'CAT':-3600,
            'UTC':0,
            'GMT':0,
            'WET':0,
            'WEST':3600,
            'CET':3600,
            'CEST':7200,
            'EET':7200,
            'EEST':10800,
            'ART':7200,
            'EAT':10800,
            'MET':12600,
            'NET':14400,
            'PLT':18000,
            'IST':19800,
            'BST':21600,
            'ICT':25200,
            'CTT':28800,
            'SGT':28800,
            'AWST':28800,
            'JST':32400,
            'ACST':34200,
            'AEST':36000,
            'SST':39600,
            'NZST':43200,
            'NZDT':46800
        };
        angular.forEach($rootScope.tzArray, function(filterObj , filterKey) {
            if(filterKey == zone) {
                var d1 = d.split(' ')[0].split('-');
                var d2 = d.split(' ')[1].split(':');
                var a = new Date(d1[0],d1[1]-1,d1[2],d2[0],d2[1],d2[2],0);
                var changedTime = a.getTime()+(filterObj*1000);
                $rootScope.newTime = $filter('date')(changedTime,'yyyy-MM-dd HH:mm:ss');
            }
        } );
        return $rootScope.newTime;
    };
} );