mainApp.constant('CONSTANTS', {
	shoutsPerPage: 4, 
	BASE_URL: 'https://dev.ringhop.com/mobile/users/',
	SITE_URL: 'https://dev.ringhop.com/',
	IMAGE_URL: 'https://dev.ringhop.com/images/'
	// BASE_URL: 'https://ringhop.com/mobile/users/',
	// SITE_URL: 'https://ringhop.com/',
	// IMAGE_URL: 'https://ringhop.com/images/'
});