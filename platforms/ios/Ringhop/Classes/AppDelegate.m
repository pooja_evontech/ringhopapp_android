/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  Ringhop
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import <NewRelicAgent/NewRelicAgent.h>
#import <NewRelicAgent/NewRelic.h>
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
    [NewRelicAgent startWithApplicationToken:@"AAc3d24e2936b1c504ccb57116e3a64e7331dbfdaa"];
    [NewRelicAgent enableCrashReporting:YES];
    [Fabric with:@[[Crashlytics class]]];
    //check if app was launched via push
//    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//    if (notification) {
//        [[[UIAlertView alloc]initWithTitle:@"notification aya" message:[NSString stringWithFormat:@"%@",launchOptions] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil] show];
//        
//    } else {
//        NSLog(@"app did not recieve notification");
//         [[[UIAlertView alloc]initWithTitle:@"RingHOP" message:@"app did not recieve notification" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil] show];
//    }
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(handleAppResigningActive:)
//                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self         selector:@selector(handleAppResigningActive:) name:UIApplicationWillResignActiveNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppResigningActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppResigningActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppResigningActive:) name:UIApplicationWillEnterForegroundNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppResigningActive:) name:UIApplicationWillTerminateNotification object:nil];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

//-(void)handleAppResigningActive:(NSNotification*)note
//{
//    
//}

//-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
//    
//}


@end
