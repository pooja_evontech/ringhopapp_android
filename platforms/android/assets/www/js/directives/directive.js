mainApp.directive("loadMoreData", [function() {
         return {
            restrict: 'ACE',
            link: function($scope, element, attrs, ctrl) {
                // alert("check");
                var raw = element[0];
                element.scroll(function() {
                    if(raw.scrollTop == 0){
                       $scope.$apply("loadMoreData(event)");
                    }
                    // if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight){
                    //     $scope.$apply("loadMoreData(event)");
                    // }
                });
            }
        };

}]);

mainApp.directive('onTouch', function() {
  console.log('touch');
  return {
        restrict: 'A',
        link: function(scope, elm, attrs) {
            var ontouchFn = scope.$eval(attrs.onTouch);
            // elm.bind('touchstart', function(evt) {
            //     scope.$apply(function() {
            //         ontouchFn.call(scope, evt.which);
            //     });
            // });
            elm.bind('click', function(evt){
                    scope.$apply(function() {
                        ontouchFn.call(scope, evt.which);
                    });
            });
        }
    };
});

mainApp.directive('allowDecimalNumbers', function () { 
// console.log("call made"); 
    return {  
        restrict: 'A',  
        link: function (scope, elm, attrs, ctrl) {  
            elm.on('keydown', function (event) {  
                var $input = $(this);  
                var value = $input.val();  
                value = value.replace(/[^0-9\.]/g, '')  
                var findsDot = new RegExp(/\./g)  
                var containsDot = value.match(findsDot)  
                if (containsDot != null && ([46, 110, 190].indexOf(event.which) > -1)) {  
                    event.preventDefault();  
                    return false;  
                }  
                $input.val(value);  
                if (event.which == 64 || event.which == 16) {  
                    // numbers  
                    return false;  
                } if ([8, 13, 27, 37, 38, 39, 40, 45, 110].indexOf(event.which) > -1) {  
                    // backspace, enter, escape, arrows  
                    return true;  
                } else if (event.which >= 48 && event.which <= 57) {  
                    // numbers  
                    return true;  
                } else if (event.which >= 96 && event.which <= 105) {  
                    // numpad number  
                    return true;  
                } else if ([45, 46, 110, 190].indexOf(event.which) > -1) {  
                    // dot and numpad dot  
                    return true;  
                } else {  
                    event.preventDefault();  
                    return false;  
                }  
            });  
        }  
    }  
});  
mainApp.directive("slideDirective", function() {
    // console.log('slide');
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            $(elem).swipe( {
            // click:function(event,target){
            //     console.log("should click");
            // },
            swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection)
            {
                // console.log('execute>>',direction);
                if(direction  == 'left'){
                    var dis  = parseInt(distance);
                    if(dis > 100){
                        var cc = 100 * -1;
                    } else {
                        var cc = parseInt(distance) * -1;
                    }
                    $(this).css('left', cc +'px'); 
                }
                if(direction  == 'right'){
                    var left=$(this).css('left').split("px"); 
                    if(parseInt(left[0]) == 0){
                        $(this).css('left', '0px'); 
                    }
                    else{
                        var dis  = parseInt(distance);
                        if(dis < 100){
                            var cc =(100- parseInt(distance)) * -1;
                        } else {
                            var cc = 100 - 100 * 1;
                        }
                        $(this).css('left', cc +'px'); 
                    }
                }
                if(phase == "end"){
                     // console.info (phase)
                    var left=$(this).css('left').split("px"); 
                    if(parseInt(left[0]) <= -70){
                        $(this).css('left', '-100px');  
                    }else{
                        $(this).css('left', '0px');
                    }
                }
            },
            // threshold:0,
            allowPageScroll:"vertical"
          });
        }
    }
});
mainApp.directive("limitTo", [function() {
    console.log('limit');
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).bind('keyup', function (e) {
                console.log('key>>' + this.value.length);
                if (this.value.length == limit)
                console.log('if>>' + this.value.length);
                return false;
            });
            // angular.element(elem).on("keydown", function(e) {
            //     console.log(limit,this.value.length);
            //     if (this.value.length == limit) e.preventDefault();
            // });
        }
    }
}]);

