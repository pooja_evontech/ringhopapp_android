//alert('1');
mainApp.config(function($stateProvider, $urlRouterProvider,  $httpProvider) {
        var rememberme_check = localStorage.getItem( "remember_appLaunch" );
        //alert(rememberme_check);
        if( rememberme_check ) {
            $urlRouterProvider.otherwise( '/home/affter_login' );
        } else {
            $urlRouterProvider.otherwise( '/home' );
        }

    } ).run( ['$rootScope', '$state', '$stateParams', function ( $rootScope,   $state,   $stateParams ) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $state.transitionTo( 'home' );
    } ] );

anonApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('home', {
            url: '/home',
            views: {
                main: {
                    templateUrl: 'templates/anons/home.html',
                    controller: 'HomeController'
                }
            }
        })

       .state('home.login', {
            url: '/login',
            templateUrl: 'templates/anons/login.html',
            controller: 'LoginController'
        })

        .state('home.register', {
            url: '/register',
            templateUrl: 'templates/anons/register.html',
            controller: 'RegStep1Controller'
        })

        .state('home.forgotpassword', {
            url: '/forgotpassword',
            templateUrl: 'templates/anons/forgotpassword.html',
            controller: 'ForgotPWDController'
        })

        .state('home.forgotpasswordnext', {
            url: '/forgotpasswordnext',
            templateUrl: 'templates/anons/forgotpasswordnext.html',
            controller: 'ResetPWDController'
        })

        .state('home.pinConfirm', {
            url: '/pinConfirm',
            templateUrl: 'templates/anons/pinConfirm.html',
            controller: 'PinMatchController'
        })

        .state('home.pinConfirmForgot', {
            url: '/pinConfirmForgot',
            templateUrl: 'templates/anons/pinConfirmForgot.html',
            controller: 'PinMatchFGController'
        })

        .state('home.register_step2', {
            url: '/register_step2',
            templateUrl: 'templates/anons/register_step2.html',
            controller: 'RegStepTwoController'
        })

        .state('home.pinsent', {
            url: '/pinsent',
            templateUrl: 'templates/anons/pinsent.html',
            controller: 'PinSentController'
        })

        .state('home.search', {
            url: '/search',
            templateUrl: 'templates/anons/search.html',
            controller: 'searchController'
        })

        .state('home.inactiveUser', {
            url: '/inactiveUser',
            templateUrl: 'templates/anons/inactiveUser.html',
            controller: 'InactiveUsercontroller'
        } )
    } )

userApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.affter_login', {
            url: '/affter_login',
            templateUrl: 'templates/users/affter_login.html',
            controller: 'DashboardController'
        })

        .state('home.phonesettings', {
            url: '/phonesettings',
            templateUrl: 'templates/users/phonesettings.html',
            controller: 'AvailabilityController'
        })

        .state('home.resetpassword', {
            url: '/resetpassword',
            templateUrl: 'templates/users/resetpassword.html',
            controller: 'ChangePWDController'
        })

        .state('home.editProfile', {
            url: '/editProfile',
            templateUrl: 'templates/users/editProfile.html',
            controller: 'EditProfileController'
        })
        .state('home.settings', {
            url: '/settings',
            templateUrl: 'templates/users/settings.html',
            controller: 'SettingsController'
        })
        .state('home.about', {
            url: '/about',
            templateUrl: 'templates/users/about.html',
        })
        .state('home.profileoptions', {
            url: '/profileoptions',
            templateUrl: 'templates/users/profileoptions.html',
            controller: 'ProfileOptionsCtrl'
        })
        .state('home.editlocation', {
            url: '/editlocation',
            templateUrl: 'templates/users/editlocation.html',
            controller: 'EditLocationController'
        })

        .state('home.editaboutme', {
            url: '/editaboutme',
            templateUrl: 'templates/users/editaboutme.html',
            controller: 'EditAboutMeController'
        })
        .state('home.viewprofile', {
            url: '/viewprofile:user_id',
            templateUrl: 'templates/users/viewprofile.html',
            controller: 'viewprofileController',
            resolve:{
                user_id: ['$stateParams', function($stateParams){
                    return $stateParams.user_id;
                }]
            }
        })
        .state('home.forsale', {
            url: '/forsale',
            templateUrl: 'templates/users/forsale.html',
            controller: 'ForSaleController'
        })

        .state('home.viewprofilenew', {
            url: '/profile:userid/{uname}',
            templateUrl: 'templates/users/profilenew.html',
            controller: 'ViewProfileNewController'
        } )

        .state('home.socialprofile', {
            url: '/socialprofile:userid',
            templateUrl: 'templates/users/socialprofile.html',
            controller: 'socialnewController'
        } )
        .state('home.myLibrary', {
            url: '/myLibrary',
            templateUrl: 'templates/users/myLibrary.html',
            controller: 'MyLibraryController'
        } )
        .state('home.deactivation', {
            url: '/deactivation',
            templateUrl: 'templates/users/deactivation.html',
            controller: 'DeactivationController'
        } )

        .state('home.notification', {
            url: '/notification',
            templateUrl: 'templates/users/notification.html',
            controller: 'NotificationController'
        } )
    } )

messageApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.messages', {
            url: '/messages',
            templateUrl: 'templates/messages/messages.html',
            controller: 'RecentMessageController'
        })

        .state('home.reply', {
            url: '/reply:messageId',
            templateUrl: 'templates/messages/reply.html',
            controller: 'ReplyController',
            resolve:{
                messageId: ['$stateParams', function($stateParams){
                    return $stateParams.messageId;
                }]
            }
        })
        .state('home.paidphoto', {
            url: '/paidphoto/:media_id/{model_id}/{displayname}',
            templateUrl: 'templates/messages/paidphoto.html',
            controller: 'PaidPhotoController'
        })

        .state('home.sendPhoto', {
            url: '/sendPhoto:recieverId',
            templateUrl: 'templates/messages/sendPhoto.html',
            controller: 'SendPhotoController',
            resolve:{
                recieverId: ['$stateParams', function($stateParams){
                    return $stateParams.recieverId;
                }]
            }
        })
        .state('home.paidphotoadd', {
            url: '/paidphotoadd/:originatrId/{fromPgName}',
            templateUrl: 'templates/messages/paidphotoadd.html',
            controller: 'PaidPhotoAddController'
        } )
    } )

paymentApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.purchasePaidPhoto', {
            url: '/purchasePaidPhoto?displayname&shout&media_id&model_id',
            templateUrl: 'templates/payment/purchasePaidPhoto.html',
            controller: 'PurchaseController'
        })
        .state('home.upgrade', {
            url: '/upgrade',
            templateUrl: 'templates/pro/upgrade.html',
            controller: 'ProUpgradeController'
        })

        .state('home.paymentoption', {
            url: '/paymentoption',
            templateUrl: 'templates/pro/paymentoption.html',
            controller: 'PaymentOptionController'
        })
        .state('home.updatepayment', {
            url: '/updatepayment',
            templateUrl: 'templates/payment/updatepayment.html',
            controller: 'proUpgradeController'
        })
        .state('home.sendTip', {
            url: '/sendTip/:originatrId/{pagename}',
            templateUrl: 'templates/payment/sendTip.html',
            controller: 'SendTipController'
        })
        .state('home.addcredits', {
            url: '/addcredits/:originatrId',
            templateUrl: 'templates/payment/addcredits.html',
            controller: 'AddCreditsController'
        } )
        .state('home.creditscomplete', {
            url: '/creditscomplete/:originatrId',
            templateUrl: 'templates/payment/creditscomplete.html',
            controller: 'CreditsCompleteController'
        } )
        .state('home.viewcredit', {
            url: '/viewcredit',
            templateUrl: 'templates/payment/viewcredit.html',
            controller: 'ViewCreditController'
        } )
        .state('home.viewTip', {
            url: '/viewTip/:originatrId/{pagename}/{sendTipData}',
            templateUrl: 'templates/payment/viewTip.html',
            controller: 'ViewTipController'
        } )
        .state('home.billinghistory', {
            url: '/billinghistory',
            templateUrl: 'templates/payment/billinghistory.html',
            controller: 'BillingHistoryController'
        } )
    } )

proApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.createnewshout', {
            url: '/createnewshout',
            templateUrl: 'templates/pro/createnewshout.html',
            controller: 'CreateNewShoutController'
        })

        .state('home.suggestPro', {
            url: '/suggestPro',
            templateUrl: 'templates/pro/suggestPro.html',
            controller: 'SuggestProController'
        })
        
        .state('home.PhotoShout', {
            url: '/PhotoShout',
            templateUrl: 'templates/pro/sendPhotoShout.html',
            controller: 'SendPhotoShoutController'
        })
        
        .state('home.getProNo', {
            url: '/getProNo',
            templateUrl: 'templates/pro/getProNo.html',
            controller: 'GetProNoController'
        })

        .state('home.learnmore', {
            url: '/learnmore',
            templateUrl: 'templates/pro/learnmore.html',
            controller: 'LearnMoreController'
        })

        .state('home.prodashboard', {
            url: '/prodashboard',
            templateUrl: 'templates/pro/prodashboard.html',
            controller: 'ProDashboardController'
        })

        .state('home.acceptterms', {
            url: '/acceptterms',
            templateUrl: 'templates/pro/acceptterms.html',
            controller: 'AcceptTermsController'
        })

        .state('home.accepttermscomplete', {
            url: '/accepttermscomplete',
            templateUrl: 'templates/pro/accepttermscomplete.html',
            controller: 'AcceptTermsCompleteController'
        })

        .state('home.verifyidentity', {
            url: '/verifyidentity',
            templateUrl: 'templates/pro/verifyidentity.html',
            controller: 'VerifyIdentityController'
        })

        .state('home.howitworks', {
            url: '/howitworks',
            templateUrl: 'templates/pro/howitworks.html'
        })

        .state('home.become_Pro', {
            url: '/become_Pro',
            templateUrl: 'templates/pro/become_Pro.html',
            controller: 'HowItWorksCtrl'
        })

        .state('home.faqs', {
            url: '/faqs',
            templateUrl: 'templates/pro/faqs.html',
            controller: 'FaqCtrl'
        })

        .state('home.verification', {
            url: '/verification',
            templateUrl: 'templates/pro/verification.html'
        })

        .state('home.paymentoptcomplete', {
            url: '/paymentoptcomplete',
            templateUrl: 'templates/pro/paymentoptcomplete.html'
        })

        .state('home.prosettings', {
            url: '/prosettings',
            templateUrl: 'templates/pro/prosettings.html',
            controller: 'ProSettingsController'
        })
        .state('home.mygreeting', {
            url: '/mygreeting:ringhop_number',
            templateUrl: 'templates/pro/mygreeting.html',
            controller: 'MyGreetingController'
        })

        .state('home.setrates', {
            url: '/setrates',
            templateUrl: 'templates/pro/setrates.html',
            controller: 'SetRatesController'
        })

        .state('home.stats', {
            url: '/stats',
            templateUrl: 'templates/pro/stats.html',
            controller: 'StatsController'
        })
        .state('home.statscalls', {
            url: '/statscalls/:calltype',
            templateUrl: 'templates/pro/statscalls.html',
            controller: 'StatsCallsController'
        } )

        .state('home.statsmessages', {
            url: '/statsmessages',
            templateUrl: 'templates/pro/statsmessages.html',
            controller: 'StatsMessagesController'
        } )

        .state('home.statsphotos', {
            url: '/statsphotos',
            templateUrl: 'templates/pro/statsphotos.html',
            controller: 'StatsPhotosController'
        } )

        .state('home.TipHistory', {
            url: '/TipHistory',
            templateUrl: 'templates/pro/TipHistory.html',
            controller: 'TipHistoryController'
        } )
        .state('home.shoutsHistory', {
            url: '/shoutsHistory',
            templateUrl: 'templates/pro/shoutsHistory.html',
            controller: 'ShoutsHistoryController'
        } )
        .state('home.userstats', {
            url: '/userstats',
            templateUrl: 'templates/pro/userstats.html',
            controller: 'UserStatsController'
        } )

        .state('home.statssummary', {
            url: '/statssummary',
            templateUrl: 'templates/pro/statssummary.html',
            controller: 'StatsSummaryController'
        } )

        .state('home.contactus', {
            url: '/contactus',
            templateUrl: 'templates/pro/contactus.html',
            controller: 'contactUsController'
        } )
        .state('home.forwardingsettings', {
            url: '/forwardingsettings',
            templateUrl: 'templates/pro/forwardingsettings.html',
            controller: 'ForwardingSettingsCtrl'
        } )

        .state('home.trialsettings', {
            url: '/trialsettings',
            templateUrl: 'templates/pro/trialsettings.html',
            controller: 'TrialSettingsCtrl'
        } )
    } )

termApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.support', {
            url: '/support',
            templateUrl: 'templates/terms/support.html',
            controller: 'SupportController'
        } )

        .state('home.privacypolicy', {
            url: '/privacypolicy',
            templateUrl: 'templates/terms/privacypolicy.html',
            controller: 'PrivacyPolicyController'
        } )

        .state('home.terms', {
            url: '/terms',
            templateUrl: 'templates/terms/terms.html',
            controller: 'TermsController'
        } )
    } )

callApp.config(function($stateProvider, $urlRouterProvider) {
     $stateProvider
        .state('home.videoCalling', {
            url: '/videoCalling/:receive_data',
            templateUrl: 'templates/videocalls/videoCalling.html',
            controller: 'VideoCallingController'
        })
        .state('home.videoCredits', {
            url: '/videoCredits/:user_name/{Bal_req}',
            templateUrl: 'templates/videocalls/videoCredits.html',
            controller: 'VideoController'
        })
        .state('home.videocallAccept', {
            url: '/videocallAccept',
            templateUrl: 'templates/videocalls/videocallAccept.html',
            controller: 'VideoCallAcceptController'
        })
        .state('home.videocallEnd', {
            url: '/videocallEnd/:totalcallcost/{totalcalltime}/{userName}/{idReceive}/{check_identity}/{infoOfpage}',
            templateUrl: 'templates/videocalls/videocallEnd.html',
            controller: 'VideoCallEndController'
        })
        .state('home.videoProReject', {
            url: '/videoProReject/:user_name/{state_page}/{id_receiver1}',
            templateUrl: 'templates/videocalls/videoProReject.html',
            controller: 'VideoProRejectController'
        })
        // .state('home.videoCallBusy', {
        //     url: '/videoCallBusy/:user_name/{state_page}/{id_receiver1}',
        //     templateUrl: 'templates/videocalls/videoCallBusy.html',
        //     controller: 'VideoCallBusyController'
        // })
        .state('home.videocallMissed', {
            url: '/videocallMissed/:user_name/{callBackid}',
            templateUrl: 'templates/videocalls/videocallMissed.html',
            controller: 'VideoCallMissedController'
        })
        .state('home.recentcalls', {
            url: '/recentcalls',
            templateUrl: 'templates/videocalls/recentcalls.html',
            controller: 'RecentCallController'
        })
        .state('home.callerhistory', {
            url: '/callerhistory:userId',
            templateUrl: 'templates/videocalls/callerhistory.html',
            controller: 'CallerHistoryController',
            resolve:{
                userId: ['$stateParams', function($stateParams){
                    return $stateParams.userId;
                }]
            }
        })
    } )

contactApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home.contacts', {
            url: '/contacts',
            templateUrl: 'templates/contacts/contacts.html',
            controller: 'ContactsController'
        })
        .state('home.blockedusers', {
            url: '/blockedusers',
            templateUrl: 'templates/contacts/blockedusers.html',
            controller: 'BlockedUsersController'
        })
        .state('home.addProContact', {
            url: '/addProContact',
            templateUrl: 'templates/contacts/addProContact.html',
            controller: 'AddProContactController'
        })
    } )