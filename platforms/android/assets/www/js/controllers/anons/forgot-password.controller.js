//+++++++++++++++++++++++++++Forgot Password page controller+++++++++++++++++++++

anonApp.controller( "ForgotPWDController", function( $scope, AuthService, $timeout, $state ) {
    var flag = false;
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $( '.wrapper, #sidebar-wrapper' ).css( 'min-height', height );
    $scope.forgotPwdAction = function ( $event ) {
        $( '#number_error' ).html( '' );
        $( '#forgot_status' ).html( '' );
        var pattern = /^[A-Za-z0-9]{10,}$/;
        var mobile_number = $( '#inputEmail5' ).val();
        var withoutSpace = $( '#inputEmail5' ).val().replace(/ /g,"");
        var withoutSpaceLength = withoutSpace.length;
        $( '#inputEmail5' ).keyup( function (e) {
            if( $( '#number_error' ).html() == 'Phone number is required.' ) {
                $( '#number_error' ).html('Please enter at least 10 characters.');
            }
            if( $( '#inputEmail5' ).val().length >= 10 ) {
                if( $('#inputEmail5').val().match( pattern ) ) {
                    $( '#number_error' ).html('');
                    $( '#phone_label' ).removeAttr('style');
                    flag = true;
                }
            }else {
                if($('#inputEmail5').val().length == 0){
                   $( '#number_error' ).html('This field is required.'); 
                }else{
                    $( '#number_error' ).html('Please enter at least 10 characters.');
                    $( '#phone_label' ).css('color','red');
                }
                
            }
        } );
        
        if($( '#number_error' ).html() == '') {
            flag = true;
        }
        if( mobile_number != '' ) {
            if( mobile_number.length < 10 ) {
                flag = false;
                $( '#number_error' ).html( 'Please enter at least 10 characters.' );
                $( '#phone_label' ).css( 'color', 'red' );
            }
            if( flag == true ) {
                localStorage.setItem( "UserPhone_FG", mobile_number );
                var getForgotData = {};
                getForgotData[ 'phonenumber' ] = $scope.mobileNumber;
                getForgotData[ 'responsetype' ] = "json";
                $( '.phpdebugbar-openhandler-overlay' ).show();
                $( '.ui-loader' ).show();

                AuthService.forgotPassword( getForgotData ).success( function( data ) {
                    //console.log(data);
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( data.status == 'success' ) {
                        window.plugins.nativepagetransitions.slide( {
                            "href" : "#/home/pinConfirmForgot"
                        } );
                    } else {
                        $( '#forgot_status' ).html( data.message );
                    }
                } ).error( function ( err ) {
                    $( '.phpdebugbar-openhandler-overlay' ).hide();
                    $( '.ui-loader' ).hide();
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        } else {
            $( '#number_error' ).html( 'Phone number is required.' );
            $( '#phone_label' ).css( 'color', 'red' );
        }
    };
} )