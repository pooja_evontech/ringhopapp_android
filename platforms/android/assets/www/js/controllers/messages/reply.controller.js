//+++++++++++++++++++++++++++Reply page controller+++++++++++++++++++++

messageApp.controller( "ReplyController", function( $scope, moment, UserService, MessagesService, $state, $stateParams, $timeout, $rootScope, ContactsService ) {
    var subscribeCount = 0;
    var start = 25;
    var count = 25;
    var loadstatus = 0;
    var flagvalue = false;
    $scope.showtipproInfo = false;
    $scope.showVideoIcon = false;
    $( '.phpdebugbar-openhandler-overlay').hide();
    var deviceHeight = $( window ).height();
    var headerHeight = $('#header').height();
    var footerHeight = $('#footer').height();
    var innerheight = deviceHeight - ( headerHeight + footerHeight );
    $( '#msg_container' ).height( innerheight );
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    $scope.userstatusData = JSON.parse( dashboard_data ).data.user_status;
    var userId = userData.id;
    var originator_id = $stateParams.messageId;
    $scope.user_id = originator_id;
    
    if( parseInt( userId ) < parseInt( originator_id ) ) {
        channel1 = userId+'X'+originator_id;
    } else {
        channel1 = originator_id+'X'+userId;
    }
    // console.log(channel1);

    $.cloudinary.config({ cloud_name: 'nobetek-llc', api_key: '247749274532722'});
    var pubnub = PUBNUB.init({
        publish_key: 'pub-c-d69a065d-1b7c-4619-baf8-f240601221bd',
        subscribe_key: 'sub-c-f7bbad58-bf8d-11e3-a219-02ee2ddab7fe',
        auth_key: userId,
        uuid: userId,
        ssl: true
    });
    pubnub.state({
        channel  : channel1,
        uuid     : userId,
        callback : function(m){
        },
        error: function(m){
            // console.log(JSON.stringify(m));
        }

    });

    $timeout( function() {
        $( '#msg_container' ).scrollTop( $(document).height()+9400 );
    }, 5000 );
    $scope.popupShow = false;
    $(".loaderId").hide();
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var user_status = JSON.parse( dashboard_data ).data.user_status;
    $scope.proInfo = proData;
    $scope.imgPopupShow = false;
    $scope.networkPopup = false;
    $scope.blockPopup = false;
    $scope.gotoBlockUsers = false;
    $scope.closePopupBlock = false;
    $scope.messagePopup = false;
    function getCurrentTime(utc){
        var utcdateSubs = utc;
        var timestampSubs = moment.unix(utcdateSubs).format("YYYY-MM-DD HH:mm:ss");
        var momentTimeobjSubs = moment(timestampSubs);
        var dateFormatSubs = 'MM/DD/YYYY';
        var timeformatSubs = moment().localeData().longDateFormat('LT')
        var localDateSubs = momentTimeobjSubs.format(dateFormatSubs);
        var localTimeSubs = momentTimeobjSubs.format(timeformatSubs);
        var dateReceivedFormatSubs = momentTimeobjSubs.format('YYYY-MM-DD');
        var getYesterdayDateMSubs = moment().subtract(1, 'day');
        var getYesterdayDateSubs = getYesterdayDateMSubs.format('YYYY-MM-DD');
        var weekDateMSubs = moment().subtract(8, 'days');
        var weekDateSubs = weekDateMSubs.format('YYYY-MM-DD');
        var dateTimeSubs;
        var getPreviousdayTime;
        if (moment(dateReceivedFormatSubs).isSame(currentTime)) {
            dateTimeSubs = localTimeSubs;
            getPreviousdayTime = '';
        }else if (moment(getYesterdayDateSubs).isSame(dateReceivedFormatSubs)) {
            dateTimeSubs = 'Yesterday';
            getPreviousdayTime = localTimeSubs;
        }else if (moment(dateReceivedFormatSubs).isBetween(weekDateSubs,getYesterdayDateSubs)) {
            dateTimeSubs = moment(dateReceivedFormatSubs).format('dddd');
            getPreviousdayTime = localTimeSubs;
        }else{
            dateTimeSubs = localDateSubs;
            getPreviousdayTime = localTimeSubs;
        }
        var formatted_date = dateTimeSubs;
        var formatted_time = getPreviousdayTime;
        // return dateTimeSubs;
        return [formatted_date, formatted_time];
    }


    $scope.videoStart = function(){
            $('.ui-loader').show();
            $('#overlay').hide();
            $scope.popupShow =  false;
            var reply_detail = localStorage.getItem( "reply_data" );
            var replyData = JSON.parse( reply_detail ).data;
            $scope.replyChats1 = replyData;
            $scope.username1 = replyData.originator.displayname;
            $scope.pageState = 'notAvailable';
            $scope.pageStateInactive = 'deactivated';
            var videoData = {};
            videoData['responsetype'] = 'json';
            videoData['receiver'] = originator_id;
            UserService.getUserDetail( videoData ).success( function( data ) {
                $('.ui-loader').hide();
                $scope.videoCallRate = data.data.video_call_rate;
                $scope.userBalance = data.data.originator_balance;
                $scope.reqBal = data.data.bal_req;
                $scope.origBlock = data.data.originator_block;
                $scope.receiverBlock = data.data.receiver_block;
                $scope.receiverAvailable = data.data.receiver_available;
                localStorage.setItem("receiverImage_data",data.data.profile_image);
                var receiveData = JSON.stringify({'username':$scope.username1,'origId':originator_id,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'reply','receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
                     pubnub.unsubscribe({
                     channel : channel1
                });
                if ($scope.receiverBlock === true) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = true;
                    $scope.closePopupBlock = false;
                    $scope.blockMsg = "This user is no longer receiving video calls";
                }else if ( $scope.origBlock === true) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = false;
                    $scope.closePopupBlock = true;
                    $scope.blockMsg = "You are blocked";
                }
                else if( $scope.origBlock === true && $scope.receiverBlock === true ) {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = true;
                    $scope.closePopupBlock = false;
                    $scope.blockMsg = "This user is no longer receiving video calls";
                } else if ( $scope.receiverAvailable == "0") {
                    $('#overlay').show();
                    $scope.blockPopup = true;
                    $scope.gotoBlockUsers = false;
                    $scope.closePopupBlock = true;
                    $scope.blockMsg = "This user is not available";
                }else if (data.data.receiver_status == 'inactive') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageStateInactive+'/'+$scope.idCallback
                    } );
                }
                else if (data.data.in_video_call == '1') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageState+'/'+originator_id
                    } );
                }
                else if ($scope.origBlock === false && $scope.receiverBlock === false){ 
                    $scope.closePopupBlock = false;
                    $('#overlay').hide();
                    if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                           $state.go('home.videoCalling',{receive_data:receiveData})
                    }else{
                        window.plugins.nativepagetransitions.slide({
                            "href" : "#/home/videoCredits/"+$scope.username1+'/'+$scope.reqBal
                        });
                    }
                }
            } ).error( function ( data ) {
                $scope.networkPopup = true;
                $('.ui-loader').hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        
        //}
        
    };
    $rootScope.$on('$stateChangeSuccess', function( ev, to, toParams, from, fromParams ) {
        // console.log("from>" +from.name +to.name);
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
        if( ($rootScope.previousState == 'home.reply' && $rootScope.currentState == 'home.videocallAccept' ) || ($rootScope.previousState == 'home.reply' && $rootScope.currentState == 'home.videoProReject')
            || ($rootScope.previousState == 'home.reply' && $rootScope.currentState == 'home.videocallEnd')|| ($rootScope.previousState == 'home.reply' && $rootScope.currentState == 'home.videocallMissed')) {
            // console.log("correct state changes");
            pubnub.unsubscribe({
                channel : channel1
            });
        } 
    });
    $scope.closeBlock = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
         pubnub.unsubscribe({
            channel : channel1
        });
    };

    $scope.blockLink = function(){
        $('#overlay').hide();
        $scope.popupShow =  false;
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
         pubnub.unsubscribe({
            channel : channel1
        });
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };

    $scope.closeNetworkPopup = function () {
        $('#overlay').hide();
         $scope.networkPopup = false;
         pubnub.unsubscribe({
            channel : channel1
        });
         window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : "#/home/affter_login"
        } );
    };
    
     // $scope.goBackAgain = function(){
     //        $('#overlay').hide();
     //        pubnub.unsubscribe({
     //            channel : channel1
     //        });
     //        window.plugins.nativepagetransitions.slide( {
     //            "direction": 'left',
     //            "href" : "#/home/messages"
     //        } );
     //    };
    $scope.closeMsgPopup = function () {
        $(".popup1").addClass('ng-hide');
        $scope.imgPopupShow = false;
        $('#overlay').hide();
    };
    $scope.closeHeaderPopup = function () {
        $('#overlay').hide();
        $('#BMPopup').hide();
    };
    /*var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    $scope.userstatusData = JSON.parse( dashboard_data ).data.user_status;
    var userId = userData.id;
    var originator_id = $stateParams.messageId;
    $scope.user_id = originator_id;*/
    // console.log(originator_id);
    var display_name = '';
    var channel1;
    var getReplyData = {};
    getReplyData['responsetype'] = 'json';
    getReplyData['uid'] = originator_id;
    getReplyData['displayname'] = display_name;
    getReplyData['timezone'] = offsetTimezone;
    $('.ui-loader').show();
    MessagesService.reply( getReplyData ).success( function( data ) {
        console.log(JSON.stringify(data));
        $('.ui-loader').hide();
        $scope.messages = data.data.messages;
        localStorage.setItem( "reply_data", JSON.stringify( data ) );
        messageHistory($scope.messages,'check');
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.getBlockInfo = data.data;
        $scope.replyChats = data.data;
        $scope.username = data.data.originator.displayname;
        $scope.receiverProInfo = data.data.originator_profile.pro;
        $scope.senderStatus = $scope.userstatusData; 
        $scope.smsTrials = data.data.originator_profile.do_smstrials;
        console.log($scope.smsTrials,$scope.senderStatus);
        if($scope.proInfo != $scope.receiverProInfo){
                $scope.showVideoIcon = true;
        } else {
            $scope.showVideoIcon = false;
        }
        if ($scope.proInfo != 2) {
            if ($scope.receiverProInfo == 2) {
                $scope.showtipproInfo = true;
            }
        }

    } ).error( function ( data ) {
        $('.ui-loader').hide();
        $scope.networkPopup = true;
        if( navigator.connection.type == Connection.NONE ) {
            checkConnection();
        }
        if( localStorage.getItem( "reply_data" ) ) {
            var reply_detail = localStorage.getItem( "reply_data" );
            var replyData = JSON.parse( reply_detail ).data;
            $scope.replyChats = replyData;
            $scope.username = replyData.originator.displayname;
        }
    } );
    var origImage = localStorage.getItem( "profileData" );
    function addZeroes( num ) {
       // Cast as number
       var num = Number(num);
       // If there is no decimal, or the decimal is less than 2 digits, toFixed
       if (String(num).split(".").length < 2 || String(num).split(".")[1].length<=2 ){
           num = num.toFixed(2);
       }
       // Return the number
       return num;
    }  
    function messageHistory(messages,getItem)
    {
        var reply_detail = localStorage.getItem( "reply_data" );
        var replyData = JSON.parse( reply_detail ).data;
        var user_session = userId;
        var photoNameid = 'open_img';
        var photoNameid1 = 'open_img1';
        var unlockNameid = 'unlock_img';
        var user_msg = '';
        var showChar = 90;
        var divheight;
        var chatYouImg = replyData.originator_profile.profileImages.profile_image;
        
        // console.log(chatYouImg);
        for(var i = 0; i < messages.length; i++) {
            // console.log(messages[i]);
            // console.log(messages[0]);
            var gethistoryTime = getCurrentTime(messages[i].utc_time);
            var historyTime = gethistoryTime[0];
            var prevTime = gethistoryTime[1];
            if (messages[i].blocked == 'yes') {
            } 
            else if (messages[i].msg_type == 'tip'){
                    if (user_session == messages[i].originator_u_id){
                        // console.log("hi tip")
                        $("#msg_container").prepend('<div class="you box" style="min-height:88px;white-space:normal;text-align:right;background:#FCF3E3;padding-right:10px;"> <div class="slideWrap_rep" id='+messages[i].messageId+' style="min-height:88px;background:#FCF3E3 !important;"><div class="receiver_you_right1" style="padding-top: 12px;"><a id="'+photoNameid+'" class="purchasePhotoLink" data-ajax="false"><img src="'+ chatYouImg +'" height="100" width="80" border="0" alt="loving it"  style="float:right;color:#007aff;font-weight:bold;"/></a></div><div class="receiver_you2"><p>Tip sent @ '+ historyTime + '</p><span>' + prevTime + '</span><p><div class="paidtext" style="background-color:green!important;float:right;">$'+ addZeroes(messages[i].msg_rate) + '</div></p></div> </div> <button type="" class="slideButton" style="margin-right: -15px; min-height:88px;">Delete</button></div>');
                    }
                    else{
                        // console.log("hi else tip")
                        $("#msg_container").prepend('<div class="you box" style="min-height:88px;white-space:normal;text-align:left;background:#FCF3E3;"><div class="slideWrap_rep" id='+messages[i].messageId+' style="min-height:88px;background:#FCF3E3 !important;"><div class="receiver_you1" style="padding-top: 12px;"> <a id="'+photoNameid+'" class="purchasePhotoLink" data-ajax="false"><img src="'+ chatYouImg +'" height="100" width="80" border="0" alt="loving it"  style="float:left;color:#007aff;font-weight:bold;"/></a></div><div class="receiver_you2"><p>Tip sent @ '+ historyTime + '</p><span>' + prevTime + '</span><p><div class="paidtext"  style="background-color:green!important;">$'+ addZeroes(messages[i].msg_rate) + '</div></p></div></div><button type="" class="slideButton" style="margin-right: -15px;min-height:88px;">Delete</button></div>');
                    }
            }
            else if (messages[i].msg_type == 'photo_sale'){ 
                            //check if a photo purchase request sent and display accordingly
                    var replyImg = IMAGE_URL+'users/'+messages[i].originator_u_id+'/'+messages[i].cloudinary_photo_id_o;
                    var tagline = (typeof messages[i].tagline === 'undefined') ? '' : messages[i].tagline.substring(0,200) ;
                    var shout = (typeof messages[i].shout === 'undefined') ? 'no' : messages[i].shout ;
                    var p_image = (typeof messages[i].cloudinary_photo_id_p === 'undefined') ? "img/loving_it.png" : IMAGE_URL+'users/'+ messages[i].originator_u_id + '/'+ messages[i].cloudinary_photo_id_p;
                    if (user_session == messages[i].originator_u_id){
                        $("#msg_container").prepend('<div class="me box" style="height:120px;white-space:normal;text-align:left;background:#424242;color:white;"><div class="slideWrap_rep" id='+messages[i].messageId+' style="border-bottom:1px solid #ccc;height:120px;background:#424242 !important;"><div style="margin-top:10px"><a id="'+photoNameid1+'" class="purchasePhotoLink" rel="#/home/purchasePaidPhoto?displayname='+messages[i].originator_u_id+'&shout='+ shout +'&media_id=' + messages[i].media_id  + '&model_id=' +  messages[i].originator_u_id +'"><img src="'+ p_image +'" height="100" width="80" border="0" alt="loving it"  style="float:left;margin-left: 14px;color:#007aff;font-weight:bold;"/></a><div class="rightMsgs"><div style="color: green;font-size:12px;float: right;margin-right: 32px;width: 60%;"><i>Photo purchase request sent!.</i></div> <div id="description" style="float: left;padding: 10px;width: 60%;margin-left: 8px;font-size: 15px;word-wrap: break-word;">' + tagline  + '</div></div> </div></div><button type="" class="slideButton" style="height:120px;margin-right: -15px;">Delete</button></div>');
                    }
                    else{ 
                        $("#msg_container").prepend('<div class="me box" style="min-height:120px;white-space:normal;text-align:left;background:#424242;color:white;"><div class="slideWrap_rep" id='+messages[i].messageId+' style="min-height:120px;background:#424242 !important;"><div class="imgUnlock" style="margin-top:10px"><a id="'+photoNameid+'" class="purchasePhotoLink" rel="#/home/purchasePaidPhoto?displayname='+messages[i].originator_u_id+'&shout='+ shout +'&media_id=' + messages[i].media_id  + '&model_id=' +  messages[i].originator_u_id +'" data-ajax="false"><img src="'+ p_image +'" height="100" width="80" border="0" alt="loving it"  style="float:left;margin-left: 14px;color:#007aff;font-weight:bold;"/></a><div class="rightMsgs"><div style="color: green;font-size:12px;float: left;margin-left: 30px;width: 45%;"><i>User has sent a paid photo request.</i></div> <a id="'+unlockNameid+'" rel="#/home/purchasePaidPhoto?displayname='+messages[i].originator_u_id+'&shout='+ shout +'&media_id=' + messages[i].media_id  + '&model_id=' +  messages[i].originator_u_id +'" class="unlockBtn" data-ajax="false" style="color:#fff;">Unlock</a><div id="description" style="float: left;padding: 10px;width: 60%;margin-left: 8px;font-size: 15px;word-wrap: break-word;">' + tagline  + '</div></div> </div></div><button type="" class="slideButton" style="min-height:120px;margin-right: -15px;">Delete</button></div>');
                        }                                                                                                                     
                } 
            else if ( messages[i].msg_type == 'video_sale' ) {   
                if ( user_session == messages[i].originator_u_id ) {
                    $( "#msg_container" ).prepend( '<div class="me box" style="min-height:50px;white-space:normal;text-align:center;background:#424242;color:white;"> <div class="slideWrap_rep" id='+messages[i].messageId+' style="background:#424242 !important;border-bottom:0px !important;height:50px;"> <div style="font-size:14px; margin-top: 10px"> Photo purchase request sent!</div> </div><button type="" class="slideButton" id='+messages[i].messageId+' style="margin-right: -15px;min-height:50px;">Delete</button> </div>');
                } else {
                    $( "#msg_container" ).prepend( '<div class="me box" style="white-space:normal;text-align:center;min-height:110px;background:#424242;color:white;"> <div class="slideWrap_rep" id='+messages[i].messageId+' style="border-bottom: 1px solid #ccc; background:#424242 !important;min-height:110px;"> <div style="font-size:12px;"> <i>User has sent a paid photo request.</i> </div> <div style="margin-top:10px"> <a class="purchasePhotoLink" rel="#/home/paidphoto/'+messages[i].media_id+'/'+messages[i].originator_u_id+'/'+replyData.originator.displayname+'"> <img src="img/loving_it.png" height="40" width="40" border="0" alt="loving it" /> </a> <a class="purchasePhotoLink" rel="#/home/paidphoto/'+messages[i].media_id+'/'+messages[i].originator_u_id+'/'+replyData.originator.displayname+'" href="javascript:;" data-ajax="false" style="color:#7ebbea; text-decoration:underline; position: absolute;bottom: 10px;left: 30%;"> Click to view photo </a> </div> </div><button type="" class="slideButton" style="margin-right: -15px;min-height:109px;">Delete</button> </div>');
                }
            } else if (messages[i].msg_type == 'trialexpired' || messages[i].msg_type == 'upgrade' || messages[i].msg_type == 'creditexpired') {
               
            }

            // start image insertion
            else if (typeof messages[i].cloudinary_photo_id != 'undefined' && messages[i].cloudinary_photo_id) {
                var imgname = messages[i].cloudinary_photo_id;                                              
                    if(messages[i].cloudinary_photo_id.indexOf(".jpg") == -1){
                        imgname = imgname + '.jpg';
                    }
                // var imgname1 = messages[i].cloudinary_photo_id_o === undefined ? '/big_'+imgname : messages[i].cloudinary_photo_id_o;   
               
                var small_image_url = IMAGE_URL+'users/'+messages[i].originator_u_id+'/'+imgname;
                var large_image_url = IMAGE_URL+'users/'+ messages[i].originator_u_id+'/big_'+imgname;
                var Orig_image_url = IMAGE_URL+'users/'+messages[i].originator_u_id+'/orig_'+imgname;
                // console.log("url>" +Orig_image_url);
                // if (imgname1 == messages[i].cloudinary_photo_id_o) {
                    $scope.popup_Orig = 'popup_Orig';
                    $scope.msgImg_Orig = 'msgImg_Orig';
                    $scope.crossImg_Orig = 'crossImg_Orig';
                // }
                var li = '';
                var slider_div = '';
                
                var slide_button = "";
                slide_button = $("<button type='' class='slideButton' style='height: 180px;'>Delete</button>");
                if ( messages[i].originator_u_id !=  user_session ) {
                    slider_div = $("<div id='slideDiv' id="+messages[i].messageId+" style='min-height: 180px;background:#ffffff !important;border-bottom: 0px;'></div>");
                    // $('#slideDiv').css('background','#ffffff !important');
                    if(messages[i].charge_rate == 0 || messages[i].charge_rate == 0.00 || messages[i].charge_rate == null ) {

                       if(messages[i].msg_rate>0)
                       {
                            li = $("<div class='you box2' style='white-space:normal; padding: .7em 1em;'> </div>");
                            var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;left: 13px;'>$"+ messages[i].msg_rate +"</div>";
                       }
                       else
                       {
                           li = $("<div class='you box2' style='white-space:normal; padding: .7em 1em;'> </div>");
                           var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;left: 13px;'>$0</div>";
                       }


                    } else {
                       li = $("<div class='you box2' style='white-space:normal; padding: .7em 1em;'> </div>");
                       var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;left: 13px;'>$"+messages[i].charge_rate + "</div>";
                    }

                } else {
                    slider_div = $("<div id='slideDiv' id="+messages[i].messageId+" style='min-height: 180px;background:#e9e9e9 !important;border-bottom: 0px;'></div>");
                    // $('#slideDiv').css('background','#e9e9e9 !important');
                    li = $("<div class='me box2' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'> </div>");
                    if( messages[i].charge_rate == 0 || messages[i].charge_rate == 0.00 || messages[i].charge_rate == null ) {

                       if(messages[i].msg_rate>0)
                       {
                            li = $("<div class='me box2' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'></div>");
                            var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;right: 0px;'>$"+ messages[i].msg_rate +"</div>";
                       }
                       else
                       {
                           li = $("<div class='me box2' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'></div>");
                           var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;right: 0px;'>$0</div>";
                       }
                    } else {
                       li = $("<div class='me box2' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'></div>");
                       var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;right: 0px;'>$"+messages[i].charge_rate + "</div>";
                    }
                }

                var pname = 'message_image';
                var tagline = (typeof messages[i].tagline === 'undefined') ? '' : messages[i].tagline;
                var linkl = $('<div style="float:left;padding-top: 10px;padding-left: 14px"><a id="'+pname+'" class="msg_image" data-bigimageurl="' + Orig_image_url + '"><img src="'+ small_image_url + '"></a></div>'); //.append('<img src="'+ image_url + '">');
                var linkr = $('<div style="float:right;padding-top: 10px;padding-right: 14px"><a id="'+pname+'" class="msg_image" data-bigimageurl="' + Orig_image_url + '"><img src="'+ small_image_url + '"></a></div>');
                var descl = '<div id="description" style="padding-top: 5px;float: left;max-width: 61%;font-size: 14px;padding-left: 14px;word-wrap: break-word;">' + tagline  + '</div>';
                var descr = '<div id="description" style="padding-top: 5px;float: right;width: 61%;font-size: 13px;white-space: normal;padding-right: 7px;word-wrap: break-word;">' + tagline  + '</div>';
                // $(li).append($("<div id='slideDiv' style='background:#e9e9e9 !important;'></div>"));                        
                if ( messages[i].originator_u_id !=  user_session ){
                        // $(li).append(linkl);
                        // $(li).append(descl);
                        $(slider_div).append(linkl);
                        $(slider_div).append(descl);
                    }else
                    {
                        // $(li).append(linkr);
                        // $(li).append(descr);
                        $(slider_div).append(linkr);
                        $(slider_div).append(descr);
                    }
                $(slider_div).append(rate);
                $(li).append(slider_div);
                $(li).append(slide_button);
                $("#msg_container").prepend(li);
            }

            else if ( messages[i].txt_available ==  0 && user_session == messages[i].originator_u_id) {
            }
            else if (messages[i].msg_type == 'trialexpired' || messages[i].msg_type == 'upgrade' || messages[i].msg_type == 'creditexpired')
            {
            }
            
            // //check if message from history is for this user or sender and prepare html
            else if ( messages[i].originator_u_id !=  user_session ) {
                messages[i].user_msg = (messages[i].user_msg.length > 140)?messages[i].user_msg.substr(0, 80)+'...<br /><span class = "readmore" data-msg-attr = "'+messages[i].user_msg+'">Read more</span>':messages[i].user_msg;
                if( messages[i].msg_rate == 0 || messages[i].msg_rate == 0.00 || messages[i].msg_rate == null ) {
                    if( messages[i].msg_type == 'trial' ) {
                       
                        $("#msg_container").prepend( "<div class='you heightbox'><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#fff!important;'><div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImg +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p class='textCount'>" + messages[i].user_msg + "</p>  <span>" + historyTime + "</span><span>" + prevTime + "</span><div class='freetext'>trial</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>" );
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);

                    } else if( messages[i].msg_type == 'active' && messages[i].receiver_user_status == 'pending' || messages[i].sender_user_status=='pending' ) {
                       
                        $("#msg_container").prepend( "<div class='you heightbox'><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#fff !important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImg +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p class='textCount'>" + messages[i].user_msg + "</p>  <span>" + historyTime + "</span><span>" + prevTime + "</span><div class='freetext'>free</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>" );
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);

                    } else {
                        $("#msg_container").prepend("<div class='you heightbox'><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#fff !important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImg +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='paidtext'>$0</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>"); 
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);
                }
                } else {
                        $("#msg_container").prepend("<div class='you heightbox' ><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#fff !important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImg +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='paidtext'>$"+messages[i].msg_rate+"</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);
                }
            } else {
                messages[i].user_msg = (messages[i].user_msg.length > 140)?messages[i].user_msg.substr(0, 80)+'...<br /><span class = "readmore" data-msg-attr = "'+messages[i].user_msg+'">Read more</span>':messages[i].user_msg;
                if( messages[i].msg_rate == 0 || messages[i].msg_rate == 0.00 || messages[i].msg_rate == null ) {
                    if(messages[i].msg_type == 'trial') {
                        $("#msg_container").prepend("<div class='me heightbox' ><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#e9e9e9 !important;'> <p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='freetext'>trial</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                       
                        divheight = $("#"+messages[i].messageId).height();
                        // console.log(divheight);
                        getRandomHeight(divheight,messages[i].messageId);
                    } else if(messages[i].msg_type == 'active' && messages[i].receiver_user_status== 'pending' || messages[i].sender_user_status=='pending') {
                        
                        $("#msg_container").prepend("<div class='me heightbox' ><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#e9e9e9 !important;'> <p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='freetext'>free</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);
                        
                    } else {
                        $("#msg_container").prepend("<div class='me heightbox' ><div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#e9e9e9 !important;'> <p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='paidtext'>$0</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        divheight = $("#"+messages[i].messageId).height();
                        getRandomHeight(divheight,messages[i].messageId);
                    }
                } else {
                    // console.log('if length');
                    $("#msg_container").prepend("<div class='me heightbox'> <div class='slideWrap_rep' id="+messages[i].messageId+" style='background:#e9e9e9 !important;'><p class='textCount'>"+messages[i].user_msg+"</p>  <span>"+ historyTime +"</span><span>" + prevTime + "</span><div class='paidtext'>$"+messages[i].msg_rate+"</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                    divheight = $("#"+messages[i].messageId).height();
                    getRandomHeight(divheight,messages[i].messageId);
                }

            }
            if (getItem == 'loadMore') {
                if (i == 0) {
                    console.log('loadmore id');
                    // var msg_id = messages[i].messageId;
                    $('.you').attr('id', 'msg_id');
                    $('.me').attr('id', 'msg_id');
                }
            }
            function getRandomHeight(div_height,id){
                console.log(div_height);
                var adjustdivheight = div_height+8;
                var adjustdivheight1 = div_height+7;
                $("#"+id).parent();
                console.log($("#"+id).parent());
                $("#"+id).parent().find( "button" ).css('height',adjustdivheight1 + 'px');
                $("#"+id).css('height',adjustdivheight1 + 'px');
                $("#"+id).parent().css('height',adjustdivheight + 'px');
            }
            $( '#message_image' ).on( 'click', function(event) {
                event.preventDefault();
                var bigimageurl = $(this).data( "bigimageurl" );
                $("#bigMsgImg" ).attr( "src", "" );
                $("#bigMsgImg" ).attr( "src", bigimageurl );
                var img = new Image();
                img.src = bigimageurl;
                // console.log(img.src);
                img.onload = function() { 
                    this.width, this.height;
                    // var heightpixel = this.height+'px';
                    $(".msgImgPopup").css('height', $("#bigMsgImg").height() + 'px');
                    // console.log(this.height);
                    // console.log($("#bigMsgImg").height());
                }
                $(".popup1").removeClass('ng-hide');
                $scope.imgPopupShow = true;
                $('#overlay').show();
                
                
            } );
            $('#msg_container').on('click', '.readmore', function(event) {
                // console.log('click');
                var messageText = $(this).attr("data-msg-attr");
                $('#overlay').show();
                $scope.messagePopup = true;
                $("#message_popup").removeClass('ng-hide');
                $("#message_popup #big_msg").text(messageText); 
            });
        }
        $('.slideButton').on( 'click', function(event) {
            var myparent = $(event.target).parent();
            var firstchildnode= myparent.children(':first-child');
            var div_id = firstchildnode.attr("id");
            // console.log(firstchildnode,firstchildnode.attr("id"));
                $('.ui-loader').show();
                $('#overlay').show();
                $.ajax({ 
                    url:BASE_URL+"deletemessage",
                    data: { message_id:div_id, uid: originator_id, responsetype: 'json'},
                    type:"post",
                    success: function(success_data){
                        console.log(success_data);
                            myparent.hide();
                            $('#overlay').hide();
                            $('.ui-loader').hide();
                            myparent.hide("medium", function(){
                                console.log('hide medium');
                                $(this).remove();
                            });

                    },
                    error : function(result) {
                        $('#overlay').hide();
                        $(".loaderId").hide();
                        console.log("error>>" +result);
                    }
                }); 
        });
        $scope.closeMessagePopup = function () {
             $('#overlay').hide();
             $scope.messagePopup = false;
             $("#message_popup").addClass('ng-hide');
        };
        $(".slideWrap_rep , #slideDiv").swipe( {
            swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection)
            {
                // console.log (event);
                if(direction  == 'left'){
                    var dis  = parseInt(distance);
                    if(dis > 100){
                        var cc = 100 * -1;
                    } else {
                        var cc = parseInt(distance) * -1;
                    }
                    $(this).css('left', cc +'px'); 
                }
                if(direction  == 'right'){
                    var left=$(this).css('left').split("px"); 
                    if(parseInt(left[0]) == 0){
                        $(this).css('left', '0px'); 
                    }
                    else{
                        var dis  = parseInt(distance);
                        if(dis < 100){
                            var cc =(100- parseInt(distance)) * -1;
                        } else {
                            var cc = 100 - 100 * 1;
                        }
                        $(this).css('left', cc +'px'); 
                    }
                }
                //console.info (phase)
                if(phase == "end"){
                    var left=$(this).css('left').split("px"); 
                    if(parseInt(left[0]) <= -70){
                        $(this).css('left', '-100px');  
                    }else{
                        $(this).css('left', '0px');
                    }
                }
            },allowPageScroll:"vertical"
          });
        $("div.imgUnlock").delegate("a.purchasePhotoLink", "click", function(event){
            console.log("photo");
            event.preventDefault();
            pubnub.unsubscribe({
                channel : channel1
            });
            window.location.href = $(this).attr("rel");
        });
        $("div").delegate("a.unlockBtn", "click", function(event){
            console.log('unlock');
            event.preventDefault();
             pubnub.unsubscribe({
                channel : channel1
            });
            window.location.href = $(this).attr("rel");
        });
        $("#msg_container").prepend("<div class='me loaderId'><img src='img/loader.gif'></div>");
        $(".loaderId").hide();
        // console.log(getItem);
        // if (getItem != 'loadMore') {
        //     console.log('insidenotloadmore');
        //     $( '#msg_container' ).scrollTop( $(document).height()+9500 );
        // }else{
        //     console.log('insideloadmore');
        //     // $('#element').on('touchmove', function (e) {
        //     //      e.stopPropagation();
        //     // });
        //     // scrollTop: $("#msg_id").offset().top - $('#msg_container').offset().top + $('#msg_container').scrollTop()
        //      $('#msg_container').animate({
        //         // console.log('inside animate');
        //         scrollTop: $("#msg_id").offset().top},
        //         // scrollTop: $("#msg_id").offset().top - $('#msg_container').offset().top + $('#msg_container').scrollTop()},
        //         'fast');
        // }
        $( '#msg_container' ).scrollTop( $(document).height()+9500 );
        if(messages.length < 25){
            loadstatus = 1;
            flagvalue = true;
            $("#msg_container").prepend("<div class='me loaderId1'>Messages</div>");
        }       
    }
        
        pubnub.subscribe({
            channel  : channel1 ,
            message : function(message) {
                var reply_detail = localStorage.getItem( "reply_data" );
                var replyData = JSON.parse( reply_detail ).data;
                var photoNameid = 'open_img';
                var photoNameid1 = 'open_img1';
                var unlockNameid = 'unlock_img';
                var user_session = userId;
                var chatYouImgSubs = replyData.originator_profile.profileImages.profile_image;
                console.log(chatYouImgSubs);
                var divheight;
                $.ajax({ 
                    url:BASE_URL+"readmsgupdate",
                    data:{'uid': message.originator_u_id},
                    type:"post",
                    success: function(){
                        // console.log("success")
                    }
                }); 
                console.log(message);
                var getsubsTime = getCurrentTime(message.utc_time);
                var subsTime = getsubsTime[0];
                var subsPrevTime = getsubsTime[1];
                if ( message.blocked == 'yes' ) {
                    // console.log('user blocked..');
                    // if (user_session == message.originator_u_id ) {
                    //     if (user_session == message.blocker){
                    //         $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+'You are attempting to send a message to a user you have blocked. If you do wish to send messages to this user, please first remove from block list.'+"</p> <a href='javascript:;' class='blockedLink' data-ajax='false'>Blocked users</a> <br/><span>"+ dateTimeSubs +"</span></div>");
                    //     } else {
                    //         $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+'This user no longer wishes to receive your messages :('+"</p> <span>"+ dateTimeSubs +"</span></div>");
                    //     }
                    // }
                } else if ( user_session == message.originator_u_id && message.msg_type ==  'notrial'){                        
                        $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#ff9600;color:white;font-weight:bold;">This pro no longer wishes to receive trial messages and you must upgrade to continue chat <p></p><div class="upgradeDiv"><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007aff;text-decoration: underline;font-weight:bold;">Upgrade now</a></div></div>');  /*Nanda 15june*/                  
                }else if (message.msg_type == 'tip'){
                    // console.log("tip");
                        if (user_session == message.originator_u_id){
                           $( "#msg_container" ).append( '<div class="you box check" style="min-height:88px;white-space:normal;text-align:right;background:#FCF3E3;"> <div class="slideWrap_rep" id='+message.messageId+' style="min-height:88px;background:#FCF3E3 !important;"><div class="receiver_you_right1"><a id="'+photoNameid+'" class="purchasePhotoLink" data-ajax="false"><img src="'+ chatYouImgSubs +'" height="100" width="80" border="0" alt="loving it"  style="float:right;color:#007aff;font-weight:bold;"/></a></div><div class="receiver_you2"><p>Tip sent @ '+ subsTime + '</p><p><div class="paidtext" style="background-color:green!important;">$'+ addZeroes(message.msg_rate) + '</div></p></div> </div><button type="" class="slideButton" style="min-height:88px;margin-right: -15px;">Delete</button></div>');
                        }
                        else{

                            $("#msg_container").append('<div class="you box check" style="min-height:88px;white-space:normal;text-align:left;background:#FCF3E3;"><div class="slideWrap_rep" id='+message.messageId+' style="min-height:88px;background:#FCF3E3 !important;"><div class="receiver_you1"><a id="'+photoNameid+'" class="purchasePhotoLink" data-ajax="false"><img src="'+ chatYouImgSubs +'" height="100" width="80" border="0" alt="loving it"  style="float:left;color:#007aff;font-weight:bold;"/></a></div><div class="receiver_you2"><p>Tip sent @ '+ subsTime + '</p><p><div class="paidtext"  style="background-color:green!important;">$'+ addZeroes(message.msg_rate) + '</div></p></div></div><button type="" class="slideButton" style="min-height:88px;margin-right: -15px;">Delete</button></div>');
                        }
                    } else if (message.msg_type == 'photo_sale'){
                            var tagline = (typeof message.tagline === 'undefined') ? '' : message.tagline;
                            var shout = (typeof message.shout === 'undefined') ? 'no' : message.shout ;
                            var p_image = (typeof message.cloudinary_photo_id_p === 'undefined') ? "img/loving_it.png" : IMAGE_URL+'users/'+ message.originator_u_id + '/'+ message.cloudinary_photo_id_p;
                        if (user_session == message.originator_u_id){
                            $("#msg_container").append('<div class="me box check" style="height:120px;white-space:normal;text-align:left;background:#424242;color:white;"><div class="slideWrap_rep" id='+message.messageId+' style="border-bottom:1px solid #ccc;height:120px;background:#424242 !important;"><div style="margin-top:10px"><a id="'+photoNameid1+'" class="purchasePhotoLink" rel="#/home/purchasePaidPhoto?displayname='+message.originator_u_id+'&shout='+ shout +'&media_id=' + message.media_id  + '&model_id=' +  message.originator_u_id +'"><img src="'+ p_image +'" height="100" width="80" border="0" alt="loving it"  style="float:left;margin-left: 10px;color:#007aff;font-weight:bold;"/></a><div class="rightMsgs"><div style="color: green;font-size:12px;float: right;margin-right: 15px;width: 60%;"><i>Photo purchase request sent!.</i></div> <div id="description" style="float: left;padding: 10px;width: 60%;margin-left: 8px;font-size: 15px;word-wrap: break-word;">' + tagline  + '</div></div> </div></div><button type="" class="slideButton" style="height:120px;margin-right: -15px;">Delete</button></div>');
                        }
                        else{
                            $("#msg_container").append('<div class="me box check" style="min-height:120px;white-space:normal;text-align:left;background:#424242;color:white;"><div class="slideWrap_rep" id='+message.messageId+' style="border-bottom: 1px solid #ccc;min-height:120px;background:#424242 !important;"><div class="imgUnlock" style="margin-top:10px"><a id="'+photoNameid+'" class="purchasePhotoLink" rel="#/home/purchasePaidPhoto?displayname='+message.originator_u_id+'&shout='+ shout +'&media_id=' + message.media_id  + '&model_id=' +  message.originator_u_id +'" data-ajax="false"><img src="'+ p_image +'" height="100" width="80" border="0" alt="loving it"  style="float:left;margin-left: 10px;color:#007aff;font-weight:bold;"/></a><div class="rightMsgs"><div style="color: green;font-size:12px;float: left;margin-left: 10px;width: 50%;"><i>User has sent a paid photo request.</i></div> <a id="'+unlockNameid+'"  rel="#/home/purchasePaidPhoto?displayname='+message.originator_u_id+'&shout='+ shout +'&media_id=' + message.media_id  + '&model_id=' +  message.originator_u_id +'" class="unlockBtn unlockBtn1" data-ajax="false" style="color:#fff;">Unlock</a> <div id="description" style="float: left;padding: 10px;width: 60%;margin-left: 8px;font-size: 15px;word-wrap: break-word;">' + tagline  + '</div></div></div></div><button type="" class="slideButton" style="min-height:120px;margin-right: -15px;">Delete</button></div>');                       
                        }
                    } else if ( message.msg_type == 'video_sale' ) {
                        // console.log("video/"); 
                    if ( user_session == message.originator_u_id ) {
                        $( "#msg_container" ).append( '<div class="me box check" style="min-height: 50px;white-space:normal;text-align:center;background:#424242;color:white;"> <div class="slideWrap_rep" id='+message.messageId+' style="height: 50px;background:#424242 !important;border-bottom:0px !important;"> <div style="font-size:14px; margin-top: 10px"> Photo purchase request sent!</div> </div><button type="" id="slideButton" style="margin-right: -15px;min-height: 50px;">Delete</button> </div>');
                    } else {
                        $("#msg_container").append('<div class="me box check" style="white-space:normal;text-align:center;background:#424242;color:white;min-height:110px;"> <div class="slideWrap_rep" id='+message.messageId+' style="background:#424242 !important;min-height:110px;"> <div style="font-size:12px;"> <i>User has sent a paid photo request.</i> </div> <div style="margin-top:10px"> <a class="purchasePhotoLink" rel="#/home/paidphoto/'+message.media_id+'/'+message.originator_u_id+'/'+replyData.originator.displayname+'" href="javascript:;"> <img src="img/loving_it.png" height="40" width="40" border="0" alt="loving it" /> </a> <a class="purchasePhotoLink" style="position: absolute;bottom: 10px;left: 30%;" rel="#/home/paidphoto/'+message.media_id+'/'+message.originator_u_id+'/'+replyData.originator.displayname+'" href="javascript:;" data-ajax="false" style="color:#7ebbea;text-decoration:underline;"> Click to view photo </a> </div> </div><button type="" class="slideButton" style="margin-right: -15px;min-height:109px;">Delete</button> </div>');
                    }
                } else if (message.msg_type == 'trialexpired' || message.msg_type == 'upgrade' || message.msg_type == 'creditexpired') {
                    if (user_session == message.originator_u_id) {
                        if (message.msg_type == 'trialexpired')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">Your free trial has expired and you must upgrade to continue chat. <p></p><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007DFD;text-decoration: underline;">Upgrade now</a></div>');
                        }
                        else if (message.msg_type == 'upgrade')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">Your free trial has expired and you must upgrade to continue chat. <p></p><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007DFD;text-decoration: underline;">Upgrade now</a></div>');
                        }
                        else if(message.msg_type == 'creditexpired')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">You must add additional credits to continue. <p></p><a href="javascript:;" class="creditLink" style="color: #007DFD;text-decoration: underline;padding-bottom:5px;">Add credits now</a></div>');
                        }

                    } else {
                        if (message.msg_type == 'upgrade')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">This user has run out of trial msgs and must upgrade to continue.</div>');
                        }
                        else if (message.msg_type == 'trialexpired')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">This user has run out of trial msgs and must upgrade to continue.</div>');
                        }
                        else if(message.msg_type == 'creditexpired')
                        {
                            $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">Users credit expired and he must add credits to continue chat.</div>');
                        }
                    }
                }
                
                 else if ( message.txt_available ==  0 && user_session == message.originator_u_id) {
                    // console.log('not available..');
                    $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#03007c;color:white;'> <p>"+'Zzzz. I am not currently available for texting, but I got your message. ttyl.'+"</p> <span>"+ subsTime +"</span></div>");
                } else if ( $scope.replyChats.isBlocked == true ) {
                    // console.log('receiver is blocked..');
                    if (user_session == message.originator_u_id) {
                        $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+'You are attempting to send a message to a user you have blocked. If you do wish to send messages to this user, please first remove from block list.'+"</p><a href='javascript:;' class='blockedLink' data-ajax='false'>Blocked users</a> <br/><span>"+ subsTime +"</span></div>");
                    } else {
                    }
                }

                // start image insertion
                else if ( typeof message.cloudinary_photo_id != 'undefined' && message.cloudinary_photo_id) {
                    var imgname = message.cloudinary_photo_id;                                              
                            if(message.cloudinary_photo_id.indexOf(".jpg") == -1){
                                imgname = imgname + '.jpg';
                            }
                    // var imgname1 = message.cloudinary_photo_id_o === undefined ? '/big_'+imgname : message.cloudinary_photo_id_o;           
                    var small_image_url = IMAGE_URL+'users/'+message.originator_u_id+'/'+imgname;
                    var large_image_url = IMAGE_URL+'users/'+message.originator_u_id+'/big_'+imgname;
                    var Orig_image_url = IMAGE_URL+'users/'+message.originator_u_id+'/orig_'+imgname;
                    console.log("sub1>>" +Orig_image_url);
                    // if (imgname1 == message.cloudinary_photo_id_o) {
                        $scope.popup_Orig = 'popup_Orig';
                        $scope.msgImg_Orig = 'msgImg_Orig';
                        $scope.crossImg_Orig = 'crossImg_Orig';
                    // }
                    var li = '';
                    var rate ='';
                    var slider_div = '';
                    var slide_button = "";
                    slide_button = $("<button type='' class='slideButton' style='height: 180px;'>Delete</button>");
                    if ( $scope.replyChats.isBlocked == true ) {
                        if (user_session == message.originator_u_id) {
                            li = "<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+'You are attempting to send a message to a user you have blocked. If you do wish to send messages to this user, please first remove from block list.'+"</p> <a href='javascript:;' class='blockedLink' data-ajax='false'>Blocked users</a><br/><span>"+ subsTime +"</span></div>";
                        } else {
                            li = "";
                        }
                    } else {
                        slider_div = $("<div id='slideDiv' id="+message.messageId+" style='min-height: 180px;background:#fff !important;border-bottom: 0px;'></div>");
                        if ( message.originator_u_id !=  user_session ) {
                            if(message.charge_rate == 0 || message.charge_rate == 0.00 || message.charge_rate == null ) {

                                li = $("<div class='you box2 check' style='white-space:normal; padding: .7em 1em;'> </div>");

                                if( message.msg_rate > 0 ) {
                                    var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;left: 10px;'>$"+ message.msg_rate +"</div>";
                                } else {
                                   var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;left: 10px;'>$0</div>";
                                }

                            } else {

                               li = $("<div class='you box2 check' style='white-space:normal; padding: .7em 1em;'> </div>");
                               var rate = "<div class='paidtextyou' style='position: absolute;bottom: 0px;left: 10px;'>$" + message.charge_rate + "</div>";

                            }
                        } else {
                            slider_div = $("<div id='slideDiv' style='min-height: 180px;background:#e9e9e9 !important;border-bottom: 0px;'></div>");
                            if(message.charge_rate == 0 || message.charge_rate == 0.00 || message.charge_rate == null ) {

                                li = $("<div class='me box2 check' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'> </div>");

                                if(message.msg_rate>0)
                                {
                                   var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;right: 0px;'>$"+ message.msg_rate +"</div>";

                                }
                                else
                                {
                                   var rate = "<div class='paidtext' style='position: absolute;bottom: 0px;right: 0px;'>$0</div>";
                                }

                            } else {

                                $("<div class='me box2 check' style='white-space:normal;text-align:right;background:none; padding: .7em 1em;'> </div>");;
                                var rate = "<p><div class='paidtextme' style='position: absolute;bottom: 0px;right: 0px;'>$" + message.charge_rate + "</div></p>";
                            }
                        }
                    var pname = 'message_image';     
                    var tagline = (typeof message.tagline === undefined) ? '' : message.tagline.substring(0,200);
                    var linkl = $('<div style="float:left;padding-top: 10px;padding-left: 10px;"><a id="'+pname+'" class="msg_image" data-bigimageurl="' + Orig_image_url + '"><img src="'+ small_image_url + '"></a></div>'); //.append('<img src="'+ image_url + '">');
                    var linkr = $('<div style="float:right;padding-top: 10px;padding-right: 10px;"><a id="'+pname+'" class="msg_image" data-bigimageurl="' + Orig_image_url + '"><img src="'+ small_image_url + '"></a></div>'); //.append('<img src="'+ image_url + '">');
                    var descl = '<div id="description" style="padding-top: 5px;float: left;max-width: 61%;font-size: 14px;padding-left: 10px;word-wrap: break-word;">' + tagline  + '</div>';
                    var descr = '<div id="description" style="padding-top: 5px;float: right;width: 61%;font-size: 13px;white-space: normal;padding-right: 5px;word-wrap: break-word;">' + tagline  + '</div>';
                                        
                if ( message.originator_u_id !=  user_session ){
                        // $(li).append(linkl);
                        // $(li).append(descl);
                        $(slider_div).append(linkl);
                        $(slider_div).append(descl);
                        
                        
                
                    }else
                    {   
                        $(slider_div).append(linkr);
                        $(slider_div).append(descr);
                        // $(li).append(linkr);
                        // $(li).append(descr);
                    }   
                        $(slider_div).append(rate);
                        $(li).append(slider_div);
                        $(li).append(slide_button);
                        // $(li).append(rate);
                    }
                    $("#msg_container").append(li);
                }
                //check if message from history is for this user or sender and prepare html
                else if ( message.originator_u_id !=  user_session ) {
                    message.user_msg = (message.user_msg.length > 140)?message.user_msg.substr(0, 80)+'...<br /><span class = "readmore" data-msg-attr = "'+message.user_msg+'">Read more</span>':message.user_msg;
                    // console.log('msg appending..');
                    if( message.msg_rate == 0 || message.msg_rate == 0.00 || message.msg_rate == null ) {
                        
                        if( message.msg_type == 'trial' ) {
                            $("#msg_container").append("<div class='you heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#fff !important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImgSubs +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='freetext'>trial</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                            
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        } else if(message.msg_type == 'active' && message.receiver_user_status== 'pending' || message.sender_user_status=='pending') {
                            $("#msg_container").append("<div class='you heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#fff !important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImgSubs +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='freetext'>free</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                           
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        } else {
                            
                            $("#msg_container").append("<div class='you heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#fff!important;'> <div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImgSubs +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='paidtext'>$0</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                            
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        }

                    } else {
                        $("#msg_container").append("<div class='you heightbox check' > <div class='slideWrap_rep' id="+message.messageId+" style='background:#fff !important;'><div class='receiver_you1'><a id='"+photoNameid+"' class='purchasePhotoLink' data-ajax='false'><img src='"+ chatYouImgSubs +"' height='100' width='80' border='0' alt='loving it'  style='float:left;color:#007aff;font-weight:bold;'/></a></div><div class='receiver_you2'><p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='paidtext'>$"+message.msg_rate+"</div></div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        
                        divheightSubs = $("#"+message.messageId).height();
                        getRandomHeightsubs(divheightSubs,message.messageId);
                    }
                } else {
                    var reciver_status = ["upgrade","trial", "trialexpired","creditexpired","notreplied"];
                    var pro_info = '';
                    if ( message.pro == 2 ) {
                        if ( message.sender_user_status != 'active' ) {
                            pro_info = message.sender_user_status ;
                        }
                    }
                    message.user_msg = (message.user_msg.length > 140)?message.user_msg.substr(0, 80)+'...<br /><span class = "readmore" data-msg-attr = "'+message.user_msg+'">Read more</span>':message.user_msg;
                    if( message.msg_rate == 0 || message.msg_rate == 0.00 || message.msg_rate == null ) {

                        if( message.msg_type == 'trial' ) {
                            $("#msg_container").append("<div class='me heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#e9e9e9 !important;'> <p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='freetext'>trial</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        } else if( message.msg_type == 'active' && message.receiver_user_status== 'pending' || message.sender_user_status=='pending' ) {
                           
                            $("#msg_container").append("<div class='me heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#e9e9e9 !important;'> <p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='freetext'>free</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        } else {
                            $("#msg_container").append("<div class='me heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#e9e9e9 !important;'> <p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='paidtext'>$0</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                        
                            divheightSubs = $("#"+message.messageId).height();
                            getRandomHeightsubs(divheightSubs,message.messageId);
                        }

                    } else {
                        $("#msg_container").append("<div class='me heightbox check' ><div class='slideWrap_rep' id="+message.messageId+" style='background:#e9e9e9 !important;'> <p>"+message.user_msg+"</p> <span>"+ subsTime +"</span><div class='paidtext'>$"+message.msg_rate+"</div></div><button type='' class='slideButton' style='margin-right: -15px;'>Delete</button></div>");
                    
                        divheightSubs = $("#"+message.messageId).height();
                        getRandomHeightsubs(divheightSubs,message.messageId);
                        
                    }

                    if( message.pro != 2 && reciver_status.indexOf(message.receiver_credit_status) != -1) {
                                //at originator pro end only
                            if (user_session == message.originator_u_id){
                                if (message.receiver_credit_status == 'upgrade' || message.receiver_credit_status == 'trial' || message.receiver_credit_status == 'trialexpired'){
                                    $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#ffae00;color:white;">Your last message was sent Free because the user has not yet upgraded. </div>');
                                }
                                else if(message.receiver_credit_status == 'creditexpired'){
                                    $("#msg_container").append('<li class="me" style="white-space:normal;text-align:center;background:#ffae00;color:white;">Your last message was sent Free because the user does not have enough credits. </div>');
                                }
                                else if(message.receiver_credit_status == 'notreplied'){
                                    $("#msg_container").append('<li class="me" style="white-space:normal;text-align:center;background:#ffae00;color:white;">Your last message was sent Free because the user did not respond. </div>');
                                }
                            }
                     }
                }
                $timeout(function() {
                    $('#msg_container').scrollTop( $(document).height()+9400 );
                }, 1000);
                    function getRandomHeightsubs(div_height,id){
                        console.log(div_height);
                        var adjustdivheight = div_height+8;
                        var adjustdivheight1 = div_height+7;
                        $("#"+id).parent();
                        console.log($("#"+id).parent());
                        $("#"+id).parent().find( "button" ).css('height',adjustdivheight1 + 'px');
                        $("#"+id).css('height',adjustdivheight1 + 'px');
                        $("#"+id).parent().css('height',adjustdivheight + 'px');
                    }
                    $("div.check").delegate("div.slideWrap_rep", "click", function(event){
                      console.log('swipe>>');
                      var $this = $(this);
                      $this.swipe({
                        swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection)
                        {
                            // console.log("subs-direction");
                            if(direction  == 'left'){
                                var dis  = parseInt(distance);
                                if(dis > 100){
                                    var cc = 100 * -1;
                                } else {
                                    var cc = parseInt(distance) * -1;
                                }
                                $(this).css('left', cc +'px'); 
                            }
                            if(direction  == 'right'){
                                var left=$(this).css('left').split("px"); 
                                if(parseInt(left[0]) == 0){
                                    $(this).css('left', '0px'); 
                                }
                                else{
                                    var dis  = parseInt(distance);
                                    if(dis < 100){
                                        var cc =(100- parseInt(distance)) * -1;
                                    } else {
                                        var cc = 100 - 100 * 1;
                                    }
                                    $(this).css('left', cc +'px'); 
                                }
                            }
                            //console.info (phase)
                            if(phase == "end"){
                                var left=$(this).css('left').split("px"); 
                                if(parseInt(left[0]) <= -70){
                                    $(this).css('left', '-100px');  
                                }else{
                                    $(this).css('left', '0px');
                                }
                            }
                        },allowPageScroll:"vertical"
                      }); 
                  }); 
                   
                    $("div.check").delegate(".slideButton", "click", function(event){
                    // $('.slideButton').on( 'click', function(event) {
                        var myparent = $(event.target).parent();
                        var firstchildnode= myparent.children(':first-child');
                        var div_id = firstchildnode.attr("id");
                        console.log(firstchildnode,firstchildnode.attr("id"));
                        $('.ui-loader').show();
                        // $('#overlay').show();
                        $.ajax({ 
                            url:BASE_URL+"deletemessage",
                            data: { message_id:div_id, uid: originator_id, responsetype: 'json'},
                            type:"post",
                            success: function(success_data){
                                console.log(success_data);
                                myparent.hide();
                                // $('#overlay').hide();
                                $('.ui-loader').hide();
                                myparent.hide("medium", function(){
                                    console.log('hide medium');
                                    $(this).remove();
                                });
                            },
                            error : function(result) {
                                $('#overlay').hide();
                                $(".loaderId").hide();
                                console.log("error>>" +result);
                            }
                        }); 
                    });
                    
                $("div").delegate("a#message_image", "click", function(event){
                    event.preventDefault();
                    var bigimageurl = $(this).data( "bigimageurl" );
                    $("#bigMsgImg" ).attr( "src", "" );
                    $("#bigMsgImg" ).attr( "src", bigimageurl );
                    var img = new Image();
                    img.src = bigimageurl;
                    console.log(img.src);
                    img.onload = function() { 
                        this.width, this.height;
                        // var heightpixel = this.height+'px';
                        $(".msgImgPopup").css('height', $("#bigMsgImg").height() + 'px');
                        console.log(this.height);
                        console.log($("#bigMsgImg").height());
                    }
                    $(".popup1").removeClass('ng-hide');
                    $scope.imgPopupShow = true;
                    $('#overlay').show();
                });
                // $( '#unlock_img' ).on( 'click', function(event) {
                //     console.log('subsunlock');
                //     event.preventDefault();
                //      pubnub.unsubscribe({
                //         channel : channel1
                //     });
                //     window.location.href = $(this).attr("rel");
                // });
                $("div.rightMsgs").delegate("a.unlockBtn", "click", function(event){
                    console.log('unlock button');
                    event.preventDefault();
                        pubnub.unsubscribe({
                        channel : channel1
                    });
                    window.location.href = $(this).attr("rel");
                });
                $("div.imgUnlock").delegate("a.purchasePhotoLink", "click", function(event){
                    event.preventDefault();
                        pubnub.unsubscribe({
                        channel : channel1
                    });
                    window.location.href = $(this).attr("rel");
                });
                
            }
        });
        var scrollfirst = 0;
        var scrollcount = 0;
        $scope.loadMoreData = function(event){
            console.log(event);
            var scroll = $(window).scrollTop();
            var getChatHistoryData = {};
            getChatHistoryData['channel'] = channel1;
            getChatHistoryData['start'] = start;
            getChatHistoryData['count'] = count;
            if(scrollcount == 0 && flagvalue == false){
                $(".loaderId").show();
                var orgHeight = $("#msg_container").height();
                console.log(orgHeight);
                // console.log($(document).height(),$(window).height());
            }
            MessagesService.getMoreHistory( getChatHistoryData ).success( function( data ) {
                // console.log(JSON.stringify(data));
                $(".loaderId").hide();
                // console.log($(window).scrollTop($(window).scrollTop() + $("#msg_container").height() - orgHeight));
                // $(window).scrollTop($(window).scrollTop() + $("#msg_container").height() - orgHeight);
                if(data != '0'){
                   messageHistory(data.messages,'loadMore');
                   start = start + data.totalcount;
                }
                else {
                   loadstatus = 1;
                }
                if(loadstatus == 1 && scroll == 0 && flagvalue == false){

                    if(scrollcount == 0){
                        $("#msg_container").prepend("<div class='me loaderId1'>Messages</div>");
                        scrollcount = scrollcount+1; 
                    }
                }
                // $('#msg_container').animate({scrollTop: '5px'}, 800);
                // element.scrollTop = (element.scrollTop+item.clientHeight+1);
            } ).error( function ( data ) {
                // console.log(JSON.stringify(data));
                $(".loaderId").hide();
            } );

            if(scrollfirst==0){
                scrollfirst = scrollfirst + 1;
            }
        }

        $scope.popupLink = function() {
            console.log($scope.receiverProInfo, $scope.proInfo);
            $scope.popupShow = true;
            $('#overlay').show();
            $('#BMPopup').show();
            if ($scope.receiverProInfo == 2 && $scope.proInfo != 2) {
                console.log(2);
                $scope.popup_div = 'popup_div';
                // $('#BMPopup').css('height','260px !important');
            }
        };
    var page_name1 = 'reply';

    $("div").delegate("a.creditLink", "click", function(event){
        event.preventDefault();
        event.stopImmediatePropagation;
            pubnub.unsubscribe({
            channel : channel1
        });
        $state.transitionTo('home.addcredits',{originatrId:originator_id});
    });
    $("div").delegate("a.upgradeLink", "click", function(event){
        event.preventDefault();
        event.stopImmediatePropagation;
         // console.log("upgrade");
            pubnub.unsubscribe({
            channel : channel1
        });
        $state.transitionTo('home.addcredits',{originatrId:originator_id});
    });
    $scope.submitChat = function($event){
        var inputChat = $scope.chatInput;
        $scope.chatInput = '';
        function sendMessage(){
            var getChatData = {};
            getChatData['chat_entry'] = inputChat;
            getChatData['receiver'] = originator_id;
            getChatData['deviceAppId'] = device_token;
            getChatData['deviceType'] = deviceType;
            getChatData['deviceId'] = uuid;
            getChatData['responsetype'] = 'json';
            getChatData['notify_data'] = {};
            getChatData['notify_data']['sound'] = 'othersoundnoti.caf';
            MessagesService.sendMessage( getChatData ).success( function( data ) {
                console.log(JSON.stringify(data));
                $scope.chatInput = '';
                if(data.status == 'inactive'){
                    $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+data.message+"</p></div>");
                }
                if (data.status == 'fail') {
                    $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>Issue communicating with the server.Please try again later.</p></div>");
                }
                if (data.status == 'blocked') {
                    var sendMsgTime = getCurrentTime(data.utc_time);
                    if (data.message == 'This user no longer wishes to receive your messages :(') {
                        $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+data.message+"</p> <span>"+ sendMsgTime +"</span></div>");
                    }else{
                        $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+data.message+"</p><a href='javascript:;' class='blockedLink' data-ajax='false'>Blocked users</a><br/><span>"+ sendMsgTime +"</span></div>"); 
                    }
                }
            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }  
        console.log($scope.smsTrials);
        if( $('#input_chat').val() != '' ) {
            if($scope.proInfo != 2 && $scope.senderStatus == 'pending'){
                var trial_sms = $scope.smsTrials;
                if(trial_sms == 0){
                    console.log("enter1");
                  $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#ff9600;color:white;font-weight:bold;">This pro no longer wishes to receive trial messages and you must upgrade to continue chat <p></p><div class="upgradeDiv"><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007aff;text-decoration: underline;font-weight:bold;">Upgrade now</a></div></div>');  //Nanda 15june               
                } else{
                    console.log("enter2");
                    sendMessage();
                }             
            }
            // else if (data.reciver_status == 'inactive') {
            //     $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#ff9600;color:white;font-weight:bold;">This pro no longer wishes to receive trial messages and you must upgrade to continue chat <p></p><div class="upgradeDiv"><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007aff;text-decoration: underline;font-weight:bold;">Upgrade now</a></div></div>');               
            // }
            else
            {
                sendMessage();
            }            
        }else{
           $event.preventDefault(); 
        }
            
    };

    $scope.deleteHistory = function() {
        if (channel1) {
            // console.log(channel1);
            var clearChatHistory = {};
            clearChatHistory['responsetype'] = 'json';
            clearChatHistory['channel'] = channel1;
            MessagesService.clearHistory( clearChatHistory ).success( function( data ) {
                // console.log(data);
                if (data == 'success') {
                    $("#msg_container").empty();
                    $('#overlay').hide();
                    $scope.popupShow = false;
                }
            } ).error( function ( data ) {
                // console.log( JSON.stringify( data ) );
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    //popup link
    $scope.backFromReply = function() {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'right',
            "href" : "#/home/messages"
            } );
            pubnub.unsubscribe( {
                channel : channel1
            } );
    };
    $scope.cameraClick = function() {
        
        var cameraPhotoData = {};
        cameraPhotoData['uid'] = originator_id;
        cameraPhotoData['responsetype'] = 'json';
        MessagesService.isBlockAtPhotoSend( cameraPhotoData ).success( function( data ) {
            if(data.blocked_by_user == userId) {
                $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#ff9600;color:white;'> <p>"+'You are attempting to send a message to a user you have blocked. If you do wish to send messages to this user, please first remove from block list.'+"</p><a href='javascript:;' class='blockedLink' data-ajax='false'>Blocked users</a></div>");                           
            }
            else if(data.blocked_by_user == originator_id){
                 $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+'This user no longer wishes to receive your messages :('+"</p></div>");
            }
            else if(data.status == 'inactive'){
                $("#msg_container").append("<div class='me' style='white-space:normal;text-align:center;background:#6189fb;color:white;'> <p>"+data.message+"</p></div>");
            }
            else{
                
                var senderstatus = data.senderstatus;
                var senderbalance = parseFloat(data.senderbalance);
                var trial_sms = parseInt(data.sendertrial_sms);
                var do_smstrials = parseInt(data.do_smstrials); //Nanda 15 june        
                var sms_rate = parseFloat(data.to_sms_rate);
                //check here if balance is sufficient before sending image                            
                if(data.senderpro != 2) {
                    if(senderstatus == 'active' && senderbalance < sms_rate){
                        $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">You must add additional credits to continue. <p></p> <a href="javascript:;" class="creditLink" style="color: #007DFD;text-decoration: underline;padding-bottom:5px;">Add credits now</a></div>');
                    }
                    else if(senderstatus == 'pending' && do_smstrials == 0){
                        $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#ff9600;color:white;font-weight:bold;">This pro no longer wishes to receive trial messages and you must upgrade to continue chat <p></p><div class="upgradeDiv"><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007aff;text-decoration: underline;font-weight:bold;">Upgrade now</a></div></div>'); 
                    }
                    else if(senderstatus == 'pending' && trial_sms <= 0){
                        $("#msg_container").append('<div class="me" style="white-space:normal;text-align:center;background:#2cba00;color:white;">Your free trial has expired and you must upgrade to continue chatting. <p></p><div class="upgradeDiv"><a href="javascript:;" class="upgradeLink" data-ajax="false" style="color: #007DFD;text-decoration: underline;">Upgrade now</a></div></div>');
                    }
                    else {
                        pubnub.unsubscribe( {
                            channel : channel1
                        } );
                        window.plugins.nativepagetransitions.slide( {
                            "href" : '#/home/sendPhoto'+$scope.user_id
                        } );
                    }                                
                }
                else if(data.senderpro == 2) {
                    pubnub.unsubscribe( {
                        channel : channel1
                    } );
                    window.plugins.nativepagetransitions.slide( {
                        "href" : '#/home/sendPhoto'+$scope.user_id
                    } );                             
                }                            
            }             
            
            $('#msg_container').scrollTop( $(document).height()+9400 );
        } ).error( function ( data ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );

    };
    $scope.viewProfile= function() {
        pubnub.unsubscribe({
            channel : channel1
        } );
        $('#overlay').hide();
        $('#BMPopup').hide();
        window.plugins.nativepagetransitions.slide({
            "href" : '#/home/callerhistory'+$scope.user_id
        } );
    };

    $(document).on('click',".blockedLink", function() {
        event.preventDefault();
        pubnub.unsubscribe({
            channel : channel1
        } );

        window.plugins.nativepagetransitions.slide({
            "direction": 'left',
            "href" : "#/home/blockedusers"
        });
    });
    $('#overlay').click(function() {
        event.preventDefault();
        $('#overlay').hide();
        $('#BMPopup').hide();
        $(".popup1").addClass('ng-hide');
        $scope.imgPopupShow = false;
    } );

    $scope.blockUser = function() {
        navigator.notification.confirm(
            'Are you sure you want to block this user?',  // message
            onConfirm,
            'Block User'
        );
    };
    $scope.sendTip = function() {
        pubnub.unsubscribe({
            channel : channel1
        } );
        var page_name = 'reply';
        
        
        window.plugins.nativepagetransitions.slide({
            "direction": 'left',
            "href" : "#/home/sendTip/"+originator_id+"/"+page_name
        });

    };
    function onConfirm(button) {
        if( button == 1 ) {
            var blockcontactData = {};
            blockcontactData['responsetype'] = 'json';
            blockcontactData['caller_id'] = originator_id;
            ContactsService.blockUser( blockcontactData ).success( function( data ) {
                $scope.replyChats.isBlocked = true;
            } ).error( function ( data ) {
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    }

} )