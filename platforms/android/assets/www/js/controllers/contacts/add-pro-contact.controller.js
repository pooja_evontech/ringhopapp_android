//+++++++++++++++++++++++++++addProContact page Controller++++++++++++++++++++
contactApp.controller( "AddProContactController", function( $scope, ContactsService, $state, $stateParams, $rootScope, $timeout ) {
    $scope.hideRemoveIcon = false;
    $scope.contactAddBtn = false;
    $scope.Show_no_result_msg = false;
    $scope.contactAlertErrorShow = false;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    if (proData == 2) {
        $scope.displayContactHeading = 'Add User Contact';
        $scope.placeholder = 'Find User';
    }else{
        $scope.displayContactHeading = 'Add Pro Contact';
        $scope.placeholder = 'Find Pro';
    }
    $scope.OnKeyupRemove = function(e){
        $scope.hideRemoveIcon= true;
        $scope.Show_no_result_msg = false;
        $scope.contactAddBtn = false;
        $scope.contactAlertErrorShow = false;
        if ($scope.searchContact =="") {
            $scope.hideRemoveIcon = false;
        }
    };

    $scope.removeTextSearch = function(){
        $scope.searchContact = "";
        $scope.hideRemoveIcon = false;
        $scope.contactAddBtn = false;
        $scope.Show_no_result_msg = false;
        $scope.contactAlertErrorShow = false;
    };

    $scope.searchThisContact = function(){
        $scope.contactAlertErrorShow = false;
        $('.ui-loader').show();
        $('.phpdebugbar-openhandler-overlay').show();
        if ($scope.searchContact !== undefined) {
            var getSearchData = {};
            getSearchData['responsetype'] = 'json';
            getSearchData['search'] = $scope.searchContact;
            ContactsService.searchContact( getSearchData ).success( function( data ) {
                // console.log(JSON.stringify(data));
                $('.ui-loader').hide();
                $('.phpdebugbar-openhandler-overlay').hide();
                if (data.srh_msg == 'User found') {
                    if (data.searchUser.profile_status != 'null') {
                        $scope.contact_profile_status = true;
                    }else{
                        $scope.contact_profile_status = false;
                    }
                    $scope.Show_no_result_msg = false;
                    $scope.contactAddBtn = true;
                    $scope.contactIdUser = data.searchUser.user_id;
                    $scope.addContactProfileStatus = data.searchUser.profile_status;
                    $scope.addContactOnlineStatus = data.online_status;
                    $scope.addContactDisplayname = data.searchUser.displayname;
                    $scope.profileImgProContact = data.profileImage.profile_image;
                }else{
                    $scope.Show_no_result_msg = true;
                    $scope.no_result = data.srh_msg;
                }
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
        else{
            $timeout(function(){
              $('.ui-loader').hide();
              $('.phpdebugbar-openhandler-overlay').hide();
            }, 1000);
        }
    };

    $scope.saveContact = function(){
        var saveSearchData = {};
        saveSearchData['responsetype'] = 'json';
        saveSearchData['contact_id'] = $scope.contactIdUser;
        ContactsService.saveAfterSearch( saveSearchData ).success( function( data ) {
            $scope.alreadyAddedContact = data.message;
            if (data.status == 'error') {
                $scope.contactAlertErrorShow = true;
                $scope.contactAddBtn = false;
                $('.contactAlertError').css ( {"color":"red"});
            }else{
                $scope.contactAlertErrorShow = true;
                $scope.contactAddBtn = false;
                $('.contactAlertError').css ( {"color":"green"});
            }
            
        } ).error( function ( data ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );

    };
})