//+++++++++++++++++++++++++++creditscomplete page controller+++++++++++++++++++++

paymentApp.controller( "BillingHistoryController", function( $scope, PaymentService, $state, $stateParams, $rootScope ) {
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.limit = 4;
    var pre_count = 1;
    var next_count = 1;
    var historyData = {};
    historyData['responsetype'] = 'json';
    historyData['page'] = 1;

    PaymentService.billingHistory( historyData ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.billinghistory = data.data.billings;
        $scope.limit = data.data.per_page;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $('#arrowNext').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
                $scope.noMessage = "No billing history available.";
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } ).error( function ( data ) {
        $('.phpdebugbar-openhandler-overlay').hide();
        $('.ui-loader').hide();
    } );
    $scope.prevBtn = function () {
        $("#arrowNext").css({"opacity":"1"});
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.prevDisabled = true;
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            historyData['page'] = pre_count;
            PaymentService.billingHistory( historyData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.billinghistory = data.data.billings;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
        $("#arrow").css({"opacity":"1"});
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            historyData['page'] = next_count;
            PaymentService.billingHistory( historyData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentChats = '';
                $scope.limit = data.data.per_page;
                $scope.billinghistory = data.data.billings;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#arrowNext").css({"opacity":"0.5"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrowNext').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
        }
    };

} );