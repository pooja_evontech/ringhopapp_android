//>>>>>>>>> Purchase controller >>>>>>>//

paymentApp.controller( "PurchaseController", function( $scope, PaymentService, $state, $stateParams, $timeout, $rootScope ) {
    $scope.showButton = true;
    $scope.showLoader = false;
    $scope.purchase_userid = $stateParams.model_id;
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    var userId = userData.id;
    var page_name1 = 'purchaseController';
    var paidPhotoData = {};
    paidPhotoData['displayname'] = $stateParams.displayname;
    paidPhotoData['shout'] = $stateParams.shout;
    paidPhotoData['media_id'] = $stateParams.media_id;
    paidPhotoData['model_id'] = $stateParams.model_id;
    paidPhotoData['timezone'] = offsetTimezone;
    paidPhotoData['responsetype'] = 'json';
    $scope.photoInfo = [];
    PaymentService.photoPurchase( paidPhotoData ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.purchase_displayname = data.displayname;
        $scope.purcahse_resulttext = data.result_text;
        $scope.purchase_buttontext = data.button_text;
        $scope.purchase_tagline = data.tagline;
        $scope.purchase_color = data.button_color;
        if( data.button_color == 'green' ){
            $("#purchase_button").css({"background":"green","border-color":"green","color":"white"});
        }else{
            $("#purchase_button").css({"background":"red","border-color":"red","color":"white"});
            $(".purchaseEarn p").css({"margin-top": "-8px" , "font-size": "11px" , "margin-left": "9px"});
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.purchaseUser = function(color, name, text){
        $scope.showLoader = true;
        $scope.showButton = false;
        $scope.buttonColor = color;
        $scope.purchaseDisplay = name;
        if( $scope.buttonColor == 'red' ) {
            if( text == 'Add credits' || text == 'Click to upgrade') {
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/addcredits/"+ userId
                });
            }
            if( text == 'Click here to view.' ) {
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/reply"+$stateParams.model_id
                } );

            }
        } 
        else if ( $scope.buttonColor == 'green' ){
            $scope.showLoader = true;
            var processpurchaseData = {};
            processpurchaseData['media_id'] = $stateParams.media_id;
            processpurchaseData['model_id'] = $stateParams.model_id;
            processpurchaseData['shout'] = $stateParams.shout;
            processpurchaseData['displayname'] = $scope.purchaseDisplay;
            processpurchaseData['responsetype'] = 'json';
            PaymentService.processPurchase( processpurchaseData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $scope.showLoader = false;
                if( data.status == 'success') {
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/reply"+$stateParams.displayname
                    });
                }
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $scope.showLoader = false;
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
       }
        
    };
})
