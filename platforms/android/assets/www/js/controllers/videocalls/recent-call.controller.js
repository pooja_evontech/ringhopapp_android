//+++++++++++++++++++++++++++Recent call page controller+++++++++++++++++++++

callApp.controller( "RecentCallController", function( $scope, moment, UserService, CallsService, $state, $stateParams, $rootScope, $filter, $timeout ) {
    $scope.messageOptions = false;
    $scope.prevDisabled = false;
    $scope.nextDisabled = false;
    $scope.paginateSection = false;
    $scope.noCallDiv = false;
    $scope.showLeadstext = false;
    $scope.showmeleads = true;
    $scope.networkPopup = false;
    $scope.blockPopup = false;
    $scope.gotoBlockUsers = false;
    $scope.closePopupBlock = false;
    $scope.limit = 2;
    var checkVal = false;
    var pre_count = 0;
    var next_count = 0;
    var nextCount = 0;
    var users = 'billable';
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.proInfo = JSON.parse( dashboard_data ).data.profile.pro;
    console.log(offsetTimezone);
    if( proData == 2 ) {
        //for pro users
        $scope.isPro = true;
        $scope.messageOptions = true;
    } else {
        $scope.isPro = false;
    }

    $('.ui-loader').show();
    var getCallData = {};
    getCallData['responsetype'] = 'json';
    getCallData['timezone'] = offsetTimezone;
    getrecentcalldata();
    function getTimeAtCall(maxtime){
        // console.log(maxtime);
        var dateFormat = 'MM-DD-YYYY';
        var utcdate = maxtime;
        // console.log(utcdate);
        var timestamp = moment.unix(utcdate).format("YYYY-MM-DD HH:mm:ss");
        var momentTimeobj = moment(timestamp);
        // console.log(momentTimeobj);
        var dateFormat = 'MM/DD/YYYY';
        var timeformat = moment().localeData().longDateFormat('LT')
        var localDate = momentTimeobj.format(dateFormat);
        var localTime = momentTimeobj.format(timeformat);
        var dateReceivedFormat = momentTimeobj.format('YYYY-MM-DD');
        var getYesterdayDateM = moment().subtract(1, 'day');
        var getYesterdayDate = getYesterdayDateM.format('YYYY-MM-DD');
        var weekDateM = moment().subtract(8, 'days');
        var weekDate = weekDateM.format('YYYY-MM-DD');
        var formattedDate = localTime;
        if (moment(dateReceivedFormat).isSame(currentTime)) {
            formattedDate = localTime;
            // console.log("1st case");
        }else if (moment(getYesterdayDate).isSame(dateReceivedFormat)) {
            formattedDate = 'Yesterday';
            // console.log("2nd case");
        }else if (moment(dateReceivedFormat).isBetween( weekDate,getYesterdayDate)) {
            // console.log("3rd case");
            formattedDate = moment(dateReceivedFormat).format('dddd');
            // console.log(moment(dateReceivedFormat).format('dddd'));
        }else{
            formattedDate = localDate;
            // console.log('4th case');
        }
        return formattedDate;
    }
    function getrecentcalldata(){
        $('.ui-loader').show();
        CallsService.getRecentCalls( getCallData ).success( function( data ) {
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            if( data.message == 'user not logged in' ){
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/affter_login"
                });
            }
            $scope.limit = data.data.per_page;
            $scope.recentCalls = data.data.callhistory;
            $scope.totalCall = data.data.total_count;
            $scope.currentPage = data.data.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;
            $scope.dontShowmeAgainCheck = data.data.dontShowmeAgain;
            angular.forEach($scope.recentCalls , function(item){
                item.formattedDate = getTimeAtCall(item.utc_time);
            });
            if ( $scope.totalCall <= $scope.currentCount ) {
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalCall == 0 ){
                    $scope.noCallDiv = true;
                    $scope.noCall = "No recent calls available.";
                }
            }  else {
                $scope.paginateSection = true;
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
            if ( $scope.totalCall == 0 ) {
            }
        } ).error( function ( data ) {
            // console.log( JSON.stringify( data ) );
            $scope.networkPopup = true;
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    }

    $scope.recentCallsLink =function(originator){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/callerhistory" +originator
        });
    };

    $scope.billableUser = function () {
        checkVal = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'billable';
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#billable_user').removeClass('fullwidth_white');
        $('#trial_user').addClass('fullwidth_white');
        $('#trial_user').removeClass('fullwidth_bluemsg');

        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var getCallData = {};
        getCallData['responsetype'] = 'json';
        getCallData['timezone'] = offsetTimezone;
        CallsService.getRecentCalls( getCallData ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            $scope.limit = data.data.per_page;
            $scope.recentCalls = data.data.callhistory;
            $scope.totalCall = data.data.total_count;
            $scope.currentPage = data.data.current_page;
            $scope.currentCount = $scope.currentPage * $scope.limit;
            angular.forEach($scope.recentCalls , function(item){
                item.formattedDate = getTimeAtCall(item.utc_time);
            });
            if ( $scope.totalCall <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
                if( $scope.totalCall == 0 ){
                    $scope.noCallDiv = true;
                    $scope.noCall = "No recent calls available.";
                }
            }  else {
                $scope.paginateSection = true;
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
            }
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.donotshowMethod = function(getvalue){
        console.log(getvalue);
        if (getvalue == true) {
            $scope.curShowmeVal = '1';
        }else{
            $scope.curShowmeVal = '0';
        }
        var getdonotshowleadsData = {};
        getdonotshowleadsData['responsetype'] = 'json';
        getdonotshowleadsData['dont_showme_again'] = $scope.curShowmeVal;
        getdonotshowleadsData['dont_showme_at'] = 'recent';
        console.log(JSON.stringify(getdonotshowleadsData));
        CallsService.donotshowleadsRecent( getdonotshowleadsData ).success( function( data ) {
            console.log(JSON.stringify(data));
        } ).error( function ( data ) {
            // $('.phpdebugbar-openhandler-overlay').hide();
            // $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.leadsUser = function(){
        checkVal = true;
        $scope.showLeadstext = false;
        $scope.paginateSection = true;
        $scope.showmeleads = true;
        $scope.messageOptions = true;
    };
    $scope.showLeads = function(){
        if(checkVal == true){
            evt.stopImmediatePropagation();
            evt.preventDefault();
        }else{
        $('.ui-loader').show();
        $('.phpdebugbar-openhandler-overlay').show();
        if ($scope.dontShowmeAgainCheck == '0') {
            $scope.showLeadstext = true;
            $scope.paginateSection = false;
            $scope.messageOptions = false;
            $scope.showmeleads = false;
        }else{
            checkVal = true;
            $scope.showLeadstext = false;
            $scope.paginateSection = true;
            $scope.showmeleads = true;
            $scope.messageOptions = true;
        }
        // $timeout(function(){
          // $scope.showLeadstext = true;
        //   $('.ui-loader').hide();
        //   $('.phpdebugbar-openhandler-overlay').hide();
        // }, 2500);
        // $scope.paginateSection = false;
        // $scope.messageOptions = false;
        // $scope.showmeleads = false;
        $scope.recentChats = {};
        pre_count = 0;
        next_count = 0;
        users = 'trial';
        $('#billable_user').addClass('fullwidth_white');
        $('#billable_user').addClass('fullwidth_bluemsg');
        $('#trial_user').addClass('fullwidth_bluemsg');
        $('#trial_user').removeClass('fullwidth_white');
        var getCallData = {};
        getCallData['responsetype'] = 'json';
        getCallData['sort'] = 'free';
        getCallData['timezone'] = offsetTimezone;
        CallsService.getRecentCalls( getCallData ).success( function( data ) {
            $('.ui-loader').hide();
            $('.phpdebugbar-openhandler-overlay').hide();
            $scope.limit = data.data.per_page;
            $scope.recentCalls = data.data.callhistory;
            $scope.totalCall = data.data.total_count;
            $scope.currentPage = data.data.current_page
            $scope.currentCount = $scope.currentPage * $scope.limit;
            angular.forEach($scope.recentCalls , function(item){
                item.formattedDate = getTimeAtCall(item.utc_time);
            });
            if( $scope.totalCall == 0 ) {
                $scope.noCallDiv = true;
                $scope.noCall = "No recent calls available.";
            } else {
                $scope.noCallDiv = false;
            }
            if ( $scope.totalCall <= $scope.currentCount ) {
                $scope.paginateSection = false;
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $('#arrow').css ( {"opacity":"0.5" });
                $scope.nextDisabled = true;
                $scope.prevDisabled = true;
            }  else {
                $scope.paginateSection = true;
                $("#prevBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                $scope.nextDisabled = false;
                $scope.prevDisabled = true;
                $('#arrow').css ( {"opacity":"0.5" });
            }
            $scope.paginateSection = false;

        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
     }
    };
    $scope.prevBtn = function () {
        pre_count = next_count;
        if ( $scope.currentPage == 1 ) {
            $('#prevBtnCall').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getCallData['page'] = pre_count;
            getCallData['timezone'] = offsetTimezone;
            CallsService.getRecentCalls( getCallData ).success( function( data ) {
                // console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentCalls = data.data.callhistory;
                $scope.totalCall = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                angular.forEach($scope.recentCalls , function(item){
                    item.formattedDate = getTimeAtCall(item.utc_time);
                });
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalCall > $scope.currentCount ){
                        $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtnCall').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtnCall').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }
                next_count = next_count - 1;
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }
    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        next_count = next_count + 1;
        if ( $scope.totalCall >= $scope.limit && $scope.currentCount < $scope.totalCall ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            getCallData['page'] = next_count+1;
            getCallData['timezone'] = offsetTimezone;
            CallsService.getRecentCalls( getCallData ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentCalls = data.data.callhistory;
                $scope.totalCall = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                angular.forEach($scope.recentCalls , function(item){
                    item.formattedDate = getTimeAtCall(item.utc_time);
                });
                if ( $scope.totalCall > $scope.currentCount ) {
                    $('#nextBtnCall').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtnCall").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }
            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtnCall").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };

    $scope.missedVideoCalling = function(userId,missCaller_name){
        $('.ui-loader').show();
        $('#overlay').show();
        console.log('enter missed');
        $scope.idCallback = userId;
        $scope.username1 = missCaller_name;
        $scope.pageState = 'notAvailable';
        $scope.pageStateInactive = 'deactivated';
        var videoData = {};
        videoData['responsetype'] = 'json';
        videoData['receiver'] = $scope.idCallback;
        UserService.getUserDetail( videoData ).success( function( data ) {
            $('.ui-loader').hide();
            $('#overlay').hide();
            console.log(JSON.stringify(data));
            $scope.videoCallRate = data.data.video_call_rate;
            $scope.userBalance = data.data.originator_balance;
            $scope.reqBal = data.data.bal_req;
            $scope.origBlock = data.data.originator_block;
            $scope.receiverBlock = data.data.receiver_block;
            $scope.receiverAvailable = data.data.receiver_available;
            localStorage.setItem("receiverImage_data",data.data.profile_image);
            var receiveData = JSON.stringify({'username':$scope.username1,'origId':$scope.idCallback,'videoRate':$scope.videoCallRate,'bal':$scope.userBalance,'page_name':'recentcalls', 'receiver_bal':data.data.receiver_balance, 'receiver_reqBal':$scope.reqBal})
            if ($scope.receiverBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            }else if ( $scope.origBlock === true) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "You are blocked";
            }
            else if( $scope.origBlock === true && $scope.receiverBlock === true ) {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = true;
                $scope.closePopupBlock = false;
                $scope.blockMsg = "This user is no longer receiving video calls";
            } else if ( $scope.receiverAvailable == "0") {
                $('#overlay').show();
                $scope.blockPopup = true;
                $scope.gotoBlockUsers = false;
                $scope.closePopupBlock = true;
                $scope.blockMsg = "This user is not available";
            } else if (data.data.receiver_status == 'inactive') {
                    window.plugins.nativepagetransitions.slide( {
                        "direction": 'left',
                        "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageStateInactive+'/'+$scope.idCallback
                    } );
                }
            else if (data.data.in_video_call == '1') {
                console.log(data.data.in_video_call);
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/videoProReject/"+$scope.username1+'/'+$scope.pageState+'/'+$scope.idCallback
                } );
            }
            else if ($scope.origBlock === false && $scope.receiverBlock === false){
                $scope.closePopupBlock = false;
                $('#overlay').hide();
                if($scope.proInfo === 2 || (data.data.originator_balance >= data.data.bal_req)){
                   $state.go('home.videoCalling',{receive_data:receiveData})
                }else{
                    window.plugins.nativepagetransitions.slide({
                        "href" : "#/home/videoCredits/"+$scope.username1+'/'+$scope.reqBal
                    });
                }
            }
        } ).error( function ( data ) {
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            $('#overlay').hide();
            $scope.networkPopup = true;
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
    };
    $scope.backFromRecentcalls = function() {
        // console.log($scope.showmeleads);
        if (checkVal == true) {
            // console.log("hello");
            checkVal = false;
            pre_count = 0;
            next_count = 0;
            users = 'billable';
            $('#billable_user').addClass('fullwidth_bluemsg');
            $('#billable_user').removeClass('fullwidth_white');
            $('#trial_user').addClass('fullwidth_white');
            $('#trial_user').removeClass('fullwidth_bluemsg');
            getrecentcalldata();

        }else if ($scope.showLeadstext == true) {
            $scope.showmeleads = true;
            $scope.showLeadstext = false;
            checkVal = false;
            $scope.isPro = true;
            $scope.messageOptions = true;
            pre_count = 0;
            next_count = 0;
            users = 'billable';
            $('#billable_user').addClass('fullwidth_bluemsg');
            $('#billable_user').removeClass('fullwidth_white');
            $('#trial_user').addClass('fullwidth_white');
            $('#trial_user').removeClass('fullwidth_bluemsg');
            getrecentcalldata();
        }else{
            window.plugins.nativepagetransitions.slide( {
                "direction": 'right',
                "href" : '#/home/affter_login'
            } );
        }
        
    };
    $scope.closeNetworkPopup = function () {
         $scope.networkPopup = false;
         window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : "#/home/affter_login"
        } );
    };
    // $scope.goBackAgain = function(){
    //     $scope.networkPopup = false;
    //     window.plugins.nativepagetransitions.slide( {
    //         "direction": 'left',
    //         "href" : "#/home/recentcalls"
    //     } );
    // };
    $scope.closeBlock = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
    };

    $scope.blockLink = function(){
        $('#overlay').hide();
        $scope.blockPopup = false;
        $scope.closePopupBlock = false;
        $scope.gotoBlockUsers = false;
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };
    
    $scope.deleteRecentCall = function(dlt_Id){
        // console.log(dlt_Id);
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        CallsService.deleteRecentCalls( dlt_Id ).success( function( data ) {
            console.log(JSON.stringify(data));
            // if (data.status == 'failed') {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $('#'+dlt_Id).hide();
                $('#overlay').hide();
                $('#recentCall_'+dlt_Id).hide("medium", function(){
                    $(this).remove();
                });
            // }
        } ).error( function ( data ) {
            console.log( JSON.stringify( data ) );
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
        } );
    };
        
} )
