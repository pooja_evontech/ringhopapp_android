//+++++++++++++++++++++++++++video calling page controller+++++++++++++++++++++
callApp.controller("VideoCallingController", function( $scope, CallsService, PaymentService, $filter,  $state, $stateParams, $timeout, $rootScope, constantData, GenerateRandomNo, $interval ) {
    $scope.networkPopup = false;
    $scope.time_out = '';
    $rootScope.check_bal_timeout = '';
    $scope.timeoutFlag = false;
    var is_pro = '';
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    var connectionflag = 0;
    $scope.proInfo = proData;
    is_pro = $scope.proInfo === 2 ? 'Pro' : 'user' ;

    if ($scope.proInfo != 2) {
        $scope.InApp_credits = [
            { credit:'10', price:'10'},
            { credit:'20', price:'20'},
            { credit:'40', price:'40'},
            { credit:'60', price:'60'}
        ];
        var user_detail = localStorage.getItem( "userDetail" );
        var useridcredits = JSON.parse( user_detail ).data.id;
        var creditData = {};
        creditData['responsetype'] = 'json';
        creditData['userid'] = useridcredits;
        PaymentService.addCredits( creditData, 'GET' ).success( function( data ) {
            $scope.creditInfo = data.balance_msg;
            $scope.payment_method = data.paymentVia;
        } ).error( function ( data ) {
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );
        var productIds = ['com.ringhop.ringhop.10','com.ringhop.ringhop.20','com.ringhop.ringhop.40','com.ringhop.ringhop.60'];
        inAppPurchase
        .getProducts(productIds)
        .then(function (products) {
        $scope.products = products;
        })
        .catch(function (err) {
        }); 
    }
    // ____________________________************************____________________________

    var currDate = new Date();
    $scope.newCurrDate = $filter('date')(currDate,'yyyy-MM-dd HH:mm:ss');
    $scope.can_continue_call = false;
    var flag = 0;
    var apiKey = '45598312';
    if($stateParams.receive_data == ''){
        $scope.showEndVideoDiv = false;
        flag = 1;
        var receiveAftrNotiData = localStorage.getItem("caller_Data");
        var afterNoti = JSON.parse( receiveAftrNotiData );
        $scope.ReceiveCallId2 = afterNoti.CallId;
        $scope.IdReceiver = afterNoti.receiverid;
        $scope.showNamedata = afterNoti.u_name;
        var userid = afterNoti.userId;
        $scope.user_id = userid;
        var profileImageUrl = afterNoti.image;
        var sessionId = afterNoti.sessionId;
        var sessionToken = afterNoti.sessionToken; 
        var callPerMinute = afterNoti.video_call_rate;
        var userBalance = afterNoti.receiver_balance;
        var call_bal_req = afterNoti.bal_req;
        var obj = {};
        obj['ApiKey'] = apiKey;
        obj['SessionId'] = sessionId;
        obj['Token'] = sessionToken;
        obj['UserType'] = is_pro;
        if ($scope.proInfo != 2) {
            if ((parseFloat(userBalance)) >= (parseFloat(call_bal_req))) {
                $scope.can_continue_call = true;
                obj['IsAbleToCall'] = 'true';
            }else{
                $scope.can_continue_call = false;
                obj['IsAbleToCall'] = 'false';
            }
        }else{
            $scope.can_continue_call = true;
            obj['IsAbleToCall'] = 'true';
        }
        obj['ProfileImage'] = profileImageUrl;
        obj['CallPerMinute'] = callPerMinute;
        obj['Amount'] = userBalance;
        obj['UserName'] = $scope.showNamedata;
        obj['isReceiverInit'] = 'true';
        var jsonObj = JSON.stringify(obj);
        // console.log(JSON.stringify(obj));
        VideoPlugin.initializeVideoCalling(jsonObj, onSuccess, onFail);
    } else {
        if (localStorage.getItem("dynamic_sessionID")) {
            flag=0;
            if (deviceType == 'Android') {
                // console.log("play1");
                snd1 = new Media( 'file:///android_asset/www/sounds/CallerRingtone.mp3', null , null , onStatus );
            }else{
                snd1 = new Media( 'sounds/CallerRingtone.caf', null , null , onStatus );
            }
            // snd1 = new Media( 'sounds/CallerRingtone.caf', null , null , onStatus );
            var sessionId = localStorage.getItem("dynamic_sessionID");
            var sessionToken = localStorage.getItem("dynamic_token");
            var callPerMinute = JSON.parse($stateParams.receive_data).videoRate;
            var userBalance = JSON.parse($stateParams.receive_data).bal;
            var imageUrl = localStorage.getItem("receiverImage_data");
            $scope.showEndVideoDiv = false;
            $scope.showNamedata = JSON.parse($stateParams.receive_data).username;
            $scope.IdReceiver = JSON.parse($stateParams.receive_data).origId;
            $scope.PageInfo = JSON.parse($stateParams.receive_data).page_name;
            $scope.pageState = $rootScope.previousState;
            $scope.receiver_bal = JSON.parse($stateParams.receive_data).receiver_bal;
            $scope.receiver_reqBalance = JSON.parse($stateParams.receive_data).receiver_reqBal
            var obj = {};
            obj['ApiKey'] = apiKey;
            obj['SessionId'] = sessionId;
            obj['Token'] = sessionToken;
            obj['UserType'] = is_pro;
            obj['IsAbleToCall'] = 'true';
            obj['ProfileImage'] = imageUrl;
            obj['CallPerMinute'] = callPerMinute;
            obj['Amount'] = userBalance;
            obj['UserName'] = $scope.showNamedata;
            obj['isReceiverInit'] = 'false';
            obj['proCharge'] = $scope.receiver_reqBalance;
            var jsonObj = JSON.stringify(obj);
            console.log(userBalance,$scope.receiver_reqBalance);
            // $('#wrapper').css ( {"background":"#E9E9E9" });
            var channel_secret = GenerateRandomNo.randomString(8, '0123456789abcdef');
            localStorage.setItem("Random_token",channel_secret);
            $scope.can_continue_call = $scope.receiver_bal >= $scope.receiver_reqBalance ? true : false ;
            var user_detail = localStorage.getItem( "userDetail" );
            var userid = JSON.parse( user_detail ).data.id;
            var detailsdata = {};
            detailsdata['responsetype'] = 'json';
            detailsdata['receiver_id'] = $scope.IdReceiver;
            detailsdata['originator_id'] = userid;
            detailsdata['event']='init';
            detailsdata['cur_time'] = $scope.newCurrDate;
            CallsService.setVideoCallDetails( detailsdata ).success( function( data ) {
                    console.log(JSON.stringify(data));
                    var profileImageUrl = imageUrl;
                    localStorage.setItem("CallId",data.data.callId);
                    if (deviceType == 'Android') {
                        // console.log("play2");
                        if (snd1 && snd1.src == 'file:///android_asset/www/sounds/CallerRingtone.mp3') {
                            snd1.play();
                            // snd1.play();
                            // snd1.setVolume('1.0');
                        }
                    }else{
                        if (snd1 && snd1.src == 'sounds/CallerRingtone.caf') {
                            // callerRingtoneLoop = true;
                            snd1.play({ numberOfLoops: 6 });
                            // snd1.play();
                            snd1.setVolume('1.0');
                        }
                    }
                    
                    VideoPlugin.initializeVideoCalling(jsonObj, onSuccess, onFail);
            } ).error( function ( data ) {
                $scope.networkPopup = true;
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        } else {
            console.log("sessionId>>" +localStorage.getItem("dynamic_sessionID"));
        }
    }

    function onStatus(status) {
        // console.log(snd1);
        if (snd1.src == 'file:///android_asset/www/sounds/CallerRingtone.mp3') {
            // console.log("first");
            if( status == Media.MEDIA_STOPPED ) {
                // console.log("second>>" +caller_tone);
                // if(callerRingtoneLoop == true){
                    // snd1.play();
                //     snd1.setVolume('1.0');
                // }
                if (caller_tone) {
                    snd1.stop();
                    snd1.release();
                }else{
                    snd1.play();
                }
            }else{
                // callerRingtoneLoop = true;
            }
        }
    }  

    function onSuccess(successmsg) {
        console.log(JSON.stringify(successmsg));
        var token_random = localStorage.getItem("Random_token");
        var receiverCallId = localStorage.getItem("CallId");
        $scope.ReceiveCallId1 = receiverCallId;
        function Endfunc(){
            $interval.cancel($rootScope.check_bal_timeout);
            $scope.timeoutFlag = false;
            var detailsdataAtEnd = {};
            detailsdataAtEnd['responsetype'] = 'json';
            detailsdataAtEnd['receiver_id'] = $scope.IdReceiver;
            detailsdataAtEnd['originator_id'] = userid;
            if(flag == 1){
            detailsdataAtEnd['call_id'] = $scope.ReceiveCallId2;
            }else{
            detailsdataAtEnd['call_id'] = $scope.ReceiveCallId1;
            }
            detailsdataAtEnd['event'] = 'ended';
            detailsdataAtEnd['cur_time'] = $scope.newCurrDate;
            detailsdataAtEnd['notify_data'] = {};
            if (deviceType == 'Android') {
                detailsdataAtEnd['notify_data']['sound'] = 'othersoundnoti.mp3';
            }else{
               detailsdataAtEnd['notify_data']['sound'] = 'othersoundnoti.caf'; 
            }
            
            detailsdataAtEnd['notify_data']['content-available'] = 1;
            detailsdataAtEnd['notify_data']['randomNo'] = GenerateRandomNo.randomString(8, '0123456789abcdef');
            CallsService.setVideoCallDetails( detailsdataAtEnd ).success( function( data ) {
                console.log('call is ended>>>>>>',JSON.stringify(data));
                $('.ui-loader').hide();
                $scope.totaldata = data.data;
                if ($scope.proInfo == 2) {
                    $scope.costOfCall = data.data.call_rev_share;
                }else{
                    $scope.costOfCall = data.data.call_cost;
                }
                $scope.durationOfCall = data.data.call_duration;
                $scope.identifier = 'sender';
               
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'right',
                    "href" : '#/home/videocallEnd/'+$scope.costOfCall+'/'+$scope.durationOfCall+'/'+$scope.showNamedata+'/'+$scope.IdReceiver+'/'+$scope.proInfo+'/'+$scope.PageInfo
                } );
            } ).error( function ( data ) {
                console.log('call is ending from error condition');
                Endfunc();
                if( navigator.connection.type == Connection.NONE ) {
                    if (connectionflag == 0) {
                        console.log("disconnecterror2");
                        checkConnection();
                        connectionflag++;
                    }
                }
            } );
        }
        function startFunc(){
            $scope.timeoutFlag = true;
            var detailsdataAtStart = {};
            detailsdataAtStart['responsetype'] = 'json';
            detailsdataAtStart['receiver_id'] = $scope.IdReceiver;
            detailsdataAtStart['originator_id'] = userid;
            if(flag == 1){
            detailsdataAtStart['call_id'] = $scope.ReceiveCallId2;
            }else{
            detailsdataAtStart['call_id'] = $scope.ReceiveCallId1;
            }
            detailsdataAtStart['event']= 'started';
            detailsdataAtStart['cur_time'] = $scope.newCurrDate;
            CallsService.setVideoCallDetails( detailsdataAtStart ).success( function( data ) {
                if ($scope.proInfo !== 2) {
                    if($scope.timeoutFlag){
                        console.log('interval called')
                        $rootScope.check_bal_timeout = $interval(BalTimeoutFunction, 10000);
                    }

                }
            } ).error( function ( data ) {
                startFunc();
                if( navigator.connection.type == Connection.NONE ) {
                    $interval.cancel($rootScope.check_bal_timeout);
                    if (connectionflag == 0) {
                        checkConnection();
                        connectionflag++;
                    }
                }
            } );
        }
        function initFunc(){
            var notificationData = {};
            notificationData['responsetype'] = 'json';
            notificationData['receiver_id'] = $scope.IdReceiver;
            notificationData['session_id'] = sessionId;
            notificationData['video_token'] = sessionToken;
            notificationData['call_id'] = $scope.ReceiveCallId1;
            notificationData['notify_data'] = {};
            if (deviceType == 'Android') {
                // console.log("Android");
                notificationData['notify_data']['sound'] = 'ringhop_2.mp3';
            }else{
                // console.log("ios");
               notificationData['notify_data']['sound'] = 'ringhop_2.caf'; 
            }
            notificationData['notify_data']['receiver_balance'] = $scope.receiver_bal;
            notificationData['notify_data']['bal_req'] = $scope.receiver_reqBalance;
            notificationData['notify_data']['video_call_rate'] = callPerMinute;
            notificationData['notify_data']['content-available'] = 1;
            notificationData['notify_data']['randomNo'] = GenerateRandomNo.randomString(8, '0123456789abcdef');
            console.log(JSON.stringify(notificationData));
            CallsService.sendVideoNotification( notificationData ).success( function( data ) {
                console.log(data);
                if (is_pro == 'Pro') {
                    if (data.data.receiver_balance < data.data.bal_req) {
                        VideoPlugin.showLowBalanceWarning(""); 
                    }
                }
            } ).error( function ( data ) {
                if( navigator.connection.type == Connection.NONE ) {
                    if (connectionflag == 0) {
                        checkConnection();
                        connectionflag++;
                    }
                }else{
                    initFunc();
                }
            } );
        }
        function successfullyDisconnect(){
            // console.log("strtdisconnecterror");
            var detailsdataAtEnd = {};
            detailsdataAtEnd['responsetype'] = 'json';
            detailsdataAtEnd['receiver_id'] = $scope.IdReceiver;
            detailsdataAtEnd['originator_id'] = userid;
            if(flag == 1){
            detailsdataAtEnd['call_id'] = $scope.ReceiveCallId2;
            }else{
            detailsdataAtEnd['call_id'] = $scope.ReceiveCallId1;
            }
            detailsdataAtEnd['event'] = 'user_rejected';
            detailsdataAtEnd['cur_time'] = $scope.newCurrDate;
            detailsdataAtEnd['notify_data'] = {};
            if (deviceType == 'Android') {
                // console.log("otherotiandroid");
                detailsdataAtEnd['notify_data']['sound'] = 'othersoundnoti.mp3';
            }else{
                // console.log("otherotiios");
               detailsdataAtEnd['notify_data']['sound'] = 'othersoundnoti.caf'; 
            }
            detailsdataAtEnd['notify_data']['content-available'] = 1;
            detailsdataAtEnd['notify_data']['randomNo'] = GenerateRandomNo.randomString(8, '0123456789abcdef');
            CallsService.setVideoCallDetails( detailsdataAtEnd ).success( function( data ) {
            
            } ).error( function ( data ) {
                successfullyDisconnect();
                if( navigator.connection.type == Connection.NONE ) {
                    if (connectionflag == 0) {
                        checkConnection();
                        connectionflag++;
                    }
                }
            } );
        }
        function sendTipVideo(){
            var sendTipData = {};
            sendTipData['responsetype'] = 'json';
            if (flag == 1) {
                sendTipData['userid'] = $scope.IdReceiver;
                sendTipData['proid'] = userid;
            }else{
                sendTipData['userid'] = userid;
                sendTipData['proid'] = $scope.IdReceiver;
            }
            sendTipData['tip_amount'] = $scope.tipAmt;
            sendTipData['in_video_call'] = 'true';
            console.log(sendTipData);
            PaymentService.sendTip( sendTipData, 'POST' ).success( function( data ) {
                console.log("tipdata>>" +JSON.stringify(data));
                var bal_remaining = data.data.account_balance;
                var type_of_response = 'tip';
                var status_type = data.status;
                if (data.status == 'error') {
                    $scope.tipAmt = data.message;
                }
                VideoPlugin.receivedResponseFromAPI(type_of_response,status_type,$scope.tipAmt);

            } ).error( function ( data ) {
                console.log("tiperr>>" +JSON.stringify(data));
                sendTipVideo();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } ); 
        }
        function BalTimeoutFunction(){
            if(!$scope.timeoutFlag){
                console.log('call is ended>>>');
                $interval.cancel($rootScope.check_bal_timeout);
            }
            var sec_in_string;
            var checkbalAmount = {};
            checkbalAmount['responsetype'] = 'json';
            checkbalAmount['receiver_id'] = $scope.IdReceiver;
            checkbalAmount['originator_id'] = userid;
            if(flag == 1){
            checkbalAmount['call_id'] = $scope.ReceiveCallId2;
            }else{
            checkbalAmount['call_id'] = $scope.ReceiveCallId1;
            }
            checkbalAmount['event']= 'in_progress';
            checkbalAmount['cur_time'] = $scope.newCurrDate;
            // console.log(JSON.stringify(detailsdataAtStart));
            CallsService.setVideoCallDetails( checkbalAmount ).success( function( data ) {
                console.log(JSON.stringify(data));
                var user_bal_on_call = data.data.account_balance;
                VideoPlugin.getUserBalance(user_bal_on_call, onSuccess, onFail);
                var remaining_seconds = data.data.video_call_sec;
                if (remaining_seconds <= 30 && remaining_seconds > 10 ) {
                    sec_in_string = (remaining_seconds.toString())+'s';
                    // console.log(sec_in_string);
                    VideoPlugin.showLowBalanceWarning(sec_in_string);
                }else if (remaining_seconds < 10) {
                    console.log("EndCallDueToLowBal");
                    VideoPlugin.endCalling("EndCallDueToLowBal");
                }
            } ).error( function ( data ) {
                if( navigator.connection.type == Connection.NONE ) {
                    $interval.cancel($rootScope.check_bal_timeout);
                    if (connectionflag == 0) {
                        checkConnection();
                        connectionflag++;
                    }
                    
                }
            } );
        }
        function alertDismissedForCredits(){
            console.log("alertdismissed");
        }
        if( successmsg.data == 'ReceiverInitializationCompleted' ) {
            console.log($scope.can_continue_call);
            if ($scope.can_continue_call == true) {
                console.log("receivetime>>" +$scope.receverTimeout);
                $scope.time_out = setTimeout( TimeoutFunction, 40000);
                $scope.receverTimeout = true;
            }
        }
        if( successmsg.data == 'Initialization completed !!' ) {
            if ($scope.can_continue_call == 'true') {
                $scope.time_out = setTimeout( TimeoutFunction, 35000);
            }
            else{
                $scope.time_out = setTimeout( TimeoutFunction, 60000);
            }
            initFunc();
        }
        if( successmsg.data == 'connectionCreated' ) {
            // callerRingtoneLoop = false;
            caller_tone = true;
            clearTimeout($scope.time_out);
            snd.release();
            snd1.stop();
            snd1.release();
        }
        if(successmsg.data == 'CallStarted'){
            snd.release();
            snd1.stop();
            snd1.release();
            // callerRingtoneLoop = false;
            clearTimeout($scope.time_out);
            startFunc();
        }else if (successmsg.data == " ") {
            clearTimeout($scope.time_out);
            $interval.cancel($rootScope.check_bal_timeout);
        } else if (successmsg.data == 'receiverMissedCall') {
            clearTimeout($scope.time_out);
            window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/recentcalls"
            } );
        } else if (successmsg.data == 'missedCall') {
            caller_tone = true;
            // callerRingtoneLoop = false;
            snd.release();
            snd1.stop();
            snd1.release();
            clearTimeout($scope.time_out);
            successfullyDisconnect();
        } else if (successmsg.data == 'proRejectsCall') {
            // callerRingtoneLoop = false;
            caller_tone = true;
            snd.release();
            snd1.stop();
            snd1.release();
            clearTimeout($scope.time_out);
            window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : "#/home/videoProReject/"+$scope.showNamedata+'/'+$scope.pageState+'/'+$scope.IdReceiver
            } );

        }else if (successmsg.data == 'callEndedByUser') {
            caller_tone = true;
            $scope.timeoutFlag = false;
            $interval.cancel($rootScope.check_bal_timeout);
            // callerRingtoneLoop = false;
            snd.release();
            snd1.stop();
            snd1.release();
            clearTimeout($scope.time_out);
            successfullyDisconnect();
            window.plugins.nativepagetransitions.slide( {
                "direction": 'left',
                "href" : "#/home/recentcalls"
            } );

        }else if(successmsg.data == 'Successfully disconnected !!' ){
            clearTimeout($scope.time_out);
            $interval.cancel($rootScope.check_bal_timeout);
            VideoPlugin.endCalling(" ");
            window.plugins.nativepagetransitions.slide( {
                    "direction": 'left',
                    "href" : "#/home/recentcalls"
            } );
        } else if(successmsg.data == 'CallEnded') {
            caller_tone = true;
            clearTimeout($scope.time_out);
            $interval.cancel($rootScope.check_bal_timeout);
            $('.ui-loader').show();
            $timeout(function(){
              $('.ui-loader').hide();
            }, 2500);
            // if (is_pro == 'user') {
                Endfunc();
            // }
        }else if (successmsg.data == 'EndCallDueToLowBal') {
            console.log('enter EndCallDueToLowBal');
            clearTimeout($scope.time_out);
            $interval.cancel($rootScope.check_bal_timeout);
            // if (is_pro == 'user') {
                Endfunc();
            // }
        } else if (successmsg.data.type == 'credit') {
            console.log("credit");
            clearTimeout($scope.time_out);
            $scope.creditAmtAdded = successmsg.data.amount;
            if ($scope.payment_method == 'in_app') {
                $scope.credit_id1 = 'com.ringhop.ringhop.'+$scope.creditAmtAdded;
                inAppPurchase
                .buy($scope.credit_id1)
                .then(function (data) {
                    var InAppcreditData = {};
                    InAppcreditData['responsetype'] = 'json';
                    InAppcreditData['credits'] =$scope.creditAmtAdded;
                    InAppcreditData['amount'] = $scope.creditAmtAdded;
                    InAppcreditData['transactionId'] = data.signature;
                    InAppcreditData['receipt'] = data.receipt;
                    InAppcreditData['status'] = 'success';
                    InAppcreditData['message'] = 'transaction successful';
                    if (flag == 1) {
                        InAppcreditData['userid'] = $scope.IdReceiver;
                        InAppcreditData['receiver_user'] = userid;
                    }else{
                        InAppcreditData['userid'] = userid;
                        InAppcreditData['receiver_user'] = $scope.IdReceiver;
                    }
                    InAppcreditData['in_video_call'] = 'true';
                    InAppcreditData['payment_via'] = deviceOS;
                    PaymentService.paymentViaInApp( InAppcreditData ).success( function( data ) {
                        var type = 'credit';
                        var res_status = data.status;
                        if (data.status == "success") {
                            var updatedUserBal = data.account_bal;
                        }else{
                            var updatedUserBal = data.message;
                        }
                        $scope.can_continue_call = true;
                        VideoPlugin.receivedResponseFromAPI(type,res_status,updatedUserBal);
                    } ).error( function ( data ) {
                        // alert('api error');
                        if( navigator.connection.type == Connection.NONE ) {
                            checkConnection();
                        }
                    } );
                    return inAppPurchase.consume(data.type, data.receipt, data.signature);
                })
                .then(function () {
                })
                .catch(function (err) {
                    // alert('err');
                    navigator.notification.alert(
                        'Issue communicating with app store for purchase. Please close app, relaunch and try again.',  // message
                        alertDismissedForCredits, // callback
                        'Ringhop',             // title
                        'Try Again'                   // buttonName
                    );
                    var InAppcreditData = {};
                    InAppcreditData['responsetype'] = 'json';
                    InAppcreditData['credits'] =$scope.creditAmtAdded;
                    InAppcreditData['amount'] = $scope.creditAmtAdded;
                    InAppcreditData['transactionId'] = '';
                    InAppcreditData['receipt'] = '';
                    InAppcreditData['status'] = 'failure';
                    InAppcreditData['message'] = err.text;
                    if (flag == 1) {
                        InAppcreditData['userid'] = $scope.IdReceiver;
                        InAppcreditData['receiver_user'] = userid;
                    }else{
                        InAppcreditData['userid'] = userid;
                        InAppcreditData['receiver_user'] = $scope.IdReceiver;
                    }
                    InAppcreditData['in_video_call'] = 'true';
                    InAppcreditData['payment_via'] = deviceOS;
                    console.log(JSON.stringify(InAppcreditData));
                    PaymentService.paymentViaInApp( InAppcreditData ).success( function( data ) {
                        console.log(JSON.stringify(data));
                    } ).error( function ( data ) {
                        if( navigator.connection.type == Connection.NONE ) {
                            checkConnection();
                        }
                    } );
                    
                });
            } else {
                var videoCallCreditData = {};
                videoCallCreditData['responsetype'] = 'json';
                videoCallCreditData['credits'] = $scope.creditAmtAdded;
                if (flag == 1) {
                    videoCallCreditData['userid'] = $scope.IdReceiver;
                    videoCallCreditData['receiver_user'] = userid;
                }else{
                    videoCallCreditData['userid'] = userid;
                    videoCallCreditData['receiver_user'] = $scope.IdReceiver;
                }
                videoCallCreditData['phoneip'] = deviceIP;
                videoCallCreditData['in_video_call'] = 'true';
                console.log(videoCallCreditData);
                PaymentService.addCredits( videoCallCreditData, 'POST' ).success( function( data ) {
                    console.log("successCredit");
                    var type = 'credit';
                    var res_status = data.status;
                    if (data.status == "success") {
                        var updatedUserBal = data.account_balance;
                    }else{
                        var updatedUserBal = data.message;
                    }
                    $scope.can_continue_call = true;
                    VideoPlugin.receivedResponseFromAPI(type,res_status,updatedUserBal);
                } ).error( function ( data ) {
                    console.log("errorCredit");
                    if( navigator.connection.type == Connection.NONE ) {
                        checkConnection();
                    }
                } );
            }
        } else if (successmsg.data.type == 'tip') {
            // console.log(successmsg.data.amount);
            console.log("tip");
            clearTimeout($scope.time_out);
            $scope.tipAmt = successmsg.data.amount;
            sendTipVideo();
        }
    }

    function onFail(errormsg) {
        console.log(JSON.stringify(errormsg));
        console.log(errormsg.error);
        clearTimeout($scope.time_out);
        $interval.cancel($rootScope.check_bal_timeout);
        $scope.errMsg = errormsg.error;
        $scope.ntwrkType = errormsg.networkType;
        if ($scope.errMsg == 'Token expired. Token') {
                navigator.notification.alert(
                'There is some issue connecting the call right now.Please try after some time.',  // message
                alertDismissedForCall, // callback
                'Ringhop',             // title
                'OK'                   // buttonName
            );
        }
        var errorCallLog = {};
        errorCallLog['responsetype'] = 'json';
        errorCallLog['networkType'] = $scope.ntwrkType;
        errorCallLog['errorMessage'] = $scope.errMsg ;
        errorCallLog['userid'] = userid ;
        if(flag == 1){
            errorCallLog['initiator'] = 'receiver';
        }else{
            errorCallLog['initiator'] = 'sender';
        }
        CallsService.sendVideoErrors( errorCallLog ).success( function( data ) {
        } ).error( function ( data, status, headers, config ) {
            if( navigator.connection.type == Connection.NONE ) {
                clearInterval($rootScope.check_bal_timeout);
                if (connectionflag == 0) {
                    checkConnection();
                    connectionflag++;
                }
            }
        } );
        if (errormsg.data == 'Please Send Valid JSON!!!') {
            //console.log(jsonObj);
        }
    }
    function alertDismissedForCall(){
        // console.log("alert dismissed for call");
        // window.plugins.nativepagetransitions.slide({
        //     "href" : "#/home/affter_login"
        // });
    }
    function TimeoutFunction() {
        console.log($scope.receverTimeout);
        if ($scope.receverTimeout == true) {
            // console.log("true/false");
            VideoPlugin.endCalling("receiverMissedCall");
        }else{
            VideoPlugin.endCalling("missedCall");
        }
        
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : "#/home/videoProReject/"+$scope.showNamedata+'/'+$scope.pageState+'/'+$scope.IdReceiver
        } );
    }
    // $scope.goBackToDashboard = function(){
    //     window.plugins.nativepagetransitions.slide( {
    //         "direction": 'left',
    //         "href" : "#/home/affter_login"
    //     } );
    // };

});