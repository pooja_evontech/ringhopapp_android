//+++++++++++++++++++++++++++notification page controller+++++++++++++++++++++

userApp.controller( "NotificationController", function( $scope, UserService, $state, $rootScope, constantData ) {
    $scope.dailyalert = 'yes';
    $scope.notifyHrList = [{ id:'', value:'(Select frequency)'}];
    angular.forEach(constantData.getNotificationData(), function(value, key) {
        var item = { id:key, value:value };
        $scope.notifyHrList.push(item);
    });
    console.log("dailyalert>>" +$scope.dailyalert);
    if ($scope.dailyalert !== '(Select daily alert)') {
        console.log("correct");
        $('#dailyalert').css({"padding-left":"120px !important"});
    }
    var notifyData = {};
    notifyData['responsetype'] = 'json';
    $scope.notifyCheck = '';
    UserService.notificationSettings( notifyData ).success( function( data ) {
        if( data.message == 'user not logged in' ){
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/affter_login"
            });
        }
        $scope.notifyServerData = data.data.notification;
        if( $scope.notifyServerData.sms == 0 ) {
            $scope.isSMS = false;
        } else {
            $scope.isSMS = true;
        }

        if( $scope.notifyServerData.inapp == 0 ) {
            $scope.isInAPP = false;
        } else {
            $scope.isInAPP = true;
        }
        // angular.forEach($scope.notifyHrList, function(notifydata){
        //     console.log(notifydata);
        //     if(notifydata.id == $scope.notifyServerData.frequency){
        //         console.log("noti1>>" +notifydata.id);
        //         console.log("noti2>>" +$scope.notifyServerData.frequency);
        //         $scope.frequency = {'id':notifydata.id,'value':notifydata.value}
        //     }
        // })

        $scope.dailyalert = data.data.dailyalert;
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.updateStatus = function () {
        console.log($scope.frequency.id);
        if($scope.dailyalert == '(Select daily alert)') {
            $scope.alertFrequency = '';
        }else{
            $scope.alertFrequency = $scope.dailyalert;
        }
        $('#notifyCheck').addClass('errorStatus').removeClass('succesStatus');
        if( $scope.isSMS == true ) {
            $('#sms').val(1);
        } else {
            $('#sms').val(0);
        }
        if( $scope.isInAPP == true ) {
            $('#inapp').val(1);
        } else {
            $('#inapp').val(0);
        }
        var notifyData = {};
        notifyData['responsetype'] = 'json';
        notifyData['sms'] = $('#sms').val();
        notifyData['inapp'] = $('#inapp').val();
        notifyData['frequency'] = $scope.frequency.id;
        notifyData['dailyalert'] = $scope.alertFrequency;
        $('.ui-loader').show();
        UserService.updateNotiSetting( notifyData ).success( function( data ) {
            console.log(JSON.stringify(data));
            $('.ui-loader').hide();
            $scope.notifyServerData1 = data.data.notification;
            $scope.notifyCheck = data.message;
            $scope.dailyalert = data.data.dailyalert;
            if( data.status == 'success' ){
                $('#notifyCheck').removeClass('errorStatus').addClass('succesStatus');
            } else {
                $('#notifyCheck').addClass('errorStatus').removeClass('succesStatus');
            }
        } ).error( function ( data ) {
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.changealert = function(alertvalue){
        console.log(alertvalue);
        if (alertvalue !== '(Select daily alert)') {
            console.log("correct");
            $('#dailyalert').css({"padding-left":"120px !important"});
        }
    };
} )
