//+++++++++++++++++++++++++++deactivation controller+++++++++++++++++++++

userApp.controller( "DeactivationController", function( $scope, UserService, $timeout, $state, $rootScope ) {
    var flagMenu = 0;
    $scope.navAction = function() {
        if( flagMenu == 0 ) {
            $( ".wrapper" ).animate( {
               "left":"-280px"
            } );
            flagMenu = 1;
        } else {
            $( ".wrapper" ).animate( {
               "left":"0px"
            } );
            flagMenu = 0;
        }
    };
    var height = $( window ).height();
    $('.wrapper, #sidebar-wrapper').css( 'min-height', height );
    $scope.deactivateConfirm = function( $event ){
        var user_detail = localStorage.getItem("userDetail");
        var userData = JSON.parse(user_detail).data;
        var getdeactivationData = {};
        getdeactivationData[ 'user_confirmed' ] = userData.id;
        getdeactivationData[ 'responsetype' ] = "json";
        $( '.phpdebugbar-openhandler-overlay' ).show();
        $( '.ui-loader' ).show();
        UserService.deactivate( getdeactivationData ).success( function( data ) {
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $( '.ui-loader' ).hide();
            if( data.status == 'success' ) {
                localStorage.removeItem("userDetail");
                localStorage.removeItem("rememberme_flag");
                localStorage.removeItem("remember_appLaunch");
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/login"
                } );
            }else{
                window.plugins.nativepagetransitions.slide( {
                    "href" : "#/home/login"
                } );
            }
        } ).error( function ( data ) {
            $( '.phpdebugbar-openhandler-overlay' ).hide();
            $( '.ui-loader' ).hide();
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );

    };

    $scope.deactivateCancel = function(){
      window.plugins.nativepagetransitions.slide({
            "href" : "#/home/settings"
        });  
    };
});