//+++++++++++++++++++++++++++Settings page controller+++++++++++++++++++++

userApp.controller( "SettingsController", function( $scope, AuthService, $timeout, $state, $rootScope, PaymentService, UserService ) {
    // var myScroll;
    // myScroll = new iScroll('wrapper');
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.user_id = userData.id;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    $scope.pro_info = JSON.parse( dashboard_data ).data.profile.pro;
    if( $scope.pro_info == 1 ) {
        $scope.showProApplicationBtn = true;
    }
    else {
        $scope.showProApplicationBtn = false;
    }
    $scope.viewProfile = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/profile"+$scope.user_id+'/'+userData.displayname
        });
    };
    $scope.settingsbackButton = function(){
        window.plugins.nativepagetransitions.slide({
            "direction": 'right',
            "href" : "#/home/affter_login"
        });
    }
    $scope.isPro = false;
    if( $scope.pro_info != 2 ) {
        $scope.userStats = function () {
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/userstats"
            });
        };
        $scope.isPro = false;
    } else {

        $scope.isPro = true;
    }
    $scope.voiceGreet = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/prosettings"
        });
    };
    $scope.proset = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/prosettings"
        });
    };
    $scope.homeLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/affter_login"
        });
    };
    $scope.editProfileLink = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/editProfile"
        });
    };
    $scope.blockingPage = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/blockedusers"
        });
    };
    $scope.viewCredits = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/viewcredit"
        });
    };
    $scope.buyCredits = function () {
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        var user_detail = localStorage.getItem( "userDetail" );
        var userid = JSON.parse( user_detail ).data.id;
        var creditData = {};
        var page_name = 'settings';
        creditData['responsetype'] = 'json';
        creditData['userid'] = userid;
        PaymentService.addCredits( creditData, 'GET' ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/addcredits/"+userid
            });
        }).error( function ( data ) {
            // console.log( JSON.stringify( data ) );
            if( navigator.connection.type == Connection.NONE ) {
                checkConnection();
            }
        } );

    };
    $scope.billingHistory = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/billinghistory"
        });
    };

    $scope.cancelApplication = function(){
        navigator.notification.confirm(
            'Are you sure you want to cancel your Pro Application?',  // message
            onConfirm,
            'Cancel Application'
        );
    };

    function onConfirm( button ) {
        if( button == 1 ) {
            var proApplicationDta = {};
            proApplicationDta['responsetype'] = 'json';
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            UserService.cancelProApplication( proApplicationDta ).success( function( data ) {
                console.log(data);
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.showProApplicationBtn = false;    

            } ).error( function ( data ) {
                console.log( JSON.stringify( data ) );
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if( navigator.connection.type == Connection.NONE ) {
                    checkConnection();
                }
            } );
        }
    }
    $scope.deactivateAcc = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/deactivation"
        });
    };

    $scope.notification = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/notification"
        });
    };

    $scope.logout = function ( $event ) {
        var getLogoutData = {};
        getLogoutData['responsetype'] = "json";
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();

        AuthService.logout( getLogoutData ).success( function( data ) {
            //console.log(data);
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.status == 'success' ) {
                localStorage.removeItem("userDetail");
                if(localStorage.getItem("rememberme_flag") == 'false'){
                    localStorage.removeItem("rememberme_data");
                }
                localStorage.removeItem("remember_appLaunch");
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/login"
                });

            } else {
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/login"
                });
            }
        } ).error( function ( err ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };
    $scope.statusAndAvail = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/phonesettings"
        });
    };
    $scope.resetPassword = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/resetpassword"
        });
    };
    $scope.helpandsupport = function () {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/support"
        });
    };
    $scope.about = function()
    {
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/about"
        });
    };
} )