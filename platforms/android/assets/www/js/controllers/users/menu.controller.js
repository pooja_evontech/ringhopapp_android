//++++++++++++++++++++++++right menu button+++++++++++++++++++++++++++++++++++

userApp.controller( "menuController", function( $scope, $http, $timeout, $state, $rootScope, $window ) {
    $scope.leftMenu = function ( $event ) {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'up',
            "href" : "#/home/settings"
        } );
    };
    $scope.backButton = function() {
        if( $rootScope.currentState == 'home.viewprofile' ) {
            if( $rootScope.previousState == 'home.profileoptions' ) {
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'right',
                    "href" : "#/home/profileoptions"
                } );
            } else {
                window.plugins.nativepagetransitions.slide( {
                    "direction": 'right',
                    "href" : window.history.back()
                } );
            }

        } else if( $rootScope.currentState == 'home.settings' ) {
            window.plugins.nativepagetransitions.slide( {
                "direction": 'down',
                "href" : window.history.back()
            } );
        } else {
            window.plugins.nativepagetransitions.slide( {
                "direction": 'right',
                "href" : window.history.back()
            } );
        }
    };
} )
