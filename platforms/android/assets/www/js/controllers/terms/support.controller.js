//+++++++++++++++++++++++++++support page controller+++++++++++++++++++++

termApp.controller( "SupportController", function( $scope, $http, $state, $rootScope ) {
    $scope.privacyPolicy = function () {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : '#/home/privacypolicy'
        } );
    };
    $scope.termsOfUse = function () {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : '#/home/terms'
        } );
    };
    $scope.contactSupport = function () {
        window.plugins.nativepagetransitions.slide( {
            "direction": 'left',
            "href" : '#/home/contactus'
        } );
    };
} );
