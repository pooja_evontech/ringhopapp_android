//+++++++++++++++++++++++++++prosettings page controller+++++++++++++++++++++

proApp.controller( "ProSettingsController", function( $scope, ProService, $timeout, $state, $rootScope ) {
    var user_detail = localStorage.getItem("userDetail");
    var userData = JSON.parse(user_detail).data;
    $scope.greetingshow = false;
    $scope.loginid = userData.id;
    $scope.showProNoli = false;
    $scope.showProNoTag = true;
    $scope.trialSetting = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/trialsettings"
        } );
    };
    $scope.viewProfile = function () {
        window.plugins.nativepagetransitions.slide( {
            "href" : "#/home/profile"+$scope.proSetData.id+'/'+$scope.proSetData.displayname
        } );
    };
    $scope.setCall = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/setrates"
        });
    };
    $scope.viewEarning = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/stats"
        });
    };
    $scope.forwardSetting = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/forwardingsettings"
        });
    };
    $scope.voicegreeting = function(){
        $state.transitionTo( 'home.mygreeting',{ ringhop_number:$scope.proSetData.ringhop_number } );
    };
    $scope.gotoGetProNo = function(){
        window.plugins.nativepagetransitions.slide({
            "href" : "#/home/getProNo"
        });
    };
    $('.ui-loader').show();
    $('#overlay').show();
    var proSettingData = {};
    proSettingData['responsetype'] = 'json';
    ProService.proSettings( proSettingData ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $('.ui-loader').hide();
        $('#overlay').hide();
        console.log(JSON.stringify(data));
        $scope.proSetData = data.data;
        $scope.linkToCopy = localStorage.getItem('User_ProfileLink');
        $scope.payment_link = data.poData.link;
        // console.log(data.poData.status);
            if (data.poData.status === null) {
            // console.log(data.poData.status);
                $scope.getColor = data.poData.color;
            }else if (data.poData.status == 'inreview') {
                $scope.getColor = data.poData.color;
            }else if (data.poData.status == 'approve') {
                $scope.getColor = '#fff';
            }else{
                $scope.getColor = data.poData.color;
            }
        if ($scope.proSetData.ringhop_number !== null) {
            $scope.showProNoli = true;
            $scope.showProNoTag = false;
            $scope.proSetData.ringhop_number = "+"+$scope.proSetData.ringhop_number.substr(0,1)+" ("+$scope.proSetData.ringhop_number.substr(1, 3)+") "+$scope.proSetData.ringhop_number.substr(4, 3)+"-"+$scope.proSetData.ringhop_number.substr(7, 4);
        }else{
            $scope.showProNoli = false;
            $scope.showProNoTag = true;
            $scope.proSetData.ringhop_number = "";
        }
        $scope.greeting = data.data.name_greeting;
        if($scope.greeting != null){
            $('.voicelist').css({"background":"#fff"});
            $scope.greetingshow = true;
        }else{
           $scope.greetingshow = false; 
        }
    } ).error( function ( data ) {
        $('.ui-loader').hide();
        $('#overlay').hide();
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );
    $scope.clickForCopy = function(){
        cordova.plugins.clipboard.copy($scope.linkToCopy);
    };
    $scope.proPaymentLink = function(){
        console.log($scope.payment_link);
        window.open($scope.payment_link,'_blank');
    };
} )