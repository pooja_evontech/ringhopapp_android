//+++++++++++++++++++++++++++proUpgrade page controller+++++++++++++++++++++

proApp.controller( "ProUpgradeController", function( $scope, ProService, $state, $rootScope, $location, constantData ) {
    $scope.countryList = [{ id:'', value:'(Select Country)'}];
    angular.forEach(constantData.getCountryData(), function(value, key){
        var item = { id:key, value:value };
        $scope.countryList.push(item);
    });
    $scope.creditsList = [{ id:'', value:'( Select credits )'}];
    angular.forEach(constantData.creditsList(), function(value, key){
        var item = { id:value, value:value+' Credits' };
        $scope.creditsList.push(item);
    });
    var flag = false;
    $scope.upgradeSubmit = function () {
        $('#token_upgrade').remove();
        var new_val = "";
        var proFormDiv = document.getElementById('proUpgrade_form');
        var input_id = '';
        var elementsLength = proFormDiv.getElementsByTagName('input').length;
        for (var i=0; i<parseInt(elementsLength); i++) {
            input_id = proFormDiv.getElementsByTagName('input')[i].id;
            new_val = proFormDiv.getElementsByTagName('input')[i].value;
            $( "label[for='"+input_id+"']" ).removeAttr('style');
            document.getElementsByTagName("span")[i].innerHTML = '';
            if( new_val == '' ) {
                var flag = false;
                document.getElementsByTagName("span")[i].innerHTML = 'This field is required.';
                $("label[for='"+input_id+"']").css( 'color', 'red' );
            }
        }
        if( $('#register_expiremonth').val() == '' ) {
            $('#expiremonth_error').html( 'This field is required.' );
            $('#expiremonth_label').css( 'color', 'red' );
            var flag = false;
        }
        if( $('#register_expireyear').val() == '' ) {
            $('#expireyear_error').html('This field is required.');
            $('#expireyear_label').css( 'color', 'red' );
            var flag = false;
        }
        if( $( '#upgrade_country' ).val() == '' ){
            $( '#upgrade_country_error' ).html( 'This field is required.' );
            $( '#upgrade_country_label' ).css( 'color', 'red' );
            var flag = false;
        }
        if( $( '#credits' ).val() == '' ) {
            $( '#credits_error' ).html( 'This field is required.' );
            $( '#credits_label' ).css( 'color', 'red' );
            var flag = false;
        }
        $( '#first_name_upgrade' ).keyup(function (e) {
            if( $( '#first_name_upgrade' ).val() == '' ) {
                $( '#firstname_error' ).html( 'This field is required.' );
                $( '#firstname_label' ).css( 'color', 'red' );
            } else {
                $( '#firstname_error' ).html( '' );
                $( '#firstname_label' ).removeAttr( 'style' );
            }
        } );

        $( '#last_name_upgrade' ).keyup(function (e) {
            if( $( '#last_name_upgrade' ).val() == '' ) {
                $( '#lastname_error' ).html( 'This field is required.' );
                $( '#lastname_label' ).css( 'color', 'red' );
            } else {
                $( '#lastname_error' ).html( '' );
                $( '#lastname_label' ).removeAttr( 'style' );
            }
        } );

        $( '#register_cardnumber' ).keyup( function (e) {
            if( $( '#register_cardnumber' ).val() == '' ) {
                $( '#cardnumber_error' ).html( 'This field is required.' );
                $( '#cardnumber_label' ).css( 'color', 'red' );
            } else if( $('#register_cardnumber').val().length < 25 ) {
                    $( '#cardnumber_error' ).html('');
                    $( '#cardnumber_label' ).removeAttr('style');
            } else {
                $( '#cardnumber_error' ).html( 'Please enter no more than 25 characters.' );
                $('#cardnumber_label').css( 'color', 'red' );
            }
        } );

        $( '#register_cvv2' ).keyup( function (e) {
            if( $( '#cvv2_error' ).html() == 'This field is required.' ) {
                $( '#cvv2_error' ).html('Please enter at least 3 characters.');
            }
            if( $('#register_cvv2').val().length >= 3 ) {
                    $( '#cvv2_error' ).html('');
                    $( '#cvv2_label' ).removeAttr('style');
            } else {
                if($('#register_cvv2').val().length == 0){
                   $( '#cvv2_error' ).html('This field is required.'); 
                }else{
                    $( '#cvv2_error' ).html('Please enter at least 3 characters.');
                    $( '#cvv2_label' ).css('color','red');
                }
                
            }
        } );

        $( '#upgrade_zipcode' ).keyup(function (e) {
            if( $( '#upgrade_zipcode_error' ).html() == 'This field is required.' ) {
                $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
            }
            if( $('#upgrade_zipcode').val().length >= 5 ) {
                    $( '#upgrade_zipcode_error' ).html('');
                    $( '#upgrade_zipcode_label' ).removeAttr('style');
            } else {
                if($('#upgrade_zipcode').val().length == 0){
                   $( '#upgrade_zipcode_error' ).html('This field is required.'); 
                }else{
                   $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
                   $( '#upgrade_zipcode_label' ).css('color','red'); 
                }
                
            }
        } );

        $( '#register_expiremonth' ).change(function() {
            if($( '#register_expiremonth' ).val() == ''){
                $( '#expiremonth_error' ).html('This field is required.');
                $('#expiremonth_label').css('color','red');
            } else {
                $( '#expiremonth_error' ).html('');
                $( '#expiremonth_label' ).removeAttr('style');
            }
        } );

        $( '#register_expireyear' ).change(function() {
            if($( '#register_expiremonth' ).val() == ''){
                $( '#expireyear_error' ).html('This field is required.');
                $('#expireyear_label').css('color','red');
            } else {
                $( '#expireyear_error' ).html('');
                $( '#expireyear_label' ).removeAttr('style');
            }
        } );

        $( '#upgrade_country' ).change(function() {
            if($( '#upgrade_country' ).val() == '') {
                $( '#upgrade_country_error' ).html('This field is required.');
                $('#upgrade_country_label').css('color','red');
            } else {
                $( '#upgrade_country_error' ).html('');
                $( '#upgrade_country_label' ).removeAttr('style');
            }
        } );

        $( '#credits' ).change( function() {
            if($( '#credits' ).val() == ''){
                $( '#credits_error' ).html( 'This field is required.' );
                $('#credits_label').css( 'color', 'red' );
            } else {
                $( '#credits_error' ).html( '' );
                $( '#credits_label' ).removeAttr( 'style' );
            }
        } );

        
        if( $('#register_cardnumber').val() != '' && $('#register_cardnumber').val().length > 25) {
            var flag = false;
            $( '#cardnumber_error' ).html('Please enter no more than 25 characters.');
            $( '#cardnumber_label' ).css('color','red');
        }

        if( $('#register_cvv2').val() != '' && $('#register_cvv2').val().length < 3) {
            var flag = false;
            $( '#cvv2_error' ).html('Please enter at least 3 characters.');
            $( '#cvv2_label' ).css('color','red');
        }

        if( $('#upgrade_zipcode').val() != '' && $('#upgrade_zipcode').val().length < 5) {
            var flag = false;
            $( '#upgrade_zipcode_error' ).html('Please enter at least 5 characters.');
            $( '#upgrade_zipcode_label' ).css('color','red');
        }
        if ( $( '#expiremonth_error' ).html() == '' && $( '#firstname_error' ).html() == '' && $( '#lastname_error' ).html() == '' && $( '#cvv2_error' ).html() == '' && $( '#expireyear_error' ).html() == '' && $( '#cardnumber_error' ).html() == '' && $( '#upgrade_zipcode_error' ).html() == '' && $( '#upgrade_country_error' ).html() == '' ) {
            flag = true;
        }
        if( flag == true ) {
           proUpgradeSubmit();
        }

    };
    function proUpgradeSubmit() {
        $( '#upgradeStatus' ).html('');
        var user_detail = localStorage.getItem("userDetail");
        var userData = JSON.parse(user_detail).data;
        var getUpgradeData = {};
        getUpgradeData['userid'] = userData.id;
        getUpgradeData['first_name'] = $scope.first_name;
        getUpgradeData['last_name'] = $scope.last_name;
        getUpgradeData['card_number'] = $scope.card_number;
        getUpgradeData['expire_month'] = $scope.expire_month;
        getUpgradeData['expire_year'] = $scope.expire_year;
        getUpgradeData['cvv_number'] = $scope.cvv_number;
        getUpgradeData['zipcode'] = $scope.zipcode;
        getUpgradeData['country'] = $scope.country;
        getUpgradeData['credits'] = $scope.credits;
        getUpgradeData['phoneip'] = deviceIP;
        if($rootScope.currentState == 'home.updatepayment' ) {
            getUpgradeData['s'] = "updatepay";
        }
        getUpgradeData['responsetype'] = 'json';
        $('.phpdebugbar-openhandler-overlay').show();
        $('.ui-loader').show();
        ProService.upgrade( getUpgradeData ).success( function( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if( data.status == 'success' ) {
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/creditscomplete/0"
                });
            } else {
                $( '#upgradeStatus' ).html( data.message );
            }
            $( '.submit-button' ).removeAttr( "disabled" );
        } ).error( function ( data ) {
            $('.phpdebugbar-openhandler-overlay').hide();
            $('.ui-loader').hide();
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
            $('.submit-button').removeAttr("disabled");
        } );
    }
} )