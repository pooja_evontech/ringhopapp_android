//+++++++++++++++++++++++++++statscalls page controller+++++++++++++++++++++

proApp.controller( "StatsCallsController", function( $scope, ProService, $timeout, $state, $filter, $stateParams, $rootScope ) {
    $scope.calltype = $stateParams.calltype;
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.limit = 4;
    var pre_count = 1;
    var next_count = 1;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.isPro = false;
    if( proData != 2 ) {
        $scope.isPro = false;
    } else {
        $scope.isPro = true;
    }
    if( $scope.isPro == true ) {
        var statscallsURL = BASE_URL+"pro/statscalls";
    } else {
        var statscallsURL = BASE_URL+"userstatscalls";
    }
    var statscalls = {};
    statscalls['responsetype'] = 'json';
    statscalls['calltype'] = $stateParams.calltype;
    ProService.stats( statscalls, statscallsURL ).success( function( data ) {
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.statsCallData = data.data;
        $scope.statscallhistory = data.data.calllogs;
        angular.forEach($scope.statscallhistory, function(history , filterKey) {
            history.call_amount = parseFloat( history.call_amount).toFixed(2);
            history.call_duration = parseFloat( history.call_duration ).toFixed(2);
        } );
        $scope.limit = data.data.per_page;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.prevBtn = function () {
        $('#arrow1').css ( {"opacity":"1" });
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
             $('#arrow').css ( {"opacity":"0.5" });
            $scope.prevDisabled = true;
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            statscalls['page'] = pre_count;
            ProService.stats( statscalls, statscallsURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.statscallhistory = data.data.calllogs;
                $scope.statsCallData = data.data;

                angular.forEach($scope.statscallhistory, function(history , filterKey) {
                    history.call_amount = parseFloat( history.call_amount).toFixed(2);
                    history.call_duration = parseFloat( history.call_duration ).toFixed(2);
                } );
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
         $('#arrow').css ( {"opacity":"1" });
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            statscalls['page'] = next_count;
            ProService.stats( statscalls, statscallsURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentChats = '';
                $scope.limit = data.data.per_page;
                $scope.statscallhistory = data.data.calllogs;
                $scope.statsCallData = data.data;

                angular.forEach($scope.statscallhistory, function(history , filterKey) {
                    history.call_amount = parseFloat( history.call_amount).toFixed(2);
                    history.call_duration = parseFloat( history.call_duration ).toFixed(2);
                } );
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $('#arrow1').css ( {"opacity":"0.5" });
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data, status, headers, config ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
            $('#arrow1').css ( {"opacity":"0.5" });
        }
    };
} )