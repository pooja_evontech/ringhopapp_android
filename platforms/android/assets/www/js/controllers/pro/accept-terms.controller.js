//+++++++++++++++++++++++++++acceptterms page controller+++++++++++++++++++++

proApp.controller( "AcceptTermsController", function( $scope, ProService, $timeout, $state, $rootScope ) {
    // var myScroll;
    // myScroll = new iScroll('wrapper');

    $scope.acceptTermsLink = function( $event ) {
        var accpetTermData = {};
        accpetTermData['accepted'] = 1;
        accpetTermData['responsetype'] = 'json';
        ProService.acceptTerms( accpetTermData ).success( function( data ) {
            if( data.message == 'user not logged in' ){
                window.plugins.nativepagetransitions.slide({
                    "href" : "#/home/affter_login"
                });
            }
            window.plugins.nativepagetransitions.slide({
                "href" : "#/home/accepttermscomplete"
            });
        } ).error( function ( data ) {
            if(navigator.connection.type == Connection.NONE) {
                checkConnection();
            }
        } );
    };

} )