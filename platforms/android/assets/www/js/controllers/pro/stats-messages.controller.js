//+++++++++++++++++++++++++++statsmessages page controller+++++++++++++++++++++

proApp.controller( "StatsMessagesController", function( $scope, ProService, $timeout, $state, $filter, $rootScope ) {
    $scope.paginateSection = false;
    $scope.prevDisabled = true;
    $scope.nextDisabled = true;
    $scope.noMessageDiv = false;
    $scope.limit = 4;
    var pre_count = 1;
    var next_count = 1;
    var dashboard_data = localStorage.getItem( "dashboard_data" );
    var proData = JSON.parse( dashboard_data ).data.profile.pro;
    $scope.loggedUserName  = JSON.parse( dashboard_data ).data.profile.fullname;
    $scope.isPro = false;
    if( proData != 2 ) {
        $scope.isPro = false;
    } else {
        $scope.isPro = true;
    }
    if( $scope.isPro == true ) {
        var statsmsgURL = BASE_URL+"pro/statsmessages";
    } else {
        var statsmsgURL = BASE_URL+"userstatsmessages";
    }
    var statsmessages = {};
    statsmessages['responsetype'] = 'json';
    ProService.stats( statsmessages, statsmsgURL ).success( function( data ) {
        console.log(JSON.stringify(data));
        if( data.message == 'user not logged in' ) {
            window.plugins.nativepagetransitions.slide( {
                "href" : "#/home/affter_login"
            } );
        }
        $scope.statsmsghistory = data.data.msglogs;
        $scope.limit = data.data.per_page;
        $scope.totalMessage = data.data.total_count;
        $scope.currentPage = data.data.current_page;
        $scope.currentCount = $scope.currentPage * $scope.limit;//total messages till current page
        if ( $scope.totalMessage <= $scope.currentCount ) {
            $scope.paginateSection = false;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = true;
            $scope.prevDisabled = true;
            if( $scope.totalMessage == 0 ) {
                $scope.noMessageDiv = true;
            } else {
                $scope.noMessageDiv = false;
            }

        }  else {
            $scope.paginateSection = true;
            $("#prevBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
            $('#arrow').css ( {"opacity":"0.5" });
            $scope.nextDisabled = false;
            $scope.prevDisabled = true;
        }
    } ).error( function ( data ) {
        if(navigator.connection.type == Connection.NONE) {
            checkConnection();
        }
    } );

    $scope.prevBtn = function () {
        next_count = next_count - 1;
        pre_count = next_count;
        if( $scope.currentPage == 1 ) {
            $( '#prevBtn' ).css( { "background":"#dedede", "border-color":"#dedede", "color":"#acacac" } );
            $scope.prevDisabled = true;
            $('#arrow').css ( {"opacity":"0.5" });
        } else {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            statsmessages['page'] = pre_count;
            ProService.stats( statsmessages, statsmsgURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.limit = data.data.per_page;
                $scope.statsmsghistory = data.data.msglogs;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if( $scope.currentPage == 1 ) {
                    if( $scope.totalMessage > $scope.currentCount ) {
                        $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                        $scope.nextDisabled = false;
                    }
                    $('#prevBtn').css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $scope.prevDisabled = true;
                    $('#arrow').css ( {"opacity":"0.5" });
                } else {
                    $("#nextBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $('#prevBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.prevDisabled = false;
                    $scope.nextDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        }

    };
    $scope.nextBtn = function () {
        $('#arrow').css ( {"opacity":"1" });
        next_count = next_count + 1;
        if ( $scope.totalMessage >= $scope.limit && $scope.currentCount < $scope.totalMessage ) {
            $('.phpdebugbar-openhandler-overlay').show();
            $('.ui-loader').show();
            statsmessages['page'] = next_count;
            ProService.stats( statsmessages, statsmsgURL ).success( function( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                $scope.recentChats = '';
                $scope.limit = data.data.per_page;
                $scope.statsmsghistory = data.data.msglogs;
                $scope.totalMessage = data.data.total_count;
                $scope.currentPage = data.data.current_page;
                $scope.currentCount = $scope.currentPage * $scope.limit;
                if ( $scope.totalMessage > $scope.currentCount ) {
                    $('#nextBtn').css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = false;
                    $scope.prevDisabled = false;
                } else {
                    $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
                    $("#prevBtn").css({"background":"#fff","border-color":"#fff","color":"#000"});
                    $scope.nextDisabled = true;
                    $scope.prevDisabled = false;
                }

            } ).error( function ( data ) {
                $('.phpdebugbar-openhandler-overlay').hide();
                $('.ui-loader').hide();
                if(navigator.connection.type == Connection.NONE) {
                    checkConnection();
                }
            } );
        } else {
            $("#nextBtn").css({"background":"#dedede","border-color":"#dedede","color":"#acacac"});
            $scope.nextDisabled = true;
        }
    };
} )
