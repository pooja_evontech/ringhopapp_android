paymentApp.factory('PaymentService', function( $http, CONSTANTS ) {
  return {
    addCredits: function( creditData, sendMethod ) {
      return $http({ 
        method: sendMethod,
        url: CONSTANTS.BASE_URL+"addcredits",
        params: creditData
      });
    },
    paymentViaInApp: function( InAppcreditData ) {
      return $http.post( CONSTANTS.BASE_URL+"upgradeinapp",JSON.stringify(InAppcreditData) );
    },
    billingHistory: function( historyData ) {
      return $http.post( CONSTANTS.BASE_URL+"billinghistory",JSON.stringify( historyData ) );
    },
    photoPurchase: function( paidPhotoData ) {
      return $http.post( CONSTANTS.BASE_URL+"photopurchase",JSON.stringify( paidPhotoData ) );
    }, 
    processPurchase: function( purchasePhotoData ) {
      return $http.post( CONSTANTS.BASE_URL+"processpurchase",JSON.stringify(purchasePhotoData) );
    },
    sendTip: function( tipData, sendMethod ) {
      return $http({ 
        method: sendMethod,
        url: CONSTANTS.BASE_URL+"sendtip",
        params: tipData
      });
    },
    viewCredit: function( viewcreditData ) {
      return $http.post( CONSTANTS.BASE_URL+"viewcredits",JSON.stringify( viewcreditData ) );
    }
  };
})