messageApp.factory('MessagesService', function( $http, CONSTANTS ) {
  return {
    messages: function( getMessageData ) {
      return $http.post( CONSTANTS.BASE_URL+"messages",JSON.stringify( getMessageData ) );
    },
    reply: function( getReplyData ) {
      return $http.post( CONSTANTS.BASE_URL+"reply", JSON.stringify( getReplyData ) );
    },
    getMoreHistory: function( getChatHistoryData ) {
      return $http.post( CONSTANTS.BASE_URL+"getmorehistory",JSON.stringify( getChatHistoryData ) );
    },
    sendMessage: function( getChatData ) {
      return $http.post( CONSTANTS.BASE_URL+"sendchatmessage",JSON.stringify( getChatData ) );
    },
    clearHistory: function( clearChatHistory ) {
      return $http.post( CONSTANTS.BASE_URL+"clearhistory",JSON.stringify( clearChatHistory ) );
    },
    isBlockAtPhotoSend: function( contact ) {
      return $http.post( CONSTANTS.BASE_URL+"isblocked",JSON.stringify( contact ) );
    },
    uploadChatMedia: function( chatImageData ) {
      return $http.post( CONSTANTS.BASE_URL+"uploadchat_media", JSON.stringify( chatImageData ) );
    },
    donotshowleads: function( getdonotshowleadsData ) {
      return $http.post( CONSTANTS.BASE_URL+"showmeagain", JSON.stringify( getdonotshowleadsData ) );
    }
  };
})