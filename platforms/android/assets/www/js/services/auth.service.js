anonApp.factory('AuthService', function( $http, CONSTANTS ) {
  return {
    login: function( credentials ) {
      return $http.post( CONSTANTS.BASE_URL+"signin",JSON.stringify(credentials));
    },
    logout: function( getLogoutData ) {
      return $http.post( CONSTANTS.BASE_URL+"logout",JSON.stringify(getLogoutData));
    },
    registerStep1: function( getRegData ) {
      return $http.post( CONSTANTS.BASE_URL+"register",JSON.stringify( getRegData ) );
    },
    pinMatch: function( getPinData ) {
      return $http.post( CONSTANTS.BASE_URL+"verifypin", JSON.stringify( getPinData ) );
    },
    registerStep2: function( getregStepTwoData ) {
      return $http.post( CONSTANTS.BASE_URL+"registernext",JSON.stringify( getregStepTwoData ) );
    },
    forgotPassword: function( getForgotData ) {
      return $http.post( CONSTANTS.BASE_URL+"forgotpassword",JSON.stringify( getForgotData ) );
    },
    resetPassword: function( getResetData ) {
      return $http.post( CONSTANTS.BASE_URL+"resetpassword",JSON.stringify( getResetData ) );
    },
    changePassword: function( getChangePWDData ) {
      return $http.post( CONSTANTS.BASE_URL+"changepassword",JSON.stringify( getChangePWDData ) );
    },
    getAppConstants: function(version) {
      // console.log(CONSTANTS.BASE_URL+"getappconstants?responsetype=json&app_version="+version)
      return $http.get( CONSTANTS.BASE_URL+"getappconstants?responsetype=json&android_app_version="+version );
    }
  };
})