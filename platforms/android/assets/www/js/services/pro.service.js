proApp.factory('ProService', function( $http, CONSTANTS ) {
  return {
    acceptTerms: function( accpetTermData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/acceptterms",JSON.stringify( accpetTermData ) );
    },
    myShouts: function( myshoutsData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/myshouts",JSON.stringify( myshoutsData ) );
    },
    deleteShout: function( deleteShoutData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/deleteshout", JSON.stringify( deleteShoutData ) );
    },
    forwardingSettings: function( statusData, sendMethod ) {
      return $http({ 
        method: sendMethod,
        url: CONSTANTS.BASE_URL+"pro/forwardingsettings",
        headers: { 'Cache-Control' : 'no-cache' },
        params: statusData
      });
    },
    myGreeting: function( getgreetingsData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/mygreeting",JSON.stringify( getgreetingsData ) );
    },
    refreshPin: function( greetingsData ) {
      return $http.post( CONSTANTS.BASE_URL+"updatepin",JSON.stringify( greetingsData ) );
    },
    deleteGreeting: function( delgreetingData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/deletegreeting",JSON.stringify( delgreetingData ) );
    },
    paymentOption: function( paymentOptData, sendMethod ) {
      return $http({ 
        method: sendMethod,
        url: CONSTANTS.BASE_URL+"pro/paymentoption",
        params: paymentOptData
      });
    },
    proDashboard: function( getProDashboardData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/prodashboard",JSON.stringify( getProDashboardData ) );
    },
    proSettings: function( proSettingData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/prosettings",JSON.stringify( proSettingData ) );
    },
    upgrade: function( getUpgradeData ) {
      return $http.post( CONSTANTS.BASE_URL+"upgrade",JSON.stringify( getUpgradeData ) );
    },
    uploadShoutMedia: function( getShoutMedia ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/uploadshout_media",JSON.stringify( getShoutMedia ) );
    },
    setRates: function( rateData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/setrates",JSON.stringify( rateData ) );
    },
    shoutHistory: function( historyData, APIURL ) {
      return $http.post( APIURL,JSON.stringify( historyData ) );
    },
    stats: function( statsData, APIURL ) {
      return $http.post( APIURL,JSON.stringify( statsData ) );
    },
    statsSummary: function( statsSummary ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/statssummary",JSON.stringify( statsSummary ) );
    },
    tipHistory: function( historyData, APIURL ) {
      return $http.post( APIURL,JSON.stringify( historyData ) );
    },
    trialSetting: function( acceptData, sendMethod ) {
      return $http({ 
        method: sendMethod,
        url: CONSTANTS.BASE_URL+"pro/trialsettings",
        headers: { 'Cache-Control' : 'no-cache' },
        params: acceptData
      });
    },
    userStats: function( statsData ) {
      return $http.post( CONSTANTS.BASE_URL+"userstats",JSON.stringify( statsData ) );
    },
    getShareableProNo: function( getProNoData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/getsharable",JSON.stringify( getProNoData ) );
    },
    verifyIdentity: function( updateVerifyData ) {
      return $http.post( CONSTANTS.BASE_URL+"pro/uploadphotoid",JSON.stringify( updateVerifyData ) );
    }
  };
} )