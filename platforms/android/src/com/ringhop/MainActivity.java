/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.ringhop;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import com.evontech.VideoPlugin.ActivityListener;

import org.apache.cordova.CordovaActivity;

public class MainActivity extends CordovaActivity
{
    private static final int REQUEST_CODE = 1004;
    private ActivityListener mActivityListener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        if(android.os.Build.VERSION.SDK_INT>=23)
            checkDrawOverlayPermission();
        loadUrl(launchUrl);
    }

    public void setActivityListener(ActivityListener listener) {
        mActivityListener = listener;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mActivityListener != null) {
            mActivityListener.onPauseActivity();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mActivityListener != null) {
            mActivityListener.onStoppedActivity();
        }
    }

    @Override
    public void onDestroy() {
        if (mActivityListener != null) {
            mActivityListener.onDestroyActivity();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mActivityListener != null) {
            mActivityListener.onResumeActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    for(int i = 0; i<grantResults.length;i++)
                    {
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            return;
                        }
                    }
                    mActivityListener.onRequestAccessed();

                } else {
                }
                return;
            }
        }

    }

    public void checkDrawOverlayPermission() {
        /** check if we already  have permission to draw over other apps */

        if (!Settings.canDrawOverlays(this)) {
            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            /** request permission via start activity for result */
            startActivityForResult(intent, REQUEST_CODE);
        }
    }
}
